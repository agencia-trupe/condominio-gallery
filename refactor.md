Extrair lógicas

Repositórios criados
  - Moradores
    - Auth
    - Avisos
    - Documentos
    - Preferencias

Serviços criados
  - Moradores
    - Logger (AtividadeRastreavel)
    - EventManager (outros eventos)


[ ] Controllers
  [ ] Moradores
    [ ] BaseMoradoresController
    [X] AlterarSenha
    [X] Dashboard
    [ ] Fale Com
    [X] Preferencias

[ ] Events

[ ] Listeners

---

Lógica relacionada à Entidades - Repository

Lógica relacionada à Infraestrutura - Service

Bind das camadas - ServiceProviders

Validações - FormRequests


Manter nos models:
Scopes
Mutators/Acessors
Relationships
Casts
Fillables
Dates
