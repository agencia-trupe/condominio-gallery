Moradores.Dashboard = {

	controlar_submenus : function() {
		$('.dashboard-mostra-areas .area').click( function(e){
			e.preventDefault();
			$(this).toggleClass('ativo');
			$('.dashboard-mostra-areas .area.ativo').not($(this)).removeClass('ativo');
			return false;
		});
	},

	fechar_submenus_click : function() {
		$(window).on('click', function(){
			$('.dashboard-mostra-areas .area.ativo').removeClass('ativo');
		});
	},

	avisos_marcar_como_lido : function() {
		$(".aviso-marcar-lido-inferior input[type=checkbox], .aviso-marcar-lido input[type=checkbox]").change( function(){

			var avisos_id = $(this).val();
			var resultado = $(this).parent().parent();
			var check_inferior = $('.aviso-marcar-lido-inferior');

			$(this).attr('disabled', true);

			$.post('moradores/avisos/lido', {
				avisos_id : avisos_id
			}, function(resposta){

				var resposta = "<span class='aviso-lido-data apagado'>Lido em: <br>"+resposta.lido_em+"</span>";

				resultado.find('label').fadeOut('normal', function(){

					resultado.append(resposta)
							 .find('.apagado')
							 .removeClass('apagado');

				});

				/**
				* Se existir um elemento com a classe -inferior
				* estamos na página de detalhes, além do elemento
				* .parent().parent() precisamos colocar o timestamp
				* no inferior também, além de remover o checkbox que
				* não foi utilizado para marcação de lido
				*/
				if(check_inferior.length && resultado.hasClass('aviso-marcar-lido-inferior')){

					$('#avisos-detalhes .aviso-marcar-lido').find('label').fadeOut('normal', function(){
						$('#avisos-detalhes .aviso-marcar-lido').append(resposta)
																.find('.apagado')
							 									.removeClass('apagado');
					});

				}else if(check_inferior.length){

					$('#avisos-detalhes .aviso-marcar-lido-inferior').find('label').fadeOut('normal', function(){
						$('#avisos-detalhes .aviso-marcar-lido-inferior').append(resposta)
																.find('.apagado')
							 											 .removeClass('apagado');
					});

				}
			});
		});
	},

	tooltip_notificacoes : function() {
		$.each($('.notificacao-tooltip'), function(){
			$(this).tooltipster({
				maxWidth: 240,
				theme: 'tema-tooltipster-custom',
				contentAsHTML: true
			});
		});
	},

	init : function() {

		this.controlar_submenus();
		this.fechar_submenus_click();

		if($('.aviso-marcar-lido').length)
			this.avisos_marcar_como_lido();

		this.tooltip_notificacoes();

	}
};
