Moradores.MoradoresDaUnidade = {

	form_datepicker : function(){
		var picker = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('moradores-da-unidade-form-data'),
			format: 'DD/MM/YYYY',
			yearRange: [1900, 2050],
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			onSelect : function(){
				$('#moradores-da-unidade-form-data').parent()
													.removeClass('input-erro')
													.removeClass('obrigatorio');
			}
		});
	},

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#moradores-da-unidade-form-nome',
				'#moradores-da-unidade-form-visibilidade',
				'#moradores-da-unidade-form-data',
				'#moradores-da-unidade-form-parentesco',
				//'#moradores-da-unidade-form-foto'
			],
			form_com_imagem   : true,
			multiplas_imagens : false,
			diretorio_imagens : 'moradores-da-unidade'
		});

		this.form_datepicker();

	}
};
