Moradores.AnimaisDeEstimacao = {

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#animais-de-estimacao-form-nome',
				'#animais-de-estimacao-form-visibilidade',
				'#animais-de-estimacao-form-especie',
				'#animais-de-estimacao-form-sexo',
				'#animais-de-estimacao-form-foto'
			],
			form_com_imagem   : true,
			multiplas_imagens : false,
			diretorio_imagens : 'animais-de-estimacao'
		});

	}
};