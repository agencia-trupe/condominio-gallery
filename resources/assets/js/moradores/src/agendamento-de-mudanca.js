Moradores.AgendamentoDeMudanca = {

	form_datepicker : function(){
		if($('#agendamento-data').length){
			var picker = new Pikaday({
				theme: 'gallery-theme',
				field: document.getElementById('agendamento-data'),
				format: 'DD/MM/YYYY',
				i18n: {
				    previousMonth : 'Mês Anterior',
				    nextMonth     : 'Próximo Mês',
				    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
				    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
				    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
				},
				onSelect : function(){
					$('#agendamento-data').parent()
										  .removeClass('input-erro')
										  .removeClass('obrigatorio');
				}
			});
		}
	},

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#agendamento-data',
				'#agendamento-periodo'
			],
			form_com_imagem : false
		});

		this.form_datepicker();

	}
};