Moradores.ChamadosDeManutencao = {

	form_datepicker : function(){
		if($('#chamados-de-manutencao-form-data').length){
			var picker = new Pikaday({
				theme: 'gallery-theme',
				field: document.getElementById('chamados-de-manutencao-form-data'),
				format: 'DD/MM/YYYY',
				i18n: {
				    previousMonth : 'Mês Anterior',
				    nextMonth     : 'Próximo Mês',
				    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
				    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
				    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
				},
				onSelect : function(){
					$('#chamados-de-manutencao-form-data').parent()
														.removeClass('input-erro')
														.removeClass('obrigatorio');
				}
			});
		}
	},

	init : function(){

		this.form_datepicker()

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#chamados-de-manutencao-form-data',
				'#chamados-de-manutencao-form-horario',
				'#chamados-de-manutencao-form-localizacao',
				'#chamados-de-manutencao-form-descricao'
			],
			form_com_imagem   : true,
			multiplas_imagens : true,
			identificador_sessao : 'chamados_de_manutencao',
			diretorio_imagens : 'chamados-de-manutencao'
		});

	}
};