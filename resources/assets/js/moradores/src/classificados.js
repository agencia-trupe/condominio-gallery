Moradores.Classificados = {

  init: function()
  {

    Moradores.Formularios.init({
			campos_obrigatorios : [
				'#classificados-form-categoria',
				'#classificados-form-titulo'
			],
			form_com_imagem   : true,
			multiplas_imagens : true,
			identificador_sessao : 'anuncio',
			diretorio_imagens : 'classificados'
		});

  }
};
