Moradores.FaleCom = {

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#fale-com-assunto',
				'#fale-com-mensagem'
			],
			form_com_imagem : false
		});

	}
};