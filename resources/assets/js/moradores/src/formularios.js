Moradores.Formularios = {

	campos_obrigatorios : [],

	toggle_on_click : function () {
		$('#form-toggle').click( function(e){
			e.preventDefault();
			var form = $(this).attr('data-toggle-target');
			$(this).toggleClass('aberto');
			$('#'+form).toggleClass('aberto');
		});
	},

	toggle_on_load : function() {

		var pagina_tem_erro     = $('.input-erro').length > 0 ? true : false;
		var pagina_de_edicao    = $('.form-unidade-edicao').length > 0 ? true : false;
		var pagina_sem_registro = $('#lista-registros li').length == 0 ? true : false;
		var iniciar_fechado     = $('#form-toggle').hasClass('iniciar-fechado') ? true : false;

		if( (pagina_tem_erro || pagina_de_edicao || pagina_sem_registro) && !iniciar_fechado )
			$('#form-toggle').click();
	},

	tooltips_on_load : function() {
		var tooltips = $('.addon-tooltip');
		$.each(tooltips, function(){

			var title_original = $(this).attr('title');
			var tooltip_title = title_original;
			var tooltip_text  = $(this).attr('data-tooltip-text');

			$(this).find('img').attr('title', '');

			$(this).tooltipster({
				content: $("<strong>"+tooltip_title+"</strong><p>"+tooltip_text+"</p>"),
				maxWidth: 240,
				theme: 'tema-tooltipster-custom'
        	});
		});
	},

	modal_visibilidade_on_click : function() {
		$('.addon-tooltip-visib').click( function(e){
			e.preventDefault();

			var titulo_shadow = $(this).attr('data-shadow-title');
			var input_armazenamento = $('#'+$(this).attr('data-shadow-storage'));

			Moradores.Formularios.carregar_modal_visibilidade(titulo_shadow, input_armazenamento);
		});
	},

	carregar_modal_visibilidade : function(titulo, input) {

		var template = "";
		template += "<div id='form-visibilidade-shadow'>";
			template += "<h2>"+titulo+"</h2>";
			template += "<p>SELECIONAR PARA QUEM ESSA INFORMAÇÃO SERÁ EXIBIDA DENTRO DO SISTEMA:</p>";
			template += "<ul>";
				template += "<li><label><input type='checkbox' value='1' class='check-marcar-todos'> Todos (moradores, corpo diretivo, zelador)</label></li>";
				template += "<li><label class='marcado'><input type='checkbox' value='2' checked id='check-marcar-admin'> Somente síndico e conselho</label></li>";
				// template += "<li><label><input type='checkbox' value='3'> Portaria</label></li>";
				template += "<li><label><input type='checkbox' value='4'> Zelador</label></li>";
				template += "<li><label><input type='checkbox' value='5' id='check-marcar-moradores'> Todos os moradores</label></li>";
				template += "<li><label><input type='checkbox' value='6' id='check-marcar-amigos'> Somente moradores selecionados como amigos</label></li>";
			template += "</ul>";
			template += "<button id='fechar-visibilidade-shadow'>CONFIRMAR</button>";
		template += "</div>";

		$.featherlight(template, {
			afterContent : function(e){

				var inputs = $(".featherlight-inner input[type='checkbox']");
				var valor_atual = input.val();

				if(valor_atual.indexOf(',') !== -1){

					var valores_atuais = valor_atual.split(',')

					$.each(inputs, function(){
						if($.inArray($(this).val(), valores_atuais) !== -1){
							$(this).attr('checked', 'checked');
							$(this).parent().addClass('marcado');
						}
					});

				}else if(valor_atual != ''){
					$.each(inputs, function(){
						if($(this).val() == valor_atual){
							$(this).attr('checked', 'checked');
							$(this).parent().addClass('marcado');
						}
					});
				}

				$("#form-visibilidade-shadow .check-marcar-todos").click( function(){
					// Ao clicar em 'marcar todos', marcar ou desmarcar todos os checkboxes
					// EXCETO o de só amigos e o da administração (esse sempre ativo)
					$("#form-visibilidade-shadow input[type=checkbox]").not('#check-marcar-amigos')
																	   .not('#check-marcar-admin')
																	   .prop('checked', this.checked);

					// Sempre DESmarcar o de 'só amigos'
					$('#check-marcar-amigos').prop('checked', false);
					$('#check-marcar-amigos').parent().removeClass('marcado');

					if(this.checked){
						$("#form-visibilidade-shadow label input").not('#check-marcar-amigos')
																  .not('#check-marcar-admin')
																  .parent()
																  .addClass('marcado');
					}else{
						$("#form-visibilidade-shadow label input").not('#check-marcar-amigos')
																  .not('#check-marcar-admin')
																  .parent()
																  .removeClass('marcado');
					}
				});

				$("#form-visibilidade-shadow input[type=checkbox]").not(".check-marcar-todos").click( function(e){

					if($(this).val() == '2'){ // Sempre visível pra administração
						e.preventDefault();
						return false;
					}

					if(!this.checked){
						$("#form-visibilidade-shadow .check-marcar-todos").prop('checked', false);
						$("#form-visibilidade-shadow .check-marcar-todos").parent().removeClass('marcado');
						$(this).parent().removeClass('marcado');
					}else{
						$(this).parent().addClass('marcado');

						if($(this).val() == '5'){
							// Se value = 5 (todos os moradores)
							// Desmarca o 6 (somente amigos)
							$('#check-marcar-amigos').prop('checked', false).parent().removeClass('marcado');
						}else if($(this).val() == '6'){
							// Se value = 6 (somente amigos)
							// Desmarca o 5 (todos os moradores)
							$('#check-marcar-moradores').prop('checked', false).parent().removeClass('marcado');
						}
					}
				});

				$('#fechar-visibilidade-shadow').click( function(){
					Moradores.Formularios.salvar_modal_visibilidade(input);
					$.featherlight.current().close();
				});
			}
		});
	},

	salvar_modal_visibilidade : function(input) {
		var marcados = $(".featherlight-inner input[type='checkbox']:checked");
		var icone = input.parent().find('.addon-tooltip-visib');
		var valores = Array();

		if(marcados.length > 0){
			$.each(marcados, function(){
				valores.push($(this).val());
			});
			valores = valores.join(',');
		}else{
			valores = '';
		}

		input.val(valores);

		input.parent().removeClass('input-erro')
					  .removeClass('obrigatorio');

		if(input.val() == '')
			icone.removeClass('on').addClass('off');
		else
			icone.removeClass('off').addClass('on');
	},

	marcar_icone_visibilidade_on_load : function() {
		var icones = $('.addon-tooltip-visib');
		$.each(icones, function(){
			var input = $(this).parent().find('input[type=hidden]');
			if(input.val() != ''){
				$(this).removeClass('off').addClass('on');
			}
		});
	},

	remover_erros : function() {
		$.each($('.input-erro input, .input-erro select, .input-erro textarea'), function(){

			var evento = $(this).prop('nodeName').toLowerCase() == 'select'  ? 'click' : 'keyup';

			$(this).on(evento, function(){
				$(this).parent().removeClass('input-erro')
								.removeClass('obrigatorio')
								.removeClass('data-invalida')
								.removeClass('data-passou')
								.removeClass('email-invalido')
								.removeClass('login-fail')
								.removeClass('confirmacao-senha-invalida')
								.removeClass('email-em-uso')
								.removeClass('minimo');
			});

		});
	},

	validacao_on_submit : function() {
		$('form').not('.form-ajax').submit( function(e) {

			for (var i = 0; i < Moradores.Formularios.campos_obrigatorios.length; i++) {

				if(Moradores.Formularios.campos_obrigatorios[i] == 'fornecedores_avaliacao_nota'){

					if( $("input[name='avaliacao[nota]']:checked").length == 0 ){

						var rating_input = $('#fornecedores-form-avaliacao .rating');

						rating_input.addClass('obrigatorio').on('click', function(){
							rating_input.removeClass('obrigatorio');
						});

						e.preventDefault();
						return false;
					}

				}else{

					var check = $(Moradores.Formularios.campos_obrigatorios[i]);

					if(!check.val()){

						check.parent()
								 .addClass('input-erro')
								 .addClass('obrigatorio');

						evento = check.prop('nodeName').toLowerCase() == 'select' ? 'click' : 'keyup';

						check.focus().on(evento, function(){
							check.parent()
									 .removeClass('input-erro')
									 .removeClass('obrigatorio');
						});

						e.preventDefault();
						return false;
					}

				}
			};

		});
	},

	foto_upload : function(diretorio_imagens) {
		$(".input-foto input[type=file]").fileupload({
			url: 'moradores/'+diretorio_imagens+'/upload-foto',
			type: 'post',
	        dataType: 'json',
	        send: function(){
	        	Moradores.Formularios.upload_imagem_esconder_erro();
	        	$('.input-foto label').addClass('enviando');
	        },
	        progress: function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('#upload-progresso .barra').css(
		            'width',
		            progress + '%'
		        );
		    },
	        done: function (e, data) {
	        	$('.input-foto label').removeClass('enviando');
	        	if(data.result.success == 1){
	        		var foto = new Image;
	        		var target = $('.input-foto label');
	        		$('.input-foto .form-foto-hidden').val(data.result.filename);
	        		foto.onload = function(){
	        			target.append(foto);
	        		};
	        		foto.src = data.result.thumb;
	        	}else{
	        		Moradores.Formularios.upload_imagem_mostrar_erro(data.result.msg);
	        	}
	        }
	    });
	},

	foto_multi_upload : function(diretorio_imagens, identificador_sessao) {
		$(".input-foto input[type=file]").fileupload({
			url: 'moradores/'+diretorio_imagens+'/upload-foto',
			type: 'post',
	        dataType: 'json',
	        send: function(){
	        	Moradores.Formularios.upload_imagem_esconder_erro();
	        	$('.input-foto label').addClass('enviando');
	        },
	        progress: function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('#upload-progresso .barra').css(
		            'width',
		            progress + '%'
		        );
		    },
	        done: function (e, data) {

	        	$('.input-foto label').removeClass('enviando');

	        	if(data.result.success == 1){

	        		var target_imgs   = $('.input-foto .lista-imagens');

	        		var foto 		  = new Image;
	        		var hidden_input  = $("<input type='hidden' name='"+identificador_sessao+"[fotos][]' class='form-foto-hidden' value='"+data.result.filename+"'>");
	        		var btn_remover   = $("<a href='#' title='Remover Imagem' class='btn btn-danger btn-mini btn-remover-imagem'>x</a>")
	        		var btn_shadow    = $("<a href='#' data-featherlight='"+data.result.thumb.replace('thumbs', 'redimensionadas')+"' title='Ampliar' class='btn-ampliar-imagem'></a>");
	        		var novo_li 	  = $('<li></li>');
	        		foto.onload = function(){

	        			btn_shadow.append(foto);

	        			novo_li.append(btn_shadow)
	        				   .append(btn_remover)
	        				   .append(hidden_input);

	        			target_imgs.append(novo_li);

	        			var nova_altura = $('.input-foto').css('height');
	        			$('form').not('.form-ajax').css('min-height', parseInt(nova_altura) + 60);

	        			Moradores.Formularios.botoes_remover_imagem();

	        			$('.btn-ampliar-imagem').off('click');
	        			$('.btn-ampliar-imagem').featherlightGallery();
	        		};
	        		foto.src = data.result.thumb;
	        	}else{
	        		Moradores.Formularios.upload_imagem_mostrar_erro(data.result.msg);
	        	}
	        }
	    });
	},

	botoes_remover_imagem : function(){
		$('.btn-remover-imagem').on('click', function(e){
			e.preventDefault();
			$(this).parent().fadeOut('normal', function(){
				$(this).remove();
				$('.btn-ampliar-imagem').off('click');
	        	$('.btn-ampliar-imagem').featherlightGallery();
			})
		});
	},

	upload_imagem_esconder_erro : function() {
		$('.input-foto label').removeClass('input-erro');
	    $('.input-foto label p').html($('.input-foto label p').attr('data-html-original'));
	},

	upload_imagem_mostrar_erro : function(msg) {
		$('.input-foto label').addClass('input-erro');
	    $('.input-foto label p').html(msg);

			if($('.input-foto .form-foto-hidden').length == 1)
	    	$('.input-foto .form-foto-hidden').val('');

	    $('.input-foto label img').fadeOut('normal', function(){
	    	$(this).remove();
	    });
	},

	carregar_foto_apos_upload : function(diretorio_imagens, multiplas_imagens) {

		var file = $('.input-foto .form-foto-hidden').val();

		if(multiplas_imagens == true){

		}else{
			if($('.input-erro').length && file != ''){
				var foto = new Image;
	    		var target = $('.input-foto label');
	    		foto.onload = function(){
	    			target.append(foto);
	    		};
	    		foto.src = 'assets/images/moradores/'+diretorio_imagens+'/thumbs/'+file;
			}
		}

	},

	adicionarCampoObrigatorio : function(campo) {
		Moradores.Formularios.campos_obrigatorios.push(campo);
	},

	removerCampoObrigatorio : function (campo) {
		var indice = Moradores.Formularios.campos_obrigatorios.indexOf(campo);
		if(indice > -1)
			Moradores.Formularios.campos_obrigatorios.splice(indice, 1);
	},

	init : function(options) {
		this.toggle_on_click();
		this.toggle_on_load();
		this.tooltips_on_load();
		this.modal_visibilidade_on_click();
		this.marcar_icone_visibilidade_on_load();
		this.remover_erros();
		this.validacao_on_submit();

		this.campos_obrigatorios = options.campos_obrigatorios;

		if(options.form_com_imagem == true){

			if(options.multiplas_imagens == true)
				this.foto_multi_upload(options.diretorio_imagens, options.identificador_sessao);
			else
				this.foto_upload(options.diretorio_imagens);

			this.botoes_remover_imagem();
			this.carregar_foto_apos_upload(options.diretorio_imagens, options.multiplas_imagens);
		}
	}
};
