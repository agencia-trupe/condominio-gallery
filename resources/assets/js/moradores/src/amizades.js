Moradores.Amizades = {

  timeout: '',
  resultados: '',

  registra_keyup: function()
  {
    $('#input-busca-amigos').on('keyup', function(e){

			var resumo = $(this).val().toLowerCase();

      clearTimeout(Moradores.Amizades.timeout);

      Moradores.Amizades.timeout = setTimeout( function(){
        Moradores.Amizades.buscarMorador(resumo);
      }, 200);

		});
  },

  buscarMorador: function(termo)
  {
    if(termo != ''){

      this.mostrarCarregamento();

      $.post('moradores/amizades/buscar', {
        termo: termo
      }, function(resposta){

        setTimeout( function(){
          Moradores.Amizades.resultados = resposta;
          Moradores.Amizades.mostrarResultados();
        }, 1000);

      });

    }else{

      $('#carregando-resultados').fadeOut('normal');
      $('#lista-amizades').fadeOut('normal', function(){
        $('#lista-amizades').html('');
      });

    }
  },

  mostrarCarregamento: function()
  {
    $('#lista-amizades').fadeOut('fast', function(){
      $('#carregando-resultados').fadeIn('normal');
    });
  },

  mostrarResultados: function()
  {
    $('#carregando-resultados').fadeOut('normal', function(){

      Moradores.Amizades.criarLista();

      $('#lista-amizades').fadeIn('normal');
    });
  },

  criarLista: function()
  {
    $('#lista-amizades').html(this.resultados);
  },

  marcarTodosAmigos: function()
  {
    $('#marcar-todos').change( function(){

      var marcar = $(this).is(':checked');
      var checkboxes = document.getElementsByName('amigo_id[]');

      for(var i=0, n=checkboxes.length; i<n; i++)
        checkboxes[i].checked = marcar;

      if(marcar)
        $('#marcar-todos-label').html('Desmarcar todos');
      else
        $('#marcar-todos-label').html('Marcar todos');

    });
  },

  verificarTodosMarcados: function()
  {
    var checkboxes = document.getElementsByName('amigo_id[]');
    var marcados = 0;
    var n = checkboxes.length;

    for(var i=0, n; i<n; i++)
      if(checkboxes[i].checked) marcados++;

    if(marcados == n){
      $('#marcar-todos').prop('checked', true);
      $('#marcar-todos-label').html('Desmarcar todos');
    }else{
      $('#marcar-todos').prop('checked', false);
      $('#marcar-todos-label').html('Marcar todos');
    }
  },

  desmarcarBotaoAoTirarTodos: function()
  {
    $("input[type='checkbox']").not('#marcar-todos').change( function(){
      Moradores.Amizades.verificarTodosMarcados();
    });
  },

  init: function()
  {
    this.registra_keyup();

    this.verificarTodosMarcados();
    this.marcarTodosAmigos();
    this.desmarcarBotaoAoTirarTodos();
  }

};