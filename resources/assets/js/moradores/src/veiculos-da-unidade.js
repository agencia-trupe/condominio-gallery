Moradores.VeiculosDaUnidade = {

	mercoSulMaskBehavior : function (val) {
        var myMask = 'AAA0A00';
        var mercosul = /([A-Za-z]{3}[0-9]{1}[A-Za-z]{1})/;
        var normal = /([A-Za-z]{3}[0-9]{2})/;
        var replaced = val.replace(/[^\w]/g, '');
        if (normal.exec(replaced)) {
            myMask = 'AAA-0000';
        } else if (mercosul.exec(replaced)) {
            myMask = 'AAA0A00';
        }
        return myMask;
	},
	
	mercoSulOptions : {
		placeholder : 'placa',
		clearIfNotMatch: true,
		onKeyPress: function(val, e, field, options) {
			field.val(val.toUpperCase())
			field.mask(Moradores.VeiculosDaUnidade.mercoSulMaskBehavior.apply({}, arguments), Moradores.VeiculosDaUnidade.mercoSulOptions);
		}
	},

	form_mascara_placa : function() {
		field = '#veiculos-da-unidade-form-placa'
		$(field).bind('paste', function(e) {
			$(this).unmask();
        });
        $(field).bind('input', function(e) {
			$(field).mask(Moradores.VeiculosDaUnidade.mercoSulMaskBehavior, Moradores.VeiculosDaUnidade.mercoSulOptions);
        });
	},

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#veiculos-da-unidade-form-veiculo',
				'#veiculos-da-unidade-form-visibilidade',
				'#veiculos-da-unidade-form-cor',
				'#veiculos-da-unidade-form-placa'
			],
			form_com_imagem : false
		});

		this.form_mascara_placa();

	}
};
