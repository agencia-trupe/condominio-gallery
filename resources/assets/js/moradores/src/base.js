var Moradores = {
	init : function(){

		var modulo = location.pathname.split('/')[2];

		if(modulo == 'auth')
			this.Auth.init();

		if(modulo != 'auth')
			this.Dashboard.init();

		if(modulo == 'livro-de-ocorrencias')
			this.Ocorrencias.init();

		if(modulo == 'moradores-da-unidade')
			this.MoradoresDaUnidade.init();

		if(modulo == 'veiculos-da-unidade')
			this.VeiculosDaUnidade.init();

		if(modulo == 'animais-de-estimacao')
			this.AnimaisDeEstimacao.init();

		if(modulo == 'fale-com')
			this.FaleCom.init();

		if(modulo == 'faq')
			this.FAQ.init();

		if(modulo == 'agendamento-de-mudanca')
			this.AgendamentoDeMudanca.init();

		if(modulo == 'chamados-de-manutencao')
			this.ChamadosDeManutencao.init();

		if(modulo == 'reserva-de-espacos')
			this.ReservaDeEspacos.init();

		if(modulo == 'pessoas-autorizadas')
			this.PessoasAutorizadas.init();

		if(modulo == 'pessoas-nao-autorizadas')
			this.PessoasNaoAutorizadas.init();

		if(modulo == 'prestadores-de-servico')
			this.PrestadoresDeServico.init();

		if(modulo == 'meu-perfil')
			this.MeuPerfil.init();

		if(modulo == 'alterar-senha')
			this.AlterarSenha.init();

		if(modulo == 'instrucoes-de-uso')
			this.InstrucoesDeUso.init();

		if(modulo == 'festas-particulares')
			this.FestasParticulares.init();

		if(modulo == 'amizades')
			this.Amizades.init();

		if(modulo == 'classificados')
			this.Classificados.init();

		if(modulo == 'fornecedores')
			this.Fornecedores.init();

		if($('.retorno_formulario').length){
			setTimeout( function(){
				$('.retorno_formulario').addClass('removendo').fadeOut('normal');
			}, 3000);
		}

		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		})
	}
};
