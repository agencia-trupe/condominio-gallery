Moradores.Fornecedores = {

  mostrar_campo_outros: function(select){
    if(select.val() == 'outros'){
      Moradores.Formularios.adicionarCampoObrigatorio('#fornecedores-form-outra_categoria');
      $('#fornecedores-form-label-outra_categoria').fadeIn('normal').css('display', 'block');
    }else{
      Moradores.Formularios.removerCampoObrigatorio('#fornecedores-form-outra_categoria');
      $('#fornecedores-form-label-outra_categoria').fadeOut('normal');
    }
  },

  mostrar_campo_outros_onload: function(){
    Moradores.Fornecedores.mostrar_campo_outros($('#fornecedores-form-categoria'));
  },

  mostrar_campo_outros_onselect: function(){
    $('#fornecedores-form-categoria').on('change', function(){
      Moradores.Fornecedores.mostrar_campo_outros($(this));
    });
  },

  validar_form_avaliacao: function(){
    var form = $('#avaliar-form');

    if(form.length > 0){
      form.on('submit', function(e) {

        if($('#avaliacao-form-texto').val() == ''){

          var campo = $('#avaliacao-form-texto');
          var parent = campo.parent();

          parent.addClass('obrigatorio');

          campo.on('keyup', function(){
            parent.removeClass('obrigatorio');
          });

          e.preventDefault();
          return false;
        }

        if( $("input[name='avaliacao[nota]']:checked").length == 0 ){

          var rating_input = $('#avaliacao-form .rating');

          rating_input.addClass('obrigatorio').on('click', function(){
            rating_input.removeClass('obrigatorio');
          });

          e.preventDefault();
          return false;
        }

      });

    }
  },

  toggle_avaliacao_form: function() {
    $('#toggle-avaliacao-form').on('click', function(e){
      e.preventDefault();
      $('#avaliar').addClass('aberto');
    });
  },

  init : function(){

    if($('#avaliar').length){

      this.validar_form_avaliacao();
      this.toggle_avaliacao_form();

    }else{

      Moradores.Formularios.init({
        campos_obrigatorios : [
          '#fornecedores-form-categoria',
          '#fornecedores-form-nome',
          '#fornecedores-form-avaliacao_texto',
          "fornecedores_avaliacao_nota"
        ],
        form_com_imagem   : true,
        multiplas_imagens : true,
        identificador_sessao : 'fornecedores',
        diretorio_imagens : 'fornecedores'
      });

      this.mostrar_campo_outros_onload();
      this.mostrar_campo_outros_onselect();

    }

  }
};
