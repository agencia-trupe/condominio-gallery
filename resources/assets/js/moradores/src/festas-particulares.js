Moradores.FestasParticulares = {

	diarias_bloqueadas : [],

	form_datepicker : function(){
		var picker = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('datepicker'),
			format: 'DD/MM/YYYY',
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			minDate: new Date(),
			bound: false,
			container: document.getElementById('calendario'),
			disableDayFn: function(data){
	        	var data_moment = moment(data);
	        	return Moradores.FestasParticulares.diarias_bloqueadas.indexOf(data_moment.format('YYYY-MM-DD')) !== -1;
	        }
		});
	},

	verificar_envio_form_diaria: function(){
		$('#form-selecao-data-festa-particular').submit( function(e){

			$('#mensagens-erro').html('');

			if($('#datepicker').val() == ''){

				var aviso = $("<p class='retorno_formulario erro'>Informe a Data da Festa</p>");

				$('#mensagens-erro').html($(aviso));

				setTimeout( function(){
					aviso.fadeOut('fast', function(){
						aviso.remove();
					});
				}, 3000);

				e.preventDefault();
				return false;
			}

			if($('#input-titulo-festa').val() == ''){

				var aviso = $("<p class='retorno_formulario erro'>Informe um Título para a Festa</p>");

				$('#mensagens-erro').html($(aviso));

				setTimeout( function(){
					aviso.fadeOut('fast', function(){
						aviso.remove();
					});
				}, 3000);

				e.preventDefault();
				return false;
			}

		});
	},

	gerenciar_convidados : function(){
		this.focar_input_convidados();
		this.inserir_lista_colada();
		this.inserir_no_enter();
		this.remover_convidado();
	},

	focar_input_convidados : function(){
		document.getElementById('convidados-textarea').focus();
	},

	inserir_lista_colada: function(){
		$("#convidados-textarea").on('paste', function (e) {
			setTimeout( function(){
				Moradores.FestasParticulares.inserir_convidado(false);
			}, 100);
	    });
	},

	inserir_no_enter: function(){
		$("#convidados-textarea").on('keyup', function (e) {
			if(e.which == 13){
				Moradores.FestasParticulares.inserir_convidado(true);
			}
	    });
	},

	inserir_convidado: function(prepend){
		var lista = $("#convidados-textarea").val().split('\n');
		var nro_nomes = lista.length;

		for (var i = 0; i < nro_nomes; i++) {
			if(lista[i] != ''){

				var item = $("<li style='display: none;'>"+lista[i]+" <a href='#' class='btn btn-danger btn-mini'>x</a><input type='hidden' name='lista_convidados[]' value='"+lista[i]+"'></li>");

				if(prepend)
					$('#convidados-lista').prepend(item);
				else
					$('#convidados-lista').append(item);

				item.fadeIn('normal');
			}
		};

		$("#convidados-textarea").val('');
	},

	remover_convidado: function(){
		$(document).on('click', '#convidados-lista > li > a', function(e){

			e.preventDefault();

			var convidado = $(this).parent();
			convidado.fadeOut('normal', function(){
				convidado.remove();
			});
		});
	},

	init : function(){

		this.is_convidados 		= $("#convidados-textarea").length;
		this.diarias_bloqueadas = $('#diarias_bloqueadas_json').length > 0 ? $('#diarias_bloqueadas_json').val() : [];

		this.form_datepicker();
		this.verificar_envio_form_diaria();

		if(this.is_convidados)
			this.gerenciar_convidados();
	}
};