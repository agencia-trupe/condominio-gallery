Portaria.Lavanderia = {

  listaUnidades : [],

  timer : null,

  iniciar_unidades : function()
	{
		this.listaUnidades = JSON.parse($('#listaUnidadesJson').val());
	},

  verificar_unidade : function(unidade)
	{
		var resumo_unidade = unidade.toUpperCase();
    var filtrado = this.listaUnidades.filter(
    	function(data){return data == resumo_unidade}
  	);

    return filtrado.length > 0;
	},

  mascara_somente_digitos : function()
  {
    $('#input-lavanderia-quantidade').mask('000');
  },

  remover_erros : function()
  {
    $.each($('.input-erro input'), function(){

      var evento = $(this).prop('nodeName').toLowerCase() == 'select'  ? 'click' : 'keyup';

      $(this).on(evento, function(){
        $(this).parent()
               .removeClass('input-erro')
               .removeClass('obrigatorio')
               .removeClass('email-invalido')
               .removeClass('login-fail');
      });

    });
  },

  validacao_on_submit : function(inputs_obrigatorios)
  {
		$('form').submit( function(e) {

			for (var i = 0; i < inputs_obrigatorios.length; i++) {

				var check = $(inputs_obrigatorios[i]);

				if(!check.val()){

					check.parent()
               .addClass('input-erro')
							 .addClass('obrigatorio');

					evento = check.prop('nodeName').toLowerCase() == 'select' ? 'click' : 'keyup';

					check.focus().on(evento, function(){
					  	check.parent()
                   .removeClass('input-erro')
					  			 .removeClass('obrigatorio');
					});

					e.preventDefault();
					return false;
				}
			};

      var check_unidade = $('#input-lavanderia-unidade');

      if(!Portaria.Lavanderia.verificar_unidade(check_unidade.val())) {
        check_unidade.parent()
                     .addClass('input-erro')
                     .addClass('unidade-invalida');

        check_unidade.focus().on('keyup', function(){
            check_unidade.parent()
                         .removeClass('input-erro')
                         .removeClass('unidade-invalida');
        });

        e.preventDefault();
        return false;
      }

		});
	},

  mostra_retorno_em_shadow : function()
  {
    var retorno = $('#retorno_formulario_shadow');

    if(retorno.length){
      $.featherlight(retorno, {
        closeOnClick: 'anywhere',
        onKeyUp: function(){
          $.featherlight.current().close();
        }
      });
    }
  },

  busca_unidade_keyup : function()
  {
    $('#consultar-unidade-lavanderia').on('keyup', function(e){

      var parent = $(this).parent();
			var termo = $(this).val();
      var loader = $('#loader');

      clearTimeout(Portaria.Lavanderia.timer);

			if(termo.length > 0){

        if(Portaria.Lavanderia.verificar_unidade(termo)){

          parent.removeClass('input-erro')
                .removeClass('unidade-invalida');

          loader.fadeIn('fast', function(){
            Portaria.Lavanderia.timer = setTimeout( function(){
              Portaria.Lavanderia.buscar_fichas(termo);
            }, 400);
          });


        }else{

          parent.addClass('input-erro')
                .addClass('unidade-invalida');

          Portaria.Lavanderia.apagar_resultado_busca();

        }

			}else{

        parent.removeClass('input-erro')
              .removeClass('unidade-invalida');

				Portaria.Lavanderia.apagar_resultado_busca();

			}

		});
  },

  buscar_fichas : function(termo)
  {
    $.post('portaria/lavanderia/consulta', {
      'unidade' : termo
    }, function(retorno){

      Portaria.Lavanderia.mostrar_resultados(termo, retorno);

    });
  },

  mostrar_resultados : function(termo, retorno)
  {
    $('#resultados-unidade-placeholder').html(termo.toUpperCase());

    if(retorno.length == 0)
      Portaria.Lavanderia.mostrar_msg_sem_resultados();
    else
      Portaria.Lavanderia.mostrar_tabela_fichas(retorno);

    $('#loader').fadeOut('fast', function(){
      $('#resultados-lavanderia').fadeIn('normal');
    });

  },

  mostrar_msg_sem_resultados : function()
  {
    $('#resultados-lavanderia .contem-tabela').html('<h4>Nenhum resultado encontrado</h4>');
  },

  mostrar_tabela_fichas : function(retorno)
  {
    var tabela = "<table>";

    for (var i = 0; i < retorno.length; i++) {
      tabela += "<tr>";
        tabela += "<td>";
          tabela += retorno[i].unidade_resumo;
        tabela += "</td>";
        tabela += "<td>";
          tabela += retorno[i].data_formatada;
        tabela += "</td>";
        tabela += "<td>";
          tabela += retorno[i].quantidade;
          tabela += " ficha";
          tabela += (retorno[i].quantidade > 1) ? "s" : "";
        tabela += "</td>";
        tabela += "<td>";
          tabela += "<a href='#' class='btn btn-circular btn-danger btn-mini' data-ficha='" + retorno[i].id + "'>X</a>";
        tabela += "</td>";
      tabela += "</tr>";
    }

    tabela += "</table>";

    $('#resultados-lavanderia .contem-tabela').html(tabela);
  },

  apagar_resultado_busca : function()
  {
    $('#resultados-lavanderia').fadeOut('normal');
  },

  bind_remover_fichas : function()
  {
    $('#resultados-lavanderia').on('click', 'table .btn', function(e){
      e.preventDefault();

      var linha    = $(this).parent().parent();
      var ficha_id = $(this).attr('data-ficha');

      $.featherlight($('#input-motivo-remocao'), {
        afterContent : function(){

          $(document).on('click', '.enviar-solicitacao-remocao', function(){

            var motivo_input = $('.featherlight textarea');
            var motivo = motivo_input.val();

            if(motivo == ''){

              motivo_input.parent()
                          .addClass('input-erro')
                          .addClass('obrigatorio');

              return false;

            }else{

              $.featherlight.current().close();
              linha.fadeOut('normal', function(){
                Portaria.Lavanderia.remover_ficha_lavanderia(linha, ficha_id, motivo);
              });

            }
          });

          $(document).on('keyup', 'textarea', function(){
            if($(this).val() != ''){
              $(this).parent()
                     .removeClass('input-erro')
                     .removeClass('obrigatorio');
            }
          });

        }
      });

    });
  },

  remover_ficha_lavanderia : function(linha, ficha_id, motivo)
  {
    $.post('portaria/lavanderia/remover', {
      ficha_id : ficha_id,
      motivo_cancelamento : motivo
    }, function(retorno){

      if(retorno == 1){

        linha.remove();

        if($('#resultados-lavanderia tr').length == 0)
          Portaria.Lavanderia.mostrar_msg_sem_resultados();

      }else{
        linha.fadeIn('normal');
      }

    });
  },

  init: function(){
    var campos_obrigatorios = [
      '#input-lavanderia-quantidade',
      '#input-lavanderia-unidade',
      '#input-lavanderia-login',
      '#input-lavanderia-senha'
		];

    this.iniciar_unidades();
    this.validacao_on_submit(campos_obrigatorios);
    this.remover_erros();
    this.mascara_somente_digitos();
    this.busca_unidade_keyup();
    this.mostra_retorno_em_shadow();
    this.bind_remover_fichas();
  }
};
