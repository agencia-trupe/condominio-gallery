Portaria.Comunicacao = {

  remover_erros : function() {
    $.each($('.input-erro input, .input-erro select, .input-erro textarea'), function(){

      var evento = $(this).prop('nodeName').toLowerCase() == 'select'  ? 'click' : 'keyup';

      $(this).on(evento, function(){
        $(this).parent()
               .removeClass('input-erro')
               .removeClass('obrigatorio');
      });

    });
  },

  validacao_on_submit : function(inputs_obrigatorios) {
		$('form').submit( function(e) {

			for (var i = 0; i < inputs_obrigatorios.length; i++) {

				var check = $(inputs_obrigatorios[i]);

				if(!check.val()){

					check.parent()
               .addClass('input-erro')
							 .addClass('obrigatorio');

					evento = check.prop('nodeName').toLowerCase() == 'select' ? 'click' : 'keyup';

					check.focus().on(evento, function(){
				  	check.parent()
                 .removeClass('input-erro')
				  			 .removeClass('obrigatorio');
					});

					e.preventDefault();
					return false;
				}
			};

		});
	},

  init: function(){
    var campos_obrigatorios = [
			'#input-comunicao-titulo',
			'#input-comunicacao-descricao'
		];

    this.validacao_on_submit(campos_obrigatorios);
    this.remover_erros();
  }
};