@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="fale-com-main">

	 		<h1>FALE COM</h1>

	 		<p>
				Para falar diretamente com a equipe diretiva do Condomínio utilize os canais abaixo:
	 		</p>

			<ul id="lista-contatos">
 				<li>

 					<div class="titulo">
 						{{ $destinatarios[$destinatario] }}
 					</div>

 					<div class="conteudo">

 						<div class="formulario texto">

							<h2>ENVIANDO MENSAGEM</h2>

							@if(Session::has('fale_com_create') && session('fale_com_create') == false)
								<p class="fale_com_retorno erro">
									<strong>Erro ao enviar mensagem.</strong> {{session('fale_com_create_msg')}}
								</p>
							@endif

							<form action="{{ route('moradores.fale-com.mensagem', $destinatario) }}" method="post" id="form-fale-com">

								{!! csrf_field() !!}

								<label @if($errors->has('falecom.assunto')) class="input-erro obrigatorio" @endif >
									<input type="text" name="falecom[assunto]" placeholder="assunto" id="fale-com-assunto" value="{{ old('falecom.assunto') }}">
								</label>

								<label @if($errors->has('falecom.mensagem')) class="input-erro obrigatorio" @endif >
									<textarea name="falecom[mensagem]" placeholder="mensagem" id="fale-com-mensagem">{{ old('falecom.mensagem') }}</textarea>
								</label>

								<input type="submit" value="ENVIAR" class="btn btn-success">
							</form>

 						</div>

 						<a href="{{ route('moradores.fale-com.index') }}" class="btn btn-com-icone btn-info" title="Voltar"><img src="assets/images/layout/moradores/setinha-voltar.png" class="imagem-seta" alt="Voltar"> VOLTAR</a>

 					</div>
 				</li>
	 		</ul>

	 	</div>

	</section>

@stop
