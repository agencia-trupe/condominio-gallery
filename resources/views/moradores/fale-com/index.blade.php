@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="fale-com-main">

	 		<h1>FALE COM</h1>

	 		<p>
				Para falar diretamente com a equipe diretiva do Condomínio utilize os canais abaixo:
	 		</p>

	 		@if(Session::has('fale_com_create') && session('fale_com_create') == true)
				<p class="retorno_formulario sucesso">
					<strong>Sua mensagem foi enviada com sucesso!</strong>
				</p>
			@endif

	 		<ul id="lista-contatos">
	 			@if($registro->contato_sindico)
	 				<li>
	 					<div class="titulo">
	 						Síndico
	 					</div>
	 					<div class="conteudo conteudo-cke">
	 						<div class="texto">
								{!! $registro->contato_sindico !!}

								<a href="{{ route('moradores.fale-com.mensagem', 'sindico') }}" class="link-enviar-mensagem">ENVIAR MENSAGEM</a>
	 						</div>
	 					</div>
	 				</li>
	 			@endif

	 			@if($registro->contato_conselho)
	 				<li>
	 					<div class="titulo">
	 						Conselho
	 					</div>
	 					<div class="conteudo">
	 						<div class="texto conteudo-cke">
								{!! $registro->contato_conselho !!}

								<a href="{{ route('moradores.fale-com.mensagem', 'conselho') }}" class="link-enviar-mensagem">ENVIAR MENSAGEM</a>
	 						</div>
	 					</div>
	 				</li>
	 			@endif

	 			@if($registro->contato_zelador)
	 				<li>
	 					<div class="titulo">
	 						Zelador
	 					</div>
	 					<div class="conteudo">
	 						<div class="texto">
								{!! $registro->contato_zelador !!}

								<a href="{{ route('moradores.fale-com.mensagem', 'zelador') }}" class="link-enviar-mensagem">ENVIAR MENSAGEM</a>
	 						</div>
	 					</div>
	 				</li>
	 			@endif

	 			@if($registro->contato_portaria)
	 				<li>
	 					<div class="titulo">
	 						Portaria
	 					</div>
	 					<div class="conteudo">
	 						<div class="texto">
								{!! $registro->contato_portaria !!}

								<a href="{{ route('moradores.fale-com.mensagem', 'portaria') }}" class="link-enviar-mensagem">ENVIAR MENSAGEM</a>
	 						</div>
	 					</div>
	 				</li>
	 			@endif

	 			@if($registro->contato_administradora)
	 				<li>
	 					<div class="titulo">
	 						Administração
	 					</div>
	 					<div class="conteudo">
	 						<div class="texto">
								{!! $registro->contato_administradora !!}
	 						</div>
	 					</div>
	 				</li>
	 			@endif

	 		</ul>

	 	</div>

	</section>

@stop
