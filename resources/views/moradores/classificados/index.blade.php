@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="classificados-main">

			<h1>MURAL & CLASSIFICADOS</h1>

      @include('moradores.classificados.partials.submenu')

			@include('moradores.classificados.partials.mensagens')

			@include('moradores.classificados.partials.lista-anuncios-por-categoria')

		</div>

	</section>

@stop
