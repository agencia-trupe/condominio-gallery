<div id="meus-anuncios">

  <h3>Meus Anúncios Publicados &middot; Gerenciar</h3>

  @if(sizeof(Auth::moradores()->get()->anuncios) > 0)

    <div id="lista-registros">
      @foreach(Auth::moradores()->get()->anuncios as $anuncio)
        <li>
          <div class="imagem">
            @if($anuncio->fotos)
              <div class="galeria-anuncio" data-featherlight-gallery data-featherlight-filter="a">
                @foreach($anuncio->fotos as $key => $value)
                  <a href="assets/images/moradores/classificados/redimensionadas/{{$value->imagem}}" class='abre-galeria-anuncio' title="Abrir galeria do anúncio">
                    <img src="assets/images/moradores/classificados/thumbs/{{$value->imagem}}" alt="Abrir galeria do anúncio" />
                  </a>
                @endforeach
              </div>
            @endif
          </div>
          <div class="corpo-anuncio">
            <div class="data-publicacao">Publicado em: {{$anuncio->created_at->format('d/m/Y')}}</div>
            <h2>{{$anuncio->titulo}}</h2>
            <div class="descricao">{{ nl2br($anuncio->descricao) }}</div>
            <div class="contato">{!! nl2br(parse_emails($anuncio->contato)) !!}</div>
            <div class="controles-anuncio">
              <a href="{{ route('moradores.classificados.editar', $anuncio->id) }}" title="Editar este anúncio" class="btn btn-info">EDITAR</a>
              <a href="{{ route('moradores.classificados.remover', $anuncio->id) }}" title="Remover este anúncio" class="btn btn-danger">X</a>
            </div>
          </div>
        </li>
      @endforeach
    </div>

  @else

    <p>
      Você ainda não possui nenhum anúncio publicado.
    </p>

  @endif

</div>