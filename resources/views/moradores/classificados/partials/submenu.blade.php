<div class="seleciona-categoria-anuncio">

  <a
    href="{{ route('moradores.classificados.index', 'venda_e_troca') }}"
    class="btn
      @if($slug_categoria == 'venda_e_troca')
        btn-success
      @else
        btn-info
      @endif "
    title="Venda & Troca">
    Venda & Troca
  </a>

  <a
    href="{{ route('moradores.classificados.index', 'servico') }}"
    class="btn
      @if($slug_categoria == 'servico')
        btn-success
      @else
        btn-info
      @endif "
    title="Serviços">
    Serviços
  </a>

  <a
    href="{{ route('moradores.classificados.index', 'recado') }}"
    class="btn
      @if($slug_categoria == 'recado')
        btn-success
      @else
        btn-info
      @endif "
    title="Mural · Recados">
    Mural · Recados
  </a>

  <a
    href="{{ route('moradores.classificados.index', 'meus_anuncios') }}"
    class="btn
      @if($slug_categoria == 'meus_anuncios')
        btn-success
      @else
        btn-info
      @endif "
    title="Publicar · Meus Anúncios">
    Publicar · Meus Anúncios
  </a>

</div>