@if(Session::has('classificados_create') && session('classificados_create') == true)
	<p class="retorno_formulario sucesso">
		<strong>Classificado publicado com sucesso.</strong>
	</p>
@endif

@if(Session::has('classificados_create') && session('classificados_create') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao publicar Classificado.</strong>
	</p>
@endif

@if(Session::has('classificados_edit') && session('classificados_edit') == true)
	<p class="retorno_formulario sucesso">
		<strong>Classificado alterado com sucesso.</strong>
	</p>
@endif

@if(Session::has('classificados_edit') && session('classificados_edit') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao alterar Classificado.</strong>
	</p>
@endif

@if(Session::has('classificados_delete') && session('classificados_delete') == true)
	<p class="retorno_formulario sucesso">
		<strong>Anúncio removido com sucesso.</strong>
	</p>
@endif