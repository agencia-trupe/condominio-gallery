@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="instrucoes-de-uso-main">

	 		<h1>INSTRUÇÕES DE USO</h1>

	 		<p>
				Consulte aqui de forma resumida como usar os diversos espaços e equipamentos do Condomínio.
	 		</p>

	 		<ul id="lista-instrucoes-de-uso">
	 			@foreach($instrucoes as $instrucao)
	 				<li>
	 					<a href="#" title="{{ $instrucao->questao }}" class="questao conteudo-cke">
	 						{!! $instrucao->questao !!}

	 						<img src="assets/images/layout/moradores/sinal-mais.png" alt="+">
	 					</a>
	 					<div class="resposta conteudo-cke">
	 						{!! $instrucao->resposta !!}
	 					</div>
	 				</li>
	 			@endforeach
	 		</ul>

	 	</div>

	</section>

@stop