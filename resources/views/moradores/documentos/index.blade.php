@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="documentos-main">

			<h1>DOCUMENTOS OFICIAIS</h1>

			<div class="contemDocumentos">

				<div class="listaCategorias">
					@if(sizeof($listaCategorias))
						<ul>
						@foreach ($listaCategorias as $categoria)
							<li>
								<a href="{{ route('moradores.documentos.index', $categoria->slug) }}" title="{{ $categoria->titulo }}" @if($categoria->id == $categoria_selecionada->id) class="ativo" @endif >{{ $categoria->titulo }}</a>
							</li>
						@endforeach
						</ul>
					@endif
				</div><!--

			 --><div class="listaArquivos">
					<h2>{{ $categoria_selecionada->titulo }}</h2>
					<p>{!! nl2br($categoria_selecionada->texto) !!}</p>
					<ul>
						@forelse ($categoria_selecionada->documentos as $documento)
							<li>
								<h3>{{ $documento->titulo }}</h3>
								<p>{!! nl2br($documento->texto) !!}</p>
								<a href="{{ route('moradores.documentos.download', $documento->id) }}" title="Download do Documento">Download do Documento</a>
							</li>
						@empty
							<li><h3>Nenhum documento cadastrado</h3></li>
						@endforelse
					</ul>
				</div>

			</div>

		</div>

	</section>

@stop