<div id="controle-prestadores-de-servico">

@if(isset($prestador_de_servico))
	<a href="#" title="Alterar prestador de serviço" id="form-toggle" class="btn btn-success btn-com-icone" data-toggle-target="prestadores-de-servico-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> ALTERAR PRESTADOR DE SERVIÇO</a>
	<form action="{{ route('moradores.prestadores-de-servico.atualizar', $prestador_de_servico->id) }}" id="prestadores-de-servico-form" class="form-unidade-edicao" method="post">
@else
	<a href="#" title="Acrescentar prestador de serviço" id="form-toggle" class="btn btn-success btn-com-icone" data-toggle-target="prestadores-de-servico-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> ACRESCENTAR PRESTADOR DE SERVIÇO</a>
	<form action="{{ route('moradores.prestadores-de-servico.cadastrar') }}" id="prestadores-de-servico-form" method="post">
@endif
		<fieldset>

			{!! csrf_field() !!}

			<label class="
				prestadores-de-servico-form-input
				@if($errors->has('prestadores_de_servico.nome')) input-erro obrigatorio @endif
			">
				<input type="text" id="prestadores-de-servico-form-nome" name="prestadores_de_servico[nome]" placeholder="nome completo" @if(isset($prestador_de_servico)) value="{{ $prestador_de_servico->nome }}" @else value="{{ old('prestadores_de_servico.nome') }}" @endif>
				<div class="
					prestadores-de-servico-input-addon
					@if($errors->has('prestadores_de_servico.visibilidade')) input-erro obrigatorio @endif
					">
					<a href="#" class="prestadores-de-servico-form-set_visibilidade addon-tooltip-visib off" title="Definir Visibilidade do registro" data-shadow-title="Cadastro de pessoa autorizada" data-shadow-storage="prestadores-de-servico-form-visibilidade"></a>
					<input type="hidden" name="prestadores_de_servico[visibilidade]" id="prestadores-de-servico-form-visibilidade" @if(isset($prestador_de_servico)) value="{{ $prestador_de_servico->visibilidades }}" @else value="{{ old('prestadores_de_servico.visibilidade') }}" @endif>
				</div>
			</label>

			<label class="
				prestadores-de-servico-form-input
				@if($errors->has('prestadores_de_servico.documento')) input-erro obrigatorio @endif
			">
				<input type="text" id="prestadores-de-servico-form-documento" name="prestadores_de_servico[documento]" placeholder="documento" @if(isset($prestador_de_servico)) value="{{ $prestador_de_servico->documento }}" @else value="{{ old('prestadores_de_servico.documento') }}" @endif>
			</label>

			<label class="
				prestadores-de-servico-form-input
				@if($errors->has('prestadores_de_servico.empresa')) input-erro obrigatorio @endif
			">
				<input type="text" id="prestadores-de-servico-form-empresa" name="prestadores_de_servico[empresa]" placeholder="empresa (opcional)" @if(isset($prestador_de_servico)) value="{{ $prestador_de_servico->empresa }}" @else value="{{ old('prestadores_de_servico.empresa') }}" @endif>
			</label>

			<label class="prestadores-de-servico-form-input">
				<input type="text" id="prestadores-de-servico-form-data_nascimento" class="datepicker" name="prestadores_de_servico[data_nascimento]" placeholder="data de nascimento (opcional)" @if(isset($prestador_de_servico) && !is_null($prestador_de_servico->data_nascimento)) value="{{ $prestador_de_servico->data_nascimento->format('d/m/Y') }}" @else value="{{ old('prestadores_de_servico.data_nascimento') }}" @endif>
			</label>

			<label class="
				prestadores-de-servico-form-input
				@if($errors->has('prestadores_de_servico.permissao_inicio')) input-erro obrigatorio @endif
			">
				<input type="text" id="prestadores-de-servico-form-permissao_inicio" class="datepicker" name="prestadores_de_servico[permissao_inicio]" placeholder="Início da autorização" @if(isset($prestador_de_servico)) value="{{ $prestador_de_servico->permissao_inicio->format('d/m/Y') }}" @else value="{{ old('prestadores_de_servico.permissao_inicio') }}" @endif>
			</label>

			<label class="
				prestadores-de-servico-form-input
				@if($errors->has('prestadores_de_servico.permissao_termino')) input-erro data-menor @endif
			">
				<input type="text" id="prestadores-de-servico-form-permissao_termino" class="datepicker" name="prestadores_de_servico[permissao_termino]" placeholder="Término da autorização" @if(isset($prestador_de_servico)) value="{{ $prestador_de_servico->permissao_termino->format('d/m/Y') }}" @else value="{{ old('prestadores_de_servico.permissao_termino') }}" @endif>
			</label>

		</fieldset>

		<input type="submit" @if(isset($prestador_de_servico)) value="ALTERAR" @else value="CADASTRAR" @endif class="btn btn-info">

	</form>

</div>