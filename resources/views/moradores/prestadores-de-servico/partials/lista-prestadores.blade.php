@if(sizeof($prestadores_de_servico))
	<ul id="lista-registros">
		@foreach($prestadores_de_servico as $prestador)
			<li>
				<div class="prestadores-de-servico-texto">
					<div class="prestadores-de-servico-espacamento">
						<h2>{{ $prestador->nome }}</h2>
						<div class="texto">
							Documento: {{ $prestador->documento }}<br>
							Empresa: {{ $prestador->empresa ? $prestador->empresa : 'não informado' }}<br>
							Data de Nascimento: {{ $prestador->data_nascimento ? $prestador->data_nascimento->format('d/m/Y') : 'não informado' }}<br>
							Autorizado de: <strong>{{ $prestador->permissao_inicio->format('d/m/Y') }}</strong> até <strong>{{ $prestador->permissao_termino->format('d/m/Y') }}</strong>
						</div>
						<div class="acoes">
							<a href="{{ route('moradores.prestadores-de-servico.editar', $prestador->id) }}" class="btn btn-info" title="Editar">EDITAR</a>
							<a href="{{ route('moradores.prestadores-de-servico.remover', $prestador->id) }}" class="btn btn-danger" title="Remover">X</a>
						</div>
					</div>
				</div>
			</li>
		@endforeach
	</ul>
@endif