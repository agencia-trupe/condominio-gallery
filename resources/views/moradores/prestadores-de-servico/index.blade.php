@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="prestadores-de-servico-main">

			<h1>PRESTADORES DE SERVIÇO</h1>

			<p>Registre os prestadores de serviços que podem acessar sua Unidade.</p>

			@include('moradores.prestadores-de-servico.partials.mensagens')

			@include('moradores.prestadores-de-servico.partials.form')

			@include('moradores.prestadores-de-servico.partials.lista-prestadores')

		</div>

	</section>

@stop