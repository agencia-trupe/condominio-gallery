<thead>
	<tr>
		<th></th>
		@for($dia = new Carbon\Carbon($data_inicial); $dia <= $hoje_mais_3_dias; $dia->modify('+1 day'))
			<th>
				{!! $dia->formatLocalized("%e %b %Y · <span class='dia-da-semana'>%a</span>") !!}
			</th>
		@endfor
	</tr>
</thead>
<tbody>
	@for ($horario = $horario_inicial; $horario <= $horario_final; $horario = $horario->addMinutes($tempo_de_reserva))
		<tr>

			<td>{{ $horario->format('H:i') }}</td>

			@for($dia_in = new Carbon\Carbon($data_inicial); $dia_in <= $hoje_mais_3_dias; $dia_in->modify('+1 day'))

				<?php
					$data_formatada = $dia_in->format('Y-m-d');
					$horario_inicio = $horario->format('H:i');
					$horario_termino = clone $horario;
					$horario_termino->addMinutes($tempo_de_reserva);
					$timestamp = $data_formatada.' '.$horario_inicio;
				?>

				{{-- Se a data/hora já passou --}}
				@if(Carbon\Carbon::createFromFormat('Y-m-d H:i', $timestamp) <= date('Y-m-d H:i'))

					<td class="bloqueado"></td>

				{{-- Se estiver bloqueado --}}
				@elseif(isset($timestamp_bloqueios[$timestamp]))

					<td class="bloqueado"></td>

				{{-- Se houver reserva minha --}}
				@elseif(isset($timestamp_minhas_reservas[$timestamp]))

					<td class="minha-reserva">
						AGENDADO
						<div class="botoes">
							<a href="#" title="Ver detalhes"
										class="btn-visualizar-reserva"
										data-formatada="{{ $dia_in->formatLocalized('%e de %B de %Y') }}"
										data-horario-inicio="{{ $horario_inicio }}"
										data-horario-fim="{{ $horario_termino->format('H:i') }}"
										data-anotacoes="{{ $timestamp_minhas_reservas[$timestamp] == '' ? 'sem anotações' : $timestamp_minhas_reservas[$timestamp] }}">
								<img src="assets/images/layout/moradores/icone-visualizacao-bco.png">
							</a>
							<a href="{{ route('moradores.reserva-de-espacos.remover', [$espaco->slug, str_replace(' ', '_', $timestamp)]) }}" title="Remover reserva" class="btn btn-danger btn-circular">x</a>
						</div>
					</td>

				{{-- Se houver reserva de outras pessoas --}}
				@elseif(isset($timestamp_reservas_outros_usuarios[$timestamp]))

					<td class="reservado">
						{{ $timestamp_reservas_outros_usuarios[$timestamp] }}
					</td>

				{{-- Se estiver livre --}}
				@else

					<td class="livre">
						<label data-formatada="{{ $dia_in->formatLocalized('%e de %B de %Y') }}" data-horario-inicio="{{ $horario_inicio }}" data-horario-fim="{{ $horario_termino->format('H:i') }}">
							<input type="checkbox" name="reserva[agendamentos]" value="{{ $timestamp }}">
						</label>
					</td>

				@endif

			@endfor

			<td></td>

		</tr>
	@endfor
</tbody>