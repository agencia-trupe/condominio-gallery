@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="reserva-de-espacos-main">

	 		<h1>RESERVA DE ESPAÇOS &middot; {{ $espaco->titulo }}</h1>

	 		<p>Para a locação de áreas comuns selecione a data e faça sua reserva.</p>

	 		@include('moradores.reserva-de-espacos.partials.mensagens')

			<div id="grid-por-periodo" class="grid-reserva">

				<div id="seleciona-data">

					<div id="calendario">

					</div>

					<div id="input-data">
						<label>
							Ou informe a data desejada:<br>
							<input type="text" id="datepicker" value="{{ Carbon\Carbon::createFromFormat('Y-m-d', $data_inicial)->format('d/m/Y') }}">
						</label>
					</div>

				</div>

				<form action="{{ route('moradores.reserva-de-espacos.reservar', $espaco->slug) }}" method="POST">

					{!! csrf_field() !!}

					<div class="table-wrapper">

						<a href="#" title="Próximo Dia" id='navegacao-por-dia'>
							<img src="assets/images/layout/moradores/setinha-regular-on.png" alt="próximo dia">
						</a>

						<table class="tabela-horarios" data-slug-espaco="{{ $espaco->slug }}">
							@include('moradores.reserva-de-espacos.por_periodo.tabela')
						</table>

					</div>

					<div id="erros"></div>

					<input type="hidden" name="reserva[anotacoes]" id="input-anotacoes">
				</form>

	 		</div>

	 	</div>

	</section>

@stop