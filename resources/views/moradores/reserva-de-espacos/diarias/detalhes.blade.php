@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="reserva-de-espacos-main">

	 		<h1>RESERVA DE ESPAÇOS &middot; {{ $espaco->titulo }}</h1>

	 		<p>Visualização das instruções de uso de reserva realizada:</p>

			<div id="aviso-diaria">

				<h2>VOCÊ RESERVOU: <strong>{{ $espaco->titulo }}</strong></h2>

				<h2>DATA SELECIONADA: <strong>{{ $reserva->reserva->formatLocalized("%e de %B de %Y") }}</strong></h2>

				<h3>NOME DO EVENTO: <strong>{{ $reserva->titulo }}</strong></h3>

				<p>
					O valor da locação do espaço: Salão de Festas é de R$ {{ $espaco->informacoes->valor }}.<br>
					Esse valor será acrescentado no seu boleto do condomínio para pagamento.
				</p>

				<hr>

				<div class="swap-texto">

					<div class="inicial">

						<p class="reduzido texto-azul">A locação deste espaço implica na aceitação de todas as regras estabelecidas no Regulamento Interno e nas instruções de uso abaixo disponibilizadas:</p>

						<a href="{{ route('moradores.instrucoes-de-uso.index') }}" class="btn btn-vazado">DOWNLOAD DAS INSTRUÇÕES DE USO E FORMULÁRIOS</a>

						<div class="resumo">
							<h3>RESUMO DAS OBRIGAÇÕES DE QUEM UTILIZA O ESPAÇO: {{ $espaco->titulo }}</h3>

							{!! $espaco->informacoes->texto !!}
						</div>

						<hr>

						<a href="{{ route('moradores.reserva-de-espacos.grid', $espaco->slug) }}" class="btn btn-info" title="VOLTAR">VOLTAR</a>

					</div>

				</div>
			</div>

	 	</div>

	</section>

@stop