@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="avisos-main">

			@if($aviso_lido_em !== false)
	 			<h1>RELENDO AVISO</h1>
	 		@else
				<h1>LENDO AVISO</h1>
	 		@endif

			<div id="avisos-detalhes">
				<div class="lista-aviso-detalhe">
					<div class="coluna-esquerda">
						<div class="aviso-timestamp">{{ $aviso->created_at->format('d/m/y · H:i \h') }}</div>
						<div class="aviso-marcar-lido">
							@if($aviso_lido_em !== false)
								<span class='aviso-lido-data'>Lido em: <br>{{ $aviso_lido_em }}</span>
							@else
								<label>
									<input type="checkbox" name="aviso_lido" value="{{ $aviso->id }}"> Li e entendi.
								</label>
							@endif
						</div>
					</div><!--
				 --><div class="coluna-direita">
						<div class="aviso-titulo">
							<h3>{{ $aviso->titulo }}</h3>
						</div>
						<div class="aviso-chamada-texto">
							{{ $aviso->olho }}
						</div>
						<div class="aviso-texto conteudo-cke">
							{!! $aviso->texto !!}
						</div>
						<div class="aviso-assinatura">
							{{ $aviso->assinatura }}
						</div>
						<div class="aviso-marcar-lido-inferior">
							@if($aviso_lido_em !== false)
								<span class='aviso-lido-data'>Lido em: {{ $aviso_lido_em }}</span>
							@else
								<label>
									<input type="checkbox" name="aviso_lido" value="{{ $aviso->id }}"> Li e entendi.
								</label>
							@endif
						</div>
					</div>
				</div>

				<a href="{{ route('moradores.avisos.index') }}" title="VER OUTROS AVISOS" class="btn btn-info btn-voltar"><img src="assets/images/layout/moradores/setinha-voltar.png" alt="Voltar"> VER OUTROS AVISOS</a>
			</div>

	 	</div>

	</section>
@stop