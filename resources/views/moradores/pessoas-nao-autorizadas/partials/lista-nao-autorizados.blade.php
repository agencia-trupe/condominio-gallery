@if(sizeof($pessoas))
	<ul id="lista-registros">
		@foreach($pessoas as $pessoa_n_aut)
			<li>
				<div class="pessoas-nao-autorizadas-foto">
					@if($pessoa_n_aut->foto && file_exists('assets/images/moradores/pessoas-nao-autorizadas/thumbs/'.$pessoa_n_aut->foto))
						<img src="assets/images/moradores/pessoas-nao-autorizadas/thumbs/{{ $pessoa_n_aut->foto }}" alt="Morador da unidade">
					@else
						<img src="moradores/assets/images/avatar-default/122/122/sem foto">
					@endif
				</div>
				<div class="pessoas-nao-autorizadas-texto">
					<div class="pessoas-nao-autorizadas-espacamento">
						<h2>{{ $pessoa_n_aut->nome }}</h2>
						<div class="data">{{ $pessoa_n_aut->data_nascimento->format('d/m/Y') }}</div>
						<div class="parentesco">{{ $pessoa_n_aut->parentesco }}</div>
						<div class="acoes">
							<a href="{{ route('moradores.pessoas-nao-autorizadas.editar', $pessoa_n_aut->id) }}" class="btn btn-info" title="Editar">EDITAR</a>
							<a href="{{ route('moradores.pessoas-nao-autorizadas.remover', $pessoa_n_aut->id) }}" class="btn btn-danger" title="Remover">X</a>
						</div>
					</div>
				</div>
			</li>
		@endforeach
	</ul>
@endif