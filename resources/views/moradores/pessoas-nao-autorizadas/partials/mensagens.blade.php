@if(Session::has('pessoa_nao_autorizada_create') && session('pessoa_nao_autorizada_create') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Pessoa Não Autorizada registrado com sucesso.</strong>
	</p>
@endif

@if(Session::has('pessoa_nao_autorizada_create') && session('pessoa_nao_autorizada_create') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Cadastrar Pessoa Não Autorizada.</strong>
	</p>
@endif

@if(Session::has('pessoa_nao_autorizada_destroy') && session('pessoa_nao_autorizada_destroy') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Pessoa Não Autorizada removido com sucesso.</strong>
	</p>
@endif

@if(Session::has('pessoa_nao_autorizada_destroy') && session('pessoa_nao_autorizada_destroy') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Remover Pessoa Não Autorizada.</strong>
	</p>
@endif

@if(Session::has('pessoa_nao_autorizada_update') && session('pessoa_nao_autorizada_update') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Pessoa Não Autorizada atualizado com sucesso.</strong>
	</p>
@endif

@if(Session::has('pessoa_nao_autorizada_update') && session('pessoa_nao_autorizada_update') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Atualizar Pessoa Não Autorizada.</strong>
	</p>
@endif

@if(Session::has('exception'))
	<p class="retorno_formulario erro">
		{{ session('exception') }}
	</p>
@endif