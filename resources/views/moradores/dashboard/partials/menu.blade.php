<ul class="dashboard-mostra-areas">
	<li class="area-admin">
		<a href="#" title="administração e comunicação" class="area main-nav-menu link-admin @if(preg_match('~moradores\.(ocorrencias|documentos|avisos|agenda|fale|faq|agendamento|fornecedores)\.*~', Route::currentRouteName())) marcado @endif">
			<p>administração e comunicação</p>
		</a>
		<ul class="submenu">
			<li>
				<h3>ADMINISTRAÇÃO E COMUNICAÇÃO</h3>
			</li>
			<li><a href="{{ route('moradores.ocorrencias.index') }}" title="Livro de ocorrências" @if(str_is('moradores.ocorrencias*', Route::currentRouteName())) class='ativo' @endif >Livro de ocorrências</a></li>
			<li><a href="{{ route('moradores.documentos.index') }}" title="Documentos oficiais" @if(str_is('moradores.documentos*', Route::currentRouteName())) class='ativo' @endif >Documentos oficiais</a></li>
			<li><a href="{{ route('moradores.avisos.index') }}" title="Avisos" @if(str_is('moradores.avisos*', Route::currentRouteName())) class='ativo' @endif >Avisos</a></li>
			<li><a href="{{ route('moradores.agenda.index') }}" title="Agenda" @if(str_is('moradores.agenda.index', Route::currentRouteName())) class='ativo' @endif >Agenda</a></li>
			<li><a href="{{ route('moradores.fale-com.index') }}" title="Fale com" @if(str_is('moradores.fale-com*', Route::currentRouteName())) class='ativo' @endif >Fale com</a></li>
			<li><a href="{{ route('moradores.faq.index') }}" title="FAQ - Perguntas frequentes" @if(str_is('moradores.faq.index', Route::currentRouteName())) class='ativo' @endif >FAQ - Perguntas frequentes</a></li>
			<li><a href="{{ route('moradores.agendamento-de-mudanca.index') }}" title="Agendamento de mudança" @if(str_is('moradores.agendamento-de-mudanca.index', Route::currentRouteName())) class='ativo' @endif >Agendamento de mudança</a></li>			
		</ul>
	</li>
	<li class="area-unidade">
		<a href="#" title="minha unidade e garagem" class="area main-nav-menu link-unidade @if(preg_match('~moradores\.(moradores-da-unidade|veiculos-da-unidade|animais-de-estimacao)\.*~', Route::currentRouteName())) marcado @endif">
			<p>minha unidade e garagem</p>
		</a>
		<ul class="submenu">
			<li>
				<h3>MINHA UNIDADE E GARAGEM</h3>
			</li>
			<li><a href="{{ route('moradores.moradores-da-unidade.index') }}" title="Cadastro de moradores da unidade" @if(str_is('moradores.moradores-da-unidade*', Route::currentRouteName())) class='ativo' @endif >Cadastro de moradores da unidade</a></li>
			<li><a href="{{ route('moradores.veiculos-da-unidade.index') }}" title="Cadastro de veículos (dos moradores)" @if(str_is('moradores.veiculos-da-unidade*', Route::currentRouteName())) class='ativo' @endif >Cadastro de veículos (dos moradores)</a></li>
			<li><a href="{{ route('moradores.animais-de-estimacao.index') }}" title="Cadastro de animais de estimação" @if(str_is('moradores.animais-de-estimacao*', Route::currentRouteName())) class='ativo' @endif >Cadastro de animais de estimação</a></li>
		</ul>
	</li>
	<li class="area-areascomuns">
		<a href="#" title="áreas comuns e lazer" class="area main-nav-menu link-areascomuns @if(preg_match('~moradores\.(chamados-de-manutencao|reserva(s)?-de-espacos|instrucoes-de-uso|festas-particulares)\.*~', Route::currentRouteName())) marcado @endif">
			<p>áreas comuns e lazer</p>
		</a>
		<ul class="submenu">
			<li>
				<h3>ÁREAS COMUNS E LAZER</h3>
			</li>
			<li><a href="{{ route('moradores.chamados-de-manutencao.index') }}" title="Chamados de Manutenção" @if(str_is('moradores.chamados-de-manutencao*', Route::currentRouteName())) class='ativo' @endif >Chamados de Manutenção</a></li>
			<li><a href="{{ route('moradores.reserva-de-espacos.index') }}" title="Reserva de espaços" @if( str_is('moradores.reserva-de-espacos*', Route::currentRouteName()) || str_is('moradores.reservas-de-espacos*', Route::currentRouteName())) class='ativo' @endif >Reserva de espaços</a></li>
			<li><a href="{{ route('moradores.festas-particulares.index') }}" title="Festas Particulares" @if(str_is('moradores.festas-particulares*', Route::currentRouteName())) class='ativo' @endif >Festa particular (dentro da unidade)</a></li>
			<li><a href="{{ route('moradores.instrucoes-de-uso.index') }}" title="Instruções de uso" @if(str_is('moradores.instrucoes-de-uso*', Route::currentRouteName())) class='ativo' @endif >Instruções de uso</a></li>
		</ul>
	</li>
	<li class="area-acesso">
		<a href="#" title="controle de acesso" class="area main-nav-menu link-acesso @if(preg_match('~moradores\.(pessoas-(nao-)?autorizadas|prestadores-de-servico)\.*~', Route::currentRouteName())) marcado @endif">
			<p>controle de acesso</p>
		</a>
		<ul class="submenu">
			<li>
				<h3>CONTROLE DE ACESSO</h3>
			</li>
			<li><a href="{{ route('moradores.pessoas-autorizadas.index') }}" title="Pessoas autorizadas (permanente)" @if(str_is('moradores.pessoas-autorizadas*', Route::currentRouteName())) class='ativo' @endif >Pessoas autorizadas (permanente)</a></li>
			<li><a href="{{ route('moradores.prestadores-de-servico.index') }}" title="Prestadores de serviço" @if(str_is('moradores.prestadores-de-servico*', Route::currentRouteName())) class='ativo' @endif >Prestadores de serviço</a></li>
			<li><a href="{{ route('moradores.pessoas-nao-autorizadas.index') }}" title="Pessoas NÃO autorizadas" @if(str_is('moradores.pessoas-nao-autorizadas*', Route::currentRouteName())) class='ativo' @endif >Pessoas NÃO autorizadas</a></li>
		</ul>
	</li>
</ul>

<div class="dashboard-aside-texto">
	<p>
		NO MENU ACIMA VOCÊ ENCONTRA TODAS AS FUNCIONALIDADES DO SISTEMA AGRUPADAS POR ASSUNTO.
	</p>

	<p>
		LEMBRE-SE DE PREENCHER INFORMAÇÕES A RESPEITO DA SUA UNIDADE, VEÍCULOS, PESSOAS COM ACESSO PERMANENTE, PRESTADORES DE SERVIÇO E OUTRAS INFORMAÇÕES RELEVANTES.
	</p>

	<p>
		AS COMUNICAÇÕES OFICIAIS TAMBÉM ESTÃO SEMPRE AQUI. PARTICIPE E COLABORE!
	</p>
</div>
