@if($notificacoes['alguma'])
	<div id="dashboard-notificacoes">
		<ul class="lista-notificacoes">

			@if(sizeof($notificacoes['ocorrencias']))
				<li>
					<a href="moradores/livro-de-ocorrencias" title="{{ $notificacoes['ocorrencias']->first()->mensagem }}" class="notificacoes-chamados notificacao-tooltip">
						<img src="assets/images/layout/moradores/icone-notificacao-respostaSindico.png" alt="{{ $notificacoes['ocorrencias']->first()->mensagem }}">
					</a>
				</li>
			@endif

			@if(sizeof($notificacoes['correspondencias']))
				<li>
					<a href="#" onclick="return false;" title="Tem correspondência que precisa da sua assinatura na portaria." class="notificacoes-correspondencias notificacao-tooltip" data-qty="{{sizeof($notificacoes['correspondencias'])}}">
						<img src="assets/images/layout/moradores/icone-notificacao-correspondencia.png" alt="Tem correspondência que precisa da sua assinatura na portaria.">
					</a>
				</li>
			@endif

			@if(sizeof($notificacoes['encomendas']))
				<li>
					<a href="#" onclick="return false;" title="Tem encomenda para ser retirada na portaria." class="notificacoes-encomenda notificacao-tooltip" data-qty="{{sizeof($notificacoes['encomendas'])}}">
						<img src="assets/images/layout/moradores/icone-notificacao-encomenda.png" alt="Tem encomenda para ser retirada na portaria.">
					</a>
				</li>
			@endif

			@if((sizeof($notificacoes['agenda']['agenda_adm'])
				 + sizeof($notificacoes['agenda']['mudanca'])
				 + sizeof($notificacoes['agenda']['reservas_diarias'])
				 + sizeof($notificacoes['agenda']['reservas_periodo'])
				 + sizeof($notificacoes['agenda']['festa_particular'])) > 0)
				<li>
					<a href="#" onclick="return false;" title="{!! $notificacoes['agenda']['msg'] !!}" class="notificacoes-eventos notificacao-tooltip">
						<img src="assets/images/layout/moradores/icone-notificacao-agenda.png" alt="{!! $notificacoes['agenda']['msg'] !!}">
					</a>
				</li>
			@endif

		</ul>
	</div>
@endif
