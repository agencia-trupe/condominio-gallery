@if(sizeof($animais))
	<ul id="lista-registros">
		@foreach($animais as $animal)
			<li>
				<div class="animais-de-estimacao-foto">
					<img src="assets/images/moradores/animais-de-estimacao/thumbs/{{ $animal->foto }}" alt="Animal de estimação">
				</div>
				<div class="animais-de-estimacao-texto">
					<div class="animais-de-estimacao-espacamento">
						<h2>nome: {{ $animal->nome }}</h2>
						<div class="especie">{{ $animal->especie }}</div>
						<div class="sexo">{{ $animal->sexo }}</div>
						<div class="acoes">
							<a href="{{ route('moradores.animais-de-estimacao.editar', $animal->id) }}" class="btn btn-info" title="Editar">EDITAR</a>
							<a href="{{ route('moradores.animais-de-estimacao.remover', $animal->id) }}" class="btn btn-danger" title="Remover">X</a>
						</div>
					</div>
				</div>
			</li>
		@endforeach
	</ul>
@endif