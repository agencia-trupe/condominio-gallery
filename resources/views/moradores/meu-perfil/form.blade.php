@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="meu-perfil-main">

	 		<h1>MEU PERFIL</h1>

	 		@include('moradores.meu-perfil.partials.mensagens')

			<form action="{{ route('moradores.meu-perfil.atualizar') }}" method="post">
				<fieldset>

					{!! csrf_field() !!}

					<p class="texto-unidade">
						UNIDADE: <span>{{ $morador->getUnidadeAbrev() }}</span>
					</p>

					<label class="
							register-form-input
							@if($errors->has('moradores.nome')) input-erro obrigatorio @endif
						" data-label="nome completo do morador:">
						<input type="text" name="moradores[nome]" placeholder="nome completo do morador" id="register-form-nome" value="{{ old('moradores.nome', $morador->nome ?: '' ) }}">
					</label>

					<label class="
							register-form-input
							@if($errors->has('moradores.apelido')) input-erro obrigatorio @endif
						" data-label="como prefere ser chamado :">
						<input type="text" name="moradores[apelido]" placeholder="como prefere ser chamado" id="register-form-apelido" value="{{ old('moradores.apelido', $morador->apelido ?: '' ) }}">
					</label>

					<label class="
							register-form-input
							@if($errors->has('moradores.relacao_unidade')) input-erro obrigatorio @endif
						" data-label="tipo de morador :">
						<select name="moradores[relacao_unidade]" id="register-form-relacao_unidade" required>
							<option value="" disabled selected>tipo de morador</option>
							<option value="locatario"    @if(old('moradores.relacao_unidade', $morador->relacao_unidade) == 'locatario')    selected @endif >Locatário</option>
							<option value="proprietario" @if(old('moradores.relacao_unidade', $morador->relacao_unidade) == 'proprietario') selected @endif >Proprietário</option>
						</select>
					</label>

					<label class="
							register-form-input
							@if($errors->has('moradores.data_nascimento')) input-erro obrigatorio @endif
						" data-label="data de nascimento :">
						<input type="text" name="moradores[data_nascimento]" placeholder="data de nascimento" id="register-form-data_nascimento"  value="{{ old('moradores.data_nascimento', !is_null($morador->data_nascimento) ? $morador->data_nascimento->format('d/m/Y') : '' ) }}">
					</label>

					<label class="
							register-form-input
							@if($errors->has('moradores.data_mudanca')) input-erro obrigatorio @endif
						" data-label="data de mudança :">
						<input type="text" name="moradores[data_mudanca]" placeholder="data de mudança" id="register-form-data_mudanca"  value="{{ old('moradores.data_mudanca', !is_null($morador->data_mudanca) ? $morador->data_mudanca->format('d/m/Y') : '' ) }}">
					</label>

					<label class="
							register-form-input
							@if($errors->has('moradores.email')) input-erro @endif
							@if(str_is('*obrigatório*' ,$errors->first('moradores.email'))) obrigatorio @endif
							@if(str_is('*e-mail válido*' ,$errors->first('moradores.email'))) email-invalido @endif
							@if(str_is('*está em uso*' ,$errors->first('moradores.email'))) email-em-uso @endif
						" data-label="e-mail :">
						<input type="email" name="moradores[email]" placeholder="e-mail" id="register-form-email" value="{{ old('moradores.email', $morador->email ?: '' ) }}">
						<div class="
							register-input-addon
							@if($errors->has('moradores.visibilidade_email')) input-erro obrigatorio @endif
							">

							<a href="#" class="register-form-set_visibilidade addon-tooltip-visib visib-email off" title="Definir Visibilidade do e-mail"></a>
							<input type="hidden" name="moradores[visibilidade_email]" id="register-form-visibilidade_email" value="{{ old('moradores.visibilidade_email', $morador->getVisibilidade('email') ?: '' ) }}">

						</div>
					</label>

					<label class="
							register-form-input
							@if($errors->has('moradores.telefone_fixo')) input-erro obrigatorio @endif
						" data-label="telefone fixo :">
						<input type="text" name="moradores[telefone_fixo]" placeholder="telefone fixo" id="register-form-telefone_fixo" value="{{ old('moradores.telefone_fixo', $morador->telefone_fixo ?: '' ) }}">
						<div class="
							register-input-addon
							@if($errors->has('moradores.visibilidade_telefone_fixo')) input-erro obrigatorio @endif
							">

							<a href="#" class="register-form-set_visibilidade addon-tooltip-visib visib-tel_fixo off" title="Definir Visibilidade do número de telefone fixo"></a>
							<input type="hidden" name="moradores[visibilidade_telefone_fixo]" id="register-form-visibilidade_tel-fixo" value="{{ old('moradores.visibilidade_telefone_fixo', $morador->getVisibilidade('telefone_fixo') ?: '' ) }}">

						</div>
					</label>

					<label class="
							register-form-input
							@if($errors->has('moradores.telefone_celular')) input-erro obrigatorio @endif
						" data-label="telefone celular :">
						<input type="text" name="moradores[telefone_celular]" placeholder="telefone celular" id="register-form-telefone_celular" value="{{ old('moradores.telefone_celular', $morador->telefone_celular ?: '' ) }}">
						<div class="
							register-input-addon
							@if($errors->has('moradores.visibilidade_telefone_celular')) input-erro obrigatorio @endif
							">

							<a href="#" class="register-form-set_visibilidade addon-tooltip-visib visib-tel_cel off" title="Definir Visibilidade do número de telefone celular"></a>
							<input type="hidden" name="moradores[visibilidade_telefone_celular]" id="register-form-visibilidade_tel-cel" value="{{ old('moradores.visibilidade_telefone_celular', $morador->getVisibilidade('telefone_celular') ?: '' ) }}">

						</div>
					</label>

					<label class="
							register-form-input
							@if($errors->has('moradores.telefone_comercial')) input-erro obrigatorio @endif
						" data-label="telefone comercial :">
						<input type="text" name="moradores[telefone_comercial]" placeholder="telefone comercial" id="register-form-telefone_comercial" value="{{ old('moradores.telefone_comercial', $morador->telefone_comercial ?: '' ) }}">
						<div class="
							register-input-addon
							@if($errors->has('moradores.visibilidade_telefone_comercial')) input-erro obrigatorio @endif
							">

							<a href="#" class="register-form-set_visibilidade addon-tooltip-visib visib-tel_com off" title="Definir Visibilidade do número de telefone comercial"></a>
							<input type="hidden" name="moradores[visibilidade_telefone_comercial]" id="register-form-visibilidade_tel-com" value="{{ old('moradores.visibilidade_telefone_comercial', $morador->getVisibilidade('telefone_comercial') ?: '' ) }}">

						</div>
					</label>
				</fieldset>

				<div class="input-foto">

					<div class="register-foto-placeholder">
						<label title="Adicionar Foto" class="
								@if($errors->has('moradores.foto')) input-erro @endif
								@if($morador->foto) com-after @endif
							">
							<p data-html-original="ADICIONAR FOTO">
								@if($errors->has('moradores.foto'))
									{{ $errors->first('moradores.foto') }}
								@else
									ADICIONAR FOTO
								@endif
							</p>
							<div id="upload-progresso"><div class="barra"></div></div>
							<input type="hidden" name="moradores[foto]" id="register-form-foto" value="{{ old('moradores.foto', $morador->foto ?: '' ) }}">
							<input type="file"   name="foto" id="register-form-fileupload">
						</label>
					</div>

					<div class="register-input-addon">
						<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip-foto" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?">
					</div>
				</div>

				<input type="submit" value="ATUALIZAR">
			</form>

	 	</div>

	</section>

@stop