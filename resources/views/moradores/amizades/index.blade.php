@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')

    <div class="main" id="amizades-main">

  	 	<h1>AMIGOS MORADORES</h1>

			<div class="amizades-subnav">
        <a href="{{ route('moradores.amizades.index') }}" class="btn @if(str_is('moradores.amizades.index', Route::currentRouteName())) btn-success @else btn-info @endif" title="+ Vizinhos">+ VIZINHOS</a>
        <a href="{{ route('moradores.amizades.autorizados') }}" class="btn @if(str_is('moradores.amizades.autorizados', Route::currentRouteName())) btn-success @else btn-info @endif" title="Autorizados a ver minhas informações">Autorizados a ver minhas informações</a>
      </div>

      <div class="input-busca">
        <label for="busca-amigos">Encontre amigos!</label>
        <input type="text" id="input-busca-amigos" placeholder="BUSCAR">
      </div>

      <p>
        <strong>Observação:</strong> as pessoas exibidas aqui são as que autorizaram
        exibir seus dados - e escolheram quais dados exibir - a outros moradores,
        assim como você também pode fazer ao cadastrar seus dados pessoais (MEU PERFIL).
      </p>

      <div id="resultado-busca-amigos">

        <div id="carregando-resultados">
        	<div class="spinner">
					  <div class="rect1"></div>
					  <div class="rect2"></div>
					  <div class="rect3"></div>
					  <div class="rect4"></div>
					  <div class="rect5"></div>
					</div>
        </div>

        <ul id="lista-amizades"></ul>

      </div>

	  </div>

  </section>

@stop
