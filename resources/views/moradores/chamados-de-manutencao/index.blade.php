@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="chamados-de-manutencao-main">

			<h1>CHAMADOS DE MANUTENÇÃO</h1>

			<p>Para informar o Condomínio sobre assuntos que pedem a ação de funcionários ou do zelador, registre uma ocorrência.</p>

			@include('moradores.chamados-de-manutencao.partials.mensagens')

			@include('moradores.chamados-de-manutencao.partials.form')

			@include('moradores.chamados-de-manutencao.partials.historico')

		</div>

	</section>

@stop