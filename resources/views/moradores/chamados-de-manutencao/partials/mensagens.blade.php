@if(Session::has('chamados_de_manutencao_create') && session('chamados_de_manutencao_create') == true)
	<p class="retorno_formulario sucesso">
		Seu Chamado de Manutenção foi <strong>registrado</strong> com sucesso!
	</p>
@endif

@if(Session::has('chamados_de_manutencao_create') && session('chamados_de_manutencao_create') == false)
	<p class="retorno_formulario erro">
		<strong>Erro</strong> ao registrar Chamado de Manutenção.
	</p>
@endif