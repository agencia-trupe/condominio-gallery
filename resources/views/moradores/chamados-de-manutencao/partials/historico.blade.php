<ul class="lista-chamados">
	@foreach($historico as $chamado)
		<li>
			<div class="coluna-esquerda">
				<div class="chamado-timestamp">
					{{ $chamado->created_at->format('d/m/Y H:i \h') }}
				</div>
				<small>
					{{ $chamado->autor->getNomeCompleto() }}
					<br>
					{{ $chamado->autor->getUnidade() }}
				</small>
			</div>
			<div class="coluna-direita">
				<div class="chamado-texto">
					<div class="chamado-original">
						<p>
							{!! nl2br($chamado->descricao) !!}
						</p>
						<p>
							<strong>Data da Ocorrência:</strong> {{ $chamado->data->format('d/m/Y') }}<br>
							<strong>Horário:</strong> {{ $chamado->horario }}<br>
							<strong>Localização:</strong> {{ $chamado->localizacao }}<br>
						</p>

						@if(sizeof($chamado->fotos))

							<hr>

							<div class="lista-imagens" data-featherlight-gallery data-featherlight-filter="a">
								@foreach($chamado->fotos as $foto)
									<a href="assets/images/moradores/chamados-de-manutencao/redimensionadas/{{ $foto->foto }}" title="Ampliar">
										<img src="assets/images/moradores/chamados-de-manutencao/thumbs/{{ $foto->foto }}">
									</a>
								@endforeach
							</div>
						@endif

						<hr>

						@if($chamado->getVistoPelaAdm())
							<p>
								<em>Seu Chamado foi lido em : {{ $chamado->visto_pela_adm_em->format('d/m/Y H:i:s') }}</em>
							</p>
						@else
							<p>
								<em>Seu Chamado ainda não foi lido</em>
							</p>
						@endif

					</div>
				</div>
			</div>
		</li>
	@endforeach
</ul>