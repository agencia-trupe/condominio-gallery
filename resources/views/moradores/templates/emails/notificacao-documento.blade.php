<!DOCTYPE html>
<html lang="en-US">
    <head>
      <meta charset="utf-8">
    </head>
    <body>
      <h2>Novo Documento</h2>

      <p>
        Um novo <strong>Documento</strong> foi publicado no sistema Gallery Online.<br>
        Acesse o sistema para acompanhar todos os assuntos publicados: <a href="http://www.galleryonline.com.br/moradores/documentos-oficiais" target="_blank" title="Clique aqui para acessar o sistema">www.galleryonline.com.br</a>
      </p>

      <p>
        Gallery Online – In Jardim Sul Gallery<br>
        <a href="http://www.galleryonline.com.br" title="Sistema Gallery Online">www.galleryonline.com.br</a>
      </p>

    </body>
</html>
