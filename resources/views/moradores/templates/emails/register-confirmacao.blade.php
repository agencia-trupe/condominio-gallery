<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>

    <h2>Ative seu cadastro</h2>

    <p>
  		Obrigado por ser cadastrar no sistema Gallery Online.
  	</p>

  	<p>
      Para ativar seu cadastro e confirmar o e-mail de utilização do sistema, por favor clique no link abaixo: <br>
      <a href="{{ route('moradores.auth.ativar', ['email' => $morador->email, 'activation_code' => $morador->activation_code]) }}">ATIVAR CADASTRO</a>
  	</p>

    <p>
      Gallery Online – In Jardim Sul Gallery<br>
      <a href="http://www.galleryonline.com.br" title="Sistema Gallery Online">www.galleryonline.com.br</a>
    </p>

  </body>
</html>
