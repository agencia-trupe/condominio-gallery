<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Redefinicão de Senha - Gallery Online</h2>

    <p>
      Você solicitou a redefinição da sua senha em nosso sistema.
    </p>

    <p>
      Utilize o link abaixo para redefinir a sua senha de acesso. <br>
      <a href="{{ url('moradores/auth/redefinir/'.$type.'/'.$token) }}">Redefinir Senha - Gallery Online</a>
    </p>

    <p>
      Gallery Online – In Jardim Sul Gallery<br>
      <a href="http://www.galleryonline.com.br" title="Sistema Gallery Online">www.galleryonline.com.br</a>
    </p>

  </body>
</html>
