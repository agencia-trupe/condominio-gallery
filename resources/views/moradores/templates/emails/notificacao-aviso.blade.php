<!DOCTYPE html>
<html lang="en-US">
    <head>
      <meta charset="utf-8">
    </head>
    <body>
      <h2>Novo Aviso</h2>

      <p>
        Um novo <strong>Aviso</strong> foi publicado no sistema Gallery Online.
      </p>

      <p>
        Acesse o sistema para acompanhar e dar sua ciência à mensagem: <a href="http://www.galleryonline.com.br/moradores/avisos" target="_blank" title="Clique aqui para acessar o sistema">www.galleryonline.com.br</a>
      </p>

      <p>
        Gallery Online – In Jardim Sul Gallery<br>
        <a href="http://www.galleryonline.com.br" title="Sistema Gallery Online">www.galleryonline.com.br</a>
      </p>

    </body>
</html>
