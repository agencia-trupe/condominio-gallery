<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Nova Mensagem de Morador - Sistema Gallery</h2>

    <h3>{{$mensagem->assunto}}</h3>
    <p>{!! nl2br($mensagem->mensagem) !!}</p>

    <p>
      Mensagem enviada por: <strong>{{ $mensagem->getNomeCompletoAutor() }} - {{ $mensagem->getUnidadeAutor() }}</strong>
      <br>
      <small>{{ $mensagem->created_at->format('d/m/Y H:i \h') }}</small>
    </p>

    <p>
      Gallery Online – In Jardim Sul Gallery<br>
      <a href="http://www.galleryonline.com.br" title="Sistema Gallery Online">www.galleryonline.com.br</a>
    </p>

  </body>
</html>
