@extends('moradores.templates.auth')

@section('conteudo')

	<section id="login-section" class="centro">

		<div class="login-banner">
			<div class="login-branding">
				<div class="login-marca">
					<img src="assets/images/layout/marca-gallery.png" alt="Sistema de Comunicação Condomínio In Jardim Sul Gallery">
				</div>
				<p>Sistema de Comunicação Condomínio In Jardim Sul Gallery</p>
			</div>
		</div>

		<div class="login-inferior">

			<div class="login-mostra-areas">
				<div class="area area-admin">
					<p>administração e comunicação</p>
				</div>
				<div class="area area-unidade">
					<p>minha unidade e garagem</p>
				</div>
				<div class="area area-areascomuns">
					<p>áreas comuns e lazer</p>
				</div>
				<div class="area area-acesso">
					<p>controle de acesso</p>
				</div>
			</div>

			<div class="login-form">

				<div class="pagina-nao-encontrada">
					<h1>Página não encontrada</h1>
				</div>

				<div class="login-disclaimer">
					<a href="{{ route('moradores.dashboard') }}" title="Voltar" class="login-register-link">VOLTAR</a>
					<p>
						Este é um sistema para comunicação interna do Condomínio In Jardim Sul Gallery.<br>
						Para utilizar o sistema é preciso ser morador do condomínio e estar previamente cadastrado.<br>
						Os administradores se reservam o direito de não aceitar cadastros de acordo com seus critérios.
					</p>
				</div>
			</div>

		</div>

	</section>

@stop