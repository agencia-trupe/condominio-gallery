@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="pessoas-autorizadas-main">

			<h1>PESSOAS AUTORIZADAS (permanente)</h1>

			<p>Registre as pessoas que podem acessar sua Unidade a qualquer momento sem restrições (pais, enteados, amigos íntimos, etc).</p>

			@include('moradores.pessoas-autorizadas.partials.mensagens')

			@include('moradores.pessoas-autorizadas.partials.form', $pessoa_autorizada)

			@include('moradores.pessoas-autorizadas.partials.lista-autorizados')

		</div>

	</section>

@stop