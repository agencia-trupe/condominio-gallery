@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="agendamento-de-mudanca-main">

			<h1>AGENDAMENTO DE MUDANÇA</h1>

			<p>Selecione a data e período disponível. Só é permitida uma mudança por torre por período (manhã ou tarde).</p>

			@include('moradores.agendamento-de-mudanca.partials.mensagens')

			<form action="{{ route('moradores.agendamento-de-mudanca.agendar') }}" method="post">

				{!! csrf_field() !!}

				<label class="
					agendamento-input
					@if($errors->has('agendamento.data')) input-erro @endif
					@if(str_is('*obrigatório*' ,$errors->first('agendamento.data'))) obrigatorio @endif
					@if(str_is('*reservada*' ,$errors->first('agendamento.data'))) data-invalida @endif
					@if(str_is('*passou*' ,$errors->first('agendamento.data'))) data-passou @endif
				">
					<input type="text" id="agendamento-data" name="agendamento[data]" placeholder="data" value="{{ old('agendamento.data') }}">
				</label>

				<label class="
					agendamento-input
					@if($errors->has('agendamento.periodo')) input-erro obrigatorio @endif
				">
					<select name="agendamento[periodo]" id="agendamento-periodo">
						<option value="">período</option>
						<option value="manha" @if(old('agendamento.periodo') == 'manha') selected @endif >manhã</option>
						<option value="tarde" @if(old('agendamento.periodo') == 'tarde') selected @endif >tarde</option>
					</select>
				</label>

				<input type="submit" class="btn btn-info" value="FAZER AGENDAMENTO">

			</form>

			<div id="link-download-regulamento">
				<a href="assets/documentos/{{$info->regulamento}}" target="_blank" title="Confira o regulamento interno sobre as regras para mudanças e reformas">&raquo; Confira o regulamento interno sobre as regras para mudanças e reformas</a>
			</div>

			<div class="mostra-datas">
				<h2>CONFERIR DATAS NÃO DISPONÍVEIS:</h2>

				<div class="bloco">Mostrando datas para o Bloco {{ Auth::moradores()->get()->unidade->bloco }}</div>

				<table id='lista-agendamentos'>
					<thead>
						<tr>
							<th></th>
							<th>manhã: {{ $info->horario_manha }}</th>
							<th>tarde: {{ $info->horario_tarde }}</th>
						</tr>
					</thead>
					<tbody>
						@forelse($agendamentos['datas_com_agendamento'] as $data)
							<tr>
								<td>{{ $data->data->format('d/m/Y') }}</td>

									@if(is_array($agendamentos['agendamentos_do_usuario_periodo_manha']) && in_array($data->data, $agendamentos['agendamentos_do_usuario_periodo_manha']))

										<td class='meu-agendamento'>
											<span>minha mudança</span>

											<a href="{{ route('moradores.agendamento-de-mudanca.cancelar', [$data->data->format('Y-m-d'), 'manha']) }}" title="Cancelar Agendamento" class="btn btn-danger btn-mini">X</a>
										</td>

									@elseif(is_array($agendamentos['agendamentos_periodo_manha']) && in_array($data->data, $agendamentos['agendamentos_periodo_manha']))

										<td>
											<span>agendado</span>
										</td>

									@else
										<td></td>
									@endif

									@if(is_array($agendamentos['agendamentos_do_usuario_periodo_tarde']) && in_array($data->data, $agendamentos['agendamentos_do_usuario_periodo_tarde']))

										<td class='meu-agendamento'>
											<span>minha mudança</span>

											<a href="{{ route('moradores.agendamento-de-mudanca.cancelar', [$data->data->format('Y-m-d'), 'tarde']) }}" title="Cancelar Agendamento" class="btn btn-danger btn-mini">X</a>
										</td>

									@elseif(is_array($agendamentos['agendamentos_periodo_tarde']) && in_array($data->data, $agendamentos['agendamentos_periodo_tarde']))

										<td>
											<span>agendado</span>
										</td>

									@else
										<td></td>
									@endif
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="3">Nenhuma data agendada</td>
							</tr>
						@endforelse

					</tbody>
				</table>

			</div>

		</div>

	</section>

@stop
