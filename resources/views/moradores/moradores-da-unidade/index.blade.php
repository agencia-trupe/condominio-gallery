@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="moradores-da-unidade-main">

			<h1>CADASTRO DE MORADORES DA UNIDADE</h1>

			<p>O Cadastro de Moradores serve para identificar as pessoas de cada unidade com maior facilidade. É importante cadastrar crianças e adolescentes para que, em caso de acidente ou necessidade, a portaria e funcionários do prédio consigam identificar facilmente a quem recorrer.</p>

			<p>
				Cadastre apenas os moradores que não terão seu próprio login no sistema. Se os cônjuges terão cada um seu login automaticamente aparecerão como moradores da mesma unidade, cadastre então apenas os filhos e outros moradores sem login. Use sempre o nome completo no cadastro para evitar duplicidade.
			</p>
			
			@include('moradores.moradores-da-unidade.partials.mensagens')

			@include('moradores.moradores-da-unidade.partials.form')

			@include('moradores.moradores-da-unidade.partials.lista-moradores')

		</div>

	</section>

@stop
