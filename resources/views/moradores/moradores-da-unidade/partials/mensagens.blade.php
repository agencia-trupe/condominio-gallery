@if(Session::has('morador_da_unidade_create') && session('morador_da_unidade_create') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Morador da Unidade registrado com sucesso.</strong>
	</p>
@endif

@if(Session::has('morador_da_unidade_create') && session('morador_da_unidade_create') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Cadastrar Morador da Unidade.</strong>
	</p>
@endif

@if(Session::has('morador_da_unidade_destroy') && session('morador_da_unidade_destroy') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Morador da Unidade removido com sucesso.</strong>
	</p>
@endif

@if(Session::has('morador_da_unidade_destroy') && session('morador_da_unidade_destroy') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Remover Morador da Unidade.</strong>
	</p>
@endif

@if(Session::has('morador_da_unidade_update') && session('morador_da_unidade_update') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Morador da Unidade atualizado com sucesso.</strong>
	</p>
@endif

@if(Session::has('morador_da_unidade_update') && session('morador_da_unidade_update') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Atualizar Morador da Unidade.</strong>
	</p>
@endif
