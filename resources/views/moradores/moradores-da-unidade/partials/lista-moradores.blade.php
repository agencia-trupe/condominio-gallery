@if(sizeof($moradores))
	<ul id="lista-registros">
		@foreach($moradores as $tipo_de_cadastro => $cadastros)
			@if(sizeof($cadastros))
				@foreach($cadastros as $morador)

					@if($tipo_de_cadastro == 'da_unidade' && !in_array($morador->nome, $lista_nomes_cadastros))

						<li>
							<div class="moradores-da-unidade-foto">
								@if($morador->foto)
									<img src="assets/images/moradores/moradores-da-unidade/thumbs/{{ $morador->foto }}" alt="Morador da unidade">
								@else
									<img src="moradores/assets/images/avatar-default/122/122/sem foto" alt="Morador da unidade">
								@endif
							</div>
							<div class="moradores-da-unidade-texto">
								<div class="moradores-da-unidade-espacamento">

									<h2>{{ $morador->nome }}</h2>

									<div class="data">{{ is_null( $morador->data_nascimento) ? 'data de nasc. não informada' : $morador->data_nascimento->format('d/m/Y') }}</div>

									<div class="parentesco">{{ $morador->parentesco }}</div>

									<div class="acoes">
										<a href="{{ route('moradores.moradores-da-unidade.editar', $morador->id) }}" class="btn btn-info" title="Editar">EDITAR</a>
										<a href="{{ route('moradores.moradores-da-unidade.remover', $morador->id) }}" class="btn btn-danger" title="Remover">X</a>
									</div>

								</div>
							</div>
						</li>

					@elseif($tipo_de_cadastro == 'com_perfil')

						<li>
							<div class="moradores-da-unidade-foto">
								@if($morador->foto)
									<img src="assets/images/moradores/fotos/thumbs/{{ $morador->foto }}" alt="Morador da unidade">
								@else
									<img src="moradores/assets/images/avatar-default/122/122/sem foto" alt="Morador da unidade">
								@endif
							</div>
							<div class="moradores-da-unidade-texto">
								<div class="moradores-da-unidade-espacamento">

									<h2>{{ $morador->nome }}</h2>

									<div class="data">{{ is_null( $morador->data_nascimento) ? 'data de nasc. não informada' : $morador->data_nascimento->format('d/m/Y') }}</div>

									<div class="parentesco">Perfil Cadastrado no Sistema</div>

								</div>
							</div>
						</li>

					@endif
				@endforeach
			@endif
		@endforeach
	</ul>
@endif
