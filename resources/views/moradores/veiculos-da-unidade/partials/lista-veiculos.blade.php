@if(sizeof($veiculos))
	<ul id="lista-registros">
		@foreach($veiculos as $veiculo)
			<li>
				<div class="veiculos-da-unidade-texto">
					<div class="texto">
						{!! $veiculo->toText !!}
					</div>
					<div class="acoes">
						<a href="{{ route('moradores.veiculos-da-unidade.remover', $veiculo->id) }}" class="btn btn-danger" title="Remover">X</a>
					</div>
				</div>
			</li>
		@endforeach
	</ul>
@endif
