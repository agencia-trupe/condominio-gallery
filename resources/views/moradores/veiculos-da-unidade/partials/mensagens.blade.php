@if(Session::has('veiculo_da_unidade_create') && session('veiculo_da_unidade_create') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Veículo da Unidade registrado com sucesso.</strong>
	</p>
@endif

@if(Session::has('veiculo_da_unidade_create') && session('veiculo_da_unidade_create') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Cadastrar Veículo da Unidade.</strong>
	</p>
@endif

@if(Session::has('veiculo_da_unidade_destroy') && session('veiculo_da_unidade_destroy') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Veículo da Unidade removido com sucesso.</strong>
	</p>
@endif

@if(Session::has('veiculo_da_unidade_destroy') && session('veiculo_da_unidade_destroy') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Remover Veículo da Unidade.</strong>
	</p>
@endif