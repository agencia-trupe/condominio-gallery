  <div id="resultados-busca">

    <h3>RESULTADOS DA BUSCA</h3>

    @if(count($resultadoBusca) > 0)

      <ul class="lista-fornecedores" id="lista-resultados-busca">
        @foreach($resultadoBusca as $indicacao)
          <li>
            <div class="imagem">
              @if(count($indicacao->imagens))
                <img src="assets/images/moradores/fornecedores/thumbs/{{$indicacao->imagens()->first()->imagem}}"/>
              @endif
            </div>
            <div class="fornecedor">
              <div class="nome">{{$indicacao->nome}}</div>
              <div class="categoria">{{ count($indicacao->categoria) > 0 ? $indicacao->categoria->titulo : $indicacao->outra_categoria}}</div>
              <div class="avaliacoes">
                Média das avaliações [{{count($indicacao->avaliacoes)}}]:
                <span class="rating">
                  @for($r = 1; $r <= 5; $r++)
                    <span class="estrela @if($r <= $indicacao->media_das_avaliacoes) marcada @endif ">★</span>
                  @endfor
                </span>
              </div>
              <div class="acoes">
                <a href="{{ route('moradores.fornecedores.detalhes', $indicacao->id) }}" class="btn btn-info">VER DETALHES</a>
                @if(Auth::moradores()->get()->id == $indicacao->indicado_por)
                  <a href="{{ route('moradores.fornecedores.editar', $indicacao->id) }}" class="btn btn-info">EDITAR</a>
                  <a href="{{ route('moradores.fornecedores.remover', $indicacao->id) }}" id="btn-remover-fornecedor" class="btn btn-danger">X</a>
                @endif
              </div>
            </div>
          </li>
        @endforeach
      </ul>

    @else

      <h4>Nenhum resultado encontrado.</h4>

    @endif

  </div>
