<div id="detalhes-fornecedores">

  <h3>INFORMAÇÕES DO FORNECEDOR</h3>

  <div class="fornecedor">
    <div class="dados">
      <div class="nome">{{ $fornecedor->nome }}</div>
      <div class="categoria">{{ count($fornecedor->categoria) > 0 ? $fornecedor->categoria->titulo : $fornecedor->outra_categoria}}</div>
      <div class="contatos">
        @if($fornecedor->telefone)
          {{$fornecedor->telefone}} <br>
        @endif

        @if($fornecedor->email)
          {!! parse_emails($fornecedor->email) !!} <br>
        @endif

        @if($fornecedor->site)
          <a href="{{ prep_url($fornecedor->site) }}" target="_blank">{{$fornecedor->site}}</a>
        @endif
      </div>
    </div>
    @if($fornecedor->descritivo)
      <div class="descritivo">
        <p>{{$fornecedor->descritivo}}</p>
      </div>
    @endif
  </div>

  @if(count($fornecedor->imagens) > 0)
    <div class="fornecedor-galeria" data-featherlight-gallery data-featherlight-filter="a">
      @foreach($fornecedor->imagens as $img)
        <a href="assets/images/moradores/fornecedores/redimensionadas/{{$img->imagem}}">
          <img src="assets/images/moradores/fornecedores/thumbs/{{$img->imagem}}">
        </a>
      @endforeach
    </div>
  @endif

</div>
