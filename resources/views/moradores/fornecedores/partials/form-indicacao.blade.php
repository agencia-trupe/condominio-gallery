<div id="controle-fornecedores">

	<a href="#" title="Indicar um Fornecedor" id="form-toggle" class="btn btn-success btn-com-icone @if(count($errors) == 0) iniciar-fechado @endif " data-toggle-target="fornecedores-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> INDICAR UM FORNECEDOR</a>

	<form action="{{ route('moradores.fornecedores.indicar') }}" id="fornecedores-form" method="post">

		<fieldset>

			{!! csrf_field() !!}

			<label class="
				fornecedores-form-input
				@if($errors->has('fornecedores.categoria')) input-erro obrigatorio @endif
			">
				<select name="fornecedores[fornecedores_categorias_id]" id="fornecedores-form-categoria">
					<option value="">Selecione o tipo de Fornecedor</option>
					@foreach($listaCategorias as $categoria)
						<option value="{{$categoria->id}}" @if(old('fornecedores.fornecedores_categorias_id') == $categoria->id) selected @endif >{{$categoria->titulo}}</option>
					@endforeach
					<option value="outros" @if(old('fornecedores.fornecedores_categorias_id') == 'outros') selected @endif >Outros</option>
				</select>
			</label>

			<label class="
				fornecedores-form-input
				@if($errors->has('fornecedores.outra_categoria')) input-erro obrigatorio @endif
			"
			id="fornecedores-form-label-outra_categoria">
				<input type="text" id="fornecedores-form-outra_categoria" name="fornecedores[outra_categoria]" placeholder="Especifique" value="{{ old('fornecedores.outra_categoria') }}">
			</label>

			<label class="
				fornecedores-form-input
				@if($errors->has('fornecedores.nome')) input-erro obrigatorio @endif
			">
				<input type="text" id="fornecedores-form-nome" name="fornecedores[nome]" placeholder="Nome" value="{{ old('fornecedores.nome') }}">
			</label>

	    <label class="
				fornecedores-form-input
				@if($errors->has('fornecedores.telefone')) input-erro obrigatorio @endif
			">
				<input type="text" id="fornecedores-form-telefone" name="fornecedores[telefone]" placeholder="Telefone" value="{{ old('fornecedores.telefone') }}">
			</label>

	    <label class="
				fornecedores-form-input
				@if($errors->has('fornecedores.email')) input-erro obrigatorio @endif
			">
				<input type="text" id="fornecedores-form-email" name="fornecedores[email]" placeholder="E-mail" value="{{ old('fornecedores.email') }}">
			</label>

	    <label class="
				fornecedores-form-input
				@if($errors->has('fornecedores.site')) input-erro obrigatorio @endif
			">
				<input type="text" id="fornecedores-form-site" name="fornecedores[site]" placeholder="Site" value="{{ old('fornecedores.site') }}">
			</label>

			<label class="
				fornecedores-form-input
				contem-textarea
				@if($errors->has('fornecedores.descritivo')) input-erro obrigatorio @endif
			">
				<textarea name="fornecedores[descritivo]" id="fornecedores-form-descritivo" placeholder="Descritivo">{{ old('fornecedores.descritivo') }}</textarea>
			</label>

			<div id="fornecedores-form-avaliacao">
				<p>
					Sua avaliação:
				</p>
				<div class="
					rating
					@if($errors->has('avaliacao.nota')) obrigatorio @endif
				">
					<input type="radio" id="star5" name="avaliacao[nota]" value="5"  @if(old('avaliacao.nota') == '5') checked @endif /><label for="star5" title="5 estrelas">5 estrelas</label>
			    <input type="radio" id="star4" name="avaliacao[nota]" value="4"  @if(old('avaliacao.nota') == '4') checked @endif /><label for="star4" title="4 estrelas">4 estrelas</label>
			    <input type="radio" id="star3" name="avaliacao[nota]" value="3"  @if(old('avaliacao.nota') == '3') checked @endif /><label for="star3" title="3 estrelas">3 estrelas</label>
			    <input type="radio" id="star2" name="avaliacao[nota]" value="2"  @if(old('avaliacao.nota') == '2') checked @endif /><label for="star2" title="2 estrelas">2 estrelas</label>
			    <input type="radio" id="star1" name="avaliacao[nota]" value="1"  @if(old('avaliacao.nota') == '1') checked @endif /><label for="star1" title="1 estrela">1 estrela</label>
				</div>
				<label class="
					fornecedores-form-input
					@if($errors->has('avaliacao.texto')) input-erro obrigatorio @endif
				">
					<textarea name="avaliacao[texto]" id="fornecedores-form-avaliacao_texto" placeholder="Avaliação do Fornecedor">{{ old('avaliacao.texto') }}</textarea>
				</label>
			</div>

		</fieldset>

		<div class="input-foto">

			<div class="fornecedores-foto-placeholder">
				<label title="Enviar Fotos">
					<p data-html-original="ENVIAR FOTOS (se houver)">
						ENVIAR FOTOS (se houver)
					</p>
					<div id="upload-progresso"><div class="barra"></div></div>

					<input type="file" multiple name="foto" id="fornecedores-form-fileupload">
				</label>
			</div>

			<ul class="lista-imagens">
				@if(old('fornecedores.fotos'))
					@foreach(old('fornecedores.fotos') as $foto)
						<li>
							<a href='#'
								data-featherlight-gallery
								data-featherlight='assets/images/moradores/fornecedores/redimensionadas/{{ $foto }}'
								data-featherlight-type='image'
								title='Ampliar'
								class='btn-ampliar-imagem'
							>
								<img src="assets/images/moradores/fornecedores/thumbs/{{ $foto }}">
							</a>
							<a href='#' title='Remover Imagem' class='btn btn-danger btn-mini btn-remover-imagem'>x</a>
							<input type='hidden' name='fornecedores[fotos][]' class='form-foto-hidden' value='{{ $foto }}'>
						</li>
					@endforeach
				@endif
			</ul>
		</div>

		<input type="submit" value="REGISTRAR INDICAÇÃO" class="btn btn-info">

	</form>

</div>
