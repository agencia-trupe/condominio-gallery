@if(Session::has('voto_na_enquete_computado') && session('voto_na_enquete_computado') == true)
  <p class="retorno_formulario sucesso">
    Seu voto foi armazenado com sucesso!
  </p>
@endif

@if(Session::has('voto_na_enquete_computado') && session('voto_na_enquete_computado') == false)
  <p class="retorno_formulario erro">
    <strong>Você já votou nessa enquete!</strong><br>Acompanhe os resultados parciais:
  </p>
@endif