@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		      	<h2>
		        	Alterar Instrução de Uso
		        </h2>

		        <hr>

		    </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    		    @include('admin.templates.partials.mensagens')
    		</div>
    	</div>

		<form action="{{ route('admin.instrucoes-de-uso.update', $registro->id) }}" method="post">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">

			    	<div class="form-group">
						<label for="inputQuestao">Título</label>
						<textarea name="questao" class="form-control textarea-simples" id="inputQuestao" required>{{ $registro->questao }}</textarea>
					</div>

					<div class="form-group">
						<label for="inputResposta">Texto</label>
						<textarea name="resposta" class="form-control" id="inputResposta" required>{{ $registro->resposta }}</textarea>
					</div>

				</div>
			</div>

			<hr>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ route('admin.instrucoes-de-uso.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection