@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Festas Particulares
                </h2>

                <hr>

            	@include('admin.templates.partials.mensagens')

                <table class="table table-striped table-bordered table-hover">

              		<thead>
                		<tr>
                            <th>Unidade</th>
                            <th>Festa</th>
                            <th>Data</th>
                  			<th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                    	@forelse ($registros as $registro)

                        	<tr class="tr-row" id="row_{{ $registro->id }}">
                                <td>
                                    {{ $registro->unidade->getResumo() }}
                                    @if($registro->getNaoLido())
                                        <span class="label label-success">nova</span>
                                    @endif
                                </td>
                                <td>
                                    {{ $registro->titulo }}
                                </td>
                          		<td>
                                    {{ $registro->reserva->format('d/m/y') }} &middot; {{ $registro->horario }}
                                </td>
                          		<td class="crud-actions visualizar">
                            		<a href="{{ route('admin.festas-particulares.show', $registro->id ) }}" class="btn btn-primary btn-sm">visualizar</a>
                          		</td>
                        	</tr>

                        @empty

                            <tr>
                                <td colspan="4" style='text-align:center'>Nenhum cadastro</td>
                            </tr>

                    	@endforelse
              		</tbody>

            	</table>

            </div>
        </div>
    </div>

@endsection