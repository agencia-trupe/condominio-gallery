@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		      	<h2>
		        	Visualizar Chamado de Manutenção
		        </h2>

		        <hr>

		    </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    		    @include('admin.templates.partials.mensagens')
    		</div>
    	</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
	    		<div class="well">
	    			<p>
	    				Chamado de manutenção registrado por: <strong>{{ $registro->autor->getNomeCompleto() }} - {{ $registro->autor->getUnidade() }}</strong>
	    				<br>
	    				<small>{{ $registro->created_at->format('d/m/Y H:i \h') }}</small>
	    			</p>
	    			<p>
	    				<strong>Data da ocorrência:</strong> {{ $registro->data->format('d/m/Y') }}<br>
	    				<strong>Localização:</strong> {{ $registro->localizacao }}<br>
	    				<strong>Horário Aproximado:</strong> {{ $registro->horario }}
	    			</p>
	    			<p>
	    				{!! nl2br($registro->descricao) !!}
	    			</p>

	    			<hr>

	    			<div class="lista-fotos" data-featherlight-gallery data-featherlight-filter="a">
		    			@forelse($registro->fotos as $foto)
							<a href="assets/images/moradores/chamados-de-manutencao/redimensionadas/{{ $foto->foto }}">
								<img src="assets/images/moradores/chamados-de-manutencao/thumbs/{{ $foto->foto }}" class="img-responsive img-thumbnail" alt="">
							</a>
		    			@empty
							<p>O morador não enviou fotos</p>
						@endforelse
					</div>

	    			<hr>

	    		</div>
			</div>
		</div>

		<hr>

		@if(!$registro->getVistoPelaAdm())
			<a href="{{ route('admin.chamados-de-manutencao.vistar', $registro->id) }}" title="Marcar Chamado como Lido" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-check"></span> Marcar Chamado como Lido</a>
		@else
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					<div class="well">
						<p>Chamado marcado como Lido por <strong>{{ $registro->getLoginVistoPor() }}</strong> em <em>{{ $registro->visto_pela_adm_em->format('d/m/Y H:i:s \h') }}</em>.</p>
					</div>
				</div>
			</div>
		@endif

		<hr>

		<a href="{{ route('admin.chamados-de-manutencao.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

    </div>

@endsection