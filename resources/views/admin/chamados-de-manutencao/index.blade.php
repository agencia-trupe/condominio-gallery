@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Chamados de Manutenção
                </h2>

                <hr>

            	@include('admin.templates.partials.mensagens')

                <table class="table table-striped table-bordered table-hover ">

              		<thead>
                		<tr>
                            <th>Data da Ocorrência</th>
                            <th>Aberto por</th>
                            <th>Local</th>
                            <th>Descrição</th>
                  			<th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                    	@forelse ($registros as $registro)

                        	<tr class="tr-row">
                                <td style="width:15%;">
                                    {{ $registro->data->format('d/m/y') }} @if($registro->getNaoLido()) <span class="label label-success">nova</span> @endif <br>
                                    <small>enviada em : {{ $registro->created_at->format('d/m/y - H:i:s \h') }}</small>
                                </td>
                          		<td>
                                    {{ $registro->autor->getNomeCompleto() }}
                                </td>
                                <td>
                                    {{ $registro->localizacao }}
                                </td>
                          		<td>
                                    {{ str_words(strip_tags($registro->descricao), 10) }}
                                </td>
                          		<td class="crud-actions visualizar">
                            		<a href="{{ route('admin.chamados-de-manutencao.show', $registro->id ) }}" class="btn btn-primary btn-sm">visualizar</a>

                                    <form action="{{ URL::route('admin.chamados-de-manutencao.destroy', $registro->id) }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                                    </form>
                          		</td>
                        	</tr>

                        @empty

                            <tr>
                                <td colspan="5" style='text-align:center'>Nenhum cadastro</td>
                            </tr>

                    	@endforelse
              		</tbody>

            	</table>

            </div>
        </div>
    </div>

@endsection
