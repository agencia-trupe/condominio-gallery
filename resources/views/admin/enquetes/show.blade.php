@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">

  	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      	<h2>
        	Resultado Parcial da Enquete
        </h2>

        <hr>

	    </div>
	  </div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
        @include('admin.templates.partials.mensagens')
    	</div>
    </div>


		<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

          <div class="form-group">
						<label for="inputTitulo">Título da Enquete</label>
						<input type="text" class="form-control" id="inputTitulo" name="titulo" value="{{ $registro->titulo }}" disabled>
					</div>

					<div class="form-group">
						<label for="inputQuestao">Questão</label>
						<input type="text" class="form-control" id="inputQuestao" name="texto" value="{{ $registro->texto }}" disabled>
					</div>

					<hr>

          <div class="clearfix opcoes-control">
            <label class="pull-left">Respostas</label>
          </div>

  				<div id="listaOpcoes" style="margin: 15px 0;">

            @foreach($registro->opcoes as $opcao)
              <ul style='margin-bottom:3px;'>
                <li>
                  {{ $opcao->texto }}
                  <br>
                  Votos: {{ sizeof($opcao->votos) }}

                  <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="{{ $opcao->porcentagem }}" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width:{{ $opcao->porcentagem }}%;">
                      {{ $opcao->porcentagem }}%
                    </div>
                  </div>

                  <hr>
                </li>
              </ul>
            @endforeach

  				</div>

				</div>
			</div>

			<hr>

			<a href="{{ route('admin.enquetes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</div>

@endsection