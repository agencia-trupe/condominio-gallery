@extends('admin.templates.dashboard')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>
        Enquetes
      </h2>

      <hr>

      @include('admin.templates.partials.mensagens')

      <div class="btn-group">
        <a href="{{ route('admin.enquetes.index', ['filtro' => 'recentes']) }}" class="btn btn-sm @if($filtro == 'recentes') btn-info @else btn-default @endif" title="Enquetes recentes">Enquetes recentes</a>
        <a href="{{ route('admin.enquetes.index', ['filtro' => 'historico']) }}" class="btn btn-sm @if($filtro == 'historico') btn-info @else btn-default @endif" title="Histórico">Histórico</a>
      </div>

      <hr>

      <a href="{{ route('admin.enquetes.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Enquete</a>

      <table class="table table-striped table-bordered table-hover ">

        <thead>
          <tr>
            @if($filtro == 'recentes')
              <th style='width: 120px;'>Ativa</th>
            @endif
            <th>Título</th>
            <th>Questão</th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
          </tr>
        </thead>

        <tbody>
          @forelse ($registros as $registro)

            <tr class="tr-row">
              @if($filtro == 'recentes')
                <td style='width: 120px;'>
                  @if($registro->ativa == 1)
                    <span class="label label-success">enquete ativa <span class="glyphicon glyphicon-ok"></span></span>
                  @else
                    <a href="{{ route('admin.enquetes.ativar', $registro->id) }}" title="tornar ativa" class="btn btn-info btn-sm btn-ativar-enquete">tornar ativa</a>
                  @endif
                </td>
              @endif
              <td>{{ $registro->titulo }}</td>
              <td>{{ $registro->texto }}</td>
              <td class="crud-actions" style='width: 220px;'>

                @if($registro->ativa == 1 || $registro->historico == 1)
                  <a href="{{ route('admin.enquetes.show', $registro->id ) }}" class="btn btn-primary btn-sm">ver resultados parciais</a>
                @else
                  <a href="{{ route('admin.enquetes.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
                @endif

                <form action="{{ URL::route('admin.enquetes.destroy', $registro->id) }}" method="post">
                  {!! csrf_field() !!}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                </form>

              </td>
            </tr>

          @empty

            <tr>
              <td colspan="4" style='text-align:center'>Nenhum cadastro</td>
            </tr>

          @endforelse
        </tbody>

      </table>

    </div>
  </div>
</div>

@endsection
