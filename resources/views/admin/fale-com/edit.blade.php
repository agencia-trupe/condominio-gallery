@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		      	<h2>
		        	Fale Com - Alterar Informações de Contato
		        </h2>

		        <hr>

		    </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    		    @include('admin.templates.partials.mensagens')
    		</div>
    	</div>

		<form action="{{ route('admin.fale-com.update', $registro->id) }}" method="post">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">

					<div class="form-group">
						<label for="inputTextoSindico">Síndico</label>
						<textarea name="contato_sindico" class="form-control" id="inputTextoSindico">{{ $registro->contato_sindico }}</textarea>
					</div>

					<div class="form-group">
						<label for="inputTextoConselho">Conselho</label>
						<textarea name="contato_conselho" class="form-control" id="inputTextoConselho">{{ $registro->contato_conselho }}</textarea>
					</div>

					<div class="form-group">
						<label for="inputTextoZelador">Zelador</label>
						<textarea name="contato_zelador" class="form-control" id="inputTextoZelador">{{ $registro->contato_zelador }}</textarea>
					</div>

					<div class="form-group">
						<label for="inputTextoPortaria">Portaria</label>
						<textarea name="contato_portaria" class="form-control" id="inputTextoPortaria">{{ $registro->contato_portaria }}</textarea>
					</div>

					<div class="form-group">
						<label for="inputTextoAdministradora">Administradora</label>
						<textarea name="contato_administradora" class="form-control" id="inputTextoAdministradora">{{ $registro->contato_administradora }}</textarea>
					</div>

				</div>
			</div>

			<hr>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ route('admin.fale-com.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection