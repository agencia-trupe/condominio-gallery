@extends('admin.templates.dashboard')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>
        Fale Com - Mensagens de Moradores
      </h2>

      <hr>

    	@include('admin.templates.partials.mensagens')

      <table class="table table-striped table-bordered table-hover ">

      	<thead>
      		<tr>
            <th>Data de Envio</th>
            <th>Enviado por</th>
            @if(Auth::admin()->get()->tipo == 'master')
              <th>Destinatário</th>
            @endif
            <th>Assunto</th>
            <th>Mensagem</th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
          </tr>
        </thead>

        <tbody>
        	@forelse ($registros as $registro)

            <tr class="tr-row">
              <td style="width:15%;">
                @if($registro->getNaoLido())
                  <span class="label label-success">nova</span>
                @endif
                {{ $registro->created_at->format('d/m/Y H:i \h') }}
              </td>
              <td style="width:15%;">{{ $registro->getNomeCompletoAutor() }} <br> {{ $registro->getUnidadeAutor() }}</td>
              @if(Auth::admin()->get()->tipo == 'master')
                <td>{{ $registro->getDestinatarioFormatado() }}</td>
              @endif
              <td style="width:15%;">{{ $registro->assunto }}</td>
              <td>{!! str_words(strip_tags($registro->mensagem), 15) !!}</td>

              <td class="crud-actions visualizar">
            		<a href="{{ route('admin.fale-com-mensagens.show', $registro->id ) }}" class="btn btn-primary btn-sm">visualizar</a>

                <form action="{{ URL::route('admin.fale-com-mensagens.destroy', $registro->id) }}" method="post">
                  {!! csrf_field() !!}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                </form>
          		</td>
        	  </tr>

          @empty

            <tr>
              @if(Auth::admin()->get()->tipo == 'master')
                <td colspan="6" style='text-align:center'>Nenhum cadastro</td>
              @else
                <td colspan="5" style='text-align:center'>Nenhum cadastro</td>
              @endif
            </tr>

          @endforelse
        </tbody>

      </table>

    </div>
  </div>
</div>

@endsection