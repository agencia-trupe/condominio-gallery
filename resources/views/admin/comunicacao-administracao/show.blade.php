@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">

  	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		    <h2>
          Visualizar Mensagem da Portaria
		    </h2>

		    <hr>

		  </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
        @include('admin.templates.partials.mensagens')
    	</div>
    </div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
	  		<div class="well">

	  			<h4>{{ $registro->assunto }}</h4>

	  			<p>
	  				Mensagem enviada por: <strong>{{ $registro->remetente_formatado }}</strong>
	  				<br>
	  				<small>{{ $registro->created_at->format('d/m/Y H:i \h') }}</small>
	  			</p>
	  			{!! nl2br($registro->descricao) !!}
	  			<hr>

	  		</div>
			</div>
		</div>

		<hr>

		<a href="{{ route('admin.comunicacao-administracao.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

  </div>

@endsection