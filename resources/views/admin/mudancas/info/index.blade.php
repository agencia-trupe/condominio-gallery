@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Mudanças - Informações
                </h2>

                <hr>

            	@include('admin.templates.partials.mensagens')

                <table class="table table-striped table-bordered table-hover ">

              		<thead>
                		<tr>
                            <th>Horário - manhã</th>
                            <th>Horário - tarde</th>
                            <th>Arquivo</th>
                  			<th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                    	@forelse ($registros as $registro)
                        	<tr class="tr-row">
                              	<td>
                                    {{ $registro->horario_manha }}
                                </td>
                                <td>
                                    {{ $registro->horario_tarde }}
                                </td>
                                <td>
                                  @if($registro->regulamento != '' && file_exists(public_path("assets/documentos/{$registro->regulamento}")))
                                    <a href="assets/documentos/{{ $registro->regulamento }}" class='btn btn-sm btn-link' target='_blank'><span class='glyphicon glyphicon-file'></span> {{ $registro->regulamento }}</a>
                                  @else
                                    nenhum arquivo cadastrado
                                  @endif
                                </td>
                          		<td class="crud-actions">
                            		<a href="{{ route('admin.informacoes-mudancas.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
                          		</td>
                        	</tr>

                    	@empty

                            <tr>
                                <td colspan="4" style='text-align:center'>Nenhum cadastro</td>
                            </tr>

                      @endforelse
              		</tbody>

            	</table>

            </div>
        </div>
    </div>

@endsection
