@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		      	<h2>
		        	Mudanças - Alterar Informações
		        </h2>

		        <hr>

		    </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    		    @include('admin.templates.partials.mensagens')
    		</div>
    	</div>

		<form action="{{ route('admin.informacoes-mudancas.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

					<div class="form-group">
						<label for="inputHorarioManha">Horário - manhã</label>
						<input type="text" class="form-control" id="inputHorarioManha" name="horario_manha" value="{{ $registro->horario_manha }}" required>
					</div>

					<div class="form-group">
						<label for="inputHorarioTarde">Horário - tarde</label>
						<input type="text" class="form-control" id="inputHorarioTarde" name="horario_tarde" value="{{ $registro->horario_tarde }}" required>
					</div>

					<div class="well">
						<div class="form-group">
							@if($registro->regulamento)
								Regulamento atual :
								<a href="assets/documentos/{{ $registro->regulamento }}" class='btn btn-sm btn-default' target='_blank'>{{ $registro->regulamento }}</a><hr>
							@endif
							<label for="inputRegulamento">novo regulamento</label>
							<input type="file" class="form-control" id="inputRegulamento" name="regulamento">
						</div>
					</div>

				</div>
			</div>

			<hr>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ route('admin.informacoes-mudancas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
