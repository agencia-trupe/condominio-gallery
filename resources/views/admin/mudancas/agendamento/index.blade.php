@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Mudanças - Agendamentos
                </h2>

                <hr>

            	@include('admin.templates.partials.mensagens')

                <table class="table table-striped table-bordered table-hover ">

              		<thead>
                		<tr>
                            <th>Data</th>
                            <th>Período</th>
                            <th>Morador</th>
                            <th>Unidade</th>
                            <th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                    	@forelse ($registros as $registro)

                        	<tr class="tr-row">
                                <td style="width:15%;">
                                    {{ $registro->data->format('d/m/y') }}
                                    @if($registro->getNaoLido())
                                        <span class="label label-success">nova</span>
                                    @endif
                                </td>
                          		<td style="width:15%;">
                                    {{ $registro->getPeriodoFormatado() }}
                                </td>
                          		<td>
                                    {{ $registro->morador->getNomeCompleto() }}
                                </td>
                                <td>
                                    {{ $registro->morador->getUnidade() }}
                                </td>
                          		<td class="crud-actions visualizar cancelar">
                                    <a href="{{ route('admin.agendamento-de-mudanca.show', $registro->id ) }}" class="btn btn-primary btn-sm">visualizar</a>

                            		<form action="{{ URL::route('admin.agendamento-de-mudanca.destroy', $registro->id) }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-danger btn-sm btn-delete">cancelar</button>
                                    </form>
                          		</td>
                        	</tr>

                    	@empty

                            <tr>
                                <td colspan="5" style='text-align:center'>Nenhum cadastro</td>
                            </tr>

                      @endforelse
              		</tbody>

            	</table>

            </div>
        </div>
    </div>

@endsection