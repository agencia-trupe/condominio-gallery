@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>
          Fornecedores
        </h2>

        <hr>

      	@include('admin.templates.partials.mensagens')

        <div class="btn-group">
          <a href="{{route('admin.fornecedores.index', ['filtro' => 'com_categoria'])}}" title="Com Categoria" class="btn @if($filtro == 'com_categoria') btn-info @else btn-default @endif">Com Categoria</a>
          <a href="{{route('admin.fornecedores.index', ['filtro' => 'sem_categoria'])}}" title="Sem Categoria" class="btn @if($filtro == 'sem_categoria') btn-info @else btn-default @endif">Sem Categoria</a>
        </div>

        <table class="table table-striped table-bordered table-hover ">

        	<thead>
        		<tr>
              <th>Categoria</th>
              <th>Nome</th>
              <th>Descritivo</th>
              <th>Avaliações</th>
              <th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@forelse ($registros as $registro)

            	<tr class="tr-row">
                <td>
                  @if(!is_null($registro->fornecedores_categorias_id))
                    {{ $registro->categoria->titulo }}
                  @else
                    Sem categoria (informada: {{$registro->outra_categoria}})
                  @endif
                </td>
              	<td>
                  {{ $registro->nome }}
                </td>
                <td>
                  {{ str_words(strip_tags($registro->descritivo), 15) }}
                </td>
                <td>
                  <a href="{{route('admin.fornecedores.avaliacoes.index', $registro->id)}}" class="btn btn-default">avaliações</a>
                </td>
                <td class="crud-actions visualizar">
              		<a href="{{ route('admin.fornecedores.edit', $registro->id ) }}" class="btn btn-primary btn-sm">visualizar</a>                  
            		</td>
            	</tr>

          	@empty

              <tr>
                <td colspan="5" style='text-align:center'>Nenhum cadastro</td>
              </tr>

            @endforelse
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
