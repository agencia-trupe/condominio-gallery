@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>
          Fornecedores - Avaliações
        </h2>

        <h3>
          Fornecedor: {{ $fornecedor->nome }}
        </h3>

        <hr>

      	@include('admin.templates.partials.mensagens')

        <a href="{{route('admin.fornecedores.index')}}" class="btn btn-default">&larr; voltar</a>

        <table class="table table-striped table-bordered table-hover ">

        	<thead>
        		<tr>
              <th>Data</th>
              <th>Morador</th>
              <th>Nota</th>
              <th>Avaliação</th>
              <th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@forelse ($fornecedor->avaliacoes as $registro)

            	<tr class="tr-row">
                <td>{{$registro->created_at->format('d/m/Y')}}</td>
                <td>{{$registro->morador->nome . ' - ' . $registro->morador->unidade->resumo}}</td>
              	<td>{{ $registro->nota }}</td>
                <td>{{ $registro->texto }}</td>
                <td class="crud-actions">
              		<form action="{{ URL::route('admin.fornecedores.avaliacoes.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@empty

              <tr>
                <td colspan="3" style='text-align:center'>Nenhum cadastro</td>
              </tr>

            @endforelse
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
