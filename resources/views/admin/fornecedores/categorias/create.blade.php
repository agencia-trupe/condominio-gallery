@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">

  	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

	    	<h2>Fornecedores - Adicionar Categoria</h2>

		    <hr>

		  </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    	  @include('admin.templates.partials.mensagens')
    	</div>
    </div>

		<form action="{{ route('admin.fornecedores-categorias.store') }}" method="post">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

			    	<div class="form-group">
						<label for="inputTitulo">Título</label>
						<input type="text" class="form-control" id="inputTitulo" name="titulo" value="{{ old('titulo') }}" required>
					</div>

				</div>
			</div>

			<hr>

			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{ route('admin.fornecedores-categorias.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
