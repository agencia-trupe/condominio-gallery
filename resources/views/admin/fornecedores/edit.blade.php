@extends('admin.templates.dashboard')

@section('conteudo')

<div class="container-fluid padded-bottom">

	<div class="row">
	  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<h2>Visualizar Fornecedor</h2>
      <hr>
    </div>
  </div>

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
	    @include('admin.templates.partials.mensagens')
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

			<form action="{{ route('admin.fornecedores.update', $fornecedor->id) }}" method="post">

				<input type="hidden" name="_method" value="PUT">

				{!! csrf_field() !!}

	      <div class="form-group">
					<label for="inputCategoria">Categoria</label>
					@if(!is_null($fornecedor->fornecedores_categorias_id))
						<input type="text" class="form-control" id="inputCategoria" name="fornecedores_categorias_id" value="{{ $fornecedor->categoria->titulo }}" disabled>
					@else
						<p>
							A categoria <strong>"{{$fornecedor->outra_categoria}}"</strong> informada pelo Morador não está cadastrada no sistema. Selecione um opção:
						</p>

						<div class="panel-group" id="seleciona-acao">

							<div class="panel panel-default">

								<div class="panel-heading">
									<h4 class="panel-title">
										<label>
											<input type="radio" value="is_nova_categoria" name="acao_categoria" data-target="#nova_categoria"> Cadastrar nova categoria
										</label>
									</h4>
								</div>

								<div class="collapse" id="nova_categoria">
									<div class="panel-body">
										<div class="input-group">
      								<div class="input-group-addon">Nova Categoria:</div>
											<input type="text" class="form-control" id="inputCategoriaNova" name="nova_categoria" value="{{ $fornecedor->outra_categoria }}">
										</div>
									</div>
								</div>

							</div>

							<div class="panel panel-default">

								<div class="panel-heading">
									<h4 class="panel-title">
										<label>
											<input type="radio" value="relacionar_categoria" name="acao_categoria" data-target="#categoria_existente"> Utilizar Categoria Existente
										</label>
									</h4>
								</div>

								<div class="collapse" id="categoria_existente">
									<div class="panel-body">
										<select class="form-control" name="fornecedores_categorias_id" id="inputCategoria">
											<option value="">Selecione</option>
											@foreach($listaCategorias as $categoria)
												<option value="{{$categoria->id}}">{{$categoria->titulo}}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>

						</div>
					@endif
				</div>

	      <div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" value="{{ $fornecedor->nome }}" disabled>
				</div>

	      <div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" value="{{ $fornecedor->telefone }}" disabled>
				</div>

	      <div class="form-group">
					<label for="inputCategoria">E-mail</label>
					<input type="text" class="form-control" id="inputCategoria" name="titulo" value="{{ $fornecedor->titulo }}" disabled>
				</div>

	      <div class="form-group">
					<label for="inputSite">Site</label>
					<input type="text" class="form-control" id="inputSite" name="site" value="{{ $fornecedor->site }}" disabled>
				</div>

	      <div class="form-group">
					<label for="inputDescritivo">Descritivo</label>
					<textarea name="descritivo" class="form-control textarea-simples" id="inputDescritivo" disabled>{{ $fornecedor->descritivo }}</textarea>
				</div>

	  		+ Fotos

		</div>
	</div>

	<hr>

	<input type="submit" value="Atualizar" class="btn btn-success">

	<a href="{{ route('admin.fornecedores.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

</div>

@endsection
