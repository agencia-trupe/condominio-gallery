@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Regulamentos</h2>

        <hr>

      	@include('admin.templates.partials.mensagens')

        <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="faq">

        	<thead>
        		<tr>
              <th>Regulamento Completo</th>
              <th>Regulamento Reduzido - Reservas</th>
            	<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>
        	<tbody>
          	@forelse ($registros as $registro)

            	<tr class="tr-row" id="row_{{ $registro->id }}">
                <td>
                  @if($registro->completo != '' && file_exists(public_path('assets/documentos/'.$registro->completo)))
                    <a href="assets/documentos/{{$registro->completo}}" class="btn btn-link" target="_blank" title="visualizar {{$registro->completo}}">{{$registro->completo}}</a>
                  @endif
                </td>
                <td>
                  @if($registro->reduzido_reservas != '' && file_exists(public_path('assets/documentos/'.$registro->reduzido_reservas)))
                    <a href="assets/documentos/{{$registro->reduzido_reservas}}" class="btn btn-link" target="_blank" title="visualizar {{$registro->reduzido_reservas}}">{{$registro->reduzido_reservas}}</a>
                  @endif
                </td>
              	<td class="crud-actions">
                	<a href="{{ route('admin.regulamentos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
              	</td>
              </tr>

            @empty

              <tr>
                <td colspan="3" style='text-align:center'>Nenhum cadastro</td>
              </tr>

            @endforelse
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
