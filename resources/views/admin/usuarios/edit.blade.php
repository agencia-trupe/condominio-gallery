@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
    	<div class="row">

    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		      	<h2>
		        	Editar Usuário do Painel Administrativo
		        </h2>

		        <hr>
	        </div>

    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">


		        @include('admin.templates.partials.mensagens')

		        <form action="{{ URL::route('admin.usuarios.update', $usuario->id) }}" method="post">

					<input type="hidden" name="_method" value="PUT">

					{!! csrf_field() !!}

					<div class="form-group">
						<label for="inputTipo">Tipo de Usuário</label>
						<select name="tipo" id="inputTipo" class="form-control" required>
							<option value=""></option>
							<option value="master" @if($usuario->tipo == 'master') selected @endif >Master</option>
							<option value="sindico" @if($usuario->tipo == 'sindico') selected @endif >Síndico</option>
							<option value="conselho" @if($usuario->tipo == 'conselho') selected @endif >Conselho</option>
							<option value="zelador" @if($usuario->tipo == 'zelador') selected @endif >Zelador</option>
						</select>
					</div>

			    	<div class="form-group">
						<label for="inputUsuario">Usuário</label>
						<input type="text" class="form-control" id="inputUsuario" name="login"  value="{{ $usuario->login }}" required>
					</div>

					<div class="form-group">
						<label for="inputEmail">E-mail</label>
						<input type="email" class="form-control" id="inputEmail" name="email" value="{{ $usuario->email }}">
					</div>

					<div class="form-group">
						<label for="inputSenha">Nova Senha</label>
						<input type="password" class="form-control" id="inputSenha" name="password">
					</div>

					<div class="form-group">
						<label for="inputConfSenha">Digite novamente a Nova Senha</label>
						<input type="password" class="form-control" id="inputConfSenha" name="password_confirm">
					</div>

					<hr>

					<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

					<a href="{{ URL::route('admin.usuarios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>
			</div>
		</div>
    </div>

@endsection