@extends('admin.templates.dashboard')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>Alterar Senha</h2>

      <hr>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">

      @include('admin.templates.partials.mensagens')

      <form action="{{route('admin.alterar-senha.update')}}" method="post" novalidate>

        {!! csrf_field() !!}

        <div class="well">
        <div class="form-group">
          <label for="inputSenhaAtual">Senha atual</label>
          <input type="password" name="senha_atual" class="form-control" required id="inputSenhaAtual">
        </div>

        <hr>

        <div class="form-group">
          <label for="inputSenhaNova">Nova Senha</label>
          <input type="password" name="senha_nova" class="form-control" required id="inputSenhaNova">
        </div>

        <div class="form-group">
          <label for="inputSenhaNovaConf">Confirmar Nova Senha</label>
          <input type="password" name="senha_nova_confirmation" class="form-control" required id="inputSenhaNovaConf">
        </div>

        <input type="submit" value="Alterar Senha" class="btn btn-success">
        </div>

      </form>

    </div>
  </div>
</div>

@endsection
