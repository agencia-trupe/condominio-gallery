@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Usuários - Administração
                </h2>

                <hr>

            	@include('admin.templates.partials.mensagens')

                 <a href="{{ route('admin.usuarios.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Usuário</a>

                <table class="table table-striped table-bordered table-hover ">

              		<thead>
                		<tr>
                  			<th>Usuário</th>
                  			<th>E-mail</th>
                  			<th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                    	@forelse ($registros as $usuario)

                        	<tr class="tr-row">
                          		<td>
                                    {{ $usuario->login }}
                                </td>
                          		<td>
                                    {{ $usuario->email }}
                                </td>
                          		<td class="crud-actions">
                            		<a href="{{ route('admin.usuarios.edit', $usuario->id ) }}" class="btn btn-primary btn-sm">editar</a>

                                    @if(\Auth::admin()->user()->id != $usuario->id)
                                        <form action="{{ URL::route('admin.usuarios.destroy', $usuario->id) }}" method="post">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                                        </form>
                                    @endif
                          		</td>
                        	</tr>

                    	@empty

                            <tr>
                                <td colspan="3" style='text-align:center'>Nenhum cadastro</td>
                            </tr>

                      @endforelse
              		</tbody>

            	</table>

            </div>
        </div>
    </div>

@endsection