@extends('admin.templates.dashboard')

@section('conteudo')

<div class="container-fluid padded-bottom">

  <div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>
        Moradores
      </h2>

      <hr>

      @include('admin.templates.partials.mensagens')

    </div>

  </div>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
      <input type="text" id="filtra-moradores" placeholder="Busca" class="form-control">
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">

      <table class="table table-striped table-bordered table-hover table-condensed table-centered">

        <thead>
          <tr>
            <th>Morador</th>
            <th>Unidade</th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
          </tr>
        </thead>

        <tbody>
          @forelse ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
              <td class="celula-nome">
                {{ $registro->nome }}
              </td>
              <td class="celula-nome">
                {{ $registro->unidade->getResumo() }}
              </td>
              <td class="crud-actions visualizar">
                <a href="{{ route('admin.moradores.show', $registro->id ) }}" class="btn btn-primary btn-sm">
                  visualizar
                </a>
              </td>
            </tr>

          @empty

            <tr>
              <td colspan="2" style='text-align:center'>Nenhum cadastro</td>
            </tr>

          @endforelse
        </tbody>

      </table>

    </div>
  </div>
</div>

@endsection
