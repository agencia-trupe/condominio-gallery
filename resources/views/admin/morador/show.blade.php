@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		      	<h2>
		        	Morador
		        </h2>

		        <hr>

		    </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    		    @include('admin.templates.partials.mensagens')
    		</div>
    	</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
	    		
                @if($registro->foto && file_exists("assets/images/moradores/fotos/thumbs/" . $registro->foto))
                    <img src="assets/images/moradores/fotos/thumbs/{{ $registro->foto }}" style="max-width: 200px; margin-right: 10px;" class="img-thumbnail pull-left" />
                @else
                    <img src="moradores/assets/images/avatar-default/90/90/sem foto" style="max-width: 200px; margin-right: 10px;" class="thumbnail pull-left">
                @endif

                <strong>Nome completo:</strong> {{ $registro->nome }}

                <p style="line-height: 160%;">
                    <strong>Como prefere ser chamado:</strong> {{ $registro->apelido }}
                    <br>
                    <strong>Tipo de morador:</strong> {{ $registro->getRelacaoUnidade() }}
                    <br>
                    <strong>Data de Nascimento:</strong> {{ $registro->data_nascimento->format('d/m/Y') }}
                    <br>
                    <strong>Data de Mudança:</strong> {{ $registro->data_mudanca->format('d/m/Y') }}
                    <br>
                    <strong>E-mail:</strong> <a href="mailto:{{ $registro->email }}" title="Enviar um e-mail">{{ $registro->email }}</a>
                    <br>
                    <br>
                    <strong>Telefone fixo:</strong> {{ $registro->telefone_fixo }}
                    <br>
                    <strong>Telefone celular:</strong> {{ $registro->telefone_celular }}
                    <br>
                    <strong>Telefone comercial:</strong> {{ $registro->telefone_comercial }}
                </p>                

			</div>
		</div>

		<hr>

        <a href="{{ route('admin.moradores.index')}}" title="Voltar" class="btn btn-default btn-voltar pull-left" style="margin-right:30px;">&larr; Voltar</a>

        <form action="{{ URL::route('admin.moradores.destroy', $registro->id) }}" method="post" class="pull-left">
            {!! csrf_field() !!}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger btn-delete">Excluir Morador</button>
        </form>


    </div>

@endsection