@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">

  	<div class="row">
  		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      	<h2>
        	Unidades
        </h2>

        <hr>

      </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	  @include('admin.templates.partials.mensagens')
    	</div>
    </div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">

	    	<div class="well" id="detalhes-unidade">

          <h4>{{ $registro->getResumo() }}</h4>

          <p>
            Apartamento: {{ $registro->unidade }}
            <br>
            Bloco: {{ $registro->bloco }}
            <br>
            Proprietário: {{ $registro->proprietario }}
          </p>

          <hr>

          <div class="row">

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <button class="btn btn-default btn-sm" type="button" data-toggle="collapse" data-target="#collapseCadastros">
                Cadastros da Unidade <span class="caret"></span>
              </button>

              <ul class="list-group collapse" id="collapseCadastros" style="margin-top:15px;">
                @forelse($registro->moradores as $morador)

                  <li class="list-group-item clearfix">

                    @if($morador->foto && file_exists("assets/images/moradores/fotos/thumbs/" . $morador->foto))
                      <img src="assets/images/moradores/fotos/thumbs/{{ $morador->foto }}" style="max-width: 100px; margin-right: 5px;" class="img-thumbnail pull-left">
                    @else
                      <img src="moradores/assets/images/avatar-default/90/90/sem foto" style="max-width: 100px; margin-right: 5px;" class="thumbnail pull-left">
                    @endif

                    @if($morador->status)
                      <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#morador-{{$morador->id}}">
                        ver perfil
                      </button>
                    @else
                      <div class="btn-group pull-right" style="margin-bottom: 4px;">
                          <button type="button" class="btn btn-default btn-sm reenviar-email-confirmacao" data-morador="{{$morador->id}}">
                            reenviar email de confirmação
                          </button>
                          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#morador-{{$morador->id}}">
                            ver perfil
                          </button>
                      </div>
                    @endif

                    {{ $morador->nome }} ({{ $morador->apelido }})
                    <p class="pull-left">
                      {{ $morador->data_nascimento->diffInYears() }} anos
                      <br>
                      {{ $morador->getRelacaoUnidade() }}
                      <br>
                      {{ $morador->email }}
                      <br>
                      status da conta: @if($morador->status) <span class="label label-success">ativa</span> @else <span class="label label-danger">inativa</span> @endif
                      <br>
                    </p>
                    <div class="modal fade" id="morador-{{$morador->id}}">

                      <div class="modal-dialog">
                        <div class="modal-content">

                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                            <h4 class="modal-title">Perfil de Morador</h4>
                          </div>

                          <div class="modal-body clearfix">

                              @if($morador->foto && file_exists("assets/images/moradores/fotos/thumbs/" . $morador->foto))
                                <img src="assets/images/moradores/fotos/thumbs/{{ $morador->foto }}" style="max-width: 200px; margin-right: 10px;" class="img-thumbnail pull-left" />
                              @else
                                <img src="moradores/assets/images/avatar-default/90/90/sem foto" style="max-width: 200px; margin-right: 10px;" class="thumbnail pull-left">
                              @endif

                              <div class="foto pull-right">
                              </div>
                              <strong>Nome completo:</strong> {{ $morador->nome }}
                              <p style="line-height: 160%;">
                                <strong>Como prefere ser chamado:</strong> {{ $morador->apelido }}
                                <br>
                                <strong>Tipo de morador:</strong> {{ $morador->getRelacaoUnidade() }}
                                <br>
                                <strong>Data de Nascimento:</strong> {{ $morador->data_nascimento->format('d/m/Y') }}
                                <br>
                                <strong>Data de Mudança:</strong> {{ $morador->data_mudanca->format('d/m/Y') }}
                                <br>
                                <strong>E-mail:</strong> <a href="mailto:{{ $morador->email }}" title="Enviar um e-mail">{{ $morador->email }}</a>
                                <br>
                                <br>
                                <strong>Telefone fixo:</strong> {{ $morador->telefone_fixo }}
                                <br>
                                <strong>Telefone celular:</strong> {{ $morador->telefone_celular }}
                                <br>
                                <strong>Telefone comercial:</strong> {{ $morador->telefone_comercial }}
                              </p>
                          </div>

                        </div>
                      </div>
                    </div>
                  </li>

                @empty
                  <li class="list-group-item">Nenhum Cadastro</li>
                @endforelse

              </ul>

            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <button class="btn btn-default btn-sm" type="button" data-toggle="collapse" data-target="#collapseMoradores">
                Moradores da Unidade <span class="caret"></span>
              </button>

              <ul class="list-group collapse" id="collapseMoradores" style="margin-top:15px;">
                @forelse($registro->moradores_da_unidade as $morador)
                  <li class="list-group-item clearfix">
                    @if($morador->foto && file_exists("assets/images/moradores/moradores-da-unidade/thumbs/" . $morador->foto))
                      <img src="assets/images/moradores/moradores-da-unidade/thumbs/{{ $morador->foto }}" style="max-width: 100px;" class="img-thumbnail pull-left">
                    @else
                      <img src="moradores/assets/images/avatar-default/90/90/sem foto" class="img-thumbnail pull-left">
                    @endif
                    <p class="pull-left">
                      {{ $morador->nome }}
                      <br>
                      {{ $morador->data_nascimento->diffInYears() }} anos
                      <br>
                      {{ $morador->parentesco }}
                    </p>
                  </li>
                @empty
                  <li class="list-group-item">Nenhum Cadastro</li>
                @endforelse
              </ul>

            </div>

          </div>

          <hr>

          <div class="row">

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <button class="btn btn-default btn-sm" type="button" data-toggle="collapse" data-target="#collapseVeiculos">
                Veículos da Unidade <span class="caret"></span>
              </button>

              <ul class="list-group collapse" id="collapseVeiculos" style="margin-top:15px;">
                @forelse($registro->veiculos_da_unidade as $veiculo)
                  <li class="list-group-item">
                    {!! $veiculo->toText !!}
                  </li>
                @empty
                  <li class="list-group-item">Nenhum Cadastro</li>
                @endforelse
              </ul>

            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <button class="btn btn-default btn-sm" type="button" data-toggle="collapse" data-target="#collapseAnimais">
                Animais de Estimação da Unidade <span class="caret"></span>
              </button>

              <ul class="list-group collapse" id="collapseAnimais" style="margin-top:15px;">
                @forelse($registro->animais_de_estimacao as $animal)
                  <li class="list-group-item clearfix">
                    @if($animal->foto && file_exists("assets/images/moradores/animais-de-estimacao/thumbs/" . $animal->foto))
                      <img src="assets/images/moradores/animais-de-estimacao/thumbs/{{ $animal->foto }}" style="max-width: 100px;" class="img-thumbnail pull-left">
                    @else
                      <img src="moradores/assets/images/avatar-default/90/90/sem foto" class="img-thumbnail pull-left">
                    @endif
                    <p class="pull-left">
                      {{ $animal->nome }}
                      <br>
                      {{ $animal->especie }}
                      <br>
                      {{ $animal->sexo }}
                    </p>
                  </li>
                @empty
                  <li class="list-group-item">Nenhum Cadastro</li>
                @endforelse
              </ul>

            </div>

          </div>

          <hr>

          <div class="row">

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <button class="btn btn-default btn-sm" type="button" data-toggle="collapse" data-target="#collapseNaoAutorizados">
                Pessoas Não Autorizadas <span class="caret"></span>
              </button>

              <ul class="list-group collapse" id="collapseNaoAutorizados" style="margin-top:15px;">
                @forelse($registro->pessoas_nao_autorizadas as $nao_autorizada)
                  <li class="list-group-item clearfix">
                    @if($nao_autorizada->foto && file_exists("assets/images/moradores/pessoas-nao-autorizadas/thumbs/" . $nao_autorizada->foto))
                      <img src="assets/images/moradores/pessoas-nao-autorizadas/thumbs/{{ $nao_autorizada->foto }}" style="max-width: 100px;" class="img-thumbnail pull-left">
                    @else
                      <img src="moradores/assets/images/avatar-default/90/90/sem foto" class="img-thumbnail pull-left">
                    @endif
                    <p class="pull-left">
                      {{ $nao_autorizada->nome }}
                      <br>
                      {{ $nao_autorizada->data_nascimento->diffInYears() }} anos
                      <br>
                      {{ $morador->parentesco }}
                    </p>
                  </li>
                @empty
                  <li class="list-group-item">Nenhum Cadastro</li>
                @endforelse
              </ul>

            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <button class="btn btn-default btn-sm" type="button" data-toggle="collapse" data-target="#collapseAutorizados">
                Pessoas Autorizadas Permanentemente <span class="caret"></span>
              </button>

              <ul class="list-group collapse" id="collapseAutorizados" style="margin-top:15px;">
                @forelse($registro->pessoas_autorizadas as $autorizada)
                  <li class="list-group-item clearfix">
                    @if($autorizada->foto && file_exists("assets/images/moradores/pessoas-autorizadas/thumbs/" . $autorizada->foto))
                      <img src="assets/images/moradores/pessoas-autorizadas/thumbs/{{ $autorizada->foto }}" style="max-width: 100px;" class="img-thumbnail pull-left">
                    @else
                      <img src="moradores/assets/images/avatar-default/90/90/sem foto" class="img-thumbnail pull-left">
                    @endif
                    <p class="pull-left">
                      {{ $autorizada->nome }}
                      <br>
                      {{ $autorizada->data_nascimento->diffInYears() }} anos
                      <br>
                      {{ $autorizada->parentesco }}
                    </p>
                  </li>
                @empty
                  <li class="list-group-item">Nenhum Cadastro</li>
                @endforelse
              </ul>

            </div>

          </div>

          <hr>

          <div class="row">

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <button class="btn btn-default btn-sm" type="button" data-toggle="collapse" data-target="#collapsePrestadores">
                Prestadores de Serviço <span class="caret"></span>
              </button>

              <ul class="list-group collapse" id="collapsePrestadores" style="margin-top:15px;">
                @forelse($registro->prestadores_de_servico as $prestador)
                <li class="list-group-item">
                  {{ $prestador->nome }}
                  <p>
                    Documento: {{ $prestador->documento }}<br>
                    Empresa: {{ $prestador->empresa ? $prestador->empresa : 'não informado' }}<br>
                    Data de Nascimento: {{ $prestador->data_nascimento ? $prestador->data_nascimento->format('d/m/Y') : 'não informado' }}<br>
                    Autorizado de: <strong>{{ $prestador->permissao_inicio->format('d/m/Y') }}</strong> até <strong>{{ $prestador->permissao_termino->format('d/m/Y') }}</strong>
                  </p>
                </li>
                @empty
                  <li class="list-group-item">Nenhum Cadastro</li>
                @endforelse
              </ul>

            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <button class="btn btn-default btn-sm" type="button" data-toggle="collapse" data-target="#collapseMudancas">
                Agendamento de Mudança <span class="caret"></span>
              </button>

              <ul class="list-group collapse" id="collapseMudancas" style="margin-top:15px;">
                @forelse($registro->agendamentos_de_mudanca as $mudancas)
                <li class="list-group-item">
                  Agendamento para: {{ $mudancas->data->format('d/m/Y') }} &middot; {{ $mudancas->getPeriodoFormatado() }}
                </li>
                @empty
                  <li class="list-group-item">Nenhum Cadastro</li>
                @endforelse
              </ul>

            </div>

          </div>

          <hr>

          <div class="row">

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <button class="btn btn-default btn-sm" type="button" data-toggle="collapse" data-target="#collapseChurrasqueira">
                Reservas - Churrasqueira <span class="caret"></span>
              </button>

              <ul class="list-group collapse" id="collapseChurrasqueira" style="margin-top:15px;">
                @forelse($registro->reservas_churrasqueira as $reserva)
                  <li class="list-group-item clearfix">
                    <p class="pull-left">
                      {{ $reserva->titulo }}
                      <br>
                      Reserva para: {{ $reserva->reserva->format('d/m/Y') }} &middot; {{ $reserva->horario }}
                    </p>

                    <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#convidadosChurrasqueira-{{$reserva->id}}">
                      convidados
                    </button>

                    <div class="modal fade" id="convidadosChurrasqueira-{{$reserva->id}}">
                      <div class="modal-dialog">
                        <div class="modal-content">

                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                            <h4 class="modal-title">Convidados - {{ $reserva->titulo }}</h4>
                          </div>

                          <div class="modal-body">
                            <ul class="list-group">
                              @forelse($reserva->convidados as $key => $convidado)
                                <li class="list-group-item">{{ $convidado->nome }}</li>
                              @empty
                                <li class="list-group-item">Nenhum convidado</li>
                              @endforelse
                            </ul>
                          </div>

                        </div>
                      </div>
                    </div>

                  </li>
                @empty
                  <li class="list-group-item">Nenhum Cadastro</li>
                @endforelse
              </ul>

            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <button class="btn btn-default btn-sm" type="button" data-toggle="collapse" data-target="#collapseSalaoFestas">
                Reservas - Salão de Festas <span class="caret"></span>
              </button>

              <ul class="list-group collapse" id="collapseSalaoFestas" style="margin-top:15px;">
                @forelse($registro->reservas_salao_festas as $reserva)
                  <li class="list-group-item clearfix">
                    <p class="pull-left">
                      {{ $reserva->titulo }}
                      <br>
                      Reserva para: {{ $reserva->reserva->format('d/m/Y') }} &middot; {{ $reserva->horario }}
                    </p>

                    <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#convidadosSalaoFestas-{{$reserva->id}}">
                      convidados
                    </button>

                    <div class="modal fade" id="convidadosSalaoFestas-{{$reserva->id}}">
                      <div class="modal-dialog">
                        <div class="modal-content">

                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                            <h4 class="modal-title">Convidados - {{ $reserva->titulo }}</h4>
                          </div>

                          <div class="modal-body">
                            <ul class="list-group">
                              @forelse($reserva->convidados as $key => $convidado)
                                <li class="list-group-item">{{ $convidado->nome }}</li>
                              @empty
                                <li class="list-group-item">Nenhum convidado</li>
                              @endforelse
                            </ul>
                          </div>

                        </div>
                      </div>
                    </div>

                  </li>
                @empty
                  <li class="list-group-item">Nenhum Cadastro</li>
                @endforelse
              </ul>

            </div>

          </div>

          <hr>

          <div class="row">

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <button class="btn btn-default btn-sm" type="button" data-toggle="collapse" data-target="#collapseSalaSquash">
                Reservas - Sala de Squash <span class="caret"></span>
              </button>

              <ul class="list-group collapse" id="collapseSalaSquash" style="margin-top:15px;">
                @forelse($registro->reservas_sala_de_squash as $reserva)
                  <li class="list-group-item clearfix">
                    <p class="pull-left">
                      Reserva para: {{ $reserva->reserva->format('d/m/Y - H:i') }}
                    </p>
                  </li>
                @empty
                  <li class="list-group-item">Nenhum Cadastro</li>
                @endforelse
              </ul>

            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <button class="btn btn-default btn-sm" type="button" data-toggle="collapse" data-target="#collapseFestas">
                Reservas - Festas Particulares <span class="caret"></span>
              </button>

              <ul class="list-group collapse" id="collapseFestas" style="margin-top:15px;">
                @forelse($registro->festas_particulares as $reserva)
                  <li class="list-group-item clearfix">
                    <p class="pull-left">
                      {{ $reserva->titulo }}
                      <br>
                      Reserva para: {{ $reserva->reserva->format('d/m/Y') }} &middot; {{ $reserva->horario }}
                    </p>

                    <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#convidadosFestas-{{$reserva->id}}">
                      convidados
                    </button>

                    <div class="modal fade" id="convidadosFestas-{{$reserva->id}}">
                      <div class="modal-dialog">
                        <div class="modal-content">

                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                            <h4 class="modal-title">Convidados - {{ $reserva->titulo }}</h4>
                          </div>

                          <div class="modal-body">
                            <ul class="list-group">
                              @forelse($reserva->convidados as $key => $convidado)
                                <li class="list-group-item">{{ $convidado->nome }}</li>
                              @empty
                                <li class="list-group-item">Nenhum convidado</li>
                              @endforelse
                            </ul>
                          </div>

                        </div>
                      </div>
                    </div>

                  </li>
                @empty
                  <li class="list-group-item">Nenhum Cadastro</li>
                @endforelse
              </ul>


            </div>

          </div>

	    	</div>

			</div>
		</div>

		<hr>

		<a href="{{ route('admin.unidades.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

  </div>

@endsection
