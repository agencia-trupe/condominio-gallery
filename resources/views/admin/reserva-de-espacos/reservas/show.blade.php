@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		      	<h2>
		        	Reservas de Espaços - Detalhes de Reserva
		        </h2>

		        <hr>

		    </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    		    @include('admin.templates.partials.mensagens')
    		</div>
    	</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
	    		<div class="well">
	    			<h4>Reserva - {{ $registro->espacoReservado->titulo }}</h4>
	    			<p>
	    				Reserva feita por: <strong>{{ $registro->getNomeCompletoAutor() }} - {{ $registro->getUnidadeAutor() }}</strong>
	    				<br>

	    				@if($registro->espacoReservado->tipo == 'por_periodo')

	    					Data reservada: <strong>{{ $registro->reserva->format('d/m/Y H:i') }}</strong>

	    					<hr>

		    				Observações:<br>
		    				@if($registro->anotacoes)
		    					{{ $registro->anotacoes }}
		    				@else
		    					sem anotações
		    				@endif

		    			@else

		    				Data reservada: <strong>{{ $registro->reserva->format('d/m/Y') }}</strong>

		    				<br>
	    					<br>

	    					@if(sizeof($registro->convidados))

				    			<button class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#collapseConvidados">
									lista de convidados
								</button>

								<ul class="list-group collapse" id="collapseConvidados">
									<li class="list-group-item" style="font-size:14px;">
										@foreach($registro->convidados as $convidado)
											{{ $convidado->nome }}<br>
										@endforeach
									</li>
								</ul>

							@endif

		    			@endif

	    				<hr>
	    				<small>reservado em: {{ $registro->created_at->format('d/m/Y H:i \h') }}</small>
	    			</p>

	    			{!! nl2br($registro->mensagem) !!}

	    		</div>
			</div>
		</div>

		<hr>

		<a href="{{ route('admin.reservas-de-espacos.index', ['filtro' => $registro->espacoReservado->tipo])}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

    </div>

@endsection