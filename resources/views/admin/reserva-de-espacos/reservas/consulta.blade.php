@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Consultar e Extrair Relatórios de Reservas</h2>

        <hr>

        @include('admin.templates.partials.mensagens')

        <div class="btn-group">
          <a href="{{ route('admin.reservas-de-espacos.index', ['filtro' => 'por_periodo']) }}" class="btn btn-default @if($filtro == 'por_periodo') btn-info @endif " title="Reservas por período" >Reservas por período @if(sizeof($notificacoes_areas_comuns['reservas_periodo']) > 0) <span class='label label-success'>{{ sizeof($notificacoes_areas_comuns['reservas_periodo']) }}</span> @endif </a>
          <a href="{{ route('admin.reservas-de-espacos.index', ['filtro' => 'diaria']) }}" class="btn btn-default @if($filtro == 'diaria') btn-info @endif " title="Reservas diárias" >Reservas diárias @if(sizeof($notificacoes_areas_comuns['reservas_diarias']) > 0) <span class='label label-success'>{{ sizeof($notificacoes_areas_comuns['reservas_diarias']) }}</span> @endif </a>
          <a href="{{ route('admin.reservas-consultar.index') }}" class="btn btn-default @if($filtro == 'consulta') btn-info @endif " title="Consultar e extrair relatórios" >Consultar e extrair relatórios</a>
        </div>

        <hr>

        <form class="form-inline" action="{{URL::route('admin.reservas-consultar.index')}}" method="post">
          {!!csrf_field()!!}
          <div class="form-group">
            <select class="form-control" id="FiltroEspaco" name="espaco_id" required>
              <option value="">Selecione um Espaço</option>
              @foreach($listaEspacos as $espaco)
                <option value="{{$espaco->id}}" @if($espaco_selecionado && $espaco->id == $espaco_selecionado->id) selected @endif >{{$espaco->titulo}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <input type="text" class="form-control datepicker" name="data_inicio" id="dataInicio" required placeholder="Data de Início" @if($data_inicio) value="{{$data_inicio->format('d/m/Y')}}" @endif >
          </div>
          <div class="form-group">
            <input type="text" class="form-control datepicker" name="data_termino" id="dataTermino" required placeholder="Data Final" @if($data_termino) value="{{$data_termino->format('d/m/Y')}}" @endif >
          </div>
          <div class="form-group">
            <label><input type="checkbox" name="incluir_bloqueios" value="1" @if($incluir_bloqueios == 1) checked @endif > incluir bloqueios</label>
          </div>
          <button type="submit" class="btn btn-success">Consultar <span class="glyphicon glyphicon-circle-arrow-right"></span></button>
        </form>

        <hr>

        @if($registros)

          <form action="{{ route('admin.reservas-consultar.download') }}" method="post" style="max-width: 500px;">
            {!! csrf_field() !!}
            <input type="hidden" name="hid_espaco_id" value="{{$espaco_selecionado->id}}">
            <input type="hidden" name="hid_dt_ini_id" value="{{$data_inicio->format('d/m/Y')}}">
            <input type="hidden" name="hid_dt_fim_id" value="{{$data_termino->format('d/m/Y')}}">
            <input type="hidden" name="hid_incl_bloq" value="{{$incluir_bloqueios}}">
            <div class="well">
              <div class="form-group">
                <p>Selecione o formato do arquivo:</p>

                <label style="width: 32%;">
                  <input type="radio" name="formato" required value="pdf"> PDF
                </label>

                <label style="width: 32%;">
                  <input type="radio" name="formato" value="csv"> CSV
                </label>

                <label style="width: 32%;">
                  <input type="radio" name="formato" value="xls"> XLS
                </label>
  					  </div>

              <button type="submit" class="btn btn-lg btn-success btn-block">
                <span class="glyphicon glyphicon-download"></span> download
              </button>
            </div>
          </form>

          <hr>

          <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
              <tr>
                <th>Data</th>
                <th>Local Reservado</th>
                <th>Morador</th>
                <th>Feita em</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($registros as $registro)
                <tr class="tr-row">
                  <td>
                    @if($registro->espacoReservado->tipo == 'por_periodo')
                      {{ $registro->reserva->format('d/m/Y H:i') }}
                    @elseif($registro->espacoReservado->tipo == 'diaria')
                      {{ $registro->reserva->format('d/m/Y') }}
                    @endif
                  </td>
                  <td>
                    {{ $registro->espacoReservado->titulo }}
                  </td>
                  <td>
                    @if(is_null($registro->moradores_id))
                      Bloqueio criado pela Administração
                    @else
                      {{ $registro->morador->getNomeCompleto() }}
                      <br>
                      {{ $registro->morador->getUnidade() }}
                    @endif
                  </td>
                  <td>
                    {{$registro->created_at->format('d/m/Y H:i')}}
                  </td>
                </tr>

              @empty

                @if($filtro)
                  <tr>
                    <td colspan="4" style='text-align:center'>Nenhum cadastro</td>
                  </tr>
                @else
                  <tr>
                    <td colspan="4" style='text-align:center; font-weight:bold;'>Selecione um tipo de Reserva</td>
                  </tr>
                @endif

              @endforelse
            </tbody>
          </table>
        @endif
      </div>
    </div>
  </div>

@endsection
