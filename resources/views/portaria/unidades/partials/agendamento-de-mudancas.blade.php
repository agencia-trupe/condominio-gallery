@if(sizeof($unidade->agendamentos_de_mudanca ))
	<div class="lista-item lista-agendamentos-de-mudanca">
		<h2>Mudança Agendada</h2>
		<ul>
			@foreach($unidade->agendamentos_de_mudanca as $mudanca)
				<li>
					<p>{{ $mudanca->data->format('d/m/Y') }} &middot; {{ $mudanca->getPeriodoFormatado() }}</p>
				</li>
			@endforeach
		</ul>
	</div>
@endif