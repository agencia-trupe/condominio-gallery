@if(sizeof($unidade->prestadores_de_servico ))
	<div class="lista-item lista-prestadores-de-servico">
		<h2>Prestadores de serviços</h2>
		<ul>
			@foreach($unidade->prestadores_de_servico as $prestador)
				<li>
					<div class="coluna">
						<h3>{{ $prestador->nome }}</h3>
						Documento: {{ $prestador->documento }}
					</div>
					<div class="coluna">
						@if($prestador->empresa)
							Empresa: {{ $prestador->empresa }}<br>
						@endif
						Entrada autorizada de {{ $prestador->permissao_inicio->format('d/m/Y') }} até {{ $prestador->permissao_termino->format('d/m/Y') }}
					</div>
				</li>
			@endforeach
		</ul>
	</div>
@endif