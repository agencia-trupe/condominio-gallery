@if(sizeof($unidade->pessoas_nao_autorizadas ))
	<div class="lista-item lista-pessoas-nao-autorizadas">
		<h2>Pessoas NÃO AUTORIZADAS</h2>
		<ul>
			@foreach($unidade->pessoas_nao_autorizadas as $pessoa_nao_autorizada)
				<li>

					@if($pessoa_nao_autorizada->foto && file_exists('assets/images/moradores/pessoas-nao-autorizadas/thumbs/'.$pessoa_nao_autorizada->foto))
						<img src="assets/images/moradores/pessoas-nao-autorizadas/thumbs/{{ $pessoa_nao_autorizada->foto }}" alt="Morador da unidade">
					@else
						<img src="{{ route('moradores.avatar-default') }}">						
					@endif

					<div class="nome">
						{{ $pessoa_nao_autorizada->nome }}
						@if($pessoa_nao_autorizada->data_nascimento)
							<span>idade aproximada: {{ $pessoa_nao_autorizada->data_nascimento->diffInYears() }} anos</span>
						@endif
					</div>
				</li>
			@endforeach
		</ul>
	</div>
@endif