@if(sizeof($unidade->reservas_sala_de_squash ))
	<div class="lista-item lista-festas-particulares">
		<h2>Reservas &middot; Sala de Squash</h2>
		<ul>
			@foreach($unidade->reservas_sala_de_squash as $reserva_squash)

				<li>
					<div class="descricao">

						<p>
							{{ $reserva_squash->reserva->format('d/m/Y · H:i') }}
						</p>

					</div>
				</li>

			@endforeach
		</ul>
	</div>
@endif