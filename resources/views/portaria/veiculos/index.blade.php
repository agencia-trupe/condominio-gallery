@extends('portaria.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('portaria.dashboard.partials.header')<!--

	--><div class="main" id="veiculos-main">

	  	<div class="main-conteudo">

				<h1>
          Veículos
        </h1>

        <div class="busca-veiculos">
					<label>
          	<input type="text" id="consultar-placa-veiculo" placeholder="CONSULTAR: placa do veículo">
					</label>

					<div id="loader">
						<div class="spinner">
						  <div class="rect1"></div>
						  <div class="rect2"></div>
						  <div class="rect3"></div>
						  <div class="rect4"></div>
						  <div class="rect5"></div>
						</div>
					</div>

					<div id="resultados-veiculos">
						<h3>Resultados: <strong>Placa <span id='resultados-placa-placeholder'></span></strong></h3>
						<div class="contem-tabela"></div>
					</div>

        </div>

				@include('portaria.veiculos.partials.mensagens')

				<form id="form-cadastrar-veiculo-temporario" action="{{ route('portaria.veiculos.cadastrar-veiculo-temporario') }}" method="post" novalidate>

          <h1>
            CADASTRAR VEÍCULO TEMPORÁRIO
          </h1>

					<input type="hidden" id="listaUnidadesJson" value="{{ $listaUnidadesJson }}">

					<fieldset>

						{!! csrf_field() !!}

						<label class="
							veiculos-da-unidade-form-input
							@if($errors->has('veiculos_da_unidade.unidade')) input-erro obrigatorio @endif
						">
							<input type="text" id="veiculos-da-unidade-form-unidade" name="veiculos_da_unidade[unidade]" placeholder="unidade" value="{{ old('veiculos_da_unidade.unidade') }}">
						</label>

            <label class="
							veiculos-da-unidade-form-input
							@if($errors->has('veiculos_da_unidade.veiculo')) input-erro obrigatorio @endif
						">
							<input type="text" id="veiculos-da-unidade-form-veiculo" name="veiculos_da_unidade[veiculo]" placeholder="veículo (modelo)" value="{{ old('veiculos_da_unidade.veiculo') }}">
						</label>

						<label class="
							veiculos-da-unidade-form-input
							@if($errors->has('veiculos_da_unidade.cor')) input-erro obrigatorio @endif
						">
							<input type="text" id="veiculos-da-unidade-form-cor" name="veiculos_da_unidade[cor]" placeholder="cor" value="{{ old('veiculos_da_unidade.cor') }}">
						</label>

						<label class="
							veiculos-da-unidade-form-input
							input-inline
							com-margem
							@if($errors->has('veiculos_da_unidade.placa')) input-erro obrigatorio @endif
						">
							<input type="text" id="veiculos-da-unidade-form-placa" name="veiculos_da_unidade[placa]" placeholder="placa" value="{{ old('veiculos_da_unidade.placa') }}">
						</label>

						<label class="
							veiculos-da-unidade-form-input
							input-inline
							@if($errors->has('veiculos_da_unidade.vaga')) input-erro obrigatorio @endif
						">
							<input type="text" id="veiculos-da-unidade-form-vaga" name="veiculos_da_unidade[vaga]" placeholder="número da vaga" value="{{ old('veiculos_da_unidade.vaga') }}">
						</label>

					</fieldset>

					<input type="submit" value="CADASTRAR" class="btn btn-info">

				</form>

			</div>

			@include('portaria.busca.resultados')

		</div>

	</section>

@stop
