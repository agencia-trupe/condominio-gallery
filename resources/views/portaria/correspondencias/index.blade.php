@extends('portaria.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('portaria.dashboard.partials.header')<!--

	 --><div class="main" id="correspondencias-main">

	 		<div class="main-conteudo">

				<h1>CADASTRO DE CORRESPONDÊNCIAS</h1>

				<div class="form-busca-unidade">
					<form action="{{ route('portaria.correspondencias.inserir') }}" method="post" id="form-inserir-correspondencia">

						{!! csrf_field() !!}

						<input type="hidden" id="listaUnidadesJson" value="{{ $listaUnidadesJson }}">

						<label>
							<div class="input-wrapper">
								<input type="text" name="termo_busca_unidade" id="input-busca-unidade" placeholder="insira o número do apartamento + enter" value="{{ old('termo_busca_unidade') }}">
							</div>
							<span class="data">{{ $hoje->format('d/m/y · H:i \h') }}</span>
						</label>

					</form>
				</div>

				<div id="lista-avisos">
					<input type="text" id="input_filtra_tabela" placeholder="BUSCA" @if(sizeof($listaAvisos) == 0) style='display:none' @endif>
					<ul>
						@foreach($listaAvisos as $aviso)
							<li>
								<div class="unidade">{{ $aviso->unidade->getResumo() }}</div>
								<div class="timestamp">{{ $aviso->created_at->format("d/m/Y · H:i \h") }}</div>
								<div class="marcacao">
									<a href="" title="Marcar como entregue" data-id-correspondencia="{{ $aviso->id }}">entregue</a>
								</div>
								<div class="remover">
									<a href="" title="Remover aviso de Correspondência" class="btn btn-danger btn-circular" data-id-correspondencia="{{ $aviso->id }}">X</a>
								</div>
							</li>
						@endforeach
					</ul>
				</div>

			</div>

			@include('portaria.busca.resultados')

		</div>

	</section>

@stop