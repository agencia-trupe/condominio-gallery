@extends('portaria.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('portaria.dashboard.partials.header')<!--

	 --><div class="main" id="comunicacao-main">

	 		<div class="main-conteudo">

				<h1>
          COMUNICAÇÃO COM O
          @if($slug_destinatario == 'sindico')
            SÍNDICO
          @elseif($slug_destinatario == 'zelador')
            ZELADOR
          @endif
        </h1>

				@include('portaria.comunicacao.partials.mensagens')

				<form action="{{ route('portaria.comunicacao.enviar', $slug_destinatario) }}" method="post" novalidate>

					<div class="data">{{ Carbon\Carbon::now()->format('d/m/Y H:i') }}</div>

					<fieldset>

						{!! csrf_field() !!}

						<input type="hidden" name="comunicacao[destinatario]" value="{{$slug_destinatario}}">

						<label class="comunicacao-form-input
									 @if($errors->has('comunicacao.titulo')) input-erro obrigatorio @endif ">
							<input type="text"
	                   name="comunicacao[titulo]"
	                   id="input-comunicao-titulo"
	                   placeholder="Título da comunicação"
	                   value="{{ old('comunicacao.titulo') }}"
										 required>
						</label>

						<label class="contem-radio @if($errors->has('comunicacao.prioridade')) input-erro obrigatorio @endif ">
							<input type="radio"
										 name="comunicacao[prioridade]"
										 value="normal"
										 id="input-comunicacao-prioridade"
										 required
										 {{old_radio('comunicacao.prioridade', 'normal', true)}}> Prioridade normal
						</label>

						<label class="contem-radio @if($errors->has('comunicacao.prioridade')) input-erro obrigatorio @endif ">
							<input type="radio"
										 name="comunicacao[prioridade]"
										 value="urgente"
										 {{old_radio('comunicacao.prioridade', 'urgente', false)}}> URGENTE
						</label>

						<label class="comunicacao-form-input
									 @if($errors->has('comunicacao.titulo')) input-erro obrigatorio @endif ">
							<textarea name="comunicacao[descricao]"
												id="input-comunicacao-descricao"
												required
												placeholder="Descrição">{{old('comunicacao.titulo')}}</textarea>
						</label>

					</fieldset>

					<input type="submit" value="ENVIAR" class="btn btn-info">

				</form>

			</div>

			@include('portaria.busca.resultados')

		</div>

	</section>

@stop