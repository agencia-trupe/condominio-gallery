<div id="listaMudancas">
	@if(sizeof($mudancas) > 0)
		<li>
			<h2>Mudanças</h2>
		</li>
		@foreach($mudancas as $mudanca)
			<li>
				<div class="unidade">{{ $mudanca->morador->getUnidadeAbrev() }}</div>
				<div class="descricao">{{ $mudanca->getPeriodoFormatado() .' - '. $mudanca->morador->getNomeCompleto() }}</div>
			</li>
		@endforeach
	@else
		<li class='lista-vazia'>
			<h2>Mudanças</h2>
		</li>
	@endif
</div>