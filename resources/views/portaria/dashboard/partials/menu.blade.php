<ul class="dashboard-menu">
	<li>
		<h3>FUNÇÕES DO SISTEMA</h3>
	</li>
	<li><a href="{{ route('portaria.dashboard') }}" title="Home &middot; Informações do dia" @if(str_is('portaria.dashboard*', Route::currentRouteName())) class='ativo' @endif >Home &middot; Informações do dia</a></li>
	<li><a href="{{ route('portaria.comunicacao.index', 'sindico') }}" title="Comunicação com o Síndico" @if(str_is('portaria.comunicacao.*', Route::currentRouteName()) && isset($slug_destinatario) && $slug_destinatario == 'sindico') class='ativo' @endif >Comunicação com o Síndico</a></li>
	<li><a href="{{ route('portaria.comunicacao.index', 'zelador') }}" title="Comunicação com o Zelador" @if(str_is('portaria.comunicacao.*', Route::currentRouteName()) && isset($slug_destinatario) && $slug_destinatario == 'zelador') class='ativo' @endif >Comunicação com o Zelador</a></li>
	<li><a href="{{ route('portaria.lavanderia.index') }}" title="Lavanderia" @if(str_is('portaria.lavanderia*', Route::currentRouteName())) class='ativo' @endif >Lavanderia</a></li>
	<li><a href="{{ route('portaria.veiculos.index') }}" title="Veículos" @if(str_is('portaria.veiculos*', Route::currentRouteName())) class='ativo' @endif >Veículos</a></li>
</ul>
