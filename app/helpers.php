<?php

/**
 * Limit the number of words in a string.
 *
 * @param  string  $value
 * @param  int     $words
 * @param  string  $end
 * @return string
 */
if ( ! function_exists('str_words'))
{
	function str_words($value, $words = 100, $end = '...')
	{
		preg_match('/^\s*+(?:\S++\s*+){1,'.$words.'}/u', $value, $matches);

		if ( ! isset($matches[0])) return $value;

		if (strlen($value) == strlen($matches[0])) return $value;

		return rtrim($matches[0]).$end;
	}
}

/**
 *
 * Simply adds the http:// part if no scheme is included
 *
 * @access	public
 * @param	string	$str
 * @return	string
 */
if ( ! function_exists('prep_url'))
{
	function prep_url($str = '')
	{
		if ($str == 'http://' OR $str == '')
		{
			return '';
		}

		$url = parse_url($str);

		if ( ! $url OR ! isset($url['scheme']))
		{
			$str = 'http://'.$str;
		}

		return $str;
	}
}

/**
 *
 * Parses a string to turn email addresses into mailto: links
 *
 * @access	public
 * @param	string	$str
 * @return	string
 */
if ( ! function_exists('parse_emails'))
{
	function parse_emails($str = '')
	{
		$regex_html = '/(<([^>]+)>)/i';
		$str = preg_replace($regex_html, '', $str);

		$regex_emails	 = '/([a-zA-Z0-9][a-zA-Z0-9\._-]+@[a-zA-Z0-9\._-]+\.[a-zA-Z-0-9]{3}+\.[a-zA-Z-0-9]{2}?)/';
		$link_template = '<a href="mailto:$1">$1</a>';
		return preg_replace($regex_emails, $link_template, $str);
	}
}

/**
 *
 * Generate embed code to a given 'google maps incorporation code'
 *
 * @access	public
 * @param	string	$str
 * @param	string  $width
 * @param	string  $height
 * @param	string  $classe
 * @return	string
 */
if ( ! function_exists('embed_maps'))
{
	function embed_maps($str = '', $width = '', $height = '', $classe = '')
	{

	    //$str = stripslashes(htmlspecialchars_decode($str));

	    if($width != '')
	        $str = preg_replace("~width=\"(\d+)\"~", 'width="'.$width.'"', $str);
	    else
	    	$str = preg_replace("~width=\"(\d+)\"~", '', $str);

	    if($height != '')
	        $str = preg_replace("~height=\"(\d+)\"~", 'height="'.$height.'"', $str);
	    else
	    	$str = preg_replace("~height=\"(\d+)\"~", '', $str);

	    if($classe != '')
	    	$str = preg_replace("~<iframe~", "<iframe class='{$classe}'", $str);

		// COM o link 'ver mapa ampliado'
	    // return $str;

	    // SEM o link 'ver mapa ampliado'
	    return preg_replace('~<br \/>(.*)~', '', $str);
	}
}

/**
 *
 * Alter a string to be all uppercase and remove utf-8 accents
 *
 * @access	public
 * @param	string	$str
 * @return	string
 */
if ( ! function_exists('prepara_dados_unidade'))
{
	function prepara_dados_unidade($str = '')
	{
		$str = mb_strtoupper($str);
		return preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml|caron);~i', '$1', htmlentities($str, ENT_QUOTES, 'UTF-8'));
	}
}

/**
 * Creates a Slug based on a filename
 *
 * @param  string  $filename
 * @param  string  $extension
 * @return string
 */
if ( ! function_exists('filename_slug'))
{
	function filename_slug($filename, $extensao)
	{
		$filename = str_replace('.'.$extensao, '', $filename);
		return date('YmdHis').'_'.str_slug($filename).'.'.$extensao;
	}
}

/**
	* Função auxiliar pra controlar a impressão de 'checked' nos input[type=radio]
	*
	* @param  string $nome
	* @param  string $valor
	* @param  bool   $default
	* @return string
*/
if ( ! function_exists('old_radio'))
{
	function old_radio($nome, $valor, $default = false)
	{
		if(empty($nome) || empty($valor) || !is_bool($default))
			return '';

		if(null !== old($nome)) {
			if(old($nome) == $valor) {
				return 'checked';
			} else {
				return '';
			}
		} else {
			if($default) {
				return 'checked';
			} else {
				return '';
			}
		}
	}
}