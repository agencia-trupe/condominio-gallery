<?php

namespace Gallery\Exceptions;

use Gallery\Models\NotificacaoAdmin;
use Auth;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Gallery\Http\Controllers\Admin\Notificavel;

use Illuminate\Session\TokenMismatchException;

use Request;

class Handler extends ExceptionHandler
{

    use Notificavel;

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        if ($e instanceof TokenMismatchException){
            return redirect()->back()->withErrors('Token Mismatch!');
        }

        return parent::render($request, $e);
    }

    /**
     * Render the given HttpException.
     *
     * @param  \Symfony\Component\HttpKernel\Exception\HttpException  $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderHttpException(HttpException $e)
    {
        $status = $e->getStatusCode();

        /*
        * Se existe login ativo para os 2 (ou mais) perfis
        * verificar pela url, caso contrário é só mostrar
        * a view de erro do login ativo, ou de moradores
        * se não estiver logado
        */
        if(Auth::admin()->check() && Auth::moradores()->check()){

            if (Request::is('admin*')) {
                $perfil = 'admin';
            }else{
                $perfil = 'moradores';
            }

        }else{

            if(Auth::admin()->check())
                $perfil = 'admin';
            else
                $perfil = 'moradores';
        }

        if (view()->exists("{$perfil}.templates.erros.{$status}")) {

            if($perfil == 'admin') $this->carregarNotificacoes();

            return response()->view("{$perfil}.templates.erros.{$status}", [ 'exception' => $e ], $status);

        } else {
            return $this->convertExceptionToResponse($e);
        }
    }

}