<?php

namespace Gallery\Repositories\Moradores\Auth;

interface AuthRepositoryInterface
{

  public function getUser();

  public function getUserId();

  public function updatePassword($newPassword);

}
