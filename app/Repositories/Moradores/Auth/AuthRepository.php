<?php

namespace Gallery\Repositories\Moradores\Auth;

use Auth;

class AuthRepository implements AuthRepositoryInterface
{

  protected $user;

  public function __construct()
  {
    $this->user = Auth::moradores()->get();
  }

  public function getUser()
  {
    return $this->user;
  }

  public function getUserId()
  {
    return $this->user->id;
  }

  public function updatePassword($newPassword)
  {
    $this->user->senha = $newPassword;
    return $this->user->save();
  }

}
