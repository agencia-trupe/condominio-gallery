<?php

namespace Gallery\Repositories\Moradores\FaleCom;

interface FaleComRepositoryInterface
{

  public function getInfo();

  public function getDestinatarios();

  public function novaMensagem($userId, $destinatario, $data);

}
