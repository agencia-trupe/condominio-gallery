<?php

namespace Gallery\Repositories\Moradores\FaleCom;

use Gallery\Models\FaleCom;
use Gallery\Models\FaleComMensagem;

class FaleComRepository implements FaleComRepositoryInterface
{

  protected $faleCom;
  protected $faleComMsg;

  public function __construct(
    FaleCom $faleCom,
    FaleComMensagem $faleComMsg
  )
  {
    $this->faleCom = $faleCom;
    $this->faleComMsg = $faleComMsg;
  }

  public function getInfo()
  {
    return $this->faleCom->first();
  }

  public function getDestinatarios()
  {
    return [
			'sindico'  => 'Síndico',
			'conselho' => 'Conselho',
			'zelador'  => 'Zelador',
			'portaria' => 'Portaria'
		];
  }

  public function novaMensagem($userId, $destinatario, $data)
  {
    $mensagem = new $this->faleComMsg;

    $mensagem->moradores_id = $userId;
    $mensagem->assunto 		= $data['assunto'];
    $mensagem->mensagem 	= $data['mensagem'];
    $mensagem->destino 		= $destinatario;
    $mensagem->save();

    return $mensagem;
  }

}
