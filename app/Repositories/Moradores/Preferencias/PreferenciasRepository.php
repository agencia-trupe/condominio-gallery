<?php

namespace Gallery\Repositories\Moradores\Preferencias;

use Gallery\Models\MoradorPreferencia;

class PreferenciasRepository implements PreferenciasRepositoryInterface
{

  protected $preferencias;

  public function __construct(MoradorPreferencia $preferencias)
  {
    $this->preferencias = $preferencias;
  }

  public function updatePreferencias($userId, $preferences = [])
  {
    $userPref = $this->preferencias->firstOrNew(['moradores_id' => $userId]);

    $userPref->enviar_email_nova_correspondencia  = isset($preferences['enviar_email_nova_correspondencia']) ? 1 : 0;
    $userPref->enviar_email_novo_aviso            = isset($preferences['enviar_email_novo_aviso']) ? 1 : 0;
    $userPref->enviar_email_novo_documento        = isset($preferences['enviar_email_novo_documento']) ? 1 : 0;
    $userPref->save();
    
    return $userPref;
  }

}
