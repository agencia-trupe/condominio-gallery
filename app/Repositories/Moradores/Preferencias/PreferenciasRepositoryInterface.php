<?php

namespace Gallery\Repositories\Moradores\Preferencias;

interface PreferenciasRepositoryInterface
{

  public function updatePreferencias($userId, $preferences = []);

}
