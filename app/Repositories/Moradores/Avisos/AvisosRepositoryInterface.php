<?php

namespace Gallery\Repositories\Moradores\Avisos;

interface AvisosRepositoryInterface
{

  public function getAvisosNaoLidos(int $userId);

}
