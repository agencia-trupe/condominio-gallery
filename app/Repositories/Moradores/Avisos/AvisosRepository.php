<?php

namespace Gallery\Repositories\Moradores\Avisos;

use Gallery\Models\Aviso;

class AvisosRepository implements AvisosRepositoryInterface
{

  protected $avisos;

  public function __construct (Aviso $avisos)
  {
    $this->avisos = $avisos;
  }

  public function getAvisosNaoLidos(int $userId)
  {
    return  $this->avisos->whereDoesntHave('Morador', function ($query) use($userId) {
              $query->where('moradores.id', $userId);
            })
            ->ordenado()
            ->get();
  }

}
