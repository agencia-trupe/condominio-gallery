<?php

namespace Gallery\Repositories\Moradores\Documentos;

use Gallery\Models\Documento;

class DocumentosRepository implements DocumentosRepositoryInterface
{

  protected $documentos;

  public function __construct(Documento $documentos)
  {
    $this->documentos = $documentos;
  }

  public function getDocumentosRecentes()
  {
    return $this->documentos->ordenado()->take(3)->get();
  }

}
