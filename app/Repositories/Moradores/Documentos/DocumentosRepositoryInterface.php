<?php

namespace Gallery\Repositories\Moradores\Documentos;

interface DocumentosRepositoryInterface
{

  public function getDocumentosRecentes();

}
