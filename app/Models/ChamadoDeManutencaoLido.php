<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class ChamadoDeManutencaoLido extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'chamados_de_manutencao_lidos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'administradores_id',
        'chamados_de_manutencao_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
}