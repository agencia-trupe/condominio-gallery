<?php

namespace Gallery\Models;

use Auth;
use DB;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class FestaParticularConvidados extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'festas_particulares_convidados';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'festas_particulares_id',
        'nome'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function scopeOrdenado($query)
    {
        return $query->orderBy('nome', 'asc');
    }

    public function festa()
    {
        return $this->belongsTo('Gallery\Models\FestaParticular', 'festas_particulares_id');
    }

}