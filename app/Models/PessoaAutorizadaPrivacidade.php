<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class PessoaAutorizadaPrivacidade extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pessoas_autorizadas_privacidade';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pessoas_autorizadas_id',
        'visibilidade'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    /**
    * Relação Privacidade - Morador
    *
    */
    public function pessoa(){
        return $this->belongsTo('Gallery\Models\PessoaAutorizada', 'pessoas_autorizadas_id');
    }
}