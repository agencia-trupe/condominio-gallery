<?php

namespace Gallery\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fornecedores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'fornecedores_categorias_id',
      'outra_categoria',
      'nome',
      'telefone',
      'email',
      'site',
      'descritivo'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function setFornecedoresCategoriasIdAttribute($value)
    {
      if($value == 'outros')
        $this->attributes['fornecedores_categorias_id'] = null;
      else
        $this->attributes['fornecedores_categorias_id'] = $value;
    }

    public function scopeOrdenado($query)
    {
      return $query->orderBy('nome', 'asc');
    }

    public function scopeComCategoria($query)
    {
      return $query->whereNotNull('fornecedores_categorias_id');
    }

    public function scopeSemCategoria($query)
    {
      return $query->whereNull('fornecedores_categorias_id');
    }

    public function scopeAvaliacoesRecentes($query)
    {
      return $query->has('avaliacoes')
                   ->leftJoin('fornecedores_avaliacoes', 'fornecedores.id', '=', 'fornecedores_avaliacoes.fornecedores_id')
                   ->orderBy('fornecedores_avaliacoes.created_at', 'desc')
                   ->select('fornecedores.*')
                   ->with('imagens', 'avaliacoes', 'categoria')
                   ->limit(5);
    }

    public function scopeBusca($query, $fornecedores_categorias_id, $nome)
    {
      if($fornecedores_categorias_id)
        $query = $query->where('fornecedores_categorias_id', '=', $fornecedores_categorias_id);

      if($nome)
        $query = $query->where('nome', 'like', "%{$nome}%");

      $query = $query->orderBy('nome', 'asc');

      return $query;
    }

    public function getMediaDasAvaliacoesAttribute()
    {
      $notas = $this->avaliacoes;
      $soma_notas = 0;
      $nro_notas = count($notas);

      if($nro_notas > 0){

        foreach ($notas as $key => $value)
          $soma_notas = $soma_notas + $value->nota;

        return round( $soma_notas / $nro_notas );

      }else{
        return 0;
      }

    }

    public function morador()
    {
      return $this->belongsTo('Gallery\Models\Morador', 'moradores_id');
    }

    public function categoria()
    {
      return $this->belongsTo('Gallery\Models\FornecedorCategoria', 'fornecedores_categorias_id');
    }

    public function imagens()
    {
      return $this->hasMany('Gallery\Models\FornecedorImagem', 'fornecedores_id');
    }

    public function avaliacoes()
    {
      return $this->hasMany('Gallery\Models\FornecedorAvaliacao', 'fornecedores_id')
                  ->orderBy('created_at', 'desc');
    }

}
