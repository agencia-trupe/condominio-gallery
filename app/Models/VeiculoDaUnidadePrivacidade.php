<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class VeiculoDaUnidadePrivacidade extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'veiculos_da_unidade_privacidade';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'veiculos_da_unidade_id',
        'visibilidade'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    /**
    * Relação Privacidade - Morador
    *
    */
    public function veiculo(){
        return $this->belongsTo('Gallery\Models\VeiculoDaUnidade', 'moradores_da_unidade_id');
    }
}