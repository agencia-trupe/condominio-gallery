<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class EnqueteOpcoes extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'enquete_opcoes';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'enquete_questoes_id',
    'texto',
    'ordem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  

  protected $dates = ['created_at', 'updated_at'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'desc');
  }

  public function questao()
  {
    return $this->belongsTo('Gallery\Models\Enquete', 'enquete_questoes_id');
  }

  public function votos()
  {
    return $this->hasMany('Gallery\Models\EnqueteVotos', 'enquete_opcoes_id');
  }

  public function getPorcentagemAttribute()
  {
    $nro_votos_total = sizeof($this->questao->votos);
    $nro_votos_opcao = sizeof($this->votos);

    return ($nro_votos_total > 0) ? floor(100 * ($nro_votos_opcao / $nro_votos_total)) : '0';
  }
}