<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class FilaEmails extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fila_emails';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'destino_nome',
      'destino_email',
      'titulo',
      'mensagem',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function scopeOrdenado($query)
    {
      return $query->orderBy('created_at', 'desc');
    }

    
}