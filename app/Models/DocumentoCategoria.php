<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentoCategoria extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'documentos_categorias';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'slug',
        'ordem'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function scopeOrdenado($query)
    {
        return $query->orderBy('ordem', 'asc');
    }

    public function scopeFindSlug($query, $slug)
    {
        return $query->where('slug', $slug)->first();
    }

    public function documentos()
    {
        return $this->hasMany('Gallery\Models\Documento', 'documentos_categoria_id');
    }

}