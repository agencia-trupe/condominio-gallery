<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class Classificado extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'classificados';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'categoria',
      'titulo',
      'descricao',
      'contato',
      'moradores_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function scopeOrdenado($query)
    {
      return $query->orderBy('created_at', 'desc');
    }

    public function scopePorCategoria($query, $categoria_slug)
    {
      return $query->where('categoria', $categoria_slug);
    }

    public function getCategoriaFormatada()
    {
      if($this->categoria == 'venda_e_troca') return 'Venda & Troca';
      if($this->categoria == 'servico') return 'Serviços';
      if($this->categoria == 'recado') return 'Recados';
    }

    /**
    * Relação Aviso - Morador
    */
    public function morador(){
      return $this->belongsTo('Gallery\Models\Morador', 'moradores_id');
    }

    public function fotos(){
      return $this->hasMany('Gallery\Models\ClassificadoFoto', 'classificados_id');
    }
}