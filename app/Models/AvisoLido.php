<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class AvisoLido extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'avisos_lidos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'avisos_id',
        'moradores_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
}