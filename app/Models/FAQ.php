<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'faq';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'questao',
        'resposta',
        'ordem'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function scopeOrdenado($query)
    {
        return $query->orderBy('ordem', 'asc');
    }

}