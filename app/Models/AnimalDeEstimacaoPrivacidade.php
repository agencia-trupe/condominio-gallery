<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class AnimalDeEstimacaoPrivacidade extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'animais_de_estimacao_privacidade';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'animais_de_estimacao_id',
        'visibilidade'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    /**
    * Relação Privacidade - Morador
    *
    */
    public function animal(){
        return $this->belongsTo('Gallery\Models\AnimalDeEstimacao', 'animais_de_estimacao_id');
    }
}