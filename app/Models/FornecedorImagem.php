<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class FornecedorImagem extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'fornecedores_imagens';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'fornecedores_id',
    'imagem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  

  public function scopeOrdenado($query)
  {
    return $query->orderBy('created_at', 'desc');
  }

  public function fornecedor()
  {
    return $this->belongsTo('Gallery\Models\Fornecedor', 'fornecedores_id');
  }

}
