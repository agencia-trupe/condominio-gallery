<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class PessoaNaoAutorizadaPrivacidade extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pessoas_nao_autorizadas_privacidade';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pessoas_nao_autorizadas_id',
        'visibilidade'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    /**
    * Relação Privacidade - Morador
    *
    */
    public function pessoa(){
        return $this->belongsTo('Gallery\Models\PessoaNaoAutorizada', 'pessoas_id');
    }
}