<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class Aviso extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'avisos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'olho',
        'texto',
        'assinatura',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function scopeOrdenado($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
    * Relação Aviso - Morador
    */
    public function morador(){
        return $this->belongsToMany('Gallery\Models\Morador', 'avisos_lidos', 'avisos_id', 'moradores_id')
                    ->withTimestamps();
    }
}