<?php

namespace Gallery\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

class ComunicacaoAdministracaoLido extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'comunicacao_administracao_lidos';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'comunicacao_adm_id',
    'administradores_id'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  

  protected $casts = [
    'is_resposta' => 'boolean'
  ];

  public function getDestinatarioFormatadoAttribute(){
    $this->formataNomeDePerfis($this->remetente);
  }

  public function getRemetenteFormatadoAttribute(){
    $this->formataNomeDePerfis($this->destinatario);
  }

  private function formataNomeDePerfis($nome_de_perfil){
    switch ($nome_de_perfil) {
      case 'sindico':
        $retorno = "Síndico";
        break;

      default:
        $retorno = ucfirst($nome_de_perfil);
        break;
    }
    return $retorno;
  }

  public function scopeOrdenado($query){
    return $query->orderBy('created_at', 'desc');
  }

  public function scopeParaPerfil($query, $perfil){
    return $query->where('destinatario', $perfil);
  }

}