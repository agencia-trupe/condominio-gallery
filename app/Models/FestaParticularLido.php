<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class FestaParticularLido extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'festas_particulares_lidas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'administradores_id',
        'festas_particulares_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
}