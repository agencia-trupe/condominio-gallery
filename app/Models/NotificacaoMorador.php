<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class NotificacaoMorador extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'moradores_notificacoes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo_notificacao',
        'mensagem',
        'moradores_id',
        'ocorrencias_id',
        'correspondencias_id',
        'encomendas_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function scopeOrdenado($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeParaMorador($query, $morador_id)
    {
        return $query->where('moradores_id', $morador_id);
    }

    /* Scopes de Tipo */
    public function scopeOcorrencias($query)
    {
        return $query->where('tipo_notificacao', 'ocorrencia');
    }

    public function scopeCorrespondencias($query)
    {
        return $query->where('tipo_notificacao', 'correspondencia');
    }

    public function scopeEncomendas($query)
    {
        return $query->where('tipo_notificacao', 'encomenda');
    }

    /* Relações */
    public function morador(){
        return $this->belongsTo('Gallery\Models\Morador', 'moradores_id');
    }

    public function occorencia(){
        return $this->belongsTo('Gallery\Models\Ocorrencia', 'ocorrencias_id');
    }

    public function correspondencia(){
        return $this->belongsTo('Gallery\Models\Correspondencia', 'correspondencias_id');
    }

    public function encomenda(){
        return $this->belongsTo('Gallery\Models\Encomenda', 'encomendas_id');
    }

}