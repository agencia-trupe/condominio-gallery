<?php

namespace Gallery\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon as Carbon;

class NotificacaoAdmin extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'administradores_notificacoes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'tipo_notificacao',
      'mensagem',
      'administradores_id',
      'ocorrencias_id',
      'fale_com_mensagens_id',
      'mudancas_agendamento_id',
      'chamados_de_manutencao_id',
      'espacos_reservas_por_periodo_id',
      'espacos_reservas_por_diaria_id',
      'diaria_com_multa_id',
      'festas_particulares_id',
      'comunicacao_adm_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function scopeOrdenado($query)
    {
      return $query->orderBy('created_at', 'desc');
    }

    public function scopeParaAdministrador($query, $id)
    {
      return $query->where('administradores_id', $id);
    }

    /* Scopes de Tipo */
    public function scopeOcorrencias($query)
    {
      return $query->where('tipo_notificacao', 'ocorrencia');
    }

    public function scopeMensagens($query)
    {
      return $query->where('tipo_notificacao', 'mensagem');
    }

    public function scopeMensagensPortaria($query)
    {
      return $query->where('tipo_notificacao', 'mensagem_portaria');
    }

    public function scopeMudancas($query)
    {
      return $query->where('tipo_notificacao', 'mudanca');
    }

    public function scopeChamadosManutencao($query)
    {
      return $query->where('tipo_notificacao', 'chamado_manutencao');
    }

    public function scopeReservaDeEspacoPeriodo($query){
      return $query->where('tipo_notificacao', 'reserva_periodo');
    }

    public function scopeReservaDeEspacoDiaria($query){
      return $query->where('tipo_notificacao', 'reserva_diaria');
    }

    public function scopeCancelamentoComMulta($query){
      return $query->where('tipo_notificacao', 'reserva_cancelada_com_multa');
    }

    public function scopeFestasParticulares($query){
      return $query->where('tipo_notificacao', 'festa_particular');
    }

    /* Relações */
    public function administrador(){
      return $this->belongsTo('Gallery\Models\Admin', 'administradores_id');
    }

    public function ocorrencia(){
      return $this->belongsTo('Gallery\Models\Ocorrencia', 'ocorrencias_id');
    }

    public function fale_com_mensagem(){
      return $this->belongsTo('Gallery\Models\FaleComMensagem', 'fale_com_mensagens_id');
    }

    public function agendamento_mudanca(){
      return $this->belongsTo('Gallery\Models\AgendamentoDeMudanca', 'mudancas_agendamento_id');
    }

    public function chamado_de_manutencao(){
      return $this->belongsTo('Gallery\Models\ChamadoDeManutencao', 'chamados_de_manutencao_id');
    }

    public function reserva_periodo(){
      return $this->belongsTo('Gallery\Models\EspacoReservaPorPeriodo', 'espacos_reservas_por_periodo_id');
    }

    public function reserva_diaria(){
      return $this->belongsTo('Gallery\Models\EspacoReservaPorDiaria','espacos_reservas_por_diaria_id');
    }

    public function diaria_com_multa(){
      return $this->belongsTo('Gallery\Models\EspacoReservaPorDiaria','diaria_com_multa_id');
    }

    public function festa_particular(){
      return $this->belongsTo('Gallery\Models\FestaParticular','festas_particulares_id');
    }

    public function mensagem_portaria(){
      return $this->belongsTo('Gallery\Models\ComunicacaoController', 'comunicacao_adm_id');
    }

}