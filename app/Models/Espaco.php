<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class Espaco extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'espacos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'slug',
        'tipo'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function scopeOrdenado($query)
    {
        return $query->orderBy('tipo', 'asc')->orderBy('titulo', 'asc');
    }

    public function scopeSlug($query, $value)
    {
        return $query->where('slug', $value);
    }

    public function scopePorTipo($query, $value)
    {
        return $query->where('tipo', $value);
    }

    public function informacoes()
    {
        if($this->tipo == 'diaria')
            return $this->hasOne('Gallery\Models\EspacoInformacoesDiaria', 'espacos_id');
        elseif($this->tipo == 'por_periodo')
            return $this->hasOne('Gallery\Models\EspacoInformacoesPeriodo', 'espacos_id');
    }

}