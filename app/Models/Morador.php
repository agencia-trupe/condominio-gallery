<?php

namespace Gallery\Models;

use Auth;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

// Traits originais (sem multiauth)
//use Illuminate\Auth\Passwords\CanResetPassword;
//use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Kbwebs\MultiAuth\PasswordResets\CanResetPassword;
use Kbwebs\MultiAuth\PasswordResets\Contracts\CanResetPassword as CanResetPasswordContract;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use Carbon\Carbon as Carbon;

class Morador extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'moradores';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
      'id',
      'senha',
      'remember_token'
    ];

    protected $dates = [
      'data_mudanca',
      'data_nascimento',
      'created_at',
      'deleted_at'
    ];

    protected $fillable = [
      'unidades_id',
      'nome',
      'apelido',
      'foto',
      'relacao_unidade',
      'email',
      'senha',
      'data_nascimento',
      'data_mudanca',
      'telefone_fixo',
      'telefone_celular',
      'telefone_comercial'
    ];


    /**************************/
    /*        Setters         */
    /**************************/

    public function setSenhaAttribute($value) {
      $this->attributes['senha'] = bcrypt($value);
    }

    public function setDataMudancaAttribute($valor) {
      $valor_carbon = Carbon::createFromFormat('d/m/Y', $valor);
      return $this->attributes['data_mudanca'] = $valor_carbon->format('Y-m-d');
    }

    public function setDataNascimentoAttribute($valor) {
      $valor_carbon = Carbon::createFromFormat('d/m/Y', $valor);
      return $this->attributes['data_nascimento'] = $valor_carbon->format('Y-m-d');
    }

    /**************************/
    /*        Getters         */
    /**************************/

    public function getNomeCompleto() {
      return $this->nome;
    }

    public function getUnidade() {
      return "Apto {$this->unidade->unidade} - Bloco {$this->unidade->bloco}";
    }

    public function getUnidadeAbrev() {
      return strtoupper("{$this->unidade->unidade}{$this->unidade->bloco}");
    }

    public function getRelacaoUnidade() {
      return ($this->relacao_unidade == 'locatario') ? 'Locatário' : 'Proprietário';
    }

    public function getAuthPassword() {
      return $this->senha;
    }

    public function getVisibilidade($dado, $retornar_array = false) {
      $query = $this->privacidade()
                    ->where('dado', '=', $dado)
                    ->lists('visibilidade')
                    ->all();

      return ($retornar_array) ? $query : implode(',', $query);
    }

    public function getUsuarioMeTemComoAmigo($usuario)
    {
      $amizade = $usuario->amizades()
                         ->where('amigo_id', '=', $this->id)
                         ->get();

      return sizeof($amizade) > 0;
    }

    public function getTemUsuarioComoAmigo($usuario)
    {
      $amizade = $this->amizades()
                      ->where('amigo_id', '=', $usuario->id)
                      ->get();

      return sizeof($amizade) > 0;
    }

    /**************************/
    /*        Relações      */
    /**************************/

    public function privacidade() {
      return $this->hasMany('Gallery\Models\MoradorPrivacidade', 'moradores_id');
    }

    public function unidade() {
      return $this->belongsTo('Gallery\Models\Unidade', 'unidades_id')->incluirTestes();
    }

    public function unidade_sem_testes() {
      return $this->belongsTo('Gallery\Models\Unidade', 'unidades_id');
    }

    public function avisos() {
      return $this->belongsToMany('Gallery\Models\Aviso', 'avisos_lidos', 'moradores_id', 'avisos_id')
                  ->withTimestamps();
    }

    public function amizades() {
      return $this->hasMany('Gallery\Models\MoradorAmizade', 'moradores_id');
    }

    public function anuncios() {
      return $this->hasMany('Gallery\Models\Classificado', 'moradores_id');
    }

    public function preferencias() {
      return $this->hasOne('Gallery\Models\MoradorPreferencia', 'moradores_id');
    }

    public function fichas_lavanderia() {
      return $this->hasOne('Gallery\Models\LavanderiaFichas', 'moradores_id');
    }

    /**************************/
    /*        Scopes          */
    /**************************/

    public function scopeOutrosDaUnidade($query) {
      return $query->where('unidades_id', Auth::moradores()->get()->unidade->id)
                   ->where('id', '!=', Auth::moradores()->get()->id);
    }

    public function scopeExcluindoMoradorLogado($query) {
      return $query->where('id', '!=', Auth::moradores()->get()->id);
    }

    public function scopeExcluindoMoradoresTeste($query) {
      return $query->has('unidade_sem_testes');
    }

    public function scopeAtivos($query) {
      return $query->where('status', '=', 1);
    }
}
