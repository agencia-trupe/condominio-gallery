<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class EspacoReservaPorPeriodoLido extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'espacos_reservas_por_periodo_lidos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'administradores_id',
        'espacos_reservas_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
}