<?php

namespace Gallery\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // Morador criando ocorrência -> notificar administração
        'Gallery\Events\MoradorCriouOcorrencia' => [
            'Gallery\Listeners\NotificarAdminOcorrencia',
        ],

        // Administração visualizou ocorrência -> marcar como
        // lida e apagar a notificação
        'Gallery\Events\AdminVisualizouOcorrencia' => [
            'Gallery\Listeners\ExcluirNotificacaoOcorrencia',
        ],

        // Administração respondeu a ocorrência -> notificar
        // morador que enviou a ocorrência
        'Gallery\Events\AdminRespondeuOcorrencia' => [
            'Gallery\Listeners\NotificarMoradorRespostaOcorrencia',
        ],

        // Morador visualizou a ocorrência -> marcar como lida
        // e apagar notificação
        'Gallery\Events\MoradorVisualizouOcorrencias' => [
            'Gallery\Listeners\ExcluirNotificacaoOcorrenciaMorador',
        ],

        // Morador enviou mensagem -> notificar administração
        'Gallery\Events\MoradorEnviouMensagem' => [
            'Gallery\Listeners\NotificarAdminMensagemMorador',
        ],

        // Administração visualizou ocorrência -> marcar como lida
        // e excluir a notificação
        'Gallery\Events\AdminVisualizouMensagem' => [
            'Gallery\Listeners\ExcluirNotificacaoMensagem',
        ],

        // Morador criou agendamento de mudança
        // notificar Administração (somente zelador)
        'Gallery\Events\MoradorAgendouMudanca' => [
            'Gallery\Listeners\NotificarAdminMudanca',
        ],

        // Administração visualizou o agendamento de mudança
        // marcar como lido e excluir notificação
        'Gallery\Events\AdminVisualizouAgendamentoDeMudanca' => [
            'Gallery\Listeners\ExcluirNotificacaoMudanca',
        ],

        // Morador criou um Chamado de Manutenção
        // notificar administração (Master, Zelador, Síndico e Conselho)
        'Gallery\Events\MoradorAbriuChamadoDeManutencao' => [
            'Gallery\Listeners\NotificarAdminChamadoManutencao',
        ],

        // Administração visualizou o Chamado de Manutenção
        // Marcar como lido e excluir notificação
        'Gallery\Events\AdminVisualizouChamadoDeManutencao' => [
            'Gallery\Listeners\ExcluirNotificacaoChamadoDeManutencao',
        ],

        // Morador criou uma Reserva de Espaço por Período
        // notificar administração (Master, Zelador, Síndico e Conselho)
        'Gallery\Events\MoradorReservouEspacoPorPeriodo' => [
            'Gallery\Listeners\NotificarAdminReservaPorPeriodo',
        ],

        // Morador criou uma Reserva de Espaço por Diária
        // notificar administração (Master, Zelador, Síndico e Conselho)
        'Gallery\Events\MoradorReservouEspacoPorDiaria' => [
            'Gallery\Listeners\NotificarAdminReservaPorDiaria',
        ],

        // Administração visualizou o Chamado de Manutenção
        // Marcar como lido e excluir notificação
        'Gallery\Events\AdminVisualizouReservaPorPeriodo' => [
            'Gallery\Listeners\ExcluirNotificacaoReservaPorPeriodo',
        ],

        // Administração visualizou o Chamado de Manutenção
        // Marcar como lido e excluir notificação
        'Gallery\Events\AdminVisualizouReservaPorDiaria' => [
            'Gallery\Listeners\ExcluirNotificacaoReservaPorDiaria',
        ],

        // Morador cancelou a reserva de um espaço do tipo diária
        // dentro do prazo para gerar multa. Notificar administração
        'Gallery\Events\MoradorCancelouDiariaComMulta' => [
            'Gallery\Listeners\NotificarAdminCancelamentoComMulta',
        ],

        // Administração visualizou notificação de cancelamento com multa
        // Remover notificação
        'Gallery\Events\AdminVisualizouReservaComMulta' => [
            'Gallery\Listeners\ExcluirNotificacaoCancelamentoComMulta',
        ],

        // Portaria inseriu aviso de correspondência
        // Notificar Moradores da Unidade
        'Gallery\Events\PortariaCadastrouCorrespondencia' => [
            'Gallery\Listeners\NotificarMoradoresUnidadeCorrespondencia',
        ],

        // Portaria inseriu aviso de correspondência
        // Notificar Moradores da Unidade
        'Gallery\Events\PortariaCadastrouEncomenda' => [
            'Gallery\Listeners\NotificarMoradoresUnidadeEncomenda',
        ],

        // Morador agendou uma Festa Particular
        // notificar administração (Master, Síndico e Conselho)
        'Gallery\Events\MoradorCriouFestaParticular' => [
            'Gallery\Listeners\NotificarAdminFestaParticular',
        ],

        // Administração visualizou notificação de festa particular
        // Remover notificação e marcar como lido
        'Gallery\Events\AdminVisualizouFestaParticular' => [
            'Gallery\Listeners\ExcluirNotificacaoFestaParticular',
        ],

        // Login no painel de Administração
        // Remover notificações de datas passadas
        'Gallery\Events\AdminLogin' => [
            'Gallery\Listeners\ExcluirNotificacoesPassadas',
        ],

        // Admin criou novo Aviso
        // Notificar Moradores que fizeram opt-in
        'Gallery\Events\AdminCriouAviso' => [
            'Gallery\Listeners\NotificarMoradoresAvisoPorEmail',
        ],

        // Admin criou novo Documento
        // Notificar Moradores que fizeram opt-in
        'Gallery\Events\AdminCriouDocumento' => [
            'Gallery\Listeners\NotificarMoradoresDocumentoPorEmail',
        ],

        // Portaria enviou mensagem ao síndico/zelador
        // Notificar Síndico/Zelador no painel
        'Gallery\Events\PortariaEnviouComunicacao' => [
          'Gallery\Listeners\NotificarAdminMensagemPortaria',
        ],

        // Administracao visualizou msg da Portaria
        // Remover notificação e marcar como lido
        'Gallery\Events\AdminVisualizouMensagemPortaria' => [
          'Gallery\Listeners\ExcluirNotificacaoMensagemPortaria'
        ],

        // Log de atividades do painel de Administração
        // Armazenar atividade no banco
        'Gallery\Events\AtividadeRastreavel' => [
          'Gallery\Listeners\LogarAtividade'
        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
