<?php

namespace Gallery\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Gallery\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);

        $router->model('enquete', 'Gallery\Models\Enquete');

        $router->model('anuncio', 'Gallery\Models\Classificado');

        $router->model('fornecedor', 'Gallery\Models\Fornecedor');

        $router->model('fornecedores', 'Gallery\Models\Fornecedor');

        $router->model('avaliacao', 'Gallery\Models\FornecedorAvaliacao');

        $router->model('espaco', 'Gallery\Models\Espaco');

    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
