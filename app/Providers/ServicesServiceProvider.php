<?php

namespace Gallery\Providers;

use Illuminate\Support\ServiceProvider;

class ServicesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind('Gallery\Services\Logger\LoggerInterface', 'Gallery\Services\Logger\Logger');
      $this->app->bind('Gallery\Services\EventManager\EventManagerInterface', 'Gallery\Services\EventManager\EventManager');
    }
}
