<?php

namespace Gallery\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind('Gallery\Repositories\Moradores\Avisos\AvisosRepositoryInterface', 'Gallery\Repositories\Moradores\Avisos\AvisosRepository');
      $this->app->bind('Gallery\Repositories\Moradores\Documentos\DocumentosRepositoryInterface', 'Gallery\Repositories\Moradores\Documentos\DocumentosRepository');
      $this->app->bind('Gallery\Repositories\Moradores\Auth\AuthRepositoryInterface', 'Gallery\Repositories\Moradores\Auth\AuthRepository');
      $this->app->bind('Gallery\Repositories\Moradores\Preferencias\PreferenciasRepositoryInterface', 'Gallery\Repositories\Moradores\Preferencias\PreferenciasRepository');
      $this->app->bind('Gallery\Repositories\Moradores\FaleCom\FaleComRepositoryInterface', 'Gallery\Repositories\Moradores\FaleCom\FaleComRepository');
    }
}
