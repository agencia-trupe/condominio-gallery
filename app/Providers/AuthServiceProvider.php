<?php

namespace Gallery\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Gallery\Models\Morador;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Gallery\Models\Ocorrencia' => 'Gallery\Policies\OcorrenciasPolicy'
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
      parent::registerPolicies($gate);

      $gate->define('gerenciar-administradores', function ($usuario_logado) {
        return $usuario_logado->tipo == 'master';
      });

      $gate->define('receber-msg-portaria', function($usuario_logado) {
        return in_array($usuario_logado->tipo, ['master', 'sindico','zelador']);
      });

      $gate->define('visualizar-mensagem', function ($usuario_logado, $mensagem) {
          if($usuario_logado->tipo == 'master') return true;
          return ($usuario_logado->tipo == $mensagem->destino) ? true : false;
      });

      $gate->define('portaria-visualizar-dado', function($usuario_logado, $usuario_busca_id, $dado){
        /*
        * Anteriormente fazíamos a verificação de permissões para portaria
        * mas depois ficou decidido que a portaria sempre vê tudo
        */
        return true;
        
        /*
        $usuario_busca = Morador::find($usuario_busca_id);

        $visibilidade_array = $usuario_busca->getVisibilidade($dado, true);

        if(!$visibilidade_array) return false;

        if( in_array(3, $visibilidade_array) ) return true;

        return false;
        */
      });

      $gate->define('visualizar-dados', function ($usuario_logado, $usuario_busca, $dado) {
          /*
            Verificar habilidade de visualizar dados sensíveis
            1. Buscar política de privacidade cadastrada pelo usuário da busca
            2. Separar em um array [ dado => visibilidade ]
            3. Checar primeiro se é público - return true (dado público)
            4. Moradores Amigos podem ver?
            4.1 Se não - return false (dado privado para outros moradores)
            4.2 Se sim, verificar amizade
            5. Se houver relação de amizade - return true (visível para amigos)
            5. Se não houver relação de amizade - return false (visível para amigos)
          */

          /*
            Visibilidade :
              Todos            => 1
              Admin            => 2
              Portaria         => 3
              Zelador          => 4
              Moradores        => 5
              Moradores Amigos => 6
          */
          $visibilidade_array = $usuario_busca->getVisibilidade($dado, true);

          if(!$visibilidade_array) return false;

          if( in_array(5, $visibilidade_array) || in_array(1, $visibilidade_array) ) return true;

          if( in_array(6, $visibilidade_array) && $usuario_logado->getUsuarioMeTemComoAmigo($usuario_busca) ) return true;

          return false;
      });

    }
}
