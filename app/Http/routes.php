<?php

Route::get('/', ['as' => 'moradores.inicial',
	function(){
		return redirect()->route('moradores.dashboard');
	}
]);

include('routes/routes-moradores.php');
include('routes/routes-admin.php');
include('routes/routes-portaria.php');

Route::get('moradores/assets/images/avatar-default/{width?}/{height?}/{texto?}/{tamanho?}',
    [
    	'as' => 'moradores.avatar-default',
		function($width = 100, $height = 100, $texto = 'SEM FOTO', $tamanho = 4){

			$im = @imagecreate($width, $height);

			if($im){

				header("Content-Type: image/png");

				$background_color = imagecolorallocate($im, 204, 204, 204);
				$text_color = imagecolorallocate($im, 79, 79, 79);

				if($width < 100)
					imagestring($im, 2, 7, 19, $texto, $text_color);
				elseif($width >= 160)
					imagestring($im, $tamanho, 40, 50, $texto, $text_color);
				else
					imagestring($im, $tamanho, 17, 40, $texto, $text_color);

				imagepng($im);
				imagedestroy($im);

			}else{
				echo "http://placehold.it/100?text=SEM+FOTO&txtsize=25";
			}
		}
	]
);
