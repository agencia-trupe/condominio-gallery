<?php

namespace Gallery\Http\Controllers\Portaria\Busca;

use Illuminate\Http\Request;
use Gallery\Http\Controllers\Portaria\BasePortariaController;

use Auth;
use Gate;
use DB;
use Carbon\Carbon as Carbon;
use Gallery\Models\Morador;
use Gallery\Models\Unidade;
use Gallery\Models\MoradorDaUnidade;

class BuscaController extends BasePortariaController
{

    public function postBusca(Request $request)
    {
        //DB::enableQueryLog();

        $termo = $request->input('termo');

        $resultados['moradores'] = Unidade::leftJoin('moradores', 'unidades.id', '=', 'moradores.unidades_id')
                                          ->where('moradores.status', '=', 1)
                                          ->where('unidades.is_teste', '=', 0)                                          
                                          ->whereRaw("CONCAT_WS('', `unidades`.`unidade`, `unidades`.`bloco`) LIKE '{$termo}%'")
                                          ->orWhere('moradores.nome', 'like', '%'.$termo.'%')
                                          ->select(
                                            'moradores.id as id',
                                            'moradores.nome as nome',
                                            'moradores.foto as foto',
                                            'moradores.data_nascimento as dt_nascimento',

                                            'moradores.email as email',
                                            'moradores.telefone_fixo as telefone_fixo',
                                            'moradores.telefone_celular as telefone_celular',
                                            'moradores.telefone_comercial as telefone_comercial',

                                            DB::raw("CONCAT_WS('', 'assets/images/moradores/fotos/thumbs/', `moradores`.`foto`) as `foto_fullpath`"),
                                            DB::raw("CONCAT_WS('', `unidades`.`unidade`, `unidades`.`bloco`) as `unidade`")
                                          )->get();

        $resultados['moradores_da_unidade'] = Unidade::leftJoin('moradores_da_unidade', 'unidades.id', '=', 'moradores_da_unidade.unidades_id')
                                                     ->whereRaw("CONCAT_WS('', `unidades`.`unidade`, `unidades`.`bloco`) LIKE '{$termo}%'")
                                                     ->orWhere('moradores_da_unidade.nome', 'like', '%'.$termo.'%')
                                                     ->select(
                                                        'moradores_da_unidade.id as id',
                                                        'moradores_da_unidade.nome as nome',
                                                        'moradores_da_unidade.foto as foto',
                                                        'moradores_da_unidade.data_nascimento as dt_nascimento',
                                                        DB::raw("CONCAT_WS('', 'assets/images/moradores/moradores-da-unidade/thumbs/', `moradores_da_unidade`.`foto`) as `foto_fullpath`"),
                                                        DB::raw("CONCAT_WS('', `unidades`.`unidade`, `unidades`.`bloco`) as `unidade`")
                                                     )->get();

        $retorno = [];

        // Reorganizando os arrays para retornar no formato correto
        // Juntando os cadastro de Moradores (perfil) e MoradoresDaUnidade
        /*
        *
            Exemplos:

            termo de busca = '11'

            $retorno = [
                [11A] = [
                    ['Bruno', 'imagem.jpg'],
                    ['Outro Morador do 11 A', 'outra-imagem.jpg']
                ],
                [11B] = [], // retornar a unidade mesmo que não haja morador cadastrado
                [11C] = [
                    ['Morador do Bloco C', 'foto.jpg']
                ]
            ];

            termo de busca = 'Bruno'

            $retorno = [
                [11A] = [
                    ['Bruno', 'imagem.jpg'],
                    ['Outro Morador do 11 A', 'outra-imagem.jpg']
                ]
            ];
        *
        *
        */
        $lista_nomes = [];

        foreach ($resultados as $tabela => $result) {
            foreach ($result as $key => $value) {

                if(!isset($retorno[$value->unidade]))
                    $retorno[$value->unidade] = [];

                if(!is_null($value->nome) && !in_array($value->unidade.$value->nome, $lista_nomes)){

                    $idade = ($value->dt_nascimento) ? Carbon::createFromFormat('Y-m-d', $value->dt_nascimento)->diffInYears() : null;

                    if($tabela == 'moradores'){
                      /*
                      $email = (Gate::forUser(Auth::portaria()->get())->allows('portaria-visualizar-dado', [$value->id, 'email'])) ? $value->email : '';
                      $telefone_fixo = (Gate::forUser(Auth::portaria()->get())->allows('portaria-visualizar-dado', [$value->id, 'telefone_fixo'])) ? $value->telefone_fixo : '';
                      $telefone_celular = (Gate::forUser(Auth::portaria()->get())->allows('portaria-visualizar-dado', [$value->id, 'telefone_celular'])) ? $value->telefone_celular : '';
                      $telefone_comercial = (Gate::forUser(Auth::portaria()->get())->allows('portaria-visualizar-dado', [$value->id, 'telefone_comercial'])) ? $value->telefone_comercial : '';
                      */
                      $email = $value->email;
                      $telefone_fixo = $value->telefone_fixo;
                      $telefone_celular = $value->telefone_celular;
                      $telefone_comercial = $value->telefone_comercial;
                    }else{
                      $email = '';
                      $telefone_fixo = '';
                      $telefone_celular = '';
                      $telefone_comercial = '';
                    }

                    $retorno[$value->unidade][] = [
                      'nome' => $value->nome,
                      'foto' => $value->foto,
                      'foto_fullpath' => $value->foto_fullpath,
                      'idade' => $idade,
                      'email' => $email,
                      'telefone_fixo' => $telefone_fixo,
                      'telefone_celular' => $telefone_celular,
                      'telefone_comercial' => $telefone_comercial,
                    ];

                    array_push($lista_nomes, $value->unidade.$value->nome);
                }
            }
        }

        //return DB::getQueryLog();
        return $retorno;
    }

}
