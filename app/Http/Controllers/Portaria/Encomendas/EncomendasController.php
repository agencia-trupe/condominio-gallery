<?php

namespace Gallery\Http\Controllers\Portaria\Encomendas;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Events\PortariaCadastrouEncomenda;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Portaria\BasePortariaController;

use Carbon\Carbon as Carbon;

use Gallery\Models\Encomenda;
use Gallery\Models\Unidade;
use Gallery\Models\NotificacaoMorador;

class EncomendasController extends BasePortariaController
{

  public function getIndex()
  {
    $hoje = Carbon::now();

    $listaUnidadesJson = Unidade::get()->pluck('resumo')->toJson();

    $listaAvisos = Encomenda::ordenado()->naoEntregues()->get();

    return view('portaria.encomendas.index', [
      'hoje' => $hoje,
      'listaUnidadesJson' => $listaUnidadesJson,
      'listaAvisos' => $listaAvisos
    ]);
  }

  public function postArmazenar(Request $request)
  {
    $unidade = $request->input('termo_busca_unidade');

    $unidade = Unidade::acharPorResumo($unidade)->first();

    if($unidade){

      // armazenar aviso de correspondencias
      $encomenda = $unidade->encomendas()->create([]);
      Event::fire( new AtividadeRastreavel('portaria_cadastrou_encomenda', $encomenda, Auth::portaria()->get()));

      // notificar usuários da unidade
      Event::fire( new PortariaCadastrouEncomenda($encomenda) );

      if($request->ajax()){
        return [
          'resumo' => $encomenda->unidade->getResumo(),
          'data' => $encomenda->created_at->format("d/m/Y · H:i \h"),
          'id' => $encomenda->id
        ];
      }
    }

    return redirect()->back();
  }

  public function getMarcarEntregue(Request $request, $encomendas_id)
  {
    // MARCAR aviso de correspondencia COMO ENTREGUE
    // Remover notificações para todos os usuários
    $encomenda = Encomenda::findOrFail($encomendas_id);

    $encomenda->is_entregue = 1;
    $encomenda->entregue_em = Carbon::now()->format('Y-m-d H:i:s');
    $encomenda->save();

    Event::fire( new AtividadeRastreavel('portaria_marcou_encomenda_entregue', $encomenda, Auth::portaria()->get()));

    // remover notificação
    NotificacaoMorador::where('encomendas_id', $encomendas_id)->delete();
  }

  public function getRemover(Request $request, $encomendas_id)
  {
    $encomenda = Encomenda::findOrFail($encomendas_id);
    Event::fire( new AtividadeRastreavel('portaria_removeu_encomenda', $encomenda, Auth::portaria()->get()));
    $encomenda->delete();
  }

}
