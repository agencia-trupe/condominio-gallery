<?php

namespace Gallery\Http\Controllers\Portaria\Comunicacao;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Events\PortariaEnviouComunicacao;

use Carbon\Carbon as Carbon;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Portaria\BasePortariaController;

use Gallery\Models\ComunicacaoAdministracao;

class ComunicacaoController extends BasePortariaController
{

  public function getIndex($slug_destinatario)
  {
    return view('portaria.comunicacao.index', [
      'slug_destinatario' => $slug_destinatario
    ]);
  }

  public function postEnviar(Request $request)
  {
    $this->validate($request, [
      'comunicacao.titulo'     => 'required',
      'comunicacao.prioridade' => 'required',
      'comunicacao.descricao'  => 'required'
    ]);

    $object = new ComunicacaoAdministracao();

    $object->titulo = $request->input('comunicacao.titulo');
    $object->prioridade = $request->input('comunicacao.prioridade');
    $object->descricao = $request->input('comunicacao.descricao');
    $object->destinatario = $request->input('comunicacao.destinatario');
    $object->remetente = 'portaria';

    try {

      $object->save();

      Event::fire( new PortariaEnviouComunicacao($object));
      Event::fire( new AtividadeRastreavel('portaria_enviou_comunicacao', $object, Auth::portaria()->get()));

      $request->session()->flash('sucesso', 'Mensagem enviada com sucesso.');

      return redirect()->route('portaria.comunicacao.index', $object->destinatario);

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao enviar Mensagem! ('.$e->getMessage().')'));

    }
  }

}
