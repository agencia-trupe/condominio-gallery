<?php

namespace Gallery\Http\Controllers\Portaria\Lavanderia;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Carbon\Carbon as Carbon;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Portaria\BasePortariaController;

use Gallery\Models\LavanderiaFichas;
use Gallery\Models\Unidade;
use Gallery\Models\Morador;

class LavanderiaController extends BasePortariaController
{

  public function getIndex(){
    $listaUnidadesJson = Unidade::get()->pluck('resumo')->toJson();

    return view('portaria.lavanderia.index', ['listaUnidadesJson' => $listaUnidadesJson]);
  }

  public function postSolicitar(Request $request){

    $this->validate($request, [
      'lavanderia.quantidade' => 'required|numeric|min:1',
      'lavanderia.unidade'    => 'required',
      'email'      => 'required',
      'password'   => 'required'
    ]);

    // Verificar USUARIO + SENHA
    // Se autenticar, verificar se é da UNIDADE
    // Se ambos checarem, cadastrar fichas

    if(Auth::moradores()->validate($request->only(['email', 'password']))) {

      $morador = Morador::where('email', $request->input('email'))->first();

      if($morador->unidade->resumo == strtoupper($request->input('lavanderia.unidade'))){

        // Cadastrar fichas e redirecionar de volta
        try {
          $ficha = new LavanderiaFichas(['quantidade' => $request->input('lavanderia.quantidade')]);

          $morador->fichas_lavanderia()->save($ficha);

          Event::fire( new AtividadeRastreavel('fichas_lavanderia_cadastradas', $ficha, Auth::portaria()->get()));

          $request->session()->flash('sucesso_fichas', 'Fichas reservadas com sucesso!');
          $request->session()->flash('cadastro_fichas', [
            'data'        => $ficha->created_at->format('d/m/Y · H\hi'),
            'apartamento' => $ficha->morador->unidade->resumo,
            'quantidade'  => $ficha->quantidade
          ]);

          return redirect()->back();

        } catch (Exception $e) {

          $request->flash();
          return redirect()->back()->withErrors('Erro ao cadastrar fichas de lavanderia. '.$e->getMessage());

        }

      }else{

        $request->flash();
        return redirect()->back()->withErrors('A unidade informada não é a mesma do cadastro');

      }

    }else{

      $request->flash();
      return redirect()->back()->withErrors('Credenciais Inválidas');

    }
  }

  public function postConsultar(Request $request){
    $unidade = strtoupper($request->input('unidade'));

    $fichas = LavanderiaFichas::whereHas('morador.unidade', function ($query) use ($unidade) {
      $query->whereRaw("CONCAT_WS('', `unidade`, `bloco`) = '{$unidade}'");
    })->naoRemovidos()
      ->naoFaturados()
      ->emAberto()
      ->get();

    return $fichas;
  }

  public function postRemover(Request $request)
  {
    $ficha_id = $request->ficha_id;

    $ficha = LavanderiaFichas::find($ficha_id);

    if(!$ficha) return 0;

    $ficha->is_retirada_solicitada = 1;
    $ficha->motivo_cancelamento = $request->motivo_cancelamento;
    $ficha->save();

    Event::fire( new AtividadeRastreavel('fichas_lavanderia_removidas', $ficha, Auth::portaria()->get()));

    return 1;
  }

}
