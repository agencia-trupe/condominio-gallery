<?php

namespace Gallery\Http\Controllers\Portaria\Unidade;

use Illuminate\Http\Request;
use Gallery\Http\Controllers\Portaria\BasePortariaController;

use Gallery\Models\Unidade;
use Gallery\Models\Espaco;
use Gallery\Models\EspacoReservaPorDiaria;

class UnidadeController extends BasePortariaController
{

    public function getDetalhes(Request $request, $sigla_unidade)
    {
    	$unidade = Unidade::eagerLoadAll()->acharPorResumo($sigla_unidade)->first();

        if(!$unidade) abort('404');

        return view('portaria.unidades.detalhes', compact('unidade'));
    }

}