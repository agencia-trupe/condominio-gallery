<?php

namespace Gallery\Http\Controllers\Moradores;

use Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\NotificacaoMorador;

use Gallery\Models\AgendaAdministracao;
use Gallery\Models\AgendamentoDeMudanca;
use Gallery\Models\EspacoReservaPorDiaria;
use Gallery\Models\EspacoReservaPorPeriodo;
use Gallery\Models\FestaParticular;

class BaseMoradoresController extends Controller
{

	private $dias_da_agenda = 7;

	public function __construct()
	{

		// Filtro de Usuário
		// As rotas desse controller só são acessíveis para
		// quem estiver logado

		$this->middleware('auth.moradores');

		if(Auth::moradores()->check())
			$this->carregarNotificacoes();
	}

	protected function carregarNotificacoes()
	{
		$notificacoes['ocorrencias'] = NotificacaoMorador::ocorrencias()
    																								 ->paraMorador(Auth::moradores()->get()->id)
                                           					 ->get();

    $notificacoes['correspondencias'] = NotificacaoMorador::correspondencias()
                                                					->paraMorador(Auth::moradores()->get()->id)
                                                					->get();

    $notificacoes['encomendas'] = NotificacaoMorador::encomendas()
                                          					->paraMorador(Auth::moradores()->get()->id)
                                          					->get();

    $notificacoes['agenda'] = $this->consultarNotificacoesAgenda();

    $total = sizeof($notificacoes['ocorrencias'])
					 + sizeof($notificacoes['correspondencias'])
					 + sizeof($notificacoes['encomendas'])
					 + sizeof($notificacoes['agenda']['agenda_adm'])
					 + sizeof($notificacoes['agenda']['mudanca'])
					 + sizeof($notificacoes['agenda']['reservas_diarias'])
					 + sizeof($notificacoes['agenda']['reservas_periodo'])
					 + sizeof($notificacoes['agenda']['festa_particular']);

    $notificacoes['alguma'] = $total > 0;

    view()->share(compact('notificacoes'));
  }

	private function consultarNotificacoesAgenda()
	{
		$hoje = Carbon::now()->format('Y-m-d');
		$uma_semana = Carbon::now()->addDays($this->dias_da_agenda)->format('Y-m-d');

		$retorno['agenda_adm']			 = $this->notificacoes_agenda_adm($hoje, $uma_semana);
		$retorno['mudanca'] 				 = $this->notificacoes_mudanca($hoje, $uma_semana);
		$retorno['reservas_diarias'] = $this->notificacoes_reservas_diarias($hoje, $uma_semana);
		$retorno['reservas_periodo'] = $this->notificacoes_reservas_periodo($hoje, $uma_semana);
		$retorno['festa_particular'] = $this->notificacoes_festa_particular($hoje, $uma_semana);

		$retorno['msg'] = $this->gerarMensagemNotificacao($retorno);

		return $retorno;
	}

	private function gerarMensagemNotificacao($retorno)
	{
		$msg = "";

		if(sizeof($retorno['agenda_adm'])){
			$msg .= "Agenda da Administração:<br>";
				foreach($retorno['agenda_adm'] as $notif){
					$msg .= $notif->data->format('d/m/Y') . " - " . $notif->titulo . "<br>";
				}
			$msg .= "<br>";
		}

		if(sizeof($retorno['mudanca'])){
			$msg .= "Agendamento de Mudança:<br>";
				foreach($retorno['mudanca'] as $notif){
					$msg .= $notif->data->format('d/m/Y') . " - " . $notif->getPeriodoFormatado() . "<br>";
				}
			$msg .= "<br>";
		}

		if(sizeof($retorno['reservas_diarias']) || sizeof($retorno['reservas_periodo'])){

			$msg .= "Reservas:<br>";

				foreach($retorno['reservas_diarias'] as $notif){
					$msg .= $notif->espacoReservado->titulo . " - ";
					$msg .= $notif->titulo . " - ";
					$msg .= $notif->reserva->format('d/m/Y') . " - ";
					$msg .= $notif->horario . "<br>";
				}

				foreach($retorno['reservas_periodo'] as $notif){
					$msg .= $notif->espacoReservado->titulo . " - ";
					$msg .= $notif->reserva->format('d/m/Y H:i');
					$msg .= "<br>";
				}

			$msg .= "<br>";
		}

		if(sizeof($retorno['festa_particular'])){
			$msg .= "Festas Particulares:<br>";
				foreach($retorno['festa_particular'] as $notif){
					$msg .= $notif->titulo . " - " . $notif->reserva->format('d/m/Y') . " - " . $notif->horario . "<br>";
				}
			$msg .= "<br>";
		}

		return $msg;
	}

	private function notificacoes_agenda_adm($data_inicio, $data_final)
	{
		return AgendaAdministracao::where('data', '>=', $data_inicio)
															->where('data', '<=', $data_final)
															->ordenado()
															->get();
	}

	private function notificacoes_mudanca($data_inicio, $data_final)
	{
		return AgendamentoDeMudanca::where('data', '>=', $data_inicio)
															 ->where('data', '<=', $data_final)
															 ->agendadoPor(Auth::moradores()->get()->id)
															 ->ordenado()
															 ->get();
	}

	private function notificacoes_reservas_diarias($data_inicio, $data_final)
	{
		return EspacoReservaPorDiaria::moradorLogado()
																 ->where('reserva', '>=', $data_inicio)
														 		 ->where('reserva', '<=', $data_final)
																 ->ordenado()
																 ->get();
	}

	private function notificacoes_reservas_periodo($data_inicio, $data_final)
	{
		return EspacoReservaPorPeriodo::moradorLogado()
																 ->where('reserva', '>=', $data_inicio)
														  	 ->where('reserva', '<=', $data_final)
																 ->ordenado()
																 ->get();
	}

	private function notificacoes_festa_particular($data_inicio, $data_final)
	{
		return FestaParticular::where('reserva', '>=', $data_inicio)
													->where('reserva', '<=', $data_final)
													->moradorLogado()
													->ordenado()
													->get();
	}

}
