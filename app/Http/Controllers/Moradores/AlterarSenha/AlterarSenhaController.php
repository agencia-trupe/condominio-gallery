<?php

namespace Gallery\Http\Controllers\Moradores\AlterarSenha;

use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Gallery\Http\Requests\Moradores\AlterarSenhaRequest;
use Gallery\Repositories\Moradores\Auth\AuthRepositoryInterface;
use Gallery\Services\Logger\LoggerInterface;

class AlterarSenhaController extends BaseMoradoresController
{

  protected $authRepository;
  protected $logger;

  public function __construct(
    AuthRepositoryInterface $authRepository,
    LoggerInterface $logger
  )
  {
    $this->logger = $logger;
    $this->authRepository = $authRepository;
  }

  public function getIndex()
  {
    return view('moradores.alterar-senha.form');
  }

  public function postAlterar(AlterarSenhaRequest $request)
  {
    $user = $this->authRepository->getUser();

    try {

      $this->authRepository->updatePassword($request->input('moradores.senha_nova'));

      $this->logger->log('senha_morador_alterada', $user, $user);

      $request->session()->flash('senha_alterada', true);

    } catch (\Exception $e) {
      $request->session()->flash('senha_alterada_fail', $e->getMessage());
    }

    return redirect()->route('moradores.alterar-senha.index');
  }
}
