<?php

namespace Gallery\Http\Controllers\Moradores\Auth;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Password;
use Gallery\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Password Reset Controller
  |--------------------------------------------------------------------------
  |
  | This controller is responsible for handling password reset requests
  | and uses a simple trait to include this behavior. You're free to
  | explore this trait and override any methods you wish to tweak.
  |
  */

  use ResetsPasswords;

  protected $emailView = 'moradores.templates.emails.redefinicao-senha';

  protected $subject = 'Redefinição de Senha - Gallery Online';

  protected $redirectPath = 'moradores/auth/login';

  protected $passwordValidator = 'validarReset';

  /**
   * Create a new password controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    // Rota somente acessível para quem não estiver logado
    $this->middleware('guest.moradores');
  }

  /**
   * Display the form to request a password reset link.
   *
   * @return \Illuminate\Http\Response
   */
  public function getEmail()
  {
    return view('moradores.auth.recuperar');
  }

  /**
   * Send a reset link to the given user.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function postEmail(Request $request)
  {
    $this->validate($request, ['email' => 'required|email']);

    $response = Password::moradores()->sendResetLink($request->only('email'), function (\Illuminate\Mail\Message $message) {
      $message->subject($this->getEmailSubject());
    });

    $data['token'] = '__token_mockup__';
    $data['type'] = 'moradores';
    if(config('mail.pretend')) \Log::info(\View::make('moradores.templates.emails.redefinicao-senha', $data)->render());

    switch ($response) {
      case Password::RESET_LINK_SENT:
        return redirect()->back()->with('status', trans($response));

      case Password::INVALID_USER:
        return redirect()->back()->withErrors(['email' => trans($response)]);
    }
  }

  /**
   * Display the password reset view for the given token.
   *
   * @param  string  $token
   * @return \Illuminate\Http\Response
   */
  public function getReset($type = 'moradores', $token = null)
  {
    if (is_null($token)) {
      throw new NotFoundHttpException;
    }

    return view('moradores.auth.redefinir')->with('token', $token);
  }

  /**
   * Reset the given user's password.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function postReset(Request $request)
  {
    $this->validate($request, [
      'token' => 'required',
      'email' => 'required|email',
      'password' => 'required|confirmed',
    ]);

    $credentials = $request->only(
      'email', 'password', 'password_confirmation', 'token'
    );

    Password::moradores()->validator( function(array $credentials){
      list($password, $confirm) = [
        $credentials['password'],
        $credentials['password_confirmation'],
      ];

      return $password === $confirm;
    });

    $response = Password::moradores()->reset($credentials, function ($user, $password) {
      $this->resetPassword($user, $password);
    });

    Event::fire( new AtividadeRastreavel('senha_morador_alterada', $request->email, ''));

    switch ($response) {
      case Password::PASSWORD_RESET:
        return redirect($this->redirectPath())->with('redefinicao_realizada', true);

      default:
        return redirect()->back()
                         ->withInput($request->only('email'))
                         ->withErrors(['email' => trans($response)]);
      }
    }

   /**
   * Reset the given user's password.
   *
   * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
   * @param  string  $password
   * @return void
   */
  protected function resetPassword($user, $password)
  {
    $user->senha = $password;
    $user->save();
  }

}
