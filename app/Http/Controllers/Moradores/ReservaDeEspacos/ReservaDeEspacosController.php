<?php

namespace Gallery\Http\Controllers\Moradores\ReservaDeEspacos;

use DB;
use Auth;
use Event;
use DateTime;
use Carbon\Carbon as Carbon;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;
use Gallery\Events\MoradorReservouEspacoPorPeriodo;
use Gallery\Events\MoradorReservouEspacoPorDiaria;
use Gallery\Events\MoradorCancelouDiariaComMulta;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Models\Espaco;
use Gallery\Models\EspacoReservaPorPeriodo;
use Gallery\Models\EspacoReservaPorDiaria;
use Gallery\Models\EspacoReservaPorDiariaConvidados;
use Gallery\Models\Regulamento;

class ReservaDeEspacosController extends BaseMoradoresController
{

    public function getIndex()
    {
    	return view('moradores.reserva-de-espacos.index', ['espacos' => Espaco::ordenado()->get()]);
    }

    public function getRegrasDeUso($espaco)
    {
      $arquivo = $espaco->regras_de_uso;
      $caminho_completo = "assets/documentos/{$arquivo}";

      if($arquivo != '' && file_exists($caminho_completo)){
        return response()->download($caminho_completo);
      }else{
        abort('404');
      }
    }

    public function getGrid($slug_espaco, $data_inicial = null)
    {
    	$espaco = Espaco::slug($slug_espaco)->first();

        if(is_null($data_inicial)) $data_inicial = date('Y-m-d');


        if($espaco->tipo == 'por_periodo'){

            $hoje_mais_3_dias = Carbon::createFromFormat('Y-m-d', $data_inicial);;
            $hoje_mais_3_dias->addDays(3);

            list($abertura_horas, $abertura_minutos) = explode(':', $espaco->informacoes->horario_abertura);
            list($encerramento_horas, $encerramento_minutos) = explode(':', $espaco->informacoes->horario_encerramento);

            $horario_inicial = Carbon::createFromTime($abertura_horas, $abertura_minutos, 0);
            $horario_final = Carbon::createFromTime($encerramento_horas, $encerramento_minutos, 0);

            $timestamp_minhas_reservas = $this->getTimestampsPeriodoArray($espaco->id, true, $data_inicial);
            $timestamp_reservas_outros_usuarios = $this->getTimestampsPeriodoArray($espaco->id, false, $data_inicial);
            $timestamp_bloqueios = $this->getTimestampsBloqueiosArray($espaco->id, $data_inicial);

            return view('moradores.reserva-de-espacos.por_periodo.index', [

                'espaco' => $espaco,
                'timestamp_minhas_reservas' => $timestamp_minhas_reservas,
                'timestamp_reservas_outros_usuarios' => $timestamp_reservas_outros_usuarios,
                'timestamp_bloqueios' => $timestamp_bloqueios,

                'hoje_mais_3_dias'  => $hoje_mais_3_dias,
                'horario_inicial'   => $horario_inicial,
                'horario_final'     => $horario_final,
                'tempo_de_reserva'  => $espaco->informacoes->tempo_de_reserva,
                'data_inicial'      => $data_inicial
            ]);

        }elseif($espaco->tipo == 'diaria'){

            $timestamp_reservas = $this->getTimestampsDiariaArray($espaco->id, false, $data_inicial); //['2015-12-06', '2015-12-08', '2015-12-10'];

            $reservas_do_morador = EspacoReservaPorDiaria::espaco($espaco->id)
                                                         ->moradorLogado()
                                                         ->datasFuturas()
                                                         ->ordenado()
                                                         ->get();

            return view('moradores.reserva-de-espacos.diarias.index', [
                'espaco' => $espaco,
                'data_inicial' => $data_inicial,
                'timestamp_reservas' => json_encode($timestamp_reservas),
                'reservas_do_morador' => $reservas_do_morador
            ]);
        }
    }

    public function postReservar(Request $request, $slug_espaco)
    {
        $espaco = Espaco::slug($slug_espaco)->first();

        if(!$espaco) abort('404');

        if($espaco->tipo == 'por_periodo'){

            $data_inicial = Carbon::now()->format('Y-m-d');

            $agendamento = $request->input('reserva.agendamentos');
            $anotacoes   = $request->input('reserva.anotacoes');
            $titulo      = $request->input('reserva.titulo');

            try {

                $check = EspacoReservaPorPeriodo::espaco($espaco->id)->acharPorHorario($agendamento)->get();

                if(sizeof($check) == 0){

                    $reserva = EspacoReservaPorPeriodo::create([
                        'espacos_id' => $espaco->id,
                        'moradores_id' => Auth::moradores()->get()->id,
                        'unidades_id' => Auth::moradores()->get()->unidades_id,
                        'reserva' => $agendamento,
                        'anotacoes' => $anotacoes
                    ]);

                    Event::fire( new MoradorReservouEspacoPorPeriodo($reserva));
                    Event::fire( new AtividadeRastreavel('morador_reservou_espaco_periodo', $reserva, Auth::moradores()->get()));

                    $data_inicial = Carbon::createFromFormat('Y-m-d H:i', $agendamento)->format('Y-m-d');

                    $request->session()->flash('reserva_de_espaco_feita', true);

                }

            } catch (Exception $e) {

                $request->session()->flash('reserva_de_espaco_feita', false);

            }

            return redirect()->route('moradores.reserva-de-espacos.grid', [$slug_espaco, $data_inicial]);

        }else{

            $reserva = $request->input('reserva');
            $titulo  = $request->input('titulo_festa');
            $horario  = $request->input('horario_festa');

            try {

                $check = EspacoReservaPorDiaria::acharPorData($reserva)
                                                ->espaco($espaco->id)
                                                ->get();

                if(sizeof($check) == 0){

                    $reserva = Carbon::createFromFormat('d/m/Y', $reserva);
                    $data_maxima_cancelamento = $this->dataMaximaCancelamento($espaco, $reserva);

                    $regulamentos = Regulamento::first();

                    return view('moradores.reserva-de-espacos.diarias.aviso', [
                        'reserva' => $reserva,
                        'espaco'  => $espaco,
                        'titulo'  => $titulo,
                        'horario' => $horario,
                        'regulamentos' => $regulamentos,
                        'data_maxima_cancelamento' => $data_maxima_cancelamento
                    ]);

                }else{
                    $request->session()->flash('data_ja_reservada', true);
                }

            } catch (Exception $e) {
                $request->session()->flash('reserva_de_espaco_feita', false);
            }

            return redirect()->route('moradores.reserva-de-espacos.grid', [$slug_espaco]);
        }
    }

    public function postConfirmarDiaria(Request $request, $slug_espaco)
    {
        $espaco = Espaco::slug($slug_espaco)->first();


        if(!$espaco) abort('404');

        if(!$request->has('aceite_termos') || $request->input('aceite_termos') != 1) return redirect()->back();

        $reserva = $request->input('reserva');
        $titulo = $request->input('titulo');
        $horario = $request->input('horario');

        $check = EspacoReservaPorDiaria::acharPorData($reserva)
                                        ->espaco($espaco->id)
                                        ->get();

        if(sizeof($check) == 0){

            $insert = EspacoReservaPorDiaria::create([
                'espacos_id' => $espaco->id,
                'moradores_id' => Auth::moradores()->get()->id,
                'unidades_id' => Auth::moradores()->get()->unidades_id,
                'titulo' => $titulo,
                'horario' => $horario,
                'reserva' => $reserva
            ]);

            Event::fire( new MoradorReservouEspacoPorDiaria($insert));
            Event::fire( new AtividadeRastreavel('morador_reservou_espaco_diaria', $insert, Auth::moradores()->get()));

            $request->session()->flash('reserva_de_espaco_feita', true);

        }

        return redirect()->route('moradores.reserva-de-espacos.grid', [$slug_espaco]);
    }

    public function postVerificarMulta(Request $request, $diaria_id)
    {
        $reserva = EspacoReservaPorDiaria::find($diaria_id);

        $dias_carencia = $reserva->espacoReservado->informacoes->dias_carencia;
        $data_maxima   = clone $reserva->reserva;
        $hoje          = Carbon::now();
        $retorno       = [];

        $data_maxima->subDays($dias_carencia);

        $retorno['dias_carencia'] = $dias_carencia;

        $retorno['slug_espaco'] = $reserva->espacoReservado->slug;
        $retorno['timestamp_reserva'] = $reserva->reserva->format('Y-m-d');

        if( $data_maxima->lt($hoje) )
            $retorno['tem_multa'] = true;
        else
            $retorno['tem_multa'] = false;

        return $retorno;
    }

    public function getDestroy(Request $request, $slug_espaco, $timestamp_reserva)
    {
        $espaco = Espaco::slug($slug_espaco)->first();

        if(!$espaco) abort('404');

        if($espaco->tipo == 'por_periodo'){

            try {

                $timestamp_reserva = str_replace('_', ' ', $timestamp_reserva);

                $reserva = EspacoReservaPorPeriodo::horario($timestamp_reserva)
                                                  ->espaco($espaco->id)
                                                  ->moradorLogado()
                                                  ->first();

                if($reserva){
                  Event::fire( new AtividadeRastreavel('morador_excluiu_reserva_periodo', $reserva, Auth::moradores()->get()));
                  $reserva->delete();
                }

                $data_inicial = Carbon::createFromFormat('Y-m-d H:i', $timestamp_reserva)->format('Y-m-d');

                $request->session()->flash('reserva_de_espaco_cancelada', true);

            } catch (Exception $e) {
                $request->session()->flash('reserva_de_espaco_cancelada', false);
            } finally {
                return redirect()->route('moradores.reserva-de-espacos.grid', [$slug_espaco, $data_inicial]);
            }


        }elseif($espaco->tipo == 'diaria'){

            try {

                $reserva = EspacoReservaPorDiaria::data($timestamp_reserva)
                                                 ->espaco($espaco->id)
                                                 ->moradorLogado()
                                                 ->first();

                if($reserva){

                    $regras_cancelamento = $this->postVerificarMulta($request, $reserva->id);

                    if($regras_cancelamento['tem_multa'] == true){

                        $reserva->removerNotificacoes();

                        Event::fire( new MoradorCancelouDiariaComMulta($reserva));
                        Event::fire( new AtividadeRastreavel('morador_excluiu_reserva_diaria_com_multa', $reserva, Auth::moradores()->get()));

                        $reserva->removerMarcacaoDeLido();

                        $reserva->delete();

                    }else{
                        Event::fire( new AtividadeRastreavel('morador_excluiu_reserva_diaria_sem_multa', $reserva, Auth::moradores()->get()));
                        $reserva->forceDelete();
                    }

                }

                $request->session()->flash('reserva_de_espaco_cancelada', true);

            } catch (Exception $e) {
                $request->session()->flash('reserva_de_espaco_cancelada', false);
            } finally {
                return redirect()->route('moradores.reserva-de-espacos.grid', $slug_espaco);
            }

        }
    }

    public function getTabelaReservasPorPeriodo(Request $request, $data_inicial, $slug_espaco)
    {
        $espaco = Espaco::slug($slug_espaco)->first();


        $hoje_mais_3_dias = Carbon::createFromFormat('Y-m-d', $data_inicial);
        $hoje_mais_3_dias->addDays(3);

        list($abertura_horas, $abertura_minutos) = explode(':', $espaco->informacoes->horario_abertura);
        list($encerramento_horas, $encerramento_minutos) = explode(':', $espaco->informacoes->horario_encerramento);

        $horario_inicial = Carbon::createFromTime($abertura_horas, $abertura_minutos, 0);
        $horario_final = Carbon::createFromTime($encerramento_horas, $encerramento_minutos, 0);

        $timestamp_minhas_reservas = $this->getTimestampsPeriodoArray($espaco->id, true, $data_inicial);
        $timestamp_reservas_outros_usuarios = $this->getTimestampsPeriodoArray($espaco->id, false, $data_inicial);

        return view('moradores.reserva-de-espacos.por_periodo.tabela', [

            'espaco'      => $espaco,
            'timestamp_minhas_reservas' => $timestamp_minhas_reservas,
            'timestamp_reservas_outros_usuarios' => $timestamp_reservas_outros_usuarios,

            'hoje_mais_3_dias'  => $hoje_mais_3_dias,
            'horario_inicial'   => $horario_inicial,
            'horario_final'     => $horario_final,
            'tempo_de_reserva'  => $espaco->informacoes->tempo_de_reserva,
            'data_inicial' => $data_inicial
        ]);
    }

    public function getDetalhes($slug_espaco, $id_reserva)
    {
        $espaco = Espaco::slug($slug_espaco)->first();

        if(!$espaco || !$id_reserva) return redirect()->route('moradores.reserva-de-espacos.grid', $espaco->slug);

        $reserva = EspacoReservaPorDiaria::find($id_reserva);

        if(!$reserva || $reserva->moradores_id != Auth::moradores()->get()->id) return redirect()->route('moradores.reserva-de-espacos.grid', $espaco->slug);

        return view('moradores.reserva-de-espacos.diarias.detalhes', compact('reserva', 'espaco'));
    }

    public function getConvidados($slug_espaco, $id_reserva)
    {
        $espaco = Espaco::slug($slug_espaco)->first();

        if(!$espaco || !$id_reserva) return redirect()->route('moradores.reserva-de-espacos.grid', $espaco->slug);

        $reserva = EspacoReservaPorDiaria::find($id_reserva);

        if(!$reserva || $reserva->moradores_id != Auth::moradores()->get()->id) return redirect()->route('moradores.reserva-de-espacos.grid', $espaco->slug);

        return view('moradores.reserva-de-espacos.diarias.convidados', compact('reserva', 'espaco'));
    }

    public function postConvidados(Request $request, $slug_espaco, $id_reserva)
    {
        $espaco = Espaco::slug($slug_espaco)->first();

        if(!$espaco || !$id_reserva) return redirect()->route('moradores.reserva-de-espacos.grid', $espaco->slug);

        $reserva = EspacoReservaPorDiaria::find($id_reserva);

        if(!$reserva || $reserva->moradores_id != Auth::moradores()->get()->id) return redirect()->route('moradores.reserva-de-espacos.grid', $espaco->slug);

        $nomes = $request->input('lista_convidados');

        EspacoReservaPorDiariaConvidados::where('reservas', '=', $id_reserva)->delete();

        Event::fire( new AtividadeRastreavel('morador_inseriu_lista_de_convidado', $reserva, Auth::moradores()->get()));

        if($nomes && is_array($nomes)){
            foreach ($nomes as $key => $value) {
                $convidado = EspacoReservaPorDiariaConvidados::create([
                    'reservas' => $id_reserva,
                    'nome' => $value,
                ]);
                Event::fire( new AtividadeRastreavel('morador_inseriu_convidado', $convidado, Auth::moradores()->get()));
            }
        }

        $request->session()->flash('lista_convidados_inserida', true);

        return redirect()->route('moradores.reserva-de-espacos.reservar', [$slug_espaco]);
    }

    /*******************************************************/

    private function getTimestampsPeriodoArray($espaco_id, $somente_usuario_logado = true, $data_inicial = null)
    {
        $array_retorno = [];

        if($somente_usuario_logado){

            $resultados = EspacoReservaPorPeriodo::semBloqueios()
                                                 ->aPartirDeData($data_inicial)
                                                 ->proximosTresDias($data_inicial)
                                                 ->espaco($espaco_id)
                                                 ->moradorLogado()
                                                 ->get();

        }else{

            $resultados = EspacoReservaPorPeriodo::semBloqueios()
                                                 ->aPartirDeData($data_inicial)
                                                 ->proximosTresDias($data_inicial)
                                                 ->espaco($espaco_id)
                                                 ->get();

        }

        foreach ($resultados as $key => $value)
            $array_retorno[$value->reserva->format('Y-m-d H:i')] = ($somente_usuario_logado) ? $value->anotacoes : $value->nome;

        return $array_retorno;
    }

    private function getTimestampsDiariaArray($espaco_id, $somente_usuario_logado = true, $data_inicial = null)
    {
        $array_retorno = [];

        $resultados = EspacoReservaPorDiaria::espaco($espaco_id);

        if($somente_usuario_logado)
            $resultados = $resultados->where('moradores.id', '=', Auth::moradores()->get()->id);

        $resultados = $resultados->get();

        foreach ($resultados as $key => $value)
            $array_retorno[] = $value->reserva->format('Y-m-d');

        return $array_retorno;
    }

    private function getTimestampsBloqueiosArray($espaco_id, $data_inicial = null)
    {
        $array_retorno = [];

        $resultados = EspacoReservaPorPeriodo::bloqueios()
                                             ->aPartirDeData($data_inicial)
                                             ->proximosTresDias($data_inicial)
                                             ->espaco($espaco_id)
                                             ->get();

        foreach ($resultados as $key => $value)
            $array_retorno[$value->reserva->format('Y-m-d H:i')] = '';

        return $array_retorno;
    }

    private function dataMaximaCancelamento($espaco, $data_reserva)
    {
        $dias_carencia = $espaco->informacoes->dias_carencia;
        $data_maxima   = clone $data_reserva;
        $hoje          = Carbon::now();

        $data_maxima->subDays($dias_carencia);

        if( $data_maxima->lt($hoje) )
        {
            return "O Período para o cancelamento da reserva sem multa expirou.";
        }
        elseif( $data_maxima->eq($hoje) )
        {
            return "Segundo o Regulamento Interno você tem até <strong>hoje<strong> para cancelar a reserva sem multa.";
        }
        else
        {
            return "Segundo o Regulamento Interno você tem até dia <strong>{$data_maxima->formatLocalized("%e de %B de %Y")}</strong> para cancelar a reserva sem multa.";
        }
    }
}
