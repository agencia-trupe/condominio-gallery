<?php

namespace Gallery\Http\Controllers\Moradores\ChamadosDeManutencao;

use Event;
use Auth;
use Validator;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Events\MoradorAbriuChamadoDeManutencao;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Libs\Thumbs;

use Gallery\Models\ChamadoDeManutencao;
use Gallery\Models\ChamadoDeManutencaoFoto;
use Gallery\Models\ChamadoDeManutencaoLido;

class ChamadosDeManutencaoController extends BaseMoradoresController
{

	public function getIndex()
	{
    $historico = ChamadoDeManutencao::ordenado()
                                    ->doUsuarioLogado()
                                    ->get();

		return view('moradores.chamados-de-manutencao.index', compact('historico'));
	}

	public function postRegistrar(Request $request)
	{
    $this->validate($request, [
      'chamados_de_manutencao.data'        => 'required|date_format:d/m/Y',
      'chamados_de_manutencao.horario'     => 'required',
      'chamados_de_manutencao.localizacao' => 'required',
      'chamados_de_manutencao.descricao'   => 'required'
    ]);

    $moradores_id = Auth::moradores()->user()->id;

    try {

      $chamado = new ChamadoDeManutencao;

      $chamado->data = $request->input('chamados_de_manutencao.data');
      $chamado->horario = $request->input('chamados_de_manutencao.horario');
      $chamado->localizacao = $request->input('chamados_de_manutencao.localizacao');
      $chamado->descricao = $request->input('chamados_de_manutencao.descricao');
      $chamado->moradores_id = $moradores_id;

      $chamado->save();

			$fotos = $request->input('chamados_de_manutencao.fotos');

      // Cadastrar Fotos
      if($fotos && is_array($fotos)){
      	foreach ($fotos as $key => $value) {
      		$foto = ChamadoDeManutencaoFoto::create([
      			'foto' => $value,
      			'chamados_de_manutencao_id' => $chamado->id,
					]);
        }
      }
			
			Event::fire( new AtividadeRastreavel('chamado_manutencao_criado', $chamado, Auth::moradores()->get()));
      Event::fire( new MoradorAbriuChamadoDeManutencao($chamado));

      $request->session()->flash('chamados_de_manutencao_create', true);

    } catch (\Exception $e) {

      $request->flash();

      $request->session()->flash('chamados_de_manutencao_create', false);
    }

    return redirect()->route('moradores.chamados-de-manutencao.index');

	}

  public function postUploadFoto(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'foto' => 'mimes:png,jpg,jpeg|max:4000'
    ]);

    /*
    * O limite de upload da imagem (4Mb inicialmente) deve sempre ser
    * menor que o limite de upload do PHP no servidor, assim quando a imagem
    * for muito grande o erro lançado vai ser o do validador (tratado) e não
    * o do PHP (500 Internal Server Errror)
    */

    if ($validator->fails()) {
      return [
        'success' => 0,
        'msg' => $validator->errors()->first()
      ];
    }

    $arquivo = $request->file('foto');

    $path_original = "assets/images/moradores/chamados-de-manutencao/originais/";
    $path_asset    = "assets/images/moradores/chamados-de-manutencao/redimensionadas/";
    $path_thumb    = "assets/images/moradores/chamados-de-manutencao/thumbs/";
    $path_upload   = public_path($path_original);

    $filename = filename_slug($arquivo->getClientOriginalName(), $arquivo->getClientOriginalExtension());

    // Armazenar Original
    $arquivo->move($path_upload, $filename);

    $thumb = $path_thumb.$filename;

    // Armazenar Redimensionada
    Thumbs::makeFromFile($path_upload, $filename, 900, null, public_path($path_asset), 'rgba(0,0,0,0)', false);
    // Armazenar Thumb
    Thumbs::makeFromFile($path_upload, $filename, 400, 400, public_path($path_thumb), 'rgba(0,0,0,0)', true);

    if(file_exists(public_path($path_original.$filename)) &&
       file_exists(public_path($path_asset.$filename)) &&
       file_exists(public_path($path_thumb.$filename)))
    {
      return [
        'thumb' => $thumb,
        'filename' => $filename,
        'success' => 1,
        'msg' => ''
      ];
    }else{
      return [
        'success' => 0,
        'msg' => 'Não foi possível armazenar a imagem.'
      ];
    }
  }
}
