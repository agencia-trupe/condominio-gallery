<?php

namespace Gallery\Http\Controllers\Moradores\PessoasNaoAutorizadas;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Validator;
use Gallery\Libs\Thumbs;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\PessoaNaoAutorizada;
use Gallery\Models\PessoaNaoAutorizadaPrivacidade;

class PessoasNaoAutorizadasController extends BaseMoradoresController
{

    public function getIndex()
    {
    	$pessoas = PessoaNaoAutorizada::daUnidade()
    								    ->ordenado()
    								    ->get();

      return view('moradores.pessoas-nao-autorizadas.index', compact('pessoas'));
    }

    public function postStore(Request $request)
    {
        $this->validate($request, [
            'pessoas_nao_autorizadas.nome'            => 'required',
            'pessoas_nao_autorizadas.visibilidade'    => 'required',
            'pessoas_nao_autorizadas.data_nascimento' => 'required|date_format:d/m/Y',
            'pessoas_nao_autorizadas.parentesco'      => 'required'
        ]);

        $moradores_id = Auth::moradores()->user()->id;
        $unidades_id = Auth::moradores()->user()->unidade->id;

        try {

            $pessoa = new PessoaNaoAutorizada;

            $visibilidade = $request->input('pessoas_nao_autorizadas.visibilidade');

            $pessoa->nome = $request->input('pessoas_nao_autorizadas.nome');
            $pessoa->data_nascimento = $request->input('pessoas_nao_autorizadas.data_nascimento');
            $pessoa->parentesco = $request->input('pessoas_nao_autorizadas.parentesco');
            $pessoa->foto = $request->input('pessoas_nao_autorizadas.foto');
            $pessoa->moradores_id = $moradores_id;
            $pessoa->unidades_id = $unidades_id;

            $pessoa->save();

            $this->armazenaPrivacidade($pessoa, $visibilidade);

            Event::fire( new AtividadeRastreavel('pessoa_nao_autorizada_inserida', $pessoa, Auth::moradores()->get()));

            $request->session()->flash('pessoa_nao_autorizada_create', true);

        } catch (\Exception $e) {

            $request->flash();

            $request->session()->flash('pessoa_nao_autorizada_create', false);

            $request->session()->flash('exception', $e->getMessage());
        }

        return redirect()->route('moradores.pessoas-nao-autorizadas.index');
    }

    public function getEdit(Request $request, $pessoa_nao_autorizada_id)
    {
        $pessoas = PessoaNaoAutorizada::daUnidade()->ordenado()->get();
        $pessoa_nao_autorizada = PessoaNaoAutorizada::find($pessoa_nao_autorizada_id);

        if(!$pessoa_nao_autorizada) abort('404');

        return view('moradores.pessoas-nao-autorizadas.edit', [
            'pessoas' => $pessoas,
            'pessoa_nao_autorizada' => $pessoa_nao_autorizada,
        ]);
    }


    public function postUpdate(Request $request, $pessoa_nao_autorizada_id)
    {
        $this->validate($request, [
            'pessoas_nao_autorizadas.nome'            => 'required',
            'pessoas_nao_autorizadas.visibilidade'    => 'required',
            'pessoas_nao_autorizadas.data_nascimento' => 'required|date_format:d/m/Y',
            'pessoas_nao_autorizadas.parentesco'      => 'required',
            'pessoas_nao_autorizadas.foto'            => 'required'
        ]);

        $moradores_id = Auth::moradores()->user()->id;
        $unidades_id = Auth::moradores()->user()->unidade->id;

        $pessoa = PessoaNaoAutorizada::find($pessoa_nao_autorizada_id);

        if(!$pessoa) abort('404');

        try {

            $visibilidade = $request->input('pessoas_nao_autorizadas.visibilidade');

            $pessoa->nome = $request->input('pessoas_nao_autorizadas.nome');
            $pessoa->data_nascimento = $request->input('pessoas_nao_autorizadas.data_nascimento');
            $pessoa->parentesco = $request->input('pessoas_nao_autorizadas.parentesco');
            $pessoa->foto = $request->input('pessoas_nao_autorizadas.foto');
            $pessoa->moradores_id = $moradores_id;
            $pessoa->unidades_id = $unidades_id;

            $pessoa->save();

            Event::fire( new AtividadeRastreavel('pessoa_nao_autorizada_alterada', $pessoa, Auth::moradores()->get()));

            $this->armazenaPrivacidade($pessoa, $visibilidade);

            $request->session()->flash('pessoa_nao_autorizada_update', true);

        } catch (\Exception $e) {

            $request->flash();

            $request->session()->flash('pessoa_nao_autorizada_update', false);
        }

        return redirect()->route('moradores.pessoas-nao-autorizadas.index');
    }

    public function getDestroy(Request $request, $pessoa_id)
    {
        try {
            $pessoa = PessoaNaoAutorizada::find($pessoa_id);
            $pessoa->delete();
            Event::fire( new AtividadeRastreavel('pessoa_nao_autorizada_removida', $pessoa, Auth::moradores()->get()));
            $request->session()->flash('pessoa_nao_autorizada_destroy', true);
        } catch (\Exception $e) {
            $request->session()->flash('pessoa_nao_autorizada_destroy', false);
        }

        return redirect()->route('moradores.pessoas-nao-autorizadas.index');
    }

    private function armazenaPrivacidade(PessoaNaoAutorizada $pessoa, $valores_concatenados){

        $valores = explode(',', $valores_concatenados);

        if(sizeof($pessoa->privacidade)){
            foreach($pessoa->privacidade as $priv)
                $priv->delete();
        }

        foreach ($valores as $valor) {
            $pessoa->privacidade()->save(
                new PessoaNaoAutorizadaPrivacidade([
                    'visibilidade' => $valor
                ])
            );
        }
    }

    public function postUploadFoto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'foto' => 'mimes:png,jpg,jpeg|max:4000'
        ]);

        /*
        * O limite de upload da imagem (4Mb inicialmente) deve sempre ser
        * menor que o limite de upload do PHP no servidor, assim quando a imagem
        * for muito grande o erro lançado vai ser o do validador (tratado) e não
        * o do PHP (500 Internal Server Errror)
        */

        if ($validator->fails()) {
            return [
                'success' => 0,
                'msg' => $validator->errors()->first()
            ];
        }

        $arquivo = $request->file('foto');

        $path_original = "assets/images/moradores/pessoas-nao-autorizadas/originais/";
        $path_asset    = "assets/images/moradores/pessoas-nao-autorizadas/redimensionadas/";
        $path_thumb    = "assets/images/moradores/pessoas-nao-autorizadas/thumbs/";
        $path_upload   = public_path($path_original);

        $filename = filename_slug($arquivo->getClientOriginalName(), $arquivo->getClientOriginalExtension());

        // Armazenar Original
        $arquivo->move($path_upload, $filename);

        $thumb = $path_thumb.$filename;

        // Armazenar Redimensionada
        Thumbs::makeFromFile($path_upload, $filename, 900, null, public_path($path_asset), 'rgba(0,0,0,0)', false);
        // Armazenar Thumb
        Thumbs::makeFromFile($path_upload, $filename, 400, 400, public_path($path_thumb), 'rgba(0,0,0,0)', true);

        if(file_exists(public_path($path_original.$filename)) &&
           file_exists(public_path($path_asset.$filename)) &&
           file_exists(public_path($path_thumb.$filename)))
        {
            return [
                'thumb' => $thumb,
                'filename' => $filename,
                'success' => 1,
                'msg' => ''
            ];
        }else{
            return [
                'success' => 0,
                'msg' => 'Não foi possível armazenar a imagem.'
            ];
        }
    }
}
