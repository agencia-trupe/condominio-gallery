<?php

namespace Gallery\Http\Controllers\Moradores\Preferencias;

use Illuminate\Http\Request;
use Gallery\Services\Logger\LoggerInterface;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Gallery\Repositories\Moradores\Auth\AuthRepositoryInterface;
use Gallery\Repositories\Moradores\Preferencias\PreferenciasRepositoryInterface;

class PreferenciasController extends BaseMoradoresController
{

  protected $preferenciasRepository;
  protected $authRepository;
  protected $logger;

  public function __construct(
    PreferenciasRepositoryInterface $preferenciasRepository,
    AuthRepositoryInterface $authRepository,
    LoggerInterface $logger
  )
  {
    $this->preferenciasRepository = $preferenciasRepository;
    $this->authRepository = $authRepository;
    $this->logger = $logger;
  }

  public function getIndex()
  {
  	$morador = $this->authRepository->getUser();

    return view('moradores.preferencias.form', [
      'preferencias' => $morador->preferencias
    ]);
  }

  public function postAlterar(Request $request)
  {
  	try {

      $preferences = $this->preferenciasRepository->updatePreferencias(
        $this->authRepository->getUserId(),
        $request->input('preferencias')
      );

      $this->logger->log('preferencias_atualizadas', $preferences, $this->authRepository->getUser());

      $request->session()->flash('preferencias_atualizadas', true);

    } catch (\Exception $e) {
      $request->session()->flash('preferencias_atualizadas_fail', $e->getMessage());
    }

    return redirect()->route('moradores.preferencias.index');
  }
}
