<?php

namespace Gallery\Http\Controllers\Moradores\FestasParticulares;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;
use Gallery\Events\MoradorCriouFestaParticular;

use Carbon\Carbon as Carbon;
use Gallery\Models\FestaParticular;
use Gallery\Models\FestaParticularConvidados;

class FestasParticularesController extends BaseMoradoresController
{


  public function getIndex()
  {
  	$timestamp_reservas = $this->getTimestampsArray();

    $festas = FestaParticular::datasFuturas()->daUnidade()->ordenado()->get();

    return view('moradores.festas-particulares.index', compact(
    	'timestamp_reservas',
      'festas'
    ));
  }

  private function getTimestampsArray()
  {
  	$array_retorno = [];
  	$resultados = FestaParticular::datasFuturas()->daUnidade()->get();

  	foreach ($resultados as $key => $value)
      $array_retorno[] = $value->reserva->format('Y-m-d');

    return $array_retorno;
  }

	public function postReservar(Request $request)
	{
		$this->validate($request, [
      'reserva'      => 'required|date_format:d/m/Y',
      'titulo_festa' => 'required'
    ]);

    $moradores_id = Auth::moradores()->user()->id;
    $unidades_id = Auth::moradores()->user()->unidade->id;

		try {

      $check = FestaParticular::data( Carbon::createFromFormat('d/m/Y', $request->input('reserva'))->format('Y-m-d') )
                              ->daUnidade()
                              ->get();

      if(sizeof($check) == 0){

        $insert = FestaParticular::create([
  				'unidades_id'  => Auth::moradores()->get()->unidades_id,
  				'moradores_id' => Auth::moradores()->get()->id,
  				'reserva'      => $request->input('reserva'),
          'horario'      => $request->input('horario_festa'),
  				'titulo'       => $request->input('titulo_festa')
		    ]);

        Event::fire( new MoradorCriouFestaParticular($insert));
        Event::fire( new AtividadeRastreavel('festa_particular_criada', $insert, Auth::moradores()->get()));
		    $request->session()->flash('reserva_de_espaco_feita', true);

      }else{
        $request->session()->flash('reserva_data_ja_reservada', true);
      }

    } catch (Exception $e) {
      $request->session()->flash('reserva_de_espaco_feita', false);
    }

    return redirect()->back();
	}

	public function getDestroy(Request $request, $festa_id)
	{
    try {

      $reserva = FestaParticular::findOrFail($festa_id);

      $reserva->delete();

      Event::fire( new AtividadeRastreavel('festa_particular_removida', $reserva, Auth::moradores()->get()));
      $request->session()->flash('reserva_de_espaco_cancelada', true);

    } catch (Exception $e) {
      $request->session()->flash('reserva_de_espaco_cancelada', false);
    } finally {
      return redirect()->route('moradores.festas-particulares.index');
    }
	}

  public function getConvidados($festa_id)
  {
    $reserva = FestaParticular::findOrFail($festa_id);

    if($reserva->moradores_id != Auth::moradores()->get()->id) return redirect()->route('moradores.festas-particulares.index');

    return view('moradores.festas-particulares.convidados', compact('reserva'));
  }

  public function postConvidados(Request $request, $festas_id)
  {
    $reserva = FestaParticular::findOrFail($festas_id);

    if($reserva->moradores_id != Auth::moradores()->get()->id) return redirect()->route('moradores.festas-particulares.index');

    $nomes = $request->input('lista_convidados');

    FestaParticularConvidados::where('festas_particulares_id', '=', $festas_id)->delete();

    if($nomes && is_array($nomes)){
      Event::fire( new AtividadeRastreavel('lista_convidados_inserida', $reserva, Auth::moradores()->get()));
      foreach ($nomes as $key => $value) {
        $convidado = FestaParticularConvidados::create([
          'festas_particulares_id' => $festas_id,
          'nome' => $value
        ]);
        Event::fire( new AtividadeRastreavel('convidado_inserido', $convidado, Auth::moradores()->get()));
      }
    }

    $request->session()->flash('lista_convidados_inserida', true);
    return redirect()->route('moradores.festas-particulares.index');
  }

}
