<?php

namespace Gallery\Http\Controllers\Moradores\Amizades;

use Auth;
use Gate;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;

use Gallery\Models\Morador;
use Gallery\Models\MoradorAmizade;

class AmizadesController extends BaseMoradoresController
{

  public function getIndex()
  {
    return view('moradores.amizades.index');
  }

  public function postBuscar(Request $request)
  {
    $termo = $request->input('termo');
    $retorno_html = "";
    $resultados = Morador::excluindoMoradorLogado()
                         ->excluindoMoradoresTeste()
                         ->ativos()
                         ->where('nome', 'like', "%{$termo}%")
                         ->get();

    if(sizeof($resultados)){

      foreach ($resultados as $morador) {
        $retorno_html .= "<li>";
          $retorno_html .= "<div class='imagem'>";

            if($morador->foto && file_exists( "assets/images/moradores/fotos/thumbs/" . $morador->foto )){
              $retorno_html .= "<img src='assets/images/moradores/fotos/thumbs/{$morador->foto}'>";
            }else{
              $retorno_html .= "<img src='" . route('moradores.avatar-default') . "'>";
            }

          $retorno_html .= "</div>";
          $retorno_html .= "<div class='texto'>";
            $retorno_html .= "<div class='nome'>".$morador->nome."</div>";
            $retorno_html .= "<div class='unidade'>unidade ".$morador->unidade->resumo."</div>";

            if(Gate::forUser(Auth::moradores()->get())->allows('visualizar-dados', [$morador, 'email']))
              $retorno_html .= "<div class='email'>".$morador->email."</div>";

            if(Gate::forUser(Auth::moradores()->get())->allows('visualizar-dados', [$morador, 'telefone_fixo']))
              $retorno_html .= "<div class='tel'><span>telefone fixo:</span> ".$morador->telefone_fixo."</div>";

            if(Gate::forUser(Auth::moradores()->get())->allows('visualizar-dados', [$morador, 'telefone_celular']))
              $retorno_html .= "<div class='tel'><span>telefone celular:</span> ".$morador->telefone_celular."</div>";

            if(Gate::forUser(Auth::moradores()->get())->allows('visualizar-dados', [$morador, 'telefone_comercial']))
              $retorno_html .= "<div class='tel'><span>telefone comercial:</span> ".$morador->telefone_comercial."</div>";

          $retorno_html .= "</div>";
        $retorno_html .= "</li>";
      }

    }else{
      $retorno_html .= "<li class='sem-resultados'>Nenhum Morador Encontrado</li>";
    }

    return $retorno_html;

  }

  public function getAutorizados()
  {
    $moradores = Morador::excluindoMoradorLogado()
                        ->excluindoMoradoresTeste()
                        ->ativos()
                        ->get();

    return view('moradores.amizades.autorizados', compact('moradores'));
  }

  public function postAutorizar(Request $request)
  {
    $amizades_selecionadas = $request->input('amigo_id');

    MoradorAmizade::where('moradores_id', Auth::moradores()->get()->id)->delete();

    if(is_array($amizades_selecionadas)){
      foreach($amizades_selecionadas as $amigo){
        $amizade = MoradorAmizade::create([
          'moradores_id' => Auth::moradores()->get()->id,
          'amigo_id'     => $amigo
        ]);
        Event::fire( new AtividadeRastreavel('amizade_autorizada', $amizade, Auth::moradores()->get()));
      }
    }

    $request->session()->flash('amizades_definidas', true);

    return redirect()->back();

  }

}
