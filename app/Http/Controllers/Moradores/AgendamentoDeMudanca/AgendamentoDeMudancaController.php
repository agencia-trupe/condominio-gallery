<?php

namespace Gallery\Http\Controllers\Moradores\AgendamentoDeMudanca;

use Auth;
use Event;
use Gallery\Events\MoradorAgendouMudanca;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;

use Gallery\Models\AgendamentoDeMudanca;
use Gallery\Models\MudancasInfo;

class AgendamentoDeMudancaController extends BaseMoradoresController
{

  public function getIndex()
  {

		$agendamentos = $this->organizarDatasEmArrays();

  	return view('moradores.agendamento-de-mudanca.index', [
      'info' 		     => MudancasInfo::first(),
      'agendamentos' => $agendamentos
		]);
  }

  private function organizarDatasEmArrays(){

		$agendamentos['datas_com_agendamento']      = AgendamentoDeMudanca::datasComAgendamento();
		$agendamentos['agendamentos_periodo_manha'] = AgendamentoDeMudanca::datasAgendadasPorPeriodoArray('manha');
		$agendamentos['agendamentos_periodo_tarde'] = AgendamentoDeMudanca::datasAgendadasPorPeriodoArray('tarde');
		$agendamentos['agendamentos_do_usuario_periodo_manha'] = AgendamentoDeMudanca::datasAgendadasPorPeriodoArray('manha', true);
		$agendamentos['agendamentos_do_usuario_periodo_tarde'] = AgendamentoDeMudanca::datasAgendadasPorPeriodoArray('tarde', true);

  	return $agendamentos;
  }

  public function getRegulamento()
  {
  	$info = MudancasInfo::first();

    $arquivo = $info->regulamento;
    $caminho_completo = "assets/documentos/{$arquivo}";

    if(file_exists($caminho_completo)){
      return response()->download($caminho_completo);
    }else{
      abort('404');
    }
  }

  public function postAgendar(Request $request)
  {
  	$periodo = $request->input('agendamento.periodo');

    $this->validate($request, [
      'agendamento.data'    => 'required|date_format:d/m/Y|data_maior_que_atual|agendamento_mudanca_data_valida:'.$periodo,
      'agendamento.periodo' => 'required'
    ]);

    $moradores_id = Auth::moradores()->user()->id;
    $unidades_id = Auth::moradores()->user()->unidade->id;

    try {

      $agendamento = new AgendamentoDeMudanca;

      $agendamento->data = $request->input('agendamento.data');
      $agendamento->periodo = $periodo;

      $agendamento->moradores_id = $moradores_id;
      $agendamento->unidades_id = $unidades_id;

      $agendamento->save();
      $request->session()->flash('agendamento_mudanca_create', true);
      Event::fire( new AtividadeRastreavel('agendamento_mudanca_inserido', $agendamento, Auth::moradores()->get()));

      Event::fire(new MoradorAgendouMudanca($agendamento));

    } catch (\Exception $e) {

      $request->flash();

      $request->session()->flash('agendamento_mudanca_create', false);

    }

    return redirect()->route('moradores.agendamento-de-mudanca.index');
  }

  public function getCancelar(Request $request, $data, $periodo)
  {
  	$agendamento = AgendamentoDeMudanca::agendadoPor(Auth::moradores()->user()->id)
  									                   ->where('data', $data)
  										                 ->where('periodo', $periodo)
  										                 ->bloco(Auth::moradores()->user()->unidade->bloco)
  										                 ->first();

  	try {

  		$agendamento->delete();

  		$request->session()->flash('agendamento_mudanca_destroy', true);

      Event::fire( new AtividadeRastreavel('agendamento_mudanca_cancelado', $agendamento, Auth::moradores()->get()));

  	} catch (Exception $e) {
  		$request->session()->flash('agendamento_mudanca_destroy', false);
  	}

    return redirect()->route('moradores.agendamento-de-mudanca.index');
  }
}
