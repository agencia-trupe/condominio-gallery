<?php

namespace Gallery\Http\Controllers\Moradores\Fornecedores;

use Event;
use Gallery\Events\AtividadeRastreavel;
use Auth;
use Validator;
use Gallery\Libs\Thumbs;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;

use Gallery\Models\Fornecedor;
use Gallery\Models\FornecedorCategoria;
use Gallery\Models\FornecedorAvaliacao;
use Gallery\Models\FornecedorImagem;

class FornecedoresController extends BaseMoradoresController
{

  public function getIndex()
  {
    $listaCategorias = FornecedorCategoria::ordenado()->get();

    $indicacoesRecentes = Fornecedor::avaliacoesRecentes()->get();

    return view('moradores.fornecedores.index', compact(
      'listaCategorias',
      'indicacoesRecentes'
    ));
  }

  public function getShow(Request $request, Fornecedor $fornecedor)
  {
    $avaliacao_recomendante = FornecedorAvaliacao::where('moradores_id', '=', $fornecedor->indicado_por)
                                                 ->where('fornecedores_id', $fornecedor->id)
                                                 ->first();

    if($avaliacao_recomendante){

      $outras_avaliacoes = FornecedorAvaliacao::where('id', '!=', $avaliacao_recomendante->id)
                                              ->where('fornecedores_id', $fornecedor->id)
                                              ->orderBy('created_at', 'desc')
                                              ->get();

    }else{

      $outras_avaliacoes = FornecedorAvaliacao::where('fornecedores_id', $fornecedor->id)
                                              ->orderBy('created_at', 'desc')
                                              ->get();

    }

    return view('moradores.fornecedores.detalhes', compact(
      'fornecedor',
      'avaliacao_recomendante',
      'outras_avaliacoes'
    ));
  }

  public function getEdit(Request $request, Fornecedor $fornecedor)
  {
    if($fornecedor->indicado_por != Auth::moradores()->get()->id)
      return redirect()->route('moradores.fornecedores.index');

    $listaCategorias = FornecedorCategoria::ordenado()->get();

    $indicacoesRecentes = Fornecedor::avaliacoesRecentes()->get();

    return view('moradores.fornecedores.index', compact(
      'listaCategorias',
      'indicacoesRecentes',
      'fornecedor'
    ));
  }

  public function postStore(Request $request)
  {
    $this->validate($request, [
      'fornecedores.nome'                       => 'required',
      'fornecedores.fornecedores_categorias_id' => 'sometimes|categoria_de_fornecedores_disponiveis',
      'fornecedores.outra_categoria'            => 'required_without:fornecedores.fornecedores_categorias_id',
      'avaliacao.nota'                          => 'required|in:1,2,3,4,5',
      'avaliacao.texto'                         => 'required'
    ]);

    $moradores_id = Auth::moradores()->user()->id;

    try {

      // Cadastrar Fornecedor
      $fornecedor = Fornecedor::create($request->fornecedores);

      $fornecedor->indicado_por = $moradores_id;
      $fornecedor->save();

      // Cadastrar Avaliação
      $avaliacao = FornecedorAvaliacao::create($request->avaliacao);
      $avaliacao->moradores_id = $moradores_id;
      $avaliacao->save();

      $fornecedor->avaliacoes()->save($avaliacao);

      // Cadastrar Fotos
      $fotos = $request->input('fornecedores.fotos');

      if($fotos && is_array($fotos)){
        foreach ($fotos as $key => $value) {
          $foto = new FornecedorImagem(['imagem' => $value]);
          $fornecedor->imagens()->save($foto);
        }
      }

      Event::fire( new AtividadeRastreavel('fornecedor_inserido', $fornecedor, Auth::moradores()->get()));
      $request->session()->flash('fornecedores_create', true);

    } catch (\Exception $e) {

      $request->flash();

      $request->session()->flash('fornecedores_create', false);
      //$request->session()->flash('fornecedores_create_msg', $e->getMessage());
    }

    return redirect()->route('moradores.fornecedores.index');

  }

  public function postUpdate(Request $request, Fornecedor $fornecedor)
  {
    $this->validate($request, [
      'fornecedores.nome'                       => 'required',
      'fornecedores.fornecedores_categorias_id' => 'sometimes|categoria_de_fornecedores_disponiveis',
      'fornecedores.outra_categoria'            => 'required_without:fornecedores.fornecedores_categorias_id'
    ]);

    $moradores_id = Auth::moradores()->user()->id;

    try {

      // Cadastrar Fornecedor
      $fornecedor = $fornecedor->fill($request->fornecedores);
      $fornecedor->save();
      Event::fire( new AtividadeRastreavel('fornecedor_atualizado', $fornecedor, Auth::moradores()->get()));

      // Cadastrar Fotos
      FornecedorImagem::where('fornecedores_id', '=', $fornecedor->id)->delete();

      $fotos = $request->input('fornecedores.fotos');
      if($fotos && is_array($fotos)){
        foreach ($fotos as $key => $value) {
          $foto = new FornecedorImagem(['imagem' => $value]);
          $fornecedor->imagens()->save($foto);
        }
      }

      $request->session()->flash('fornecedores_update', true);

    } catch (\Exception $e) {

      $request->flash();

      $request->session()->flash('fornecedores_update', false);

    }

    return redirect()->route('moradores.fornecedores.index');
  }

  public function postAvaliar(Request $request, Fornecedor $fornecedor)
  {
    $this->validate($request, [
      'avaliacao.nota'  => 'required|in:1,2,3,4,5',
      'avaliacao.texto' => 'required'
    ]);

    try {

      // Cadastrar Avaliação
      $avaliacao = FornecedorAvaliacao::create($request->avaliacao);
      $avaliacao->moradores_id = Auth::moradores()->user()->id;
      $avaliacao->save();

      $fornecedor->avaliacoes()->save($avaliacao);

      Event::fire( new AtividadeRastreavel('fornecedor_avaliado', $avaliacao, Auth::moradores()->get()));

      $request->session()->flash('fornecedores_avaliado', true);

    } catch (Exception $e) {

      $request->flash();

      $request->session()->flash('fornecedores_avaliado', false);

    }

    return redirect()->route('moradores.fornecedores.detalhes', $fornecedor->id);
  }

  public function postUploadFoto(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'foto' => 'mimes:png,jpg,jpeg|max:4000'
    ]);

    /*
    * O limite de upload da imagem (4Mb inicialmente) deve sempre ser
    * menor que o limite de upload do PHP no servidor, assim quando a imagem
    * for muito grande o erro lançado vai ser o do validador (tratado) e não
    * o do PHP (500 Internal Server Errror)
    */

    if ($validator->fails()) {
      return [
        'success' => 0,
        'msg' => $validator->errors()->first()
      ];
    }

    $arquivo = $request->file('foto');

    $path_original = "assets/images/moradores/fornecedores/originais/";
    $path_asset    = "assets/images/moradores/fornecedores/redimensionadas/";
    $path_thumb    = "assets/images/moradores/fornecedores/thumbs/";
    $path_upload   = public_path($path_original);

    $filename = filename_slug($arquivo->getClientOriginalName(), $arquivo->getClientOriginalExtension());

    // Armazenar Original
    $arquivo->move($path_upload, $filename);

    $thumb = $path_thumb.$filename;

    // Armazenar Redimensionada
    Thumbs::makeFromFile($path_upload, $filename, 900, null, public_path($path_asset), 'rgba(0,0,0,0)', false);
    // Armazenar Thumb
    Thumbs::makeFromFile($path_upload, $filename, 400, 400, public_path($path_thumb), 'rgba(0,0,0,0)', true);

    if(file_exists(public_path($path_original.$filename)) &&
       file_exists(public_path($path_asset.$filename)) &&
       file_exists(public_path($path_thumb.$filename)))
    {
      return [
        'thumb' => $thumb,
        'filename' => $filename,
        'success' => 1,
        'msg' => ''
      ];
    }else{
      return [
        'success' => 0,
        'msg' => 'Não foi possível armazenar a imagem.'
      ];
    }
  }

  public function postBusca(Request $request)
  {
    $busca_categoria = $request->busca_categoria;
    $busca_nome      = $request->busca_nome;

    $listaCategorias = FornecedorCategoria::ordenado()->get();

    $resultadoBusca = Fornecedor::with('avaliacoes', 'imagens', 'categoria')
                                ->busca($busca_categoria, $busca_nome)
                                ->get();

    return view('moradores.fornecedores.index', compact(
      'listaCategorias',
      'resultadoBusca',
      'busca_categoria',
      'busca_nome'
    ));
  }

  public function getDestroy(Request $request, Fornecedor $fornecedor)
  {
    if(Auth::moradores()->get()->id == $fornecedor->indicao_por){
      try {
        $fornecedor->delete();
        Event::fire( new AtividadeRastreavel('fornecedor_removido', $fornecedor, Auth::moradores()->get()));
        $request->session()->flash('fornecedor_destroy', true);
      } catch (\Exception $e) {
        $request->session()->flash('fornecedor_destroy', false);
      }
    }

    return redirect()->route('moradores.fornecedores.index');
  }

  public function getDestroyAvaliacao(Request $request, FornecedorAvaliacao $avaliacao)
  {
    if(Auth::moradores()->get()->id == $avaliacao->moradores_id){

      $fornecedor_id = $avaliacao->fornecedores_id;

      try {
        $avaliacao->delete();
        Event::fire( new AtividadeRastreavel('fornecedor_avaliacao_removida', $avaliacao, Auth::moradores()->get()));
        $request->session()->flash('avaliacao_destroy', true);
      } catch (\Exception $e) {
        $request->session()->flash('avaliacao_destroy', false);
      }
    }

    return redirect()->route('moradores.fornecedores.detalhes', $fornecedor_id);
  }

}
