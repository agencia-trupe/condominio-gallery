<?php

namespace Gallery\Http\Controllers\Moradores\Classificados;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Illuminate\Http\Request;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Validator;
use Gallery\Libs\Thumbs;

use Gallery\Models\Classificado;
use Gallery\Models\ClassificadoFoto;

class ClassificadosController extends BaseMoradoresController
{

  public function getIndex($slug_categoria = 'venda_e_troca')
  {
    $anuncios = Classificado::porCategoria($slug_categoria)->ordenado()->get();

  	return view('moradores.classificados.index', compact('slug_categoria', 'anuncios'));
  }

  public function getMeusAnuncios()
  {
    $slug_categoria = 'meus_anuncios';
    return view('moradores.classificados.meus-anuncios', compact('slug_categoria'));
  }

  public function postPublicar(Request $request)
  {
    $this->validate($request, [
      'anuncio.categoria' => 'required|in:venda_e_troca,servico,recado',
      'anuncio.titulo'    => 'required'
    ]);

    $moradores_id = Auth::moradores()->user()->id;

    try {

      $anuncio = new Classificado;

      $anuncio->categoria = $request->input('anuncio.categoria');
      $anuncio->titulo = $request->input('anuncio.titulo');
      $anuncio->descricao = $request->input('anuncio.descricao');
      $anuncio->contato = $request->input('anuncio.contato');
      $anuncio->moradores_id = $moradores_id;

      $anuncio->save();

      $fotos = $request->input('anuncio.fotos');

      // Cadastrar Fotos
      if($fotos && is_array($fotos)){
        foreach ($fotos as $key => $value) {
          $foto = ClassificadoFoto::create([
            'imagem' => $value,
            'classificados_id' => $anuncio->id,
          ]);
        }
      }

      Event::fire( new AtividadeRastreavel('classificado_inserido', $anuncio, Auth::moradores()->get()));
      $request->session()->flash('classificados_create', true);

    } catch (Exception $e) {

      $request->flash();

      $request->session()->flash('classificados_create', false);

    }

    return redirect()->route('moradores.classificados.index', $anuncio->categoria);

  }

  public function postUploadFoto(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'foto' => 'mimes:png,jpg,jpeg|max:4000'
    ]);

    /*
    * O limite de upload da imagem (4Mb inicialmente) deve sempre ser
    * menor que o limite de upload do PHP no servidor, assim quando a imagem
    * for muito grande o erro lançado vai ser o do validador (tratado) e não
    * o do PHP (500 Internal Server Errror)
    */

    if ($validator->fails()) {
      return [
        'success' => 0,
        'msg' => $validator->errors()->first()
      ];
    }

    $arquivo = $request->file('foto');

    $path_original = "assets/images/moradores/classificados/originais/";
    $path_asset    = "assets/images/moradores/classificados/redimensionadas/";
    $path_thumb    = "assets/images/moradores/classificados/thumbs/";
    $path_upload   = public_path($path_original);

    $filename = filename_slug($arquivo->getClientOriginalName(), $arquivo->getClientOriginalExtension());

    // Armazenar Original
    $arquivo->move($path_upload, $filename);

    $thumb = $path_thumb.$filename;

    // Armazenar Redimensionada
    Thumbs::makeFromFile($path_upload, $filename, 900, null, public_path($path_asset), 'rgba(0,0,0,0)', false);
    // Armazenar Thumb
    Thumbs::makeFromFile($path_upload, $filename, 400, 400, public_path($path_thumb), 'rgba(0,0,0,0)', true);

    if(file_exists(public_path($path_original.$filename)) &&
       file_exists(public_path($path_asset.$filename)) &&
       file_exists(public_path($path_thumb.$filename)))
    {
        return [
            'thumb' => $thumb,
            'filename' => $filename,
            'success' => 1,
            'msg' => ''
        ];
    }else{
        return [
            'success' => 0,
            'msg' => 'Não foi possível armazenar a imagem.'
        ];
    }
  }

  public function getRemoverAnuncio(Request $request, Classificado $classificado)
  {
    $classificado->delete();
    $request->session()->flash('classificados_delete', true);
    return redirect()->route('moradores.classificados.meus-anuncios');
  }

  public function getEditarAnuncio(Request $request, Classificado $anuncio)
  {
    if($anuncio->moradores_id != Auth::moradores()->get()->id)
      return redirect()->back();

    $slug_categoria = 'meus_anuncios';

  	return view('moradores.classificados.editar-anuncio', compact('anuncio', 'slug_categoria'));
  }

  public function postEditarAnuncio(Request $request, Classificado $anuncio)
  {
    $this->validate($request, [
      'anuncio.categoria' => 'required|in:venda_e_troca,servico,recado',
      'anuncio.titulo'    => 'required'
    ]);

    try {

      $moradores_id = Auth::moradores()->user()->id;

      $anuncio->categoria = $request->input('anuncio.categoria');
      $anuncio->titulo = $request->input('anuncio.titulo');
      $anuncio->descricao = $request->input('anuncio.descricao');
      $anuncio->contato = $request->input('anuncio.contato');
      $anuncio->moradores_id = $moradores_id;

      $anuncio->save();

      foreach($anuncio->fotos as $foto)
        $foto->delete();

      $fotos = $request->input('anuncio.fotos');

      // Cadastrar Fotos
      if($fotos && is_array($fotos)){
        foreach ($fotos as $key => $value) {
          $foto = ClassificadoFoto::create([
            'imagem' => $value,
            'classificados_id' => $anuncio->id,
          ]);
        }
      }

      Event::fire( new AtividadeRastreavel('classificado_alterado', $anuncio, Auth::moradores()->get()));
      $request->session()->flash('classificados_edit', true);

    } catch (Exception $e) {

      $request->flash();

      $request->session()->flash('classificados_edit', false);

    }

    return redirect()->route('moradores.classificados.meus-anuncios');
  }

}
