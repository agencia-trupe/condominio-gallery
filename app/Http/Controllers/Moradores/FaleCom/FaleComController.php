<?php

namespace Gallery\Http\Controllers\Moradores\FaleCom;

use Gallery\Http\Requests\Moradores\FaleComRequest;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Gallery\Services\Logger\LoggerInterface;
use Gallery\Services\EventManager\EventManagerInterface;
use Gallery\Repositories\Moradores\Auth\AuthRepositoryInterface;
use Gallery\Repositories\Moradores\FaleCom\FaleComRepositoryInterface;

class FaleComController extends BaseMoradoresController
{

  protected $logger;
  protected $eventManager;
  protected $authRepository;
  protected $faleComRepository;

  public function __construct(
    AuthRepositoryInterface $authRepository,
    EventManagerInterface $eventManager,
    FaleComRepositoryInterface $faleComRepository,
    LoggerInterface $logger
  )
  {
    $this->logger = $logger;
    $this->eventManager = $eventManager;
    $this->authRepository = $authRepository;
    $this->faleComRepository = $faleComRepository;
  }

  public function getIndex()
  {
    $faleComInfo = $this->faleComRepository->getInfo();

  	return view('moradores.fale-com.index', [
      'registro' => $faleComInfo,
    ]);
  }

	public function getMensagem($destinatario)
	{
		$destinatarios = $this->faleComRepository->getDestinatarios();

		return view('moradores.fale-com.mensagem', [
		    'destinatario'  => $destinatario,
		    'destinatarios'	=> $destinatarios
		]);
	}

	public function postMensagem(FaleComRequest $request, $destinatario)
	{
    $user = $this->authRepository->getUser();

		try {

      $mensagem = $this->faleComRepository->novaMensagem(
        $user->id,
        $destinatario,
        $request->falecom
      );

      $this->eventManager->fire('MoradorEnviouMensagem', $mensagem);

      $this->logger->log('morador_envio_mensagem', $mensagem, $user);

      $request->session()->flash('fale_com_create', true);
			return redirect()->route('moradores.fale-com.index', $destinatario);

		} catch (\Exception $e) {

			$request->flash();

      $request->session()->flash('fale_com_create', false);
      $request->session()->flash('fale_com_create_msg', $e->getMessage());

      return back();
		}

	}
}
