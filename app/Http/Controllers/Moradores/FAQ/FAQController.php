<?php

namespace Gallery\Http\Controllers\Moradores\FAQ;

use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\FAQ;
use Gallery\Models\FAQInfo;

class FAQController extends BaseMoradoresController
{
    public function getIndex()
    {
      return view('moradores.faq.index', ['questoes' => FAQ::ordenado()->get(), 'faqinfo' => FAQInfo::first()]);
    }
}
