<?php

namespace Gallery\Http\Controllers\Moradores\MeuPerfil;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Validator;
use Log;
use View;
use Mail;
use Gallery\Libs\Thumbs;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\Morador;
use Gallery\Models\MoradorPrivacidade;

class MeuPerfilController extends BaseMoradoresController
{

  public function getIndex()
  {
  	$morador = Auth::moradores()->get();

    return view('moradores.meu-perfil.form', compact('morador'));
  }

  public function postUpdate(Request $request)
  {
  	$id = Auth::moradores()->get()->id;

  	$this->validate($request, [
      'moradores.nome'                            => 'required',
      'moradores.apelido'                         => 'required',
      'moradores.relacao_unidade'                 => 'required|in:locatario,proprietario',
      'moradores.data_nascimento'                 => 'required|date_format:d/m/Y',
      'moradores.data_mudanca'                    => 'required|date_format:d/m/Y',
      'moradores.email'                           => 'required|email|unique:moradores,email,'.$id,
      'moradores.visibilidade_email'              => 'required',
      'moradores.telefone_fixo'                   => 'required',
      'moradores.visibilidade_telefone_fixo'      => 'required',
      'moradores.telefone_celular'                => 'required',
      'moradores.visibilidade_telefone_celular'   => 'required',
      'moradores.telefone_comercial'              => 'required',
      'moradores.visibilidade_telefone_comercial' => 'required'
    ]);

    $dados_usuario = $request->input('moradores');
    $dados_usuario_filtrados = array_except($dados_usuario, [
		  'email',
      'visibilidade_email',
      'visibilidade_telefone_fixo',
      'visibilidade_telefone_celular',
      'visibilidade_telefone_comercial'
    ]);

    try {

      $morador = Morador::find($id);
      $morador->fill($dados_usuario_filtrados);

      // Se trocou o email, enviar e-mail de confirmação
      if($dados_usuario['email'] != $morador->email){
	      $morador->novo_email_activation_code = str_random(30);
	      $morador->novo_email = $dados_usuario['email'];

      	$this->enviarEmailConfirmacao($morador);

      	$request->session()->flash('confirmar_email', true);
      }else{
		    $morador->novo_email_activation_code = '';
	      $morador->novo_email = '';
      }

	    $morador->save();

      // armazenar - configurações de visibilidade
      $this->armazenarPrivacidade($morador, $request);
      Event::fire( new AtividadeRastreavel('perfil_atualizado', $morador, Auth::moradores()->get()));
      $request->session()->flash('cadastro_realizado', true);

    } catch (\Exception $e) {
      $request->session()->flash('cadastro_fail', $e->getMessage());
    }

    return redirect()->route('moradores.meu-perfil.index');
  }

	public function postUploadFoto(Request $request)
	{
		$validator = Validator::make($request->all(), [
      'foto' => 'mimes:png,jpg,jpeg|max:4000'
    ]);

        /*
        * O limite de upload da imagem (4Mb inicialmente) deve sempre ser
        * menor que o limite de upload do PHP no servidor, assim quando a imagem
        * for muito grande o erro lançado vai ser o do validador (tratado) e não
        * o do PHP (500 Internal Server Errror)
        */

        if ($validator->fails()) {
            return [
                'success' => 0,
                'msg' => $validator->errors()->first()
            ];
        }

        $arquivo = $request->file('foto');

        $path_original = "assets/images/moradores/fotos/originais/";
        $path_asset    = "assets/images/moradores/fotos/redimensionadas/";
        $path_thumb    = "assets/images/moradores/fotos/thumbs/";
        $path_upload   = public_path($path_original);

        $filename = filename_slug($arquivo->getClientOriginalName(), $arquivo->getClientOriginalExtension());

        // Armazenar Original
        $arquivo->move($path_upload, $filename);

        $thumb = $path_thumb.$filename;

        // Armazenar Redimensionada
        Thumbs::makeFromFile($path_upload, $filename, 900, null, public_path($path_asset), 'rgba(0,0,0,0)', false);
        // Armazenar Thumb
        Thumbs::makeFromFile($path_upload, $filename, 400, 400, public_path($path_thumb), 'rgba(0,0,0,0)', true);

        if(file_exists(public_path($path_original.$filename)) &&
           file_exists(public_path($path_asset.$filename)) &&
           file_exists(public_path($path_thumb.$filename)))
        {
            return [
                'thumb' => $thumb,
                'filename' => $filename,
                'success' => 1,
                'msg' => ''
            ];
        }else{
            return [
                'success' => 0,
                'msg' => 'Não foi possível armazenar a imagem.'
            ];
        }
	}

    private function armazenarPrivacidade(Morador $morador, Request $request)
    {
        $visibilidades = [
            'email' => $request->input('moradores.visibilidade_email'),
            'telefone_fixo' => $request->input('moradores.visibilidade_telefone_fixo'),
            'telefone_celular' => $request->input('moradores.visibilidade_telefone_celular'),
            'telefone_comercial' => $request->input('moradores.visibilidade_telefone_comercial')
        ];

        if(sizeof($morador->privacidade)){
            foreach($morador->privacidade as $priv)
                $priv->delete();
        }

        foreach ($visibilidades as $dado => $valores_concatenados) {
            $valores = explode(',', $valores_concatenados);
            foreach ($valores as $valor) {
                $morador->privacidade()->save(
                    new MoradorPrivacidade([
                        'dado' => $dado,
                        'visibilidade' => $valor
                    ])
                );
            }
        }
    }

	private function enviarEmailConfirmacao(Morador $morador)
  {
    $template_email = 'moradores.templates.emails.troca-email-confirmacao';

    $data['morador'] = $morador;

    Mail::send( $template_email, $data, function ($m) use ($morador) {
      $m->to($morador->novo_email, $morador->nome)
        ->bcc('bruno@trupe.net')
        ->subject('Alteração de E-mail - Gallery Online');
    });

    if(config('mail.pretend')) Log::info(View::make($template_email, $data)->render());
  }

  public function getAtivar(Request $request, $email, $activation_code)
  {
  	$morador = Morador::where('novo_email', $email)
                        ->where('novo_email_activation_code', $activation_code)
                        ->first();

    if($morador){

    	if($morador->novo_email != '')
    		$morador->email = $morador->novo_email;

    	$morador->novo_email = '';
      $morador->novo_email_activation_code = '';

      $morador->save();

      $request->session()->flash('ativacao_realizada', true);

    }else{

      $request->session()->flash('ativacao_fail', true);

    }

    return redirect()->route('moradores.meu-perfil.index');
  }
}
