<?php

namespace Gallery\Http\Controllers\Moradores\Dashboard;

use Auth;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Gallery\Repositories\Moradores\Avisos\AvisosRepositoryInterface;
use Gallery\Repositories\Moradores\Documentos\DocumentosRepositoryInterface;
use Gallery\Repositories\Moradores\Auth\AuthRepositoryInterface;

class DashboardController extends BaseMoradoresController
{

  /**
  * Show the application login form.
  *
  * @return \Illuminate\Http\Response
  */
  public function getDashboard(
    AvisosRepositoryInterface $avisosRepository,
    DocumentosRepositoryInterface $documentosRepository,
    AuthRepositoryInterface $authRepository
  ){
    $userId = $authRepository->getUserId();

    $avisos = $avisosRepository->getAvisosNaoLidos($userId);
    $novidades = $documentosRepository->getDocumentosRecentes();

    return view('moradores.dashboard.index', [
      'avisos'    => $avisos,
      'novidades' => $novidades
    ]);
  }

}
