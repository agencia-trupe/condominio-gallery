<?php

namespace Gallery\Http\Controllers\Moradores\AgendasAdministracao;

use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\AgendaAdministracao;

class AgendasAdministracaoController extends BaseMoradoresController
{

  public function getIndex()
  {
    return view('moradores.agenda-administracao.index', ['agenda' => AgendaAdministracao::ordenado()->get()]);
  }

}
