<?php

namespace Gallery\Http\Controllers\Admin\Fornecedores;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Gallery\Models\Fornecedor;
use Gallery\Models\FornecedorCategoria;

class FornecedoresController extends BaseAdminController
{

  public function index(Request $request)
  {
    $filtro = $request->has('filtro') ? $request->filtro : 'com_categoria';

    if($filtro == 'com_categoria')
      $registros = Fornecedor::ordenado()->comCategoria()->get();
    else
      $registros = Fornecedor::ordenado()->semCategoria()->get();


    return view('admin.fornecedores.index', compact('registros', 'filtro'));
  }

  public function edit(Request $request, Fornecedor $fornecedor)
  {
    $listaCategorias = FornecedorCategoria::ordenado()->get();
    return view('admin.fornecedores.edit', compact('fornecedor', 'listaCategorias'));
  }

  public function update(Request $request, Fornecedor $fornecedor)
  {
    if($request->acao_categoria == 'is_nova_categoria'){

      $this->validate($request, [
        'nova_categoria' => 'required|unique:fornecedores_categorias,titulo'
      ]);

      try {

        $nova_categoria = new FornecedorCategoria;
        $nova_categoria->titulo = $request->nova_categoria;
        $nova_categoria->slug = str_slug($request->nova_categoria);
        $nova_categoria->save();

        Event::fire( new AtividadeRastreavel('categoria_fornecedor_inserido', $nova_categoria, Auth::admin()->get()));

        $fornecedor->fornecedores_categorias_id = $nova_categoria->id;
        $fornecedor->save();

        Event::fire( new AtividadeRastreavel('fornecedor_associado_a_categoria', $fornecedor, Auth::admin()->get()));

        $request->session()->flash('sucesso', 'Nova Categoria de Fornecedor adicionada e associada com sucesso.');

      } catch (Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar e associar Categoria de Fornecedor.! ('.$e->getMessage().')'));

      }



    }elseif($request->acao_categoria == 'relacionar_categoria'){

      $this->validate($request, [
        'fornecedores_categorias_id' => 'exists:fornecedores_categorias,id'
      ]);

      $fornecedor->fornecedores_categorias_id = $request->fornecedores_categorias_id;

      try {

        $fornecedor->save();

        Event::fire( new AtividadeRastreavel('fornecedor_associado_a_categoria', $fornecedor, Auth::admin()->get()));

        $request->session()->flash('sucesso', 'Categoria de Fornecedor associada com sucesso.');

      } catch (Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao associar Categoria de Fornecedor.! ('.$e->getMessage().')'));
      }

    }

    return redirect()->route('admin.fornecedores.index');
  }

}
