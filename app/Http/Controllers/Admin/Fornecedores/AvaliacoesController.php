<?php

namespace Gallery\Http\Controllers\Admin\Fornecedores;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Gallery\Models\Fornecedor;
use Gallery\Models\FornecedorAvaliacao;

class AvaliacoesController extends BaseAdminController
{

    public function getIndex(Request $request,Fornecedor $fornecedor)
    {
      return view('admin.fornecedores.avaliacoes.index', compact('fornecedor'));
    }

    public function deleteDestroy(Request $request, FornecedorAvaliacao $avaliacao)
    {
      $id = $avaliacao->fornecedores_id;

      $avaliacao->delete();

      Event::fire( new AtividadeRastreavel('avaliacao_fornecedor_removida', $avaliacao, Auth::admin()->get()));

      $request->session()->flash('sucesso', 'Avaliação de Fornecedor removida com sucesso.');

      return redirect()->route('admin.fornecedores.avaliacoes.index', $id);
    }

}
