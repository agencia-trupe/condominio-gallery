<?php

namespace Gallery\Http\Controllers\Admin\Enquetes;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use DB;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Admin\BaseAdminController;

use Gallery\Models\Enquete;
use Gallery\Models\EnqueteOpcoes;
use Gallery\Models\EnqueteVotos;

class EnquetesController extends BaseAdminController
{

  public function index(Request $request)
  {
    if(!$request->has('filtro'))
      $filtro = 'recentes';
    else
      $filtro = $request->input('filtro');

    if($filtro == 'recentes')
      $registros = Enquete::atuais()->ordenado()->get();
    else
      $registros = Enquete::historico()->ordenado()->get();

    return view('admin.enquetes.index', compact('registros', 'filtro'));
  }

  public function getAtivar(Enquete $enquete)
  {
    $ativa = Enquete::with('votos')->where('ativa', 1)->first();

    if($ativa && count($ativa->votos) > 0){
      $ativa->historico = 1;
      $ativa->ativa = 0;
      $ativa->save();
      Event::fire( new AtividadeRastreavel('enquete_desativada', $ativa, Auth::admin()->get()));
    }

    $enquete->ativa = 1;
		$enquete->save();
    Event::fire( new AtividadeRastreavel('enquete_ativada', $enquete, Auth::admin()->get()));

		return redirect()->route('admin.enquetes.index');
  }

  public function create()
  {
    return view('admin.enquetes.create');
  }

  public function store(Request $request)
  {
    $object = new Enquete;

		$object->titulo = $request->input('titulo');
    $object->texto = $request->input('texto');

		try {

			$object->save();

			if($request->has('enquete_opcao')){
				$opcoes = $request->input('enquete_opcao');

				foreach ($opcoes as $key => $value) {
					if ($value) {
						$opcao = EnqueteOpcoes::create([
              'enquete_questoes_id' => $object->id,
              'texto' => $value,
              'ordem' => $key
            ]);
					}
				}
			}

      Event::fire( new AtividadeRastreavel('enquete_criada', $object, Auth::admin()->get()));

      $request->session()->flash('sucesso', 'Enquete criada com sucesso.');

      return redirect()->route('admin.enquetes.index');

		} catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar enquetes! ('.$e->getMessage().')'));

		}
  }

  public function show($id)
  {
    $registro = Enquete::find($id);

    return view('admin.enquetes.show', compact('registro'));
  }

  public function edit($id)
  {
    $registro = Enquete::find($id);

    return view('admin.enquetes.edit', compact('registro'));
  }

  public function update(Request $request, $id)
  {
    $object = Enquete::findOrFail($id);

    $object->titulo = $request->input('titulo');
		$object->texto = $request->input('texto');

		try {

			$object->save();

      EnqueteOpcoes::where('enquete_questoes_id', $object->id)->delete();

			if($request->has('enquete_opcao')){
				$opcoes = $request->input('enquete_opcao');

				foreach ($opcoes as $key => $value) {
					if ($value) {
						$opcao = EnqueteOpcoes::create([
              'enquete_questoes_id' => $object->id,
              'texto' => $value,
              'ordem' => $key
            ]);
					}
				}

			}

      Event::fire( new AtividadeRastreavel('enquete_alterada', $object, Auth::admin()->get()));

      $request->session()->flash('sucesso', 'Enquete alterada com sucesso.');
      return redirect()->route('admin.enquetes.index');

		} catch (\Exception $e) {

      $request->flash();
      return back()->withErrors(array('Erro ao criar enquetes! ('.$e->getMessage().')'));

		}
  }

  public function destroy(Request $request, $id)
  {
    $object = Enquete::find($id);
    $object->delete();

    Event::fire( new AtividadeRastreavel('enquete_removida', $object, Auth::admin()->get()));
    $request->session()->flash('sucesso', 'Enquete removida com sucesso.');

    return redirect()->route('admin.enquetes.index');
  }

}
