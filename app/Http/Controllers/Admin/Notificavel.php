<?php

namespace Gallery\Http\Controllers\Admin;

use Auth;
use Gallery\Models\NotificacaoAdmin;

trait Notificavel
{
  /**
   * Carrega as notificações da administração
   *
   * @return mixed
   */

  public function carregarNotificacoes()
  {
    if(Auth::admin()->check()){

      $id = Auth::admin()->get()->id;


      $notificacoes_administracao_e_comunicacao['ocorrencias'] = NotificacaoAdmin::paraAdministrador($id)
                                                                                 ->ocorrencias()
                                                                                 ->get();

      $notificacoes_administracao_e_comunicacao['mensagens'] = NotificacaoAdmin::paraAdministrador($id)
                                                                               ->mensagens()
                                                                               ->get();

      $notificacoes_administracao_e_comunicacao['mensagens_portaria'] = NotificacaoAdmin::paraAdministrador($id)
                                                                               ->mensagensPortaria()
                                                                               ->get();

      $notificacoes_administracao_e_comunicacao['mudancas'] =  NotificacaoAdmin::paraAdministrador($id)
                                                                               ->mudancas()
                                                                               ->get();



      $notificacoes_areas_comuns['chamados_manutencao'] = NotificacaoAdmin::paraAdministrador($id)
                                                                          ->chamadosManutencao()
                                                                          ->get();

      $notificacoes_areas_comuns['reservas_periodo'] = NotificacaoAdmin::paraAdministrador($id)
                                                                       ->reservaDeEspacoPeriodo()
                                                                       ->get();

      $notificacoes_areas_comuns['reservas_diarias'] = NotificacaoAdmin::paraAdministrador($id)
                                                                       ->reservaDeEspacoDiaria()
                                                                       ->get();

      $notificacoes_areas_comuns['cancelamentos_com_multa'] = NotificacaoAdmin::paraAdministrador($id)
                                                                              ->cancelamentoComMulta()
                                                                              ->get();

      $notificacoes_areas_comuns['festas_particulares'] = NotificacaoAdmin::paraAdministrador($id)
                                                                          ->festasParticulares()
                                                                          ->get();


      $has_notificacoes_administracao_e_comunicacao = false;
      foreach($notificacoes_administracao_e_comunicacao as $notif){
        if(sizeof($notif) > 0)
          $has_notificacoes_administracao_e_comunicacao = true;
      }

      $has_notificacoes_areas_comuns = false;
      foreach($notificacoes_areas_comuns as $notif){
        if(sizeof($notif) > 0)
          $has_notificacoes_areas_comuns = true;
      }

      view()->share([

        'notificacoes_administracao_e_comunicacao'  => $notificacoes_administracao_e_comunicacao,
        'notificacoes_areas_comuns'                 => $notificacoes_areas_comuns,

        'has_notificacoes_administracao_e_comunicacao' => $has_notificacoes_administracao_e_comunicacao,
        'has_notificacoes_areas_comuns'                => $has_notificacoes_areas_comuns

      ]);

    }
  }


}
