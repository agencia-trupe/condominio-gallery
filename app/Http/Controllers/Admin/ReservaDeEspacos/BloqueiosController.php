<?php

namespace Gallery\Http\Controllers\Admin\ReservaDeEspacos;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Carbon\Carbon as Carbon;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Gallery\Models\Espaco;
use Gallery\Models\EspacoReservaPorPeriodo;
use Gallery\Models\EspacoReservaPorDiaria;

class BloqueiosController extends BaseAdminController
{

  public function index(Request $request)
  {
    $filtro = $request->input('filtro');
    $espaco_id = $request->has('espaco_id') ? $request->input('espaco_id') : false;

    $data = $request->input('data');

    if($request->has('data'))
      $data = Carbon::createFromFormat('Y-m-d_H:i', $data);
    else
      $data = Carbon::now();

    $listaEspacosSelect = Espaco::ordenado()->get();

    if($filtro == 'por_periodo'){

      $listaEspacos = Espaco::porTipo($filtro)->ordenado()->get();

      $registros = EspacoReservaPorPeriodo::bloqueios()
                                          ->aPartirDeDataEHora($data->format('Y-m-d H:i'))
                                          ->espaco($espaco_id)
                                          ->orderBy('reserva', 'asc')
                                          ->orderBy('espacos_id', 'id')
                                          ->get();


    }elseif($filtro == 'diaria'){

      $listaEspacos = Espaco::porTipo($filtro)->ordenado()->get();

      $registros = EspacoReservaPorDiaria::bloqueios()
                                          ->datasFuturas($data->format('Y-m-d'))
                                          ->espaco($espaco_id)
                                          ->orderBy('reserva', 'asc')
                                          ->orderBy('espacos_id', 'id')
                                          ->get();

    } else{

      $listaEspacos = Espaco::ordenado()->get();

      $registros = [];

    }

    return view('admin.reserva-de-espacos.bloqueios.index', compact(
      'registros',
      'filtro',
      'espaco_id',
      'data',
      'listaEspacos',
      'listaEspacosSelect'
    ));
  }

  public function store(Request $request)
  {
    $tipo_espaco = $request->input('tipo_espaco');
    $formato_data = $tipo_espaco == 'por_periodo' ? 'd/m/Y H:i' : 'd/m/Y';

    $this->validate($request, [
      'bloqueio.espacos_id'   => 'required|exists:espacos,id',
      'bloqueio.data_inicio'  => 'required|date_format:'.$formato_data,
      'bloqueio.data_termino' => 'required|date_format:'.$formato_data.'|after_equal:bloqueio.data_inicio'
    ]);

    $espacos_id = $request->input('bloqueio.espacos_id');
    $datetime_inicio = $request->input('bloqueio.data_inicio');
    $datetime_termino = $request->input('bloqueio.data_termino');

    $espaco = Espaco::find($espacos_id);

    if($espaco->tipo == 'por_periodo'){

      list($horas_inicio_reservas, $minutos_inicio_reservas) = explode(':', $espaco->informacoes->horario_abertura);

      $inicio_bloqueio = Carbon::createFromFormat('d/m/Y H:i', $datetime_inicio);

      $inicio_reservas  = Carbon::createFromFormat('d/m/Y H:i', $datetime_inicio);
      $inicio_reservas->hour   = $horas_inicio_reservas;
      $inicio_reservas->minute = $minutos_inicio_reservas;

      if($inicio_bloqueio < $inicio_reservas){
        $inicio_bloqueio->hour   = $horas_inicio_reservas;
        $inicio_bloqueio->minute = $minutos_inicio_reservas;
      }

      $fim_bloqueio = Carbon::createFromFormat('d/m/Y H:i', $datetime_termino);

      for (
        $horario = $inicio_bloqueio;
        $horario < $fim_bloqueio;
        $horario = $horario->addMinutes($espaco->informacoes->tempo_de_reserva)
      ){

        EspacoReservaPorPeriodo::espaco($espaco->id)->acharPorHorario($horario->format('Y-m-d H:i'))->delete();

        $novo_bloqueio = EspacoReservaPorPeriodo::create([
          'espacos_id'  => $espaco->id,
          'is_bloqueio' => 1,
          'reserva'     => $horario->format('Y-m-d H:i'),
          'anotacoes'   => 'bloqueado pela administração'
        ]);

        Event::fire( new AtividadeRastreavel('bloqueio_datas_por_periodo_inserido', $novo_bloqueio, Auth::admin()->get()));
      }

    }else{

      $inicio_bloqueio = Carbon::createFromFormat('d/m/Y', $datetime_inicio);
      $fim_bloqueio = Carbon::createFromFormat('d/m/Y', $datetime_termino);

      for (
        $data = $inicio_bloqueio;
        $data <= $fim_bloqueio;
        $data = $data->addDay()
      ){

        EspacoReservaPorDiaria::espaco($espaco->id)->acharPorData($data->format('d/m/Y'))->forceDelete();

        $novo_bloqueio = EspacoReservaPorDiaria::create([
          'espacos_id'  => $espaco->id,
          'is_bloqueio' => 1,
          'reserva'     => $data->format('d/m/Y')
        ]);

        Event::fire( new AtividadeRastreavel('bloqueio_datas_por_diaria_inserido', $novo_bloqueio, Auth::admin()->get()));
      }

    }


    $request->session()->flash('sucesso', 'Bloqueio de Datas cadastrado com sucesso.');

    return redirect()->route('admin.reservas-bloqueios.index', ['filtro' => $espaco->tipo]);
  }

  public function destroy(Request $request, $id)
  {
    $tipo_reserva = $request->input('_tipo_reserva');

    if($tipo_reserva == 'por_periodo'){

      $object = EspacoReservaPorPeriodo::findOrFail($id);

      $espaco_id = $object->espacos_id;

    }else{

      $object = EspacoReservaPorDiaria::findOrFail($id);

      $espaco_id = $object->espacos_id;

    }

    $object->delete();

    $request->session()->flash('sucesso', 'Reserva de espaço removida com sucesso.');

    Event::fire( new AtividadeRastreavel('reserva_espaco_removida', $object, Auth::admin()->get()));

    return redirect()->route('admin.reservas-bloqueios.index', [
      'filtro' => $tipo_reserva
    ]);
  }
}
