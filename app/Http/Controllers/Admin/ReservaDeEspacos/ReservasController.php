<?php

namespace Gallery\Http\Controllers\Admin\ReservaDeEspacos;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Events\AdminVisualizouReservaPorPeriodo;
use Gallery\Events\AdminVisualizouReservaPorDiaria;
use Carbon\Carbon as Carbon;
use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\Espaco;
use Gallery\Models\EspacoReservaPorPeriodo;
use Gallery\Models\EspacoReservaPorDiaria;

class ReservasController extends BaseAdminController
{

  public function index(Request $request)
  {
    $filtro = $request->input('filtro');
    $espaco_id = $request->has('espaco_id') ? $request->input('espaco_id') : false;

    $data = $request->input('data');

    if($request->has('data')){

      if($filtro == 'por_periodo')
        $data = Carbon::createFromFormat('Y-m-d_H:i', $data);

      elseif($filtro == 'diaria')
        $data = Carbon::createFromFormat('Y-m-d', $data);

    }else

    	$data = Carbon::now();

      if($filtro == 'por_periodo')
        $listaEspacos = Espaco::porTipo('por_periodo')->ordenado()->get();
      elseif($filtro == 'diaria')
        $listaEspacos = Espaco::porTipo('diaria')->ordenado()->get();
      else
        $listaEspacos = Espaco::ordenado()->get();


      if($filtro == 'por_periodo'){

      	$registros = EspacoReservaPorPeriodo::semBloqueios()
                                            ->aPartirDeDataEHora($data->format('Y-m-d H:i'))
        										                ->espaco($espaco_id)
        										                ->orderBy('reserva', 'asc')
        										                ->get();


    }elseif($filtro == 'diaria'){

    	$registros = EspacoReservaPorDiaria::semBloqueios()
                                            ->datasFuturas($data->format('Y-m-d'))
                                            ->espaco($espaco_id)
                                            ->orderBy('reserva', 'asc')
                                            ->get();

    } else{

      	$registros = [];

    }

    return view('admin.reserva-de-espacos.reservas.index', compact(
      'registros',
      'filtro',
      'espaco_id',
      'data',
      'listaEspacos'
    ));
  }

  public function show(Request $request, $id)
  {
    $tipo = $request->input('tipo');

    if($tipo == 'por_periodo')
    {
      $registro = EspacoReservaPorPeriodo::find($id);
      Event::fire( new AdminVisualizouReservaPorPeriodo($registro) );
    }
    else
    {
      $registro = EspacoReservaPorDiaria::find($id);
      Event::fire( new AdminVisualizouReservaPorDiaria($registro) );
    }

    $this->carregarNotificacoes();

    return view('admin.reserva-de-espacos.reservas.show', compact('registro'));
  }

  public function destroy(Request $request, $id)
  {
  	$tipo_reserva = $request->input('_tipo_reserva');

	  if($tipo_reserva == 'por_periodo'){

  		$object = EspacoReservaPorPeriodo::find($id);

  		$espaco_id = $object->espacos_id;

      $object->delete();

    }else{

      $object = EspacoReservaPorDiaria::find($id);

  		$espaco_id = $object->espacos_id;

      $object->delete();

    }

    Event::fire( new AtividadeRastreavel('reserva_removida', $object, Auth::admin()->get()));
    $request->session()->flash('sucesso', 'Reserva de espaço removida com sucesso.');

    return redirect()->route('admin.reservas-de-espacos.index', [
		  'filtro' => $tipo_reserva,
		  'espaco_id' => $espaco_id
    ]);
  }
}
