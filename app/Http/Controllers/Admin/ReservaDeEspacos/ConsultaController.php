<?php

namespace Gallery\Http\Controllers\Admin\ReservaDeEspacos;

use Carbon\Carbon as Carbon;
use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Gallery\Models\Espaco;
use Gallery\Models\EspacoReservaPorPeriodo;
use Gallery\Models\EspacoReservaPorDiaria;

class ConsultaController extends BaseAdminController
{

  public function getIndex(Request $request)
  {
    $filtro = 'consulta';
    $listaEspacos = Espaco::ordenado()->get();

    if($request->has('espaco_id')){
      $espaco_id = $request->espaco_id;
      $espaco_selecionado = Espaco::findOrFail($espaco_id);
    }else{
      $espaco_id = null;
      $espaco_selecionado = null;
    }

    if($request->has('data_inicio')){
      $data_inicio = Carbon::createFromFormat('d/m/Y', $request->data_inicio);
    }else{
    	$data_inicio = '';
    }

    if($request->has('data_termino')){
      $data_termino = Carbon::createFromFormat('d/m/Y', $request->data_termino);
    }else{
    	$data_termino = '';
    }

    $incluir_bloqueios = $request->incluir_bloqueios;

    if($espaco_selecionado && $espaco_selecionado->tipo == 'por_periodo'){

    	$registros = EspacoReservaPorPeriodo::semBloqueios()
                                          ->aPartirDeDataEHora($data_inicio->format('Y-m-d').' 00:00:00')
                                          ->ateDataEHora($data_termino->format('Y-m-d').' 00:00:00')
        										              ->espaco($espaco_id)
        										              ->orderBy('reserva', 'asc')
        										              ->get();


    }elseif($espaco_selecionado && $espaco_selecionado->tipo == 'diaria'){

      if($incluir_bloqueios == 1){
      	$registros = EspacoReservaPorDiaria::deData($data_inicio->format('Y-m-d'))
                                            ->ateData($data_termino->format('Y-m-d'))
                                            ->espaco($espaco_id)
                                            ->orderBy('reserva', 'asc')
                                            ->get();
      }else{
        $registros = EspacoReservaPorDiaria::semBloqueios()
                                            ->deData($data_inicio->format('Y-m-d'))
                                            ->ateData($data_termino->format('Y-m-d'))
                                            ->espaco($espaco_id)
                                            ->orderBy('reserva', 'asc')
                                            ->get();
      }
    }else{

    	$registros = [];

    }

    return view('admin.reserva-de-espacos.reservas.consulta', compact(
      'registros',
      'filtro',
      'espaco_id',
      'espaco_selecionado',
      'data_inicio',
      'data_termino',
      'listaEspacos',
      'incluir_bloqueios'
    ));
  }

  public function postDownload(Request $request){
    $formato = $request->formato;
    $espaco_id = $request->hid_espaco_id;
    $incluir_bloqueios = $request->hid_incl_bloq;
    $espaco_selecionado = Espaco::findOrFail($espaco_id);
    $data_inicio = Carbon::createFromFormat('d/m/Y', $request->hid_dt_ini_id);
    $data_termino = Carbon::createFromFormat('d/m/Y', $request->hid_dt_fim_id);

    if($espaco_selecionado->tipo == 'por_periodo'){
      if($incluir_bloqueios == 1){
        $registros = EspacoReservaPorPeriodo::aPartirDeDataEHora($data_inicio->format('Y-m-d').' 00:00:00')
                                            ->ateDataEHora($data_termino->format('Y-m-d').' 00:00:00')
          										              ->espaco($espaco_id)
          										              ->orderBy('reserva', 'asc')
          										              ->get();
      }else{
        $registros = EspacoReservaPorPeriodo::semBloqueios()
                                            ->aPartirDeDataEHora($data_inicio->format('Y-m-d').' 00:00:00')
                                            ->ateDataEHora($data_termino->format('Y-m-d').' 00:00:00')
          										              ->espaco($espaco_id)
          										              ->orderBy('reserva', 'asc')
          										              ->get();
      }

    }elseif($espaco_selecionado->tipo == 'diaria'){
      if($incluir_bloqueios == 1){
        $registros = EspacoReservaPorDiaria::deData($data_inicio->format('Y-m-d'))
                                            ->ateData($data_termino->format('Y-m-d'))
                                            ->espaco($espaco_id)
                                            ->orderBy('reserva', 'asc')
                                            ->get();
      }else{
        $registros = EspacoReservaPorDiaria::semBloqueios()
                                            ->deData($data_inicio->format('Y-m-d'))
                                            ->ateData($data_termino->format('Y-m-d'))
                                            ->espaco($espaco_id)
                                            ->orderBy('reserva', 'asc')
                                            ->get();
      }

    }

    $filename = 'Gallery-relatorio-'.$espaco_selecionado->slug;

    $registrosArray = [];

    foreach ($registros as $key => $value) {
      $registrosArray[$key]['Espaço'] = $value->espacoReservado->titulo;

      if($espaco_selecionado->tipo == 'por_periodo')
        $registrosArray[$key]['Data da Reserva'] = $value->reserva->format('d/m/Y H:i');

      elseif($espaco_selecionado->tipo == 'diaria')
        $registrosArray[$key]['Data da Reserva'] = $value->reserva->format('d/m/Y');

      if(is_null($value->moradores_id)){
        $registrosArray[$key]['Morador'] = 'Bloqueio';
        $registrosArray[$key]['Unidade'] = 'Administração';
      }else{
        $registrosArray[$key]['Morador'] = $value->morador->getNomeCompleto();
        $registrosArray[$key]['Unidade'] = $value->morador->getUnidade();
      }

      $registrosArray[$key]['Reserva feita em'] = $value->created_at->format('d/m/Y H:i');
    }

    \Excel::create($filename, function($excel) use($registrosArray) {
	    $excel->sheet('Relatório de Uso de Espaço', function($sheet) use ($registrosArray) {
	      $sheet->fromArray($registrosArray);
	    });
		})->download($formato);
  }
}
