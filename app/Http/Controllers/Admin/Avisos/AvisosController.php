<?php

namespace Gallery\Http\Controllers\Admin\Avisos;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Events\AdminCriouAviso;
use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\Aviso;

class AvisosController extends BaseAdminController
{

  public function index()
  {
    $registros = Aviso::ordenado()->get();
    return view('admin.avisos.index', compact('registros'));
  }

  public function create()
  {
    return view('admin.avisos.create');
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'titulo' => 'required',
      'olho'   => 'required',
      'texto'  => 'required',
      'assinatura' => 'required'
    ]);

    $object = Aviso::create($request->input());

    try {

      $object->save();

      $request->session()->flash('sucesso', 'Aviso criado com sucesso.');

      Event::fire( new AtividadeRastreavel('aviso_criado', $object, Auth::admin()->get()));
      Event::fire(new AdminCriouAviso());

      return redirect()->route('admin.avisos.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar aviso! ('.$e->getMessage().')'));

    }
  }

  public function edit($id)
  {
    return view('admin.avisos.edit', ['registro' => Aviso::find($id)]);
  }

  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'titulo' => 'required',
      'olho'   => 'required',
      'texto'  => 'required',
      'assinatura' => 'required'
    ]);

    $object = Aviso::find($id);

    $object->fill($request->input());

    try {

      $object->save();

      Event::fire( new AtividadeRastreavel('aviso_alterado', $object, Auth::admin()->get()));
      $request->session()->flash('sucesso', 'Aviso alterado com sucesso.');

      return redirect()->route('admin.avisos.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar aviso! ('.$e->getMessage().')'));

    }
  }

  public function destroy(Request $request, $id)
  {
    $object = Aviso::find($id);
    $object->delete();

    Event::fire( new AtividadeRastreavel('aviso_removido', $object, Auth::admin()->get()));
    $request->session()->flash('sucesso', 'Aviso removido com sucesso.');

    return redirect()->route('admin.avisos.index');
  }

}
