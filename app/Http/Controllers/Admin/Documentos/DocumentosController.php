<?php

namespace Gallery\Http\Controllers\Admin\Documentos;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Events\AdminCriouDocumento;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\DocumentoCategoria;
use Gallery\Models\Documento;

class DocumentosController extends BaseAdminController
{

  protected $uploadPath = 'assets/documentos';

  public function index()
  {
    $registros  = Documento::ordenado()->get();
    return view('admin.documentos.index', compact('registros'));
  }

  public function create()
  {
    $categorias = DocumentoCategoria::ordenado()->get();
    return view('admin.documentos.create', compact('categorias'));
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'documentos_categoria_id' => 'required|exists:documentos_categorias,id',
      'titulo'  => 'required',
      'arquivo' => 'required|mimes:pdf'
    ]);

    $object = new Documento;

    $object->documentos_categoria_id = $request->input('documentos_categoria_id');
    $object->titulo = $request->input('titulo');
    $object->slug = str_slug($object->titulo);
    $object->texto = $request->input('texto');

    if($request->file('arquivo')){
      $arquivo = $request->file('arquivo');
      $filename = $object->slug.'.'.$arquivo->getClientOriginalExtension();
      if($arquivo->move($this->uploadPath, $filename))
        $object->arquivo = $filename;
    }

    try {

      $object->save();

      $request->session()->flash('sucesso', 'Documento criado com sucesso.');

      Event::fire( new AdminCriouDocumento());
      Event::fire( new AtividadeRastreavel('documento_criado', $object, Auth::admin()->get()));

      return redirect()->route('admin.documentos.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar Documento! ('.$e->getMessage().')'));

    }
  }

  public function edit($id)
  {
    $categorias = DocumentoCategoria::ordenado()->get();
    return view('admin.documentos.edit', [
      'categorias' => $categorias,
      'registro' => Documento::find($id)
    ]);
  }

  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'documentos_categoria_id' => 'required|exists:documentos_categorias,id',
      'titulo'  => 'required',
      'arquivo' => 'sometimes|mimes:pdf'
    ]);

    $object = Documento::find($id);

    $object->documentos_categoria_id = $request->input('documentos_categoria_id');
    $object->titulo = $request->input('titulo');
    $object->slug = str_slug($object->titulo);
    $object->texto = $request->input('texto');

    if($request->file('arquivo')){
      $arquivo = $request->file('arquivo');
      $filename = $object->slug.'.'.$arquivo->getClientOriginalExtension();
      if($arquivo->move($this->uploadPath, $filename))
        $object->arquivo = $filename;
    }

    try {

      $object->save();

      Event::fire( new AtividadeRastreavel('documento_alterado', $object, Auth::admin()->get()));

      $request->session()->flash('sucesso', 'Documento alterado com sucesso.');

      return redirect()->route('admin.documentos.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar documento! ('.$e->getMessage().')'));

    }
  }

  public function destroy(Request $request, $id)
  {
    $object = Documento::find($id);

    Event::fire( new AtividadeRastreavel('documento_removido', $object, Auth::admin()->get()));

    $object->delete();

    $request->session()->flash('sucesso', 'Documento removido com sucesso.');

    return redirect()->route('admin.documentos.index');
  }

}
