<?php

namespace Gallery\Http\Controllers\Admin\Documentos;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\DocumentoCategoria;

class DocumentosCategoriasController extends BaseAdminController
{

    public function index()
    {
        $registros  = DocumentoCategoria::ordenado()->get();
        return view('admin.documentos.categorias.index', compact('registros'));
    }

    public function edit($id)
    {
        return view('admin.documentos.categorias.edit', ['registro' => DocumentoCategoria::find($id)]);
    }

    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'titulo'  => 'required'
      ]);

      $object = DocumentoCategoria::find($id);

      $object->titulo = $request->input('titulo');
      $object->slug = str_slug($object->titulo);
      $object->texto = $request->input('texto');

      try {

        $object->save();

        Event::fire( new AtividadeRastreavel('categoria_documentos_alterada', $object, Auth::admin()->get()));

        $request->session()->flash('sucesso', 'Categoria de Documentos alterada com sucesso.');

        return redirect()->route('admin.documentos-categorias.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar Categoria de Documentos! ('.$e->getMessage().')'));

      }
    }

}
