<?php

namespace Gallery\Http\Controllers\Admin\Usuarios;

use Event;
use Auth;
use Illuminate\Http\Request;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Gallery\Http\Requests;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\Admin;

class UsuariosController extends BaseAdminController
{

  public function __construct(){
    parent::__construct();
    $this->authorize('gerenciar-administradores');
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $registros = Admin::all();
    return view('admin.usuarios.index', compact('registros'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return view('admin.usuarios.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  Request  $request
   * @return Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'tipo'             => 'required|in:master,sindico,conselho,zelador',
      'login'            => 'required|unique:administradores,login',
      'password'         => 'required|min:6',
      'password_confirm' => 'required|min:6|same:password',
    ]);

    $object = new Admin;

    $object->tipo     = $request->input('tipo');
    $object->login    = $request->input('login');
    $object->email    = $request->input('email');
    $object->senha    = $request->input('password');

    try {

      $object->save();

      $request->session()->flash('sucesso', 'Usuário criado com sucesso.');

      Event::fire( new AtividadeRastreavel('usuario_alterado', $object, Auth::admin()->get()));

      return redirect()->route('admin.usuarios.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar usuário! ('.$e->getMessage().')'));

    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    return view('admin.usuarios.edit', ['usuario' => Admin::find($id)]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  Request  $request
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'tipo'             => 'required|in:master,sindico,conselho,zelador',
      'login'            => 'required|unique:administradores,login,'.$id,
      'password'         => 'min:6',
      'password_confirm' => 'required_with:password|min:6|same:password',
    ]);

    $object = Admin::find($id);

    $object->tipo  = $request->input('tipo');
    $object->login = $request->input('login');
    $object->email = $request->input('email');

    if($request->has('password'))
      $object->senha = $request->input('password');

    try {

      $object->save();

      Event::fire( new AtividadeRastreavel('usuario_alterado', $object, Auth::admin()->get()));

      $request->session()->flash('sucesso', 'Usuário alterado com sucesso.');

      return redirect()->route('admin.usuarios.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar usuário! ('.$e->getMessage().')'));

    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    $object = Admin::find($id);
    $object->delete();

    $request->session()->flash('sucesso', 'Usuário removido com sucesso.');

    Event::fire( new AtividadeRastreavel('usuario_removido', $object, Auth::admin()->get()));

    return redirect()->route('admin.usuarios.index');
  }
}
