<?php

namespace Gallery\Http\Controllers\Admin\Mudancas;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Events\AdminVisualizouAgendamentoDeMudanca;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Gallery\Models\AgendamentoDeMudanca;

class AgendamentoDeMudancaController extends BaseAdminController
{

  public function index()
  {
    $registros = AgendamentoDeMudanca::ordenado()->proximasDatas()->get();
    return view('admin.mudancas.agendamento.index', compact('registros'));
  }

  public function show($id)
  {
    $agendamento = AgendamentoDeMudanca::find($id);

    Event::fire(new AdminVisualizouAgendamentoDeMudanca($agendamento));
    $this->carregarNotificacoes();

    return view('admin.mudancas.agendamento.show', ['registro' => $agendamento]);
  }

  public function destroy(Request $request, $id)
  {
    $object = AgendamentoDeMudanca::find($id);

    $object->delete();

    $request->session()->flash('sucesso', 'Agendamento de Mudança cancelado com sucesso.');

    Event::fire( new AtividadeRastreavel('agendamento_mudanca_removido', $object, Auth::admin()->get()));

    return redirect()->route('admin.agendamento-de-mudanca.index');
  }

}
