<?php

namespace Gallery\Http\Controllers\Admin\Moradores;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Models\Morador;
use Gallery\Models\Ocorrencia;
use Gallery\Models\FaleComMensagem;

class MoradoresController extends BaseAdminController
{

  public function index()
  {
    $registros = Morador::orderBy('nome', 'ASC')->get();

    return view('admin.morador.index', compact('registros'));
  }

  public function show($id_morador)
  {
    $registro = Morador::findOrFail($id_morador);

    return view('admin.morador.show', compact('registro'));
  }

  public function destroy(Request $request, $id)
  {
    $object = Morador::find($id);

    $this->updateOcorrenciasTable($object);
    $this->updateMensagensTable($object);

    $object->delete();

    Event::fire( new AtividadeRastreavel('morador_removido', $object, Auth::admin()->get()));
    $request->session()->flash('sucesso', 'Morador removido com sucesso.');

    return redirect()->route('admin.moradores.index');
  }

  private function updateOcorrenciasTable($morador)
  {
    $ocorrencias = Ocorrencia::where('autor_id', $morador->id)->get();

    foreach ($ocorrencias as $ocorrencia) {
      $ocorrencia->morador_removido = $morador->getNomeCompleto() . ' - ' . $morador->getUnidade();
      $ocorrencia->save();
    }
  }

  private function updateMensagensTable($morador)
  {
    $mensagens = FaleComMensagem::where('moradores_id', $morador->id)->get();

    foreach ($mensagens as $mensagem) {
      $mensagem->morador_removido = $morador->getNomeCompleto() . ' - ' . $morador->getUnidade();
      $mensagem->save();
    }
  }
}
