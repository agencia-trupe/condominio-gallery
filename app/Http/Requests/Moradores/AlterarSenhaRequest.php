<?php

namespace Gallery\Http\Requests\Moradores;

use Gallery\Http\Requests\Request;

class AlterarSenhaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'moradores.senha_atual'			    => 'required|same_password',
          'moradores.senha_nova'          => 'required|same:moradores.confirmacao_senha',
          'moradores.confirmacao_senha'   => 'required'
        ];
    }
}
