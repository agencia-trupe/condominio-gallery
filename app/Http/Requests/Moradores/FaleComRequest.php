<?php

namespace Gallery\Http\Requests\Moradores;

use Gallery\Http\Requests\Request;

class FaleComRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'falecom.assunto'  => 'required',
          'falecom.mensagem' => 'required'
        ];
    }
}
