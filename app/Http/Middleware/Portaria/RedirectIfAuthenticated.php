<?php

namespace Gallery\Http\Middleware\Portaria;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
    * URL para redirecionar quando o usuário acessar
    * uma URL protegida para Guests enquanto logado
    */
    protected $redirectUrl = 'portaria/';

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::portaria()->check()) {
            return redirect($this->redirectUrl);
        }

        return $next($request);
    }
}
