<?php

namespace Gallery\Listeners;

use Gallery\Events\AdminVisualizouOcorrencia;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Auth;
use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\OcorrenciaLida;

class ExcluirNotificacaoOcorrencia
{

    protected $notificacao;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificacaoAdmin $notificacao)
    {
        $this->notificacao = $notificacao;
    }

    /**
     * Handle the event.
     *
     * @param  AdminVisualizouOcorrencia  $event
     * @return void
     */
    public function handle(AdminVisualizouOcorrencia $event)
    {
        $link_visitado = 'admin/ocorrencias/'.$event->ocorrencia->id.'/show';

        // Remover notificação da Ocorrência original
        $this->notificacao
             ->ocorrencias()
             ->paraAdministrador(Auth::admin()->get()->id)
             ->where('ocorrencias_id', $event->ocorrencia->id)
             ->delete();

        // Marcar Ocorrência original como lida se
        // não tiver sido lida ainda

        // if(is_null($event->ocorrencia->visto_em)){
        //     $event->ocorrencia->visto_em = date('Y-m-d H:i:s');
        //     $event->ocorrencia->save();
        // }

        $check_lida = OcorrenciaLida::where('ocorrencias_id', $event->ocorrencia->id)
                                    ->where('administradores_id', Auth::admin()->get()->id)
                                    ->get();

        if(sizeof($check_lida) == 0){
            OcorrenciaLida::create([
                'administradores_id' => Auth::admin()->get()->id,
                'ocorrencias_id'     => $event->ocorrencia->id
            ]);
        }

        foreach($event->ocorrencia->respostas as $value){
            // Se as respostas forem de morador,
            if($value->origem == 'morador'){

                // Se a resposta não tiver sido vista ainda
                $check_lida = OcorrenciaLida::where('ocorrencias_id', $value->id)
                                            ->where('administradores_id', Auth::admin()->get()->id)
                                            ->get();

                if(sizeof($check_lida) == 0){

                    OcorrenciaLida::create([
                        'administradores_id' => Auth::admin()->get()->id,
                        'ocorrencias_id'     => $value->id
                    ]);

                    // Remover notificação da resposta
                    $this->notificacao
                         ->ocorrencias()
                         ->paraAdministrador(Auth::admin()->get()->id)
                         ->where('ocorrencias_id', $value->id)
                         ->delete();

                }
            }
        }
    }
}
