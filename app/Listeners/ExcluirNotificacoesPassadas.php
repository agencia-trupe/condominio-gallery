<?php

namespace Gallery\Listeners;

use Gallery\Events\AdminLogin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;

class ExcluirNotificacoesPassadas
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdminLogin  $event
     * @return void
     */
    public function handle(AdminLogin $event)
    {
      // Agendamento de Mudança
      $notificacoes = NotificacaoAdmin::whereHas('agendamento_mudanca', function ($query) {
        $query->where('data', '<', date('Y-m-d'));
      })->delete();

      // Reservas por período
      $notificacoes = NotificacaoAdmin::whereHas('reserva_periodo', function ($query) {
        $query->where('reserva', '<', date('Y-m-d'));
      })->delete();

      // Reservas por diaria
      $notificacoes = NotificacaoAdmin::whereHas('reserva_diaria', function ($query) {
        $query->where('reserva', '<', date('Y-m-d'));
      })->delete();

    }
}
