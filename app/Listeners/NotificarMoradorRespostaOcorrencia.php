<?php

namespace Gallery\Listeners;

use Gallery\Events\AdminRespondeuOcorrencia;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoMorador;

class NotificarMoradorRespostaOcorrencia
{

    protected $notificacao;


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificacaoMorador $notificacao)
    {
        $this->notificacao = $notificacao;
    }

    /**
     * Handle the event.
     *
     * @param  AdminRespondeuOcorrencia  $event
     * @return void
     */
    public function handle(AdminRespondeuOcorrencia $event)
    {
        $this->notificacao->tipo_notificacao = 'ocorrencia';
        $this->notificacao->mensagem         = "A Administração respondeu sua ocorrência.";
        $this->notificacao->moradores_id     = $event->ocorrencia->mensagemInicial->autor->id;
        $this->notificacao->ocorrencias_id   = $event->ocorrencia->id;
        $this->notificacao->save();
    }
}
