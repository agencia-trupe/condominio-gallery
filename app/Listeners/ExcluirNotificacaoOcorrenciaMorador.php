<?php

namespace Gallery\Listeners;

use Auth;
use Gallery\Events\MoradorVisualizouOcorrencias;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoMorador;

class ExcluirNotificacaoOcorrenciaMorador
{
    protected $notificacao;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificacaoMorador $notificacao)
    {
        $this->notificacao = $notificacao;
    }

    /**
     * Handle the event.
     *
     * @param  MoradorVisualizouOcorrencias  $event
     * @return void
     */
    public function handle()
    {
        $this->notificacao
             ->paraMorador(Auth::moradores()->get()->id)
             ->ocorrencias()
             ->delete();
    }
}
