<?php

namespace Gallery\Listeners\Traits;

use Gallery\Models\Morador;
use Gallery\Models\FilaEmails;
use Mail;
use Log;
use View;

trait NotificarMoradorPorEmail{

  protected $template_email;
  protected $titulo_notificacao;
  protected $morador;

  private function colocar_na_fila() {

    $data['morador'] = $this->morador;
    $data['titulo_notificacao'] = $this->titulo_notificacao;

    /***************************************************************************

    Coloca os emails em uma lista no banco de dados para processamento
    posterior

    FilaEmails::create([
     'destino_nome'  => $data['morador']->nome,
     'destino_email' => $data['morador']->email,
     'titulo'        => $this->titulo_notificacao,
     'mensagem'      => View::make($this->template_email, $data)->render()
    ]);

    $log_str = "Email para {$data['morador']->nome} ({$data['morador']->email})
                colocado na Fila de envio.";

    Log::info($log_str);

    ***************************************************************************/

    /***************************************************************************
    Envio direto do e-mail
    ***************************************************************************/
    \Log::info('envio de e-mail');
    Mail::send( $this->template_email, $data, function ($m) use ($data) {
      $m->to($data['morador']->email, $data['morador']->nome)        
        ->subject($data['titulo_notificacao']);
    });

    if(count(Mail::failures()) > 0){
      \Log::info('erro ao enviar e-mail');
      \Log::info(Mail::failures());
    }

    if(config('mail.pretend')) Log::info(View::make($this->template_email, $data)->render());
    \Log::info('sucesso');
  }

  public function enviar_email_nova_correspondencia(Morador $morador) {
    $this->titulo_notificacao = 'Nova Correspondência na Portaria';
    $this->template_email = 'moradores.templates.emails.notificacao-correspondencia';
    $this->morador = $morador;
    \Log::info('aviso preparado');
    $this->colocar_na_fila();
  }

  public function enviar_email_novo_aviso(Morador $morador) {
    $this->titulo_notificacao = 'Novo Aviso publicado';
    $this->template_email = 'moradores.templates.emails.notificacao-aviso';
    $this->morador = $morador;
    $this->colocar_na_fila();
  }

  public function enviar_email_novo_documento(Morador $morador) {
    $this->titulo_notificacao = 'Novo Documento publicado';
    $this->template_email = 'moradores.templates.emails.notificacao-documento';
    $this->morador = $morador;
    $this->colocar_na_fila();
  }

  public function enviar_email_nova_mensagem() {

  }

}
