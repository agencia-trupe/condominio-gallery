<?php

namespace Gallery\Listeners;

use Auth;
use Gallery\Events\AdminVisualizouMensagemPortaria;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\ComunicacaoAdministracaoLido;

class ExcluirNotificacaoMensagemPortaria
{

  protected $notificacao;

  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct(NotificacaoAdmin $notificacao)
  {
    $this->notificacao = $notificacao;
  }

  /**
   * Handle the event.
   *
   * @param  AdminVisualizouMensagemPortaria  $event
   * @return void
   */
  public function handle(AdminVisualizouMensagemPortaria $event)
  {
    // Remover notificação da Mensagem original
    // mas só remover se for o perfil destinatário
    // lançando o evento.

    $this->notificacao
         ->mensagensPortaria()
         ->paraAdministrador(Auth::admin()->get()->id)
         ->where('comunicacao_adm_id', $event->mensagem->id)
         ->delete();


    // Marcar Mensagem original como lida se
    // não tiver sido lida ainda
    $check_lida = ComunicacaoAdministracaoLido::where('comunicacao_adm_id', $event->mensagem->id)
                                              ->where('administradores_id', Auth::admin()->get()->id)
                                              ->get();

    if(sizeof($check_lida) == 0){
      ComunicacaoAdministracaoLido::create([
        'administradores_id' => Auth::admin()->get()->id,
        'comunicacao_adm_id' => $event->mensagem->id
      ]);
    }
  }
}
