<?php

namespace Gallery\Listeners;

use Gallery\Events\PortariaCadastrouEncomenda;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoMorador;
use Gallery\Models\Unidade;
use Gallery\Listeners\Traits\NotificarMoradorPorEmail;

class NotificarMoradoresUnidadeEncomenda
{

  use NotificarMoradorPorEmail;

  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct()
  {
      //
  }

  /**
   * Handle the event.
   *
   * @param  PortariaCadastrouEncomenda  $event
   * @return void
   */
  public function handle(PortariaCadastrouEncomenda $event)
  {
    $unidade = Unidade::find($event->encomenda->unidades_id);

    foreach ($unidade->moradores as $key => $morador) {

      $notificacao = new NotificacaoMorador;
      $notificacao->tipo_notificacao = 'encomenda';
      $notificacao->mensagem         = "Tem encomenda para ser retirada na portaria.";
      $notificacao->moradores_id     = $morador->id;
      $notificacao->encomendas_id    = $event->encomenda->id;
      $notificacao->save();

      if(sizeof($morador->preferencias)){
        if($morador->preferencias->enviar_email_nova_correspondencia){
          $this->enviar_email_nova_correspondencia($morador);
        }
      }

    }
  }
}
