<?php

namespace Gallery\Listeners;

use Gallery\Events\PortariaEnviouComunicacao;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\Admin;

class NotificarAdminMensagemPortaria
{
  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Handle the event.
   *
   * @param  PortariaEnviouComunicacao  $event
   * @return void
   */
  public function handle(PortariaEnviouComunicacao $event)
  {
    $notificar = [
      $event->comunicacao->destinatario
    ];

    foreach ($notificar as $perfil) {

      $admins = Admin::where('tipo', $perfil)->get();

      foreach($admins as $admin){

        NotificacaoAdmin::create([
          'tipo_notificacao'   => 'mensagem_portaria',
          'mensagem'           => "A Portaria enviou uma nova mensagem.".$event->comunicacao->id,
          'administradores_id' => $admin->id,
          'comunicacao_adm_id' => $event->comunicacao->id
        ]);

      }
    }
  }

}