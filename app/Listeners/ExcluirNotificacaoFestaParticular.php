<?php

namespace Gallery\Listeners;

use Auth;
use Gallery\Events\AdminVisualizouFestaParticular;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\FestaParticularLido;

class ExcluirNotificacaoFestaParticular
{

    protected $notificacao;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificacaoAdmin $notificacao)
    {
        $this->notificacao = $notificacao;
    }

    /**
     * Handle the event.
     *
     * @param  AdminVisualizouFestaParticular  $event
     * @return void
     */
    public function handle(AdminVisualizouFestaParticular $event)
    {
        $this->notificacao
             ->festasParticulares()
             ->paraAdministrador(Auth::admin()->get()->id)
             ->where('festas_particulares_id', $event->festa->id)
             ->delete();


        // Marcar Mensagem original como lida se
        // não tiver sido lida ainda
        $check_lida = FestaParticularLido::where('festas_particulares_id', $event->festa->id)
                                                ->where('administradores_id', Auth::admin()->get()->id)
                                                ->get();

        if(sizeof($check_lida) == 0){
            FestaParticularLido::create([
                'administradores_id'  => Auth::admin()->get()->id,
                'festas_particulares_id' => $event->festa->id
            ]);
        }
    }
}
