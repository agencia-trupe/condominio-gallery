<?php

namespace Gallery\Listeners;

use Gallery\Events\MoradorEnviouMensagem;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;
use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\Admin;
use Gallery\Listeners\Traits\NotificarAdminPorEmail;

class NotificarAdminMensagemMorador
{

  use NotificarAdminPorEmail;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  MoradorEnviouMensagem  $event
     * @return void
     */
    public function handle(MoradorEnviouMensagem $event)
    {
      $morador = $event->mensagem->autor->getNomeCompleto();

      $notificar = [
        $event->mensagem->destino,
        'master'
      ];

      foreach ($notificar as $perfil) {

        $admins = Admin::where('tipo', $perfil)->get();

        foreach($admins as $admin){

          NotificacaoAdmin::create([
            'tipo_notificacao'          => 'mensagem',
            'mensagem'                  => "{$morador} enviou uma nova mensagem.",
            'administradores_id'        => $admin->id,
            'fale_com_mensagens_id'     => $event->mensagem->id
          ]);

          if($admin->email != '')
            $this->enviar_email_nova_mensagem($event->mensagem, $admin->email);

        }
      }
    }
}
