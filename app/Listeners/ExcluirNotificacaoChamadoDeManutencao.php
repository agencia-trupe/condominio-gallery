<?php

namespace Gallery\Listeners;

use Auth;
use Gallery\Events\AdminVisualizouChamadoDeManutencao;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\ChamadoDeManutencao;
use Gallery\Models\ChamadoDeManutencaoFoto;
use Gallery\Models\ChamadoDeManutencaoLido;

class ExcluirNotificacaoChamadoDeManutencao
{

    protected $notificacao;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificacaoAdmin $notificacao)
    {
        $this->notificacao = $notificacao;
    }

    /**
     * Handle the event.
     *
     * @param  AdminVisualizouChamadoDeManutencao  $event
     * @return void
     */
    public function handle(AdminVisualizouChamadoDeManutencao $event)
    {
        $link_visitado = 'admin/chamados-de-manutencao/'.$event->chamado->id;

        // Remover notificação de Chamado de Manutenção
        // mas só remover se for o perfil destinatário
        // lançando o evento.

        $this->notificacao
             ->chamadosManutencao()
             ->paraAdministrador(Auth::admin()->get()->id)
             ->where('chamados_de_manutencao_id', $event->chamado->id)
             ->delete();

        // Marcar Mensagem original como lida se
        // não tiver sido lida ainda
        $check_lida = ChamadoDeManutencaoLido::where('chamados_de_manutencao_id', $event->chamado->id)
                                             ->where('administradores_id', Auth::admin()->get()->id)
                                             ->get();

        if(sizeof($check_lida) == 0){
            ChamadoDeManutencaoLido::create([
                'administradores_id'    => Auth::admin()->get()->id,
                'chamados_de_manutencao_id' => $event->chamado->id
            ]);
        }
    }
}
