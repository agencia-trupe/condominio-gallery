<?php

namespace Gallery\Listeners;

use Auth;
use Gallery\Events\AdminVisualizouAgendamentoDeMudanca;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\AgendamentoDeMudancaLido;

class ExcluirNotificacaoMudanca
{
    protected $notificacao;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificacaoAdmin $notificacao)
    {
        $this->notificacao = $notificacao;
    }

    /**
     * Handle the event.
     *
     * @param  AdminVisualizouAgendamentoDeMudanca  $event
     * @return void
     */
    public function handle(AdminVisualizouAgendamentoDeMudanca $event)
    {
        $link_visitado = 'admin/agendamento-de-mudanca/'.$event->agendamento->id;

        // Remover notificação da Mensagem original
        // mas só remover se for o perfil destinatário
        // lançando o evento. O perfil master tem acesso
        // mas não deve excluir a notificação

        $this->notificacao
             ->mudancas()
             ->paraAdministrador(Auth::admin()->get()->id)
             ->where('mudancas_agendamento_id', $event->agendamento->id)
             ->delete();

        // Marcar Mensagem original como lida se
        // não tiver sido lida ainda
        $check_lida = AgendamentoDeMudancaLido::where('mudancas_agendamento_id', $event->agendamento->id)
                                                ->where('administradores_id', Auth::admin()->get()->id)
                                                ->get();

        if(sizeof($check_lida) == 0){
            AgendamentoDeMudancaLido::create([
                'administradores_id'    => Auth::admin()->get()->id,
                'mudancas_agendamento_id' => $event->agendamento->id
            ]);
        }

    }
}
