<?php

namespace Gallery\Listeners;

use Gallery\Events\MoradorCriouFestaParticular;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Gallery\Models\Admin;
use Gallery\Models\NotificacaoAdmin;

class NotificarAdminFestaParticular
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MoradorCriouFestaParticular  $event
     * @return void
     */
    public function handle(MoradorCriouFestaParticular $event)
    {
        $morador = $event->festa->morador->getNomeCompleto();

        $notificar = [
            'sindico',
            'conselho',
            'master'
        ];

        foreach ($notificar as $perfil) {

            $admins = Admin::where('tipo', $perfil)->get();

            foreach($admins as $admin){

                NotificacaoAdmin::create([
                    'tipo_notificacao'          => 'festa_particular',
                    'mensagem'                  => "{$morador} agendou uma nova festa particular.",
                    'administradores_id'        => $admin->id,
                    'festas_particulares_id'    => $event->festa->id
                ]);

            }
        }
    }
}
