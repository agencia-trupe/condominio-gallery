<?php

namespace Gallery\Events;

use Gallery\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AtividadeRastreavel extends Event
{
    use SerializesModels;

    public $acao;
    public $registro;
    public $usuario;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($acao, $registro, $usuario)
    {
      $this->acao = $acao;
      $this->registro = $registro;
      $this->usuario = $usuario;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
