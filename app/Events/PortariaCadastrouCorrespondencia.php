<?php

namespace Gallery\Events;

use Gallery\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Gallery\Models\Correspondencia;

class PortariaCadastrouCorrespondencia extends Event
{
    use SerializesModels;

    public $correspondencia;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Correspondencia $correspondencia)
    {
        $this->correspondencia = $correspondencia;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
