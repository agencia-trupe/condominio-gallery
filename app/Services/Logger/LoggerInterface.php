<?php

namespace Gallery\Services\Logger;

interface LoggerInterface
{

  public function log($action, $row = null, $user = null);
  
}
