<?php

namespace Gallery\Services\Logger;

use Event;
use Gallery\Events\AtividadeRastreavel;

class Logger implements LoggerInterface
{

  public function log($action, $row = null, $user = null)
  {
    Event::fire( new AtividadeRastreavel($action, $row, $user));
  }

}
