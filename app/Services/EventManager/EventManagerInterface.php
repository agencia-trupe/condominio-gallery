<?php

namespace Gallery\Services\EventManager;

interface EventManagerInterface
{

  public function fire($event, $data);
  
}
