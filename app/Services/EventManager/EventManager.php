<?php

namespace Gallery\Services\EventManager;

use Event;

class EventManager implements EventManagerInterface
{

  public function fire($event, $data)
  {

    if ($event == 'MoradorEnviouMensagem')
      return Event::fire(new \Gallery\Events\MoradorEnviouMensagem($data));
      
  }

}
