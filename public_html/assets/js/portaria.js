/**
 * Featherlight - ultra slim jQuery lightbox
 * Version 1.3.5 - http://noelboss.github.io/featherlight/
 *
 * Copyright 2015, Noël Raoul Bossart (http://www.noelboss.com)
 * MIT Licensed.
**/
(function($) {
	"use strict";

	if('undefined' === typeof $) {
		if('console' in window){ window.console.info('Too much lightness, Featherlight needs jQuery.'); }
		return;
	}

	/* Featherlight is exported as $.featherlight.
	   It is a function used to open a featherlight lightbox.

	   [tech]
	   Featherlight uses prototype inheritance.
	   Each opened lightbox will have a corresponding object.
	   That object may have some attributes that override the
	   prototype's.
	   Extensions created with Featherlight.extend will have their
	   own prototype that inherits from Featherlight's prototype,
	   thus attributes can be overriden either at the object level,
	   or at the extension level.
	   To create callbacks that chain themselves instead of overriding,
	   use chainCallbacks.
	   For those familiar with CoffeeScript, this correspond to
	   Featherlight being a class and the Gallery being a class
	   extending Featherlight.
	   The chainCallbacks is used since we don't have access to
	   CoffeeScript's `super`.
	*/

	function Featherlight($content, config) {
		if(this instanceof Featherlight) {  /* called with new */
			this.id = Featherlight.id++;
			this.setup($content, config);
			this.chainCallbacks(Featherlight._callbackChain);
		} else {
			var fl = new Featherlight($content, config);
			fl.open();
			return fl;
		}
	}

	var opened = [],
		pruneOpened = function(remove) {
			opened = $.grep(opened, function(fl) {
				return fl !== remove && fl.$instance.closest('body').length > 0;
			} );
			return opened;
		};

	// structure({iframeMinHeight: 44, foo: 0}, 'iframe')
	//   #=> {min-height: 44}
	var structure = function(obj, prefix) {
		var result = {},
			regex = new RegExp('^' + prefix + '([A-Z])(.*)');
		for (var key in obj) {
			var match = key.match(regex);
			if (match) {
				var dasherized = (match[1] + match[2].replace(/([A-Z])/g, '-$1')).toLowerCase();
				result[dasherized] = obj[key];
			}
		}
		return result;
	};

	/* document wide key handler */
	var eventMap = { keyup: 'onKeyUp', resize: 'onResize' };

	var globalEventHandler = function(event) {
		$.each(Featherlight.opened().reverse(), function() {
			if (!event.isDefaultPrevented()) {
				if (false === this[eventMap[event.type]](event)) {
					event.preventDefault(); event.stopPropagation(); return false;
			  }
			}
		});
	};

	var toggleGlobalEvents = function(set) {
			if(set !== Featherlight._globalHandlerInstalled) {
				Featherlight._globalHandlerInstalled = set;
				var events = $.map(eventMap, function(_, name) { return name+'.'+Featherlight.prototype.namespace; } ).join(' ');
				$(window)[set ? 'on' : 'off'](events, globalEventHandler);
			}
		};

	Featherlight.prototype = {
		constructor: Featherlight,
		/*** defaults ***/
		/* extend featherlight with defaults and methods */
		namespace:      'featherlight',        /* Name of the events and css class prefix */
		targetAttr:     'data-featherlight',   /* Attribute of the triggered element that contains the selector to the lightbox content */
		variant:        null,                  /* Class that will be added to change look of the lightbox */
		resetCss:       false,                 /* Reset all css */
		background:     null,                  /* Custom DOM for the background, wrapper and the closebutton */
		openTrigger:    'click',               /* Event that triggers the lightbox */
		closeTrigger:   'click',               /* Event that triggers the closing of the lightbox */
		filter:         null,                  /* Selector to filter events. Think $(...).on('click', filter, eventHandler) */
		root:           'body',                /* Where to append featherlights */
		openSpeed:      250,                   /* Duration of opening animation */
		closeSpeed:     250,                   /* Duration of closing animation */
		closeOnClick:   'background',          /* Close lightbox on click ('background', 'anywhere' or false) */
		closeOnEsc:     true,                  /* Close lightbox when pressing esc */
		closeIcon:      '&#10005;',            /* Close icon */
		loading:        '',                    /* Content to show while initial content is loading */
		persist:        false,                 /* If set, the content will persist and will be shown again when opened again. 'shared' is a special value when binding multiple elements for them to share the same content */
		otherClose:     null,                  /* Selector for alternate close buttons (e.g. "a.close") */
		beforeOpen:     $.noop,                /* Called before open. can return false to prevent opening of lightbox. Gets event as parameter, this contains all data */
		beforeContent:  $.noop,                /* Called when content is loaded. Gets event as parameter, this contains all data */
		beforeClose:    $.noop,                /* Called before close. can return false to prevent opening of lightbox. Gets event as parameter, this contains all data */
		afterOpen:      $.noop,                /* Called after open. Gets event as parameter, this contains all data */
		afterContent:   $.noop,                /* Called after content is ready and has been set. Gets event as parameter, this contains all data */
		afterClose:     $.noop,                /* Called after close. Gets event as parameter, this contains all data */
		onKeyUp:        $.noop,                /* Called on key up for the frontmost featherlight */
		onResize:       $.noop,                /* Called after new content and when a window is resized */
		type:           null,                  /* Specify type of lightbox. If unset, it will check for the targetAttrs value. */
		contentFilters: ['jquery', 'image', 'html', 'ajax', 'iframe', 'text'], /* List of content filters to use to determine the content */

		/*** methods ***/
		/* setup iterates over a single instance of featherlight and prepares the background and binds the events */
		setup: function(target, config){
			/* all arguments are optional */
			if (typeof target === 'object' && target instanceof $ === false && !config) {
				config = target;
				target = undefined;
			}

			var self = $.extend(this, config, {target: target}),
				css = !self.resetCss ? self.namespace : self.namespace+'-reset', /* by adding -reset to the classname, we reset all the default css */
				$background = $(self.background || [
					'<div class="'+css+'-loading '+css+'">',
						'<div class="'+css+'-content">',
							'<span class="'+css+'-close-icon '+ self.namespace + '-close">',
								self.closeIcon,
							'</span>',
							'<div class="'+self.namespace+'-inner">' + self.loading + '</div>',
						'</div>',
					'</div>'].join('')),
				closeButtonSelector = '.'+self.namespace+'-close' + (self.otherClose ? ',' + self.otherClose : '');

			self.$instance = $background.clone().addClass(self.variant); /* clone DOM for the background, wrapper and the close button */

			/* close when click on background/anywhere/null or closebox */
			self.$instance.on(self.closeTrigger+'.'+self.namespace, function(event) {
				var $target = $(event.target);
				if( ('background' === self.closeOnClick  && $target.is('.'+self.namespace))
					|| 'anywhere' === self.closeOnClick
					|| $target.closest(closeButtonSelector).length ){
					self.close(event);
					event.preventDefault();
				}
			});

			return this;
		},

		/* this method prepares the content and converts it into a jQuery object or a promise */
		getContent: function(){
			if(this.persist !== false && this.$content) {
				return this.$content;
			}
			var self = this,
				filters = this.constructor.contentFilters,
				readTargetAttr = function(name){ return self.$currentTarget && self.$currentTarget.attr(name); },
				targetValue = readTargetAttr(self.targetAttr),
				data = self.target || targetValue || '';

			/* Find which filter applies */
			var filter = filters[self.type]; /* check explicit type like {type: 'image'} */

			/* check explicit type like data-featherlight="image" */
			if(!filter && data in filters) {
				filter = filters[data];
				data = self.target && targetValue;
			}
			data = data || readTargetAttr('href') || '';

			/* check explicity type & content like {image: 'photo.jpg'} */
			if(!filter) {
				for(var filterName in filters) {
					if(self[filterName]) {
						filter = filters[filterName];
						data = self[filterName];
					}
				}
			}

			/* otherwise it's implicit, run checks */
			if(!filter) {
				var target = data;
				data = null;
				$.each(self.contentFilters, function() {
					filter = filters[this];
					if(filter.test)  {
						data = filter.test(target);
					}
					if(!data && filter.regex && target.match && target.match(filter.regex)) {
						data = target;
					}
					return !data;
				});
				if(!data) {
					if('console' in window){ window.console.error('Featherlight: no content filter found ' + (target ? ' for "' + target + '"' : ' (no target specified)')); }
					return false;
				}
			}
			/* Process it */
			return filter.process.call(self, data);
		},

		/* sets the content of $instance to $content */
		setContent: function($content){
			var self = this;
			/* we need a special class for the iframe */
			if($content.is('iframe') || $('iframe', $content).length > 0){
				self.$instance.addClass(self.namespace+'-iframe');
			}

			self.$instance.removeClass(self.namespace+'-loading');

			/* replace content by appending to existing one before it is removed
			   this insures that featherlight-inner remain at the same relative
				 position to any other items added to featherlight-content */
			self.$instance.find('.'+self.namespace+'-inner')
				.not($content)                /* excluded new content, important if persisted */
				.slice(1).remove().end()      /* In the unexpected event where there are many inner elements, remove all but the first one */
				.replaceWith($.contains(self.$instance[0], $content[0]) ? '' : $content);

			self.$content = $content.addClass(self.namespace+'-inner');

			return self;
		},

		/* opens the lightbox. "this" contains $instance with the lightbox, and with the config.
			Returns a promise that is resolved after is successfully opened. */
		open: function(event){
			var self = this;
			self.$instance.hide().appendTo(self.root);
			if((!event || !event.isDefaultPrevented())
				&& self.beforeOpen(event) !== false) {

				if(event){
					event.preventDefault();
				}
				var $content = self.getContent();

				if($content) {
					opened.push(self);

					toggleGlobalEvents(true);

					self.$instance.fadeIn(self.openSpeed);
					self.beforeContent(event);

					/* Set content and show */
					return $.when($content)
						.always(function($content){
							self.setContent($content);
							self.afterContent(event);
						})
						.then(self.$instance.promise())
						/* Call afterOpen after fadeIn is done */
						.done(function(){ self.afterOpen(event); });
				}
			}
			self.$instance.detach();
			return $.Deferred().reject().promise();
		},

		/* closes the lightbox. "this" contains $instance with the lightbox, and with the config
			returns a promise, resolved after the lightbox is successfully closed. */
		close: function(event){
			var self = this,
				deferred = $.Deferred();

			if(self.beforeClose(event) === false) {
				deferred.reject();
			} else {

				if (0 === pruneOpened(self).length) {
					toggleGlobalEvents(false);
				}

				self.$instance.fadeOut(self.closeSpeed,function(){
					self.$instance.detach();
					self.afterClose(event);
					deferred.resolve();
				});
			}
			return deferred.promise();
		},

		/* Utility function to chain callbacks
		   [Warning: guru-level]
		   Used be extensions that want to let users specify callbacks but
		   also need themselves to use the callbacks.
		   The argument 'chain' has callback names as keys and function(super, event)
		   as values. That function is meant to call `super` at some point.
		*/
		chainCallbacks: function(chain) {
			for (var name in chain) {
				this[name] = $.proxy(chain[name], this, $.proxy(this[name], this));
			}
		}
	};

	$.extend(Featherlight, {
		id: 0,                                    /* Used to id single featherlight instances */
		autoBind:       '[data-featherlight]',    /* Will automatically bind elements matching this selector. Clear or set before onReady */
		defaults:       Featherlight.prototype,   /* You can access and override all defaults using $.featherlight.defaults, which is just a synonym for $.featherlight.prototype */
		/* Contains the logic to determine content */
		contentFilters: {
			jquery: {
				regex: /^[#.]\w/,         /* Anything that starts with a class name or identifiers */
				test: function(elem)    { return elem instanceof $ && elem; },
				process: function(elem) { return this.persist !== false ? $(elem) : $(elem).clone(true); }
			},
			image: {
				regex: /\.(png|jpg|jpeg|gif|tiff|bmp|svg)(\?\S*)?$/i,
				process: function(url)  {
					var self = this,
						deferred = $.Deferred(),
						img = new Image(),
						$img = $('<img src="'+url+'" alt="" class="'+self.namespace+'-image" />');
					img.onload  = function() {
						/* Store naturalWidth & height for IE8 */
						$img.naturalWidth = img.width; $img.naturalHeight = img.height;
						deferred.resolve( $img );
					};
					img.onerror = function() { deferred.reject($img); };
					img.src = url;
					return deferred.promise();
				}
			},
			html: {
				regex: /^\s*<[\w!][^<]*>/, /* Anything that starts with some kind of valid tag */
				process: function(html) { return $(html); }
			},
			ajax: {
				regex: /./,            /* At this point, any content is assumed to be an URL */
				process: function(url)  {
					var self = this,
						deferred = $.Deferred();
					/* we are using load so one can specify a target with: url.html #targetelement */
					var $container = $('<div></div>').load(url, function(response, status){
						if ( status !== "error" ) {
							deferred.resolve($container.contents());
						}
						deferred.fail();
					});
					return deferred.promise();
				}
			},
			iframe: {
				process: function(url) {
					var deferred = new $.Deferred();
					var $content = $('<iframe/>')
						.hide()
						.attr('src', url)
						.css(structure(this, 'iframe'))
						.on('load', function() { deferred.resolve($content.show()); })
						// We can't move an <iframe> and avoid reloading it,
						// so let's put it in place ourselves right now:
						.appendTo(this.$instance.find('.' + this.namespace + '-content'));
					return deferred.promise();
				}
			},
			text: {
				process: function(text) { return $('<div>', {text: text}); }
			}
		},

		functionAttributes: ['beforeOpen', 'afterOpen', 'beforeContent', 'afterContent', 'beforeClose', 'afterClose'],

		/*** class methods ***/
		/* read element's attributes starting with data-featherlight- */
		readElementConfig: function(element, namespace) {
			var Klass = this,
				regexp = new RegExp('^data-' + namespace + '-(.*)'),
				config = {};
			if (element && element.attributes) {
				$.each(element.attributes, function(){
					var match = this.name.match(regexp);
					if (match) {
						var val = this.value,
							name = $.camelCase(match[1]);
						if ($.inArray(name, Klass.functionAttributes) >= 0) {  /* jshint -W054 */
							val = new Function(val);                           /* jshint +W054 */
						} else {
							try { val = $.parseJSON(val); }
							catch(e) {}
						}
						config[name] = val;
					}
				});
			}
			return config;
		},

		/* Used to create a Featherlight extension
		   [Warning: guru-level]
		   Creates the extension's prototype that in turn
		   inherits Featherlight's prototype.
		   Could be used to extend an extension too...
		   This is pretty high level wizardy, it comes pretty much straight
		   from CoffeeScript and won't teach you anything about Featherlight
		   as it's not really specific to this library.
		   My suggestion: move along and keep your sanity.
		*/
		extend: function(child, defaults) {
			/* Setup class hierarchy, adapted from CoffeeScript */
			var Ctor = function(){ this.constructor = child; };
			Ctor.prototype = this.prototype;
			child.prototype = new Ctor();
			child.__super__ = this.prototype;
			/* Copy class methods & attributes */
			$.extend(child, this, defaults);
			child.defaults = child.prototype;
			return child;
		},

		attach: function($source, $content, config) {
			var Klass = this;
			if (typeof $content === 'object' && $content instanceof $ === false && !config) {
				config = $content;
				$content = undefined;
			}
			/* make a copy */
			config = $.extend({}, config);

			/* Only for openTrigger and namespace... */
			var namespace = config.namespace || Klass.defaults.namespace,
				tempConfig = $.extend({}, Klass.defaults, Klass.readElementConfig($source[0], namespace), config),
				sharedPersist;

			$source.on(tempConfig.openTrigger+'.'+tempConfig.namespace, tempConfig.filter, function(event) {
				/* ... since we might as well compute the config on the actual target */
				var elemConfig = $.extend(
					{$source: $source, $currentTarget: $(this)},
					Klass.readElementConfig($source[0], tempConfig.namespace),
					Klass.readElementConfig(this, tempConfig.namespace),
					config);
				var fl = sharedPersist || $(this).data('featherlight-persisted') || new Klass($content, elemConfig);
				if(fl.persist === 'shared') {
					sharedPersist = fl;
				} else if(fl.persist !== false) {
					$(this).data('featherlight-persisted', fl);
				}
				elemConfig.$currentTarget.blur(); // Otherwise 'enter' key might trigger the dialog again
				fl.open(event);
			});
			return $source;
		},

		current: function() {
			var all = this.opened();
			return all[all.length - 1] || null;
		},

		opened: function() {
			var klass = this;
			pruneOpened();
			return $.grep(opened, function(fl) { return fl instanceof klass; } );
		},

		close: function(event) {
			var cur = this.current();
			if(cur) { return cur.close(event); }
		},

		/* Does the auto binding on startup.
		   Meant only to be used by Featherlight and its extensions
		*/
		_onReady: function() {
			var Klass = this;
			if(Klass.autoBind){
				/* Bind existing elements */
				$(Klass.autoBind).each(function(){
					Klass.attach($(this));
				});
				/* If a click propagates to the document level, then we have an item that was added later on */
				$(document).on('click', Klass.autoBind, function(evt) {
					if (evt.isDefaultPrevented() || evt.namespace === 'featherlight') {
						return;
					}
					evt.preventDefault();
					/* Bind featherlight */
					Klass.attach($(evt.currentTarget));
					/* Click again; this time our binding will catch it */
					$(evt.target).trigger('click.featherlight');
				});
			}
		},

		/* Featherlight uses the onKeyUp callback to intercept the escape key.
		   Private to Featherlight.
		*/
		_callbackChain: {
			onKeyUp: function(_super, event){
				if(27 === event.keyCode) {
					if (this.closeOnEsc) {
						$.featherlight.close(event);
					}
					return false;
				} else {
					return _super(event);
				}
			},

			onResize: function(_super, event){
				if (this.$content.naturalWidth) {
					var w = this.$content.naturalWidth, h = this.$content.naturalHeight;
					/* Reset apparent image size first so container grows */
					this.$content.css('width', '').css('height', '');
					/* Calculate the worst ratio so that dimensions fit */
					var ratio = Math.max(
						w  / parseInt(this.$content.parent().css('width'),10),
						h / parseInt(this.$content.parent().css('height'),10));
					/* Resize content */
					if (ratio > 1) {
						this.$content.css('width', '' + w / ratio + 'px').css('height', '' + h / ratio + 'px');
					}
				}
				return _super(event);
			},

			afterContent: function(_super, event){
				var r = _super(event);
				this.onResize(event);
				return r;
			}
		}
	});

	$.featherlight = Featherlight;

	/* bind jQuery elements to trigger featherlight */
	$.fn.featherlight = function($content, config) {
		return Featherlight.attach(this, $content, config);
	};

	/* bind featherlight on ready if config autoBind is set */
	$(document).ready(function(){ Featherlight._onReady(); });
}(jQuery));

/**
 * Featherlight Gallery – an extension for the ultra slim jQuery lightbox
 * Version 1.3.5 - http://noelboss.github.io/featherlight/
 *
 * Copyright 2015, Noël Raoul Bossart (http://www.noelboss.com)
 * MIT Licensed.
**/
(function($) {
	"use strict";

	var warn = function(m) {
		if(window.console && window.console.warn) {
			window.console.warn('FeatherlightGallery: ' + m);
		}
	};

	if('undefined' === typeof $) {
		return warn('Too much lightness, Featherlight needs jQuery.');
	} else if(!$.featherlight) {
		return warn('Load the featherlight plugin before the gallery plugin');
	}

	var isTouchAware = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
		jQueryConstructor = $.event && $.event.special.swipeleft && $,
		hammerConstructor = window.Hammer && function($el){
			var mc = new window.Hammer.Manager($el[0]);
			mc.add(new window.Hammer.Swipe());
			return mc;
		},
		swipeAwareConstructor = isTouchAware && (jQueryConstructor || hammerConstructor);
	if(isTouchAware && !swipeAwareConstructor) {
		warn('No compatible swipe library detected; one must be included before featherlightGallery for swipe motions to navigate the galleries.');
	}

	var callbackChain = {
			afterClose: function(_super, event) {
					var self = this;
					self.$instance.off('next.'+self.namespace+' previous.'+self.namespace);
					if (self._swiper) {
						self._swiper
							.off('swipeleft', self._swipeleft) /* See http://stackoverflow.com/questions/17367198/hammer-js-cant-remove-event-listener */
							.off('swiperight', self._swiperight);
						self._swiper = null;
					}
					return _super(event);
			},
			beforeOpen: function(_super, event){
					var self = this;

					self.$instance.on('next.'+self.namespace+' previous.'+self.namespace, function(event){
						var offset = event.type === 'next' ? +1 : -1;
						self.navigateTo(self.currentNavigation() + offset);
					});

					if (swipeAwareConstructor) {
						self._swiper = swipeAwareConstructor(self.$instance)
							.on('swipeleft', self._swipeleft = function()  { self.$instance.trigger('next'); })
							.on('swiperight', self._swiperight = function() { self.$instance.trigger('previous'); });
					} else {
						self.$instance.find('.'+self.namespace+'-content')
							.append(self.createNavigation('previous'))
							.append(self.createNavigation('next'));
					}
					return _super(event);
			},
			onKeyUp: function(_super, event){
				var dir = {
					37: 'previous', /* Left arrow */
					39: 'next'			/* Rigth arrow */
				}[event.keyCode];
				if(dir) {
					this.$instance.trigger(dir);
					return false;
				} else {
					return _super(event);
				}
			}
		};

	function FeatherlightGallery($source, config) {
		if(this instanceof FeatherlightGallery) {  /* called with new */
			$.featherlight.apply(this, arguments);
			this.chainCallbacks(callbackChain);
		} else {
			var flg = new FeatherlightGallery($.extend({$source: $source, $currentTarget: $source.first()}, config));
			flg.open();
			return flg;
		}
	}

	$.featherlight.extend(FeatherlightGallery, {
		autoBind: '[data-featherlight-gallery]'
	});

	$.extend(FeatherlightGallery.prototype, {
		/** Additional settings for Gallery **/
		previousIcon: '&#9664;',     /* Code that is used as previous icon */
		nextIcon: '&#9654;',         /* Code that is used as next icon */
		galleryFadeIn: 100,          /* fadeIn speed when image is loaded */
		galleryFadeOut: 300,         /* fadeOut speed before image is loaded */

		slides: function() {
			if (this.filter) {
				return this.$source.find(this.filter);
			}
			return this.$source;
		},

		images: function() {
			warn('images is deprecated, please use slides instead');
			return this.slides();
		},

		currentNavigation: function() {
			return this.slides().index(this.$currentTarget);
		},

		navigateTo: function(index) {
			var self = this,
				source = self.slides(),
				len = source.length,
				$inner = self.$instance.find('.' + self.namespace + '-inner');
			index = ((index % len) + len) % len; /* pin index to [0, len[ */

			self.$currentTarget = source.eq(index);
			self.beforeContent();
			return $.when(
				self.getContent(),
				$inner.fadeTo(self.galleryFadeOut,0.2)
			).always(function($newContent) {
					self.setContent($newContent);
					self.afterContent();
					$newContent.fadeTo(self.galleryFadeIn,1);
			});
		},

		createNavigation: function(target) {
			var self = this;
			return $('<span title="'+target+'" class="'+this.namespace+'-'+target+'"><span>'+this[target+'Icon']+'</span></span>').click(function(){
				$(this).trigger(target+'.'+self.namespace);
			});
		}
	});

	$.featherlightGallery = FeatherlightGallery;

	/* extend jQuery with selector featherlight method $(elm).featherlight(config, elm); */
	$.fn.featherlightGallery = function(config) {
		return FeatherlightGallery.attach(this, config);
	};

	/* bind featherlight on ready if config autoBind is set */
	$(document).ready(function(){ FeatherlightGallery._onReady(); });

}(jQuery));

// jQuery Mask Plugin v1.13.9
// github.com/igorescobar/jQuery-Mask-Plugin
(function(a){"function"===typeof define&&define.amd?define(["jquery"],a):"object"===typeof exports?module.exports=a(require("jquery")):a(jQuery||Zepto)})(function(a){var x=function(c,e,d){var b={invalid:[],getCaret:function(){try{var r,a=0,e=c.get(0),f=document.selection,d=e.selectionStart;if(f&&-1===navigator.appVersion.indexOf("MSIE 10"))r=f.createRange(),r.moveStart("character",-b.val().length),a=r.text.length;else if(d||"0"===d)a=d;return a}catch(h){}},setCaret:function(r){try{if(c.is(":focus")){var b;
b=c.get(0).createTextRange();b.collapse(!0);b.moveEnd("character",r);b.moveStart("character",r);b.select()}}catch(a){}},events:function(){c.on("keydown.mask",function(b){c.data("mask-keycode",b.keyCode||b.which)}).on(a.jMaskGlobals.useInput?"input.mask":"keyup.mask",b.behaviour).on("paste.mask drop.mask",function(){setTimeout(function(){c.keydown().keyup()},100)}).on("change.mask",function(){c.data("changed",!0)}).on("blur.mask",function(){n===b.val()||c.data("changed")||c.trigger("change");c.data("changed",
!1)}).on("blur.mask",function(){n=b.val()}).on("focus.mask",function(b){!0===d.selectOnFocus&&a(b.target).select()}).on("focusout.mask",function(){d.clearIfNotMatch&&!k.test(b.val())&&b.val("")})},getRegexMask:function(){for(var b=[],c,a,f,d,h=0;h<e.length;h++)(c=g.translation[e.charAt(h)])?(a=c.pattern.toString().replace(/.{1}$|^.{1}/g,""),f=c.optional,(c=c.recursive)?(b.push(e.charAt(h)),d={digit:e.charAt(h),pattern:a}):b.push(f||c?a+"?":a)):b.push(e.charAt(h).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&"));
b=b.join("");d&&(b=b.replace(new RegExp("("+d.digit+"(.*"+d.digit+")?)"),"($1)?").replace(new RegExp(d.digit,"g"),d.pattern));return new RegExp(b)},destroyEvents:function(){c.off("input keydown keyup paste drop blur focusout ".split(" ").join(".mask "))},val:function(b){var a=c.is("input")?"val":"text";if(0<arguments.length){if(c[a]()!==b)c[a](b);a=c}else a=c[a]();return a},getMCharsBeforeCount:function(b,c){for(var a=0,d=0,l=e.length;d<l&&d<b;d++)g.translation[e.charAt(d)]||(b=c?b+1:b,a++);return a},
caretPos:function(c,a,d,f){return g.translation[e.charAt(Math.min(c-1,e.length-1))]?Math.min(c+d-a-f,d):b.caretPos(c+1,a,d,f)},behaviour:function(d){d=d||window.event;b.invalid=[];var e=c.data("mask-keycode");if(-1===a.inArray(e,g.byPassKeys)){var p=b.getCaret(),f=b.val().length,l=b.getMasked(),h=l.length,n=b.getMCharsBeforeCount(h-1)-b.getMCharsBeforeCount(f-1),m=p<f;b.val(l);m&&(8!==e&&46!==e&&(p=b.caretPos(p,f,h,n)),b.setCaret(p));return b.callbacks(d)}},getMasked:function(c){var a=[],p=b.val(),
f=0,l=e.length,h=0,n=p.length,m=1,k="push",t=-1,s,v;d.reverse?(k="unshift",m=-1,s=0,f=l-1,h=n-1,v=function(){return-1<f&&-1<h}):(s=l-1,v=function(){return f<l&&h<n});for(;v();){var w=e.charAt(f),u=p.charAt(h),q=g.translation[w];if(q)u.match(q.pattern)?(a[k](u),q.recursive&&(-1===t?t=f:f===s&&(f=t-m),s===t&&(f-=m)),f+=m):q.optional?(f+=m,h-=m):q.fallback?(a[k](q.fallback),f+=m,h-=m):b.invalid.push({p:h,v:u,e:q.pattern}),h+=m;else{if(!c)a[k](w);u===w&&(h+=m);f+=m}}c=e.charAt(s);l!==n+1||g.translation[c]||
a.push(c);return a.join("")},callbacks:function(a){var g=b.val(),k=g!==n,f=[g,a,c,d],l=function(b,c,a){"function"===typeof d[b]&&c&&d[b].apply(this,a)};l("onChange",!0===k,f);l("onKeyPress",!0===k,f);l("onComplete",g.length===e.length,f);l("onInvalid",0<b.invalid.length,[g,a,c,b.invalid,d])}};c=a(c);var g=this,n=b.val(),k;e="function"===typeof e?e(b.val(),void 0,c,d):e;g.mask=e;g.options=d;g.remove=function(){var a=b.getCaret();b.destroyEvents();b.val(g.getCleanVal());b.setCaret(a-b.getMCharsBeforeCount(a));
return c};g.getCleanVal=function(){return b.getMasked(!0)};g.init=function(e){e=e||!1;d=d||{};g.clearIfNotMatch=a.jMaskGlobals.clearIfNotMatch;g.byPassKeys=a.jMaskGlobals.byPassKeys;g.translation=a.extend({},a.jMaskGlobals.translation,d.translation);g=a.extend(!0,{},g,d);k=b.getRegexMask();!1===e?(d.placeholder&&c.attr("placeholder",d.placeholder),c.data("mask")&&c.attr("autocomplete","off"),b.destroyEvents(),b.events(),e=b.getCaret(),b.val(b.getMasked()),b.setCaret(e+b.getMCharsBeforeCount(e,!0))):
(b.events(),b.val(b.getMasked()))};g.init(!c.is("input"))};a.maskWatchers={};var z=function(){var c=a(this),e={},d=c.attr("data-mask");c.attr("data-mask-reverse")&&(e.reverse=!0);c.attr("data-mask-clearifnotmatch")&&(e.clearIfNotMatch=!0);"true"===c.attr("data-mask-selectonfocus")&&(e.selectOnFocus=!0);if(y(c,d,e))return c.data("mask",new x(this,d,e))},y=function(c,e,d){d=d||{};var b=a(c).data("mask"),g=JSON.stringify;c=a(c).val()||a(c).text();try{return"function"===typeof e&&(e=e(c)),"object"!==
typeof b||g(b.options)!==g(d)||b.mask!==e}catch(k){}};a.fn.mask=function(c,e){e=e||{};var d=this.selector,b=a.jMaskGlobals,g=b.watchInterval,b=e.watchInputs||b.watchInputs,k=function(){if(y(this,c,e))return a(this).data("mask",new x(this,c,e))};a(this).each(k);d&&""!==d&&b&&(clearInterval(a.maskWatchers[d]),a.maskWatchers[d]=setInterval(function(){a(document).find(d).each(k)},g));return this};a.fn.unmask=function(){clearInterval(a.maskWatchers[this.selector]);delete a.maskWatchers[this.selector];
return this.each(function(){var c=a(this).data("mask");c&&c.remove().removeData("mask")})};a.fn.cleanVal=function(){return this.data("mask").getCleanVal()};a.applyDataMask=function(c){c=c||a.jMaskGlobals.maskElements;(c instanceof a?c:a(c)).filter(a.jMaskGlobals.dataMaskAttr).each(z)};var k={maskElements:"input,td,span,div",dataMaskAttr:"*[data-mask]",dataMask:!0,watchInterval:300,watchInputs:!0,useInput:function(a){var e=document.createElement("div"),d;a="on"+a;d=a in e;d||(e.setAttribute(a,"return;"),
d="function"===typeof e[a]);return d}("input"),watchDataMask:!1,byPassKeys:[9,16,17,18,36,37,38,39,40,91],translation:{0:{pattern:/\d/},9:{pattern:/\d/,optional:!0},"#":{pattern:/\d/,recursive:!0},A:{pattern:/[a-zA-Z0-9]/},S:{pattern:/[a-zA-Z]/}}};a.jMaskGlobals=a.jMaskGlobals||{};k=a.jMaskGlobals=a.extend(!0,{},k,a.jMaskGlobals);k.dataMask&&a.applyDataMask();setInterval(function(){a.jMaskGlobals.watchDataMask&&a.applyDataMask()},k.watchInterval)});

var Portaria = {
	init : function(){

		var modulo = location.pathname.split('/')[2];

		if(modulo == 'auth')
			this.Auth.init();

		if(modulo == 'correspondencias')
			this.Correspondencias.init();

		if(modulo == 'encomendas')
			this.Encomendas.init();

		if(modulo == 'unidade')
			this.Unidades.init();

		if(modulo == 'comunicacao')
			this.Comunicacao.init();

		if(modulo == 'lavanderia')
			this.Lavanderia.init();

		if(modulo == 'veiculos')
			this.Veiculos.init();

		this.Dashboard.init();
		this.Busca.init();

		if($('.retorno_formulario').length){
			setTimeout( function(){
				$('.retorno_formulario').addClass('removendo').fadeOut('normal');
			}, 3000);
		}

		$.ajaxSetup({
	    headers: {
	      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
		})
	}
};

Portaria.Auth = {

	login_form_submit : function() {
		$('.login-form form').submit( function(e){

			var campo_email = $('#login-form-login');
			var campo_passw = $('#login-form-senha');

			if(!campo_email.val()){

				campo_email.parent()
						   .addClass('erro')
						   .addClass('obrigatorio')
						   .find('input')
						   .focus();

				e.preventDefault();
				return false;
			}

			if(!campo_passw.val()){

				campo_passw.parent()
						   .addClass('erro')
						   .addClass('obrigatorio')
						   .find('input')
						   .focus();

				e.preventDefault();
				return false;
			}
		});
	},

	login_form_keyup : function() {
		$('#login-form-login, #login-form-senha').on('keyup', function(){
			$(this).parent().removeClass('erro')
							.removeClass('obrigatorio');
		});
	},

	init : function() {
		this.login_form_submit();
		this.login_form_keyup();
	}
};
Portaria.Correspondencias = {

	listaUnidades : [],

	iniciarUnidades: function()
	{
		this.listaUnidades = JSON.parse($('#listaUnidadesJson').val());
	},

	verificarUnidade: function()
	{
		$('#input-busca-unidade').on('keyup', function(e){

			var resumo = $(this).val().toUpperCase();

			$(this).parent().removeClass('bloqueado');

			if(e.which != 13){

				if(Portaria.Correspondencias.buscarUnidade(resumo).length > 0){
					Portaria.Correspondencias.liberarForm();
				}else{
					Portaria.Correspondencias.bloquearForm();
				}

			}

		});

	},

	removerClassesNoBlur: function()
	{
		$('#input-busca-unidade').on('blur', function(){
			$('#input-busca-unidade').parent().removeClass('bloqueado').removeClass('liberado');
		});
	},

	liberarForm: function()
	{
		$('#input-busca-unidade').parent().removeClass('bloqueado').addClass('liberado');
	},

	bloquearForm: function()
	{
		$('#input-busca-unidade').parent().removeClass('liberado').addClass('bloqueado');
	},

	enviarForm: function()
	{
		$('#form-inserir-correspondencia').submit('keyup', function(e){

			e.preventDefault();

			if($('#input-busca-unidade').parent().hasClass('liberado')){

				$.post('portaria/correspondencias', {
					'termo_busca_unidade' : $('#input-busca-unidade').val()
				}, function(resposta){

					$('#input-busca-unidade').val('');
					$('#input-busca-unidade').parent().removeClass('liberado')

					var lista = $('#lista-avisos ul');
					var item_str = "";

					item_str += "<li style='display:none;'>";
						item_str += "<div class='unidade'>" + resposta.resumo + "</div>";
						item_str += "<div class='timestamp'>" + resposta.data + "</div>";
						item_str += "<div class='marcacao'>";
							item_str += "<a href='#' title='Marcar como entregue' data-id-correspondencia='" + resposta.id + "'>entregue</a>";
						item_str += "</div>";
						item_str += "<div class='remover'>";
							item_str += "<a href='#' title='Remover aviso de Correspondência' class='btn btn-danger btn-circular' data-id-correspondencia='" + resposta.id + "'>X</a>";
						item_str += "</div>";
					item_str += "</li>";

					if(lista.find('li').length > 0)
					{
						lista.prepend(item_str);
					}
					else
					{
						Portaria.Encomendas.mostrarBusca();
						lista.html(item_str);
					}

					$('#lista-avisos ul li:first').fadeIn('slow');
				});
			}

		});
	},

	buscarUnidade: function(resumo)
	{
		return this.listaUnidades.filter(
	    	function(data){return data == resumo}
	  	);
	},

	marcarEntregue: function()
	{
		$(document).on('click', '.marcacao a', function(e){
			e.preventDefault();
			if(!$(this).hasClass('marcado')){

				var botao = $(this);
				var id = botao.attr('data-id-correspondencia');
				var linha_inteira = botao.parent().parent();
				var texto_sem_check = botao.html();

				botao.addClass('marcado').html(texto_sem_check + ' ✓');

				setTimeout( function(){
					linha_inteira.fadeOut('normal', function(){

						linha_inteira.remove();

						Portaria.Encomendas.esconderBuscaSeVazio();

						// MARCAR aviso de correspondencia COMO ENTREGUE
						// Remover notificações para todos os usuários
						$.ajax('portaria/correspondencias/marcarEntregue/'+id);

					});
				}, 1000);
			}
		});
	},

	removerCorrespondencia: function(){
		$(document).on('click', '.remover a', function(e){
			e.preventDefault();

			var botao = $(this);

			var id = botao.attr('data-id-correspondencia');
			var linha_inteira = botao.parent().parent();

			linha_inteira.fadeOut('normal', function(){
				linha_inteira.remove();

				// REMOVER aviso de correspondencia
				// Remover notificações para todos os usuários
				$.ajax('portaria/correspondencias/remover/'+id,{
					success : function(){
						Portaria.Encomendas.esconderBuscaSeVazio();
					}
				});

			});
		});
	},

	filtraTabela: function(){
		$('#input_filtra_tabela').on('keyup', function(){

	        var termo = $(this).val();

	        $("#lista-avisos ul").find("li").each( function(index) {
        		var campo_unidade = $(this).find(".unidade").first().text().toLowerCase();
	        	$(this).toggle(campo_unidade.indexOf( termo.toLowerCase() ) !== -1);
	    	});
    	});
	},

	mostrarBusca : function(){
		$('#input_filtra_tabela').is(':hidden')
			$('#input_filtra_tabela').fadeIn('normal');
	},

	esconderBuscaSeVazio : function(){
		if($('#lista-avisos ul li').length == 0)
			$('#input_filtra_tabela').fadeOut('normal');
	},

	init: function(){
		this.iniciarUnidades();
		this.verificarUnidade();
		this.removerClassesNoBlur();
		this.enviarForm();
		this.marcarEntregue();
		this.removerCorrespondencia();
		this.filtraTabela();
	}
};
Portaria.Encomendas = {

	listaUnidades : [],

	iniciarUnidades: function()
	{
		this.listaUnidades = JSON.parse($('#listaUnidadesJson').val());
	},

	verificarUnidade: function()
	{
		$('#input-busca-unidade').on('keyup', function(e){

			var resumo = $(this).val().toUpperCase();

			$(this).parent().removeClass('bloqueado');

			if(e.which != 13){

				if(Portaria.Encomendas.buscarUnidade(resumo).length > 0){
					Portaria.Encomendas.liberarForm();
				}else{
					Portaria.Encomendas.bloquearForm();
				}

			}

		});

	},

	removerClassesNoBlur: function()
	{
		$('#input-busca-unidade').on('blur', function(){
			$('#input-busca-unidade').parent().removeClass('bloqueado').removeClass('liberado');
		});
	},

	liberarForm: function()
	{
		$('#input-busca-unidade').parent().removeClass('bloqueado').addClass('liberado');
	},

	bloquearForm: function()
	{
		$('#input-busca-unidade').parent().removeClass('liberado').addClass('bloqueado');
	},

	enviarForm: function()
	{
		$('#form-inserir-encomenda').submit('keyup', function(e){

			e.preventDefault();

			if($('#input-busca-unidade').parent().hasClass('liberado')){

				$.post('portaria/encomendas', {
					'termo_busca_unidade' : $('#input-busca-unidade').val()
				}, function(resposta){

					$('#input-busca-unidade').val('');
					$('#input-busca-unidade').parent().removeClass('liberado')

					var lista = $('#lista-avisos ul');
					var item_str = "";

					item_str += "<li style='display:none;'>";
						item_str += "<div class='unidade'>" + resposta.resumo + "</div>";
						item_str += "<div class='timestamp'>" + resposta.data + "</div>";
						item_str += "<div class='marcacao'>";
							item_str += "<a href='#' title='Marcar como entregue' data-id-encomenda='" + resposta.id + "'>entregue</a>";
						item_str += "</div>";
						item_str += "<div class='remover'>";
							item_str += "<a href='#' title='Remover aviso de Encomenda' class='btn btn-danger btn-circular' data-id-encomenda='" + resposta.id + "'>X</a>";
						item_str += "</div>";
					item_str += "</li>";

					if(lista.find('li').length > 0)
					{
						lista.prepend(item_str);
					}
					else
					{
						Portaria.Encomendas.mostrarBusca();
						lista.html(item_str);
					}

					$('#lista-avisos ul li:first').fadeIn('slow');
				});
			}

		});
	},

	buscarUnidade: function(resumo)
	{
		return this.listaUnidades.filter(
    	function(data){return data == resumo}
  	);
	},

	marcarEntregue: function()
	{
		$(document).on('click', '.marcacao a', function(e){
			e.preventDefault();
			if(!$(this).hasClass('marcado')){

				var botao = $(this);
				var id = botao.attr('data-id-encomenda');
				var linha_inteira = botao.parent().parent();
				var texto_sem_check = botao.html();

				botao.addClass('marcado').html(texto_sem_check + ' ✓');

				setTimeout( function(){
					linha_inteira.fadeOut('normal', function(){

						linha_inteira.remove();

						Portaria.Encomendas.esconderBuscaSeVazio();

						// MARCAR aviso de encomenda COMO ENTREGUE
						// Remover notificações para todos os usuários
						$.ajax('portaria/encomendas/marcarEntregue/'+id);

					});
				}, 1000);
			}
		});
	},

	removerCorrespondencia: function()
	{
		$(document).on('click', '.remover a', function(e){
			e.preventDefault();

			var botao = $(this);

			var id = botao.attr('data-id-encomenda');
			var linha_inteira = botao.parent().parent();

			linha_inteira.fadeOut('normal', function(){
				linha_inteira.remove();

				// REMOVER aviso de encomenda
				// Remover notificações para todos os usuários
				$.ajax('portaria/encomendas/remover/'+id,{
					success : function(){
						Portaria.Encomendas.esconderBuscaSeVazio();
					}
				});

			});
		});
	},

	filtraTabela: function()
	{
		$('#input_filtra_tabela').on('keyup', function(){

	        var termo = $(this).val();

	        $("#lista-avisos ul").find("li").each( function(index) {
        		var campo_unidade = $(this).find(".unidade").text().toLowerCase();
	        	$(this).toggle( campo_unidade.indexOf( termo.toLowerCase() ) == 0);
	    	});
    	});
	},

	mostrarBusca : function(){
		$('#input_filtra_tabela').is(':hidden')
			$('#input_filtra_tabela').fadeIn('normal');
	},

	esconderBuscaSeVazio : function(){
		if($('#lista-avisos ul li').length == 0)
			$('#input_filtra_tabela').fadeOut('normal');
	},

	init: function(){
		this.iniciarUnidades();
		this.verificarUnidade();
		this.removerClassesNoBlur();
		this.enviarForm();
		this.marcarEntregue();
		this.removerCorrespondencia();
		this.filtraTabela();
	}
};
Portaria.Busca = {

	timer : null,

	busca_on_keyup : function(){
		$('#busca-principal').on('keyup', function(e){

			var termo = $(this).val();

			clearTimeout(Portaria.Busca.timer);

			if(e.which != 13){

				if(termo.length > 0){

					if($('.main-conteudo').is(":visible"))
						Portaria.Busca.esconder_conteudo_anterior();

					Portaria.Busca.timer = setTimeout( function(){
						Portaria.Busca.esconder_aviso();
						Portaria.Busca.busca_ajax(termo);
					}, 400);

				}else{

					Portaria.Busca.mostrar_conteudo_anterior();

				}
			}

		});
	},

	bloquear_envio_form : function(){
		$('#form-busca').submit( function(e){

			e.preventDefault();
			e.stopPropagation();

			var resultados = $('#resultado-busca .resultado');
			var nro_resultados = resultados.length;
			var avisos_div = $('#avisos-busca');

			if(nro_resultados > 1){

				// Selecione um resultado
				avisos_div.html("<p class='retorno_busca erro'>Selecione um resultado abaixo</p>");

			}else if(nro_resultados == 1){

				// Entrar no 1o resultado
				window.location.href = resultados.find(':first').attr('href');

			}else{
				// nenhum resultado
			}

		});
	},

	busca_ajax: function(termo){

		$.post('portaria/busca', {
			termo : termo
		}, function(resposta){
			Portaria.Busca.trata_retorno_ajax(resposta);
		});

	},

	trata_retorno_ajax : function(resposta){

		if($.isEmptyObject(resposta))
			this.nenhumResultado();
		else
			this.mostrarResultados(resposta);

	},

	esconder_conteudo_anterior : function(){
		$('.main-conteudo').fadeOut('fast', function(){
			$('#resultado-busca').fadeIn('fast');
		});
	},

	mostrar_conteudo_anterior : function(){
		$('#resultado-busca').fadeOut('fast', function(){
			$('.main-conteudo').fadeIn('fast');
		});
	},

	mostrarResultados : function(resposta){

		var template   = '';
		var img_padrao = 'moradores/assets/images/avatar-default/70/70/sem foto';
		var nro_resultados = resposta.length;

		for (var unidade in resposta) {

			lista_moradores = resposta[unidade];
			numero_moradores = lista_moradores.length;

			template += "<div class='resultado'>";
				template += "<a href='portaria/unidade/"+unidade+"' title='"+unidade+"'>";

					template += "<div class='abreviacao-unidade'>";
						template += unidade;
					template += "</div>";

					template += "<div class='lista-moradores'>";

					if(numero_moradores > 0){
						for(var c = 0; c < numero_moradores; c++){

							template += "<div class='morador'>";

								template += !lista_moradores[c].foto ? "<img src='"+img_padrao+"'>" : "<img src='"+lista_moradores[c].foto_fullpath+"'>";

								template += "<div class='nome-morador'>";
									template += lista_moradores[c].nome;
									template += !lista_moradores[c].idade ? "<span>idade não informada</span>" : "<span>"+lista_moradores[c].idade+" anos</span>";
									template += " <span>e-mail: " + lista_moradores[c].email+"</span>";
								template += "</div>";

								template += "<div class='dados-morador'>";
									template += lista_moradores[c].telefone_fixo == '' ? "Tel. Fixo: --<br>" : "Tel. Fixo: "+lista_moradores[c].telefone_fixo + '<br>';
									template += lista_moradores[c].telefone_celular == '' ? "Tel. Celular: --<br>" : "Tel. Celular: "+lista_moradores[c].telefone_celular + '<br>';
									template += lista_moradores[c].telefone_comercial == '' ? "Tel. Comercial: --" : "Tel. Comercial: "+lista_moradores[c].telefone_comercial;
								template += "</div>";

							template += "</div>";

						}
					}else{
						template += "<div class='morador'>";

							template += "<img src='"+img_padrao+"'>";

							template += "<div class='nome-morador'>";
								template += 'nenhum morador';
							template += "</div>";

						template += "</div>";
					}

					template += "</div>";

				template += "</a>";
			template += "</div>";
		}

		$('#resultado-busca').html(template);
	},

	nenhumResultado : function(){
		$('#resultado-busca').html("<h2>Nenhum Resultado</h2>");
	},

	esconder_aviso : function(){
		$('#avisos-busca p').fadeOut('fast', function(){
			$('#avisos-busca').html('');
		})
	},

	init : function(){
		this.busca_on_keyup();
		this.bloquear_envio_form();
	}
};

Portaria.Unidades = {

	init: function(){

	}
};
Portaria.Dashboard = {

	abre_lista_convidados: function(){

		var botoes = $('.btn-convidados');

		$.each(botoes, function(){
			$(this).featherlight($(this).attr('data-featherlight-abrir'), {
				persist: true,
				afterOpen : function(){
					this.$content.addClass('featherlight-visible');
					Portaria.Dashboard.iniciarBusca();
					Portaria.Dashboard.toggle_presenca_convidado();
					Portaria.Dashboard.toggle_visibilidade_presentes();
				},
				afterClose: function(){
					Portaria.Dashboard.removerBinds();
					this.$content.removeClass('featherlight-visible');
				}
			});
		});

	},

	iniciarBusca: function(){

		var busca = $('.featherlight .featherlight-visible').find('input');

		busca.val('');
		Portaria.Dashboard.filtrar_lista_convidados('');

		busca.on('keyup', function(){
			Portaria.Dashboard.filtrar_lista_convidados($(this).val());
		});

		busca.focus();
	},

	filtrar_lista_convidados: function(termo){

		var resultados = 0;

		$(".featherlight .featherlight-visible .lista-convidados").find("li").each( function(index) {

			if($(this).find('a').length > 0){

				var nome = $(this).find('a').text().toLowerCase();

    		var check = nome.indexOf( termo.toLowerCase() ) !== -1;

      	$(this).toggle(check);

      	if(check){
      		resultados++;
      	}
			}

    });

    if(resultados == 0){
    	$(".featherlight .featherlight-visible .lista-convidados li:last-child").show();
    }else{
    	$(".featherlight .featherlight-visible .lista-convidados li:last-child").hide();
    }
	},

	toggle_presenca_convidado: function(){
		$('.lista-convidados .btn-presenca').on('click.toggle_presenca_convidado', function(e){
			e.preventDefault();

			var btn = $(this);
			var span = btn.find('span');

			var convidado = btn.attr('data-convidado');
			var lista = btn.attr('data-lista');

			if(btn.hasClass('oculto')){

				span.html('marcar como presente');
				btn.removeClass('presente');

				setTimeout( function(){
					$.post('portaria/toggle-presenca-convidado', {
						lista: lista,
						convidado: convidado,
						presenca: 0
					}, function(){
						btn.removeClass('oculto');
					});
				}, 300);

			}else{

				span.html('presente');
				btn.addClass('presente');

				setTimeout( function(){
					$.post('portaria/toggle-presenca-convidado', {
						lista: lista,
						convidado: convidado,
						presenca: 1
					}, function(){
						btn.addClass('oculto');
					});
				}, 300);

			}
		});
	},

	toggle_visibilidade_presentes: function(){
		$('.lista-convidados-popup .toggle-visibilidade-lista-convidados').on('click.toggle_presenca_convidado', function(e){
			e.preventDefault();

			var lista = $(this).parent().parent().find('ul.lista-convidados');
			var span = $(this).find('span');

			if(lista.hasClass('mostrar-ocultos')){

				span.html('mostrar');
				lista.removeClass('mostrar-ocultos');

			}else{

				span.html('ocultar');
				lista.addClass('mostrar-ocultos');

			}
		});
	},

	removerBinds: function(){
		$('.featherlight .featherlight-visible input').off('keyup');
		$('.lista-convidados .btn-presenca').off('click.toggle_presenca_convidado');
		$('.lista-convidados-popup .toggle-visibilidade-lista-convidados').off('click.toggle_presenca_convidado');
	},

	init: function(){

		this.abre_lista_convidados();

	}
};

Portaria.Comunicacao = {

  remover_erros : function() {
    $.each($('.input-erro input, .input-erro select, .input-erro textarea'), function(){

      var evento = $(this).prop('nodeName').toLowerCase() == 'select'  ? 'click' : 'keyup';

      $(this).on(evento, function(){
        $(this).parent()
               .removeClass('input-erro')
               .removeClass('obrigatorio');
      });

    });
  },

  validacao_on_submit : function(inputs_obrigatorios) {
		$('form').submit( function(e) {

			for (var i = 0; i < inputs_obrigatorios.length; i++) {

				var check = $(inputs_obrigatorios[i]);

				if(!check.val()){

					check.parent()
               .addClass('input-erro')
							 .addClass('obrigatorio');

					evento = check.prop('nodeName').toLowerCase() == 'select' ? 'click' : 'keyup';

					check.focus().on(evento, function(){
				  	check.parent()
                 .removeClass('input-erro')
				  			 .removeClass('obrigatorio');
					});

					e.preventDefault();
					return false;
				}
			};

		});
	},

  init: function(){
    var campos_obrigatorios = [
			'#input-comunicao-titulo',
			'#input-comunicacao-descricao'
		];

    this.validacao_on_submit(campos_obrigatorios);
    this.remover_erros();
  }
};
Portaria.Lavanderia = {

  listaUnidades : [],

  timer : null,

  iniciar_unidades : function()
	{
		this.listaUnidades = JSON.parse($('#listaUnidadesJson').val());
	},

  verificar_unidade : function(unidade)
	{
		var resumo_unidade = unidade.toUpperCase();
    var filtrado = this.listaUnidades.filter(
    	function(data){return data == resumo_unidade}
  	);

    return filtrado.length > 0;
	},

  mascara_somente_digitos : function()
  {
    $('#input-lavanderia-quantidade').mask('000');
  },

  remover_erros : function()
  {
    $.each($('.input-erro input'), function(){

      var evento = $(this).prop('nodeName').toLowerCase() == 'select'  ? 'click' : 'keyup';

      $(this).on(evento, function(){
        $(this).parent()
               .removeClass('input-erro')
               .removeClass('obrigatorio')
               .removeClass('email-invalido')
               .removeClass('login-fail');
      });

    });
  },

  validacao_on_submit : function(inputs_obrigatorios)
  {
		$('form').submit( function(e) {

			for (var i = 0; i < inputs_obrigatorios.length; i++) {

				var check = $(inputs_obrigatorios[i]);

				if(!check.val()){

					check.parent()
               .addClass('input-erro')
							 .addClass('obrigatorio');

					evento = check.prop('nodeName').toLowerCase() == 'select' ? 'click' : 'keyup';

					check.focus().on(evento, function(){
					  	check.parent()
                   .removeClass('input-erro')
					  			 .removeClass('obrigatorio');
					});

					e.preventDefault();
					return false;
				}
			};

      var check_unidade = $('#input-lavanderia-unidade');

      if(!Portaria.Lavanderia.verificar_unidade(check_unidade.val())) {
        check_unidade.parent()
                     .addClass('input-erro')
                     .addClass('unidade-invalida');

        check_unidade.focus().on('keyup', function(){
            check_unidade.parent()
                         .removeClass('input-erro')
                         .removeClass('unidade-invalida');
        });

        e.preventDefault();
        return false;
      }

		});
	},

  mostra_retorno_em_shadow : function()
  {
    var retorno = $('#retorno_formulario_shadow');

    if(retorno.length){
      $.featherlight(retorno, {
        closeOnClick: 'anywhere',
        onKeyUp: function(){
          $.featherlight.current().close();
        }
      });
    }
  },

  busca_unidade_keyup : function()
  {
    $('#consultar-unidade-lavanderia').on('keyup', function(e){

      var parent = $(this).parent();
			var termo = $(this).val();
      var loader = $('#loader');

      clearTimeout(Portaria.Lavanderia.timer);

			if(termo.length > 0){

        if(Portaria.Lavanderia.verificar_unidade(termo)){

          parent.removeClass('input-erro')
                .removeClass('unidade-invalida');

          loader.fadeIn('fast', function(){
            Portaria.Lavanderia.timer = setTimeout( function(){
              Portaria.Lavanderia.buscar_fichas(termo);
            }, 400);
          });


        }else{

          parent.addClass('input-erro')
                .addClass('unidade-invalida');

          Portaria.Lavanderia.apagar_resultado_busca();

        }

			}else{

        parent.removeClass('input-erro')
              .removeClass('unidade-invalida');

				Portaria.Lavanderia.apagar_resultado_busca();

			}

		});
  },

  buscar_fichas : function(termo)
  {
    $.post('portaria/lavanderia/consulta', {
      'unidade' : termo
    }, function(retorno){

      Portaria.Lavanderia.mostrar_resultados(termo, retorno);

    });
  },

  mostrar_resultados : function(termo, retorno)
  {
    $('#resultados-unidade-placeholder').html(termo.toUpperCase());

    if(retorno.length == 0)
      Portaria.Lavanderia.mostrar_msg_sem_resultados();
    else
      Portaria.Lavanderia.mostrar_tabela_fichas(retorno);

    $('#loader').fadeOut('fast', function(){
      $('#resultados-lavanderia').fadeIn('normal');
    });

  },

  mostrar_msg_sem_resultados : function()
  {
    $('#resultados-lavanderia .contem-tabela').html('<h4>Nenhum resultado encontrado</h4>');
  },

  mostrar_tabela_fichas : function(retorno)
  {
    var tabela = "<table>";

    for (var i = 0; i < retorno.length; i++) {
      tabela += "<tr>";
        tabela += "<td>";
          tabela += retorno[i].unidade_resumo;
        tabela += "</td>";
        tabela += "<td>";
          tabela += retorno[i].data_formatada;
        tabela += "</td>";
        tabela += "<td>";
          tabela += retorno[i].quantidade;
          tabela += " ficha";
          tabela += (retorno[i].quantidade > 1) ? "s" : "";
        tabela += "</td>";
        tabela += "<td>";
          tabela += "<a href='#' class='btn btn-circular btn-danger btn-mini' data-ficha='" + retorno[i].id + "'>X</a>";
        tabela += "</td>";
      tabela += "</tr>";
    }

    tabela += "</table>";

    $('#resultados-lavanderia .contem-tabela').html(tabela);
  },

  apagar_resultado_busca : function()
  {
    $('#resultados-lavanderia').fadeOut('normal');
  },

  bind_remover_fichas : function()
  {
    $('#resultados-lavanderia').on('click', 'table .btn', function(e){
      e.preventDefault();

      var linha    = $(this).parent().parent();
      var ficha_id = $(this).attr('data-ficha');

      $.featherlight($('#input-motivo-remocao'), {
        afterContent : function(){

          $(document).on('click', '.enviar-solicitacao-remocao', function(){

            var motivo_input = $('.featherlight textarea');
            var motivo = motivo_input.val();

            if(motivo == ''){

              motivo_input.parent()
                          .addClass('input-erro')
                          .addClass('obrigatorio');

              return false;

            }else{

              $.featherlight.current().close();
              linha.fadeOut('normal', function(){
                Portaria.Lavanderia.remover_ficha_lavanderia(linha, ficha_id, motivo);
              });

            }
          });

          $(document).on('keyup', 'textarea', function(){
            if($(this).val() != ''){
              $(this).parent()
                     .removeClass('input-erro')
                     .removeClass('obrigatorio');
            }
          });

        }
      });

    });
  },

  remover_ficha_lavanderia : function(linha, ficha_id, motivo)
  {
    $.post('portaria/lavanderia/remover', {
      ficha_id : ficha_id,
      motivo_cancelamento : motivo
    }, function(retorno){

      if(retorno == 1){

        linha.remove();

        if($('#resultados-lavanderia tr').length == 0)
          Portaria.Lavanderia.mostrar_msg_sem_resultados();

      }else{
        linha.fadeIn('normal');
      }

    });
  },

  init: function(){
    var campos_obrigatorios = [
      '#input-lavanderia-quantidade',
      '#input-lavanderia-unidade',
      '#input-lavanderia-login',
      '#input-lavanderia-senha'
		];

    this.iniciar_unidades();
    this.validacao_on_submit(campos_obrigatorios);
    this.remover_erros();
    this.mascara_somente_digitos();
    this.busca_unidade_keyup();
    this.mostra_retorno_em_shadow();
    this.bind_remover_fichas();
  }
};

Portaria.Veiculos = {

  listaUnidades : [],

  timer : null,

  mercoSulMaskBehavior : function (val) {
    var myMask = 'AAA0A00';
    var mercosul = /([A-Za-z]{3}[0-9]{1}[A-Za-z]{1})/;
    var normal = /([A-Za-z]{3}[0-9]{2})/;
    var replaced = val.replace(/[^\w]/g, '');
    if (normal.exec(replaced)) {
      myMask = 'AAA-0000';
    } else if (mercosul.exec(replaced)) {
      myMask = 'AAA0A00';
    }
    return myMask;
  },

  mercoSulOptions : {
    placeholder : 'placa',
    clearIfNotMatch: true,
    onKeyPress: function(val, e, field, options) {
      field.val(val.toUpperCase())
      field.mask(Portaria.Veiculos.mercoSulMaskBehavior.apply({}, arguments), Portaria.Veiculos.mercoSulOptions);
      Portaria.Veiculos.apagar_resultado_busca();
    },
    onComplete: function(val, e, currentField, options) {
      var input = $(currentField);
      var termo = input.val();
      var loader = $('#loader');
      clearTimeout(Portaria.Veiculos.timer);
      if(termo.length > 0){
        loader.fadeIn('fast', function(){
          Portaria.Veiculos.timer = setTimeout( function(){
            Portaria.Veiculos.buscar_placa(termo);
          }, 400);
        });
      }else{
        Portaria.Veiculos.apagar_resultado_busca();
      }
    }
  },

  mercoSulOptionsBasic: {
    placeholder : 'placa',
    clearIfNotMatch: true,
    onKeyPress: function(val, e, field, options) {
      field.val(val.toUpperCase())
      field.mask(Portaria.Veiculos.mercoSulMaskBehavior.apply({}, arguments), Portaria.Veiculos.mercoSulOptions);      
    }
  },

  iniciar_unidades : function()
	{
		this.listaUnidades = JSON.parse($('#listaUnidadesJson').val());
	},

  verificar_unidade : function(unidade)
	{
		var resumo_unidade = unidade.toUpperCase();
    var filtrado = this.listaUnidades.filter(
    	function(data){return data == resumo_unidade}
  	);

    return filtrado.length > 0;
	},

  mascara_placa_veiculos_busca : function(){
    var field = '#consultar-placa-veiculo'
    $(field).bind('paste', function(e) {
			$(this).unmask();
    });
    $(field).bind('input', function(e) {
		  $(field).mask(Portaria.Veiculos.mercoSulMaskBehavior, Portaria.Veiculos.mercoSulOptions);
    });
  },

  mascara_placa_veiculos_form : function(){
    var field = '#veiculos-da-unidade-form-placa'
    $(field).bind('paste', function(e) {
			$(this).unmask();
    });
    $(field).bind('input', function(e) {
		  $(field).mask(Portaria.Veiculos.mercoSulMaskBehavior, Portaria.Veiculos.mercoSulOptionsBasic);
    });
  },

  remover_erros : function()
  {
    $.each($('.input-erro input'), function(){

      var evento = 'click';

      $(this).on(evento, function(){
        $(this).parent()
               .removeClass('input-erro')
               .removeClass('obrigatorio');
      });

    });
  },

  mostra_retorno_em_shadow : function()
  {
    var retorno = $('#retorno_formulario_shadow');

    if(retorno.length){
      $.featherlight(retorno, {
        closeOnClick: 'anywhere',
        onKeyUp: function(){
          $.featherlight.current().close();
        }
      });
    }
  },

  validacao_on_submit : function(inputs_obrigatorios)
  {
		$('#form-cadastrar-veiculo-temporario').submit( function(e) {

			for (var i = 0; i < inputs_obrigatorios.length; i++) {

				var check = $(inputs_obrigatorios[i]);

				if(!check.val()){

					check.parent()
               .addClass('input-erro')
							 .addClass('obrigatorio');

					evento = check.prop('nodeName').toLowerCase() == 'select' ? 'click' : 'keyup';

					check.focus().on(evento, function(){
					  	check.parent()
                   .removeClass('input-erro')
					  			 .removeClass('obrigatorio');
					});

					e.preventDefault();
					return false;
				}
			};

      var check_unidade = $('#veiculos-da-unidade-form-unidade');

      if(!Portaria.Veiculos.verificar_unidade(check_unidade.val())) {
        check_unidade.parent()
                     .addClass('input-erro')
                     .addClass('unidade-invalida');

        check_unidade.focus().on('keyup', function(){
            check_unidade.parent()
                         .removeClass('input-erro')
                         .removeClass('unidade-invalida');
        });

        e.preventDefault();
        return false;
      }

		});
	},

  buscar_placa : function(termo)
  {
    $.post('portaria/veiculos/consulta', {
      'placa' : termo
    }, function(retorno){

      Portaria.Veiculos.mostrar_resultados(termo, retorno);

    });
  },

  mostrar_resultados : function(termo, retorno)
  {
    $('#resultados-placa-placeholder').html(termo.toUpperCase());

    if(retorno.length == 0)
      Portaria.Veiculos.mostrar_msg_sem_resultados();
    else
      Portaria.Veiculos.mostrar_tabela_veiculos(retorno);

    $('#loader').fadeOut('fast', function(){
      $('#resultados-veiculos').fadeIn('normal');
    });

  },

  mostrar_msg_sem_resultados : function()
  {
    $('#resultados-veiculos .contem-tabela').html('<h4>Nenhum resultado encontrado</h4>');
  },

  mostrar_tabela_veiculos : function(retorno)
  {
    var tabela = "<table>";

    tabela += "<thead>";
      tabela += "<tr>";
        tabela += "<th>";
        tabela += "Unidade";
        tabela += "</th>";
        tabela += "<th>";
        tabela += "Veículo";
        tabela += "</th>";
        tabela += "<th>";
        tabela += "Cor";
        tabela += "</th>";
        tabela += "<th>";
        tabela += "Placa";
        tabela += "</th>";
        tabela += "<th>";
        tabela += "Vaga";
        tabela += "</th>";
      tabela += "</tr>";
    tabela += "</thead>";

    tabela += "<tbody>";
    for (var i = 0; i < retorno.length; i++) {
      tabela += "<tr>";
        tabela += "<td>";
          tabela += retorno[i].unidade_resumo;
        tabela += "</td>";
        tabela += "<td>";
          tabela += retorno[i].veiculo;
        tabela += "</td>";
        tabela += "<td>";
          tabela += retorno[i].cor;
        tabela += "</td>";
        tabela += "<td>";
          tabela += retorno[i].placa;
        tabela += "</td>";
        tabela += "<td>";
          tabela += retorno[i].vaga;
        tabela += "</td>";
      tabela += "</tr>";
    }
    tabela += "</tbody>";

    tabela += "</table>";

    $('#resultados-veiculos .contem-tabela').html(tabela);
  },

  apagar_resultado_busca : function()
  {
    $('#resultados-veiculos').fadeOut('normal');
  },

  init: function(){
    var campos_obrigatorios = [
      '#veiculos-da-unidade-form-unidade',
      '#veiculos-da-unidade-form-veiculo',
      '#veiculos-da-unidade-form-cor',
      '#veiculos-da-unidade-form-placa',
      //'#veiculos-da-unidade-form-vaga'
		];

    this.iniciar_unidades();
    this.validacao_on_submit(campos_obrigatorios);
    this.remover_erros();
    this.mascara_placa_veiculos_busca();
    this.mascara_placa_veiculos_form();
    this.mostra_retorno_em_shadow();
  }
};

$('document').ready( function(){

	Portaria.init();

	$('.modulo-inativo').click( function(e){
		e.preventDefault();
		alert('Módulo não implementado');
	});

});