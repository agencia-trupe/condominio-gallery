/* Tooltipster v3.3.0 */;(function(e,t,n){function s(t,n){this.bodyOverflowX;this.callbacks={hide:[],show:[]};this.checkInterval=null;this.Content;this.$el=e(t);this.$elProxy;this.elProxyPosition;this.enabled=true;this.options=e.extend({},i,n);this.mouseIsOverProxy=false;this.namespace="tooltipster-"+Math.round(Math.random()*1e5);this.Status="hidden";this.timerHide=null;this.timerShow=null;this.$tooltip;this.options.iconTheme=this.options.iconTheme.replace(".","");this.options.theme=this.options.theme.replace(".","");this._init()}function o(t,n){var r=true;e.each(t,function(e,i){if(typeof n[e]==="undefined"||t[e]!==n[e]){r=false;return false}});return r}function f(){return!a&&u}function l(){var e=n.body||n.documentElement,t=e.style,r="transition";if(typeof t[r]=="string"){return true}v=["Moz","Webkit","Khtml","O","ms"],r=r.charAt(0).toUpperCase()+r.substr(1);for(var i=0;i<v.length;i++){if(typeof t[v[i]+r]=="string"){return true}}return false}var r="tooltipster",i={animation:"fade",arrow:true,arrowColor:"",autoClose:true,content:null,contentAsHTML:false,contentCloning:true,debug:true,delay:200,minWidth:0,maxWidth:null,functionInit:function(e,t){},functionBefore:function(e,t){t()},functionReady:function(e,t){},functionAfter:function(e){},hideOnClick:false,icon:"(?)",iconCloning:true,iconDesktop:false,iconTouch:false,iconTheme:"tooltipster-icon",interactive:false,interactiveTolerance:350,multiple:false,offsetX:0,offsetY:0,onlyOne:false,position:"top",positionTracker:false,positionTrackerCallback:function(e){if(this.option("trigger")=="hover"&&this.option("autoClose")){this.hide()}},restoration:"current",speed:350,timer:0,theme:"tooltipster-default",touchDevices:true,trigger:"hover",updateAnimation:true};s.prototype={_init:function(){var t=this;if(n.querySelector){var r=null;if(t.$el.data("tooltipster-initialTitle")===undefined){r=t.$el.attr("title");if(r===undefined)r=null;t.$el.data("tooltipster-initialTitle",r)}if(t.options.content!==null){t._content_set(t.options.content)}else{t._content_set(r)}var i=t.options.functionInit.call(t.$el,t.$el,t.Content);if(typeof i!=="undefined")t._content_set(i);t.$el.removeAttr("title").addClass("tooltipstered");if(!u&&t.options.iconDesktop||u&&t.options.iconTouch){if(typeof t.options.icon==="string"){t.$elProxy=e('<span class="'+t.options.iconTheme+'"></span>');t.$elProxy.text(t.options.icon)}else{if(t.options.iconCloning)t.$elProxy=t.options.icon.clone(true);else t.$elProxy=t.options.icon}t.$elProxy.insertAfter(t.$el)}else{t.$elProxy=t.$el}if(t.options.trigger=="hover"){t.$elProxy.on("mouseenter."+t.namespace,function(){if(!f()||t.options.touchDevices){t.mouseIsOverProxy=true;t._show()}}).on("mouseleave."+t.namespace,function(){if(!f()||t.options.touchDevices){t.mouseIsOverProxy=false}});if(u&&t.options.touchDevices){t.$elProxy.on("touchstart."+t.namespace,function(){t._showNow()})}}else if(t.options.trigger=="click"){t.$elProxy.on("click."+t.namespace,function(){if(!f()||t.options.touchDevices){t._show()}})}}},_show:function(){var e=this;if(e.Status!="shown"&&e.Status!="appearing"){if(e.options.delay){e.timerShow=setTimeout(function(){if(e.options.trigger=="click"||e.options.trigger=="hover"&&e.mouseIsOverProxy){e._showNow()}},e.options.delay)}else e._showNow()}},_showNow:function(n){var r=this;r.options.functionBefore.call(r.$el,r.$el,function(){if(r.enabled&&r.Content!==null){if(n)r.callbacks.show.push(n);r.callbacks.hide=[];clearTimeout(r.timerShow);r.timerShow=null;clearTimeout(r.timerHide);r.timerHide=null;if(r.options.onlyOne){e(".tooltipstered").not(r.$el).each(function(t,n){var r=e(n),i=r.data("tooltipster-ns");e.each(i,function(e,t){var n=r.data(t),i=n.status(),s=n.option("autoClose");if(i!=="hidden"&&i!=="disappearing"&&s){n.hide()}})})}var i=function(){r.Status="shown";e.each(r.callbacks.show,function(e,t){t.call(r.$el)});r.callbacks.show=[]};if(r.Status!=="hidden"){var s=0;if(r.Status==="disappearing"){r.Status="appearing";if(l()){r.$tooltip.clearQueue().removeClass("tooltipster-dying").addClass("tooltipster-"+r.options.animation+"-show");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(i)}else{r.$tooltip.stop().fadeIn(i)}}else if(r.Status==="shown"){i()}}else{r.Status="appearing";var s=r.options.speed;r.bodyOverflowX=e("body").css("overflow-x");e("body").css("overflow-x","hidden");var o="tooltipster-"+r.options.animation,a="-webkit-transition-duration: "+r.options.speed+"ms; -webkit-animation-duration: "+r.options.speed+"ms; -moz-transition-duration: "+r.options.speed+"ms; -moz-animation-duration: "+r.options.speed+"ms; -o-transition-duration: "+r.options.speed+"ms; -o-animation-duration: "+r.options.speed+"ms; -ms-transition-duration: "+r.options.speed+"ms; -ms-animation-duration: "+r.options.speed+"ms; transition-duration: "+r.options.speed+"ms; animation-duration: "+r.options.speed+"ms;",f=r.options.minWidth?"min-width:"+Math.round(r.options.minWidth)+"px;":"",c=r.options.maxWidth?"max-width:"+Math.round(r.options.maxWidth)+"px;":"",h=r.options.interactive?"pointer-events: auto;":"";r.$tooltip=e('<div class="tooltipster-base '+r.options.theme+'" style="'+f+" "+c+" "+h+" "+a+'"><div class="tooltipster-content"></div></div>');if(l())r.$tooltip.addClass(o);r._content_insert();r.$tooltip.appendTo("body");r.reposition();r.options.functionReady.call(r.$el,r.$el,r.$tooltip);if(l()){r.$tooltip.addClass(o+"-show");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(i)}else{r.$tooltip.css("display","none").fadeIn(r.options.speed,i)}r._interval_set();e(t).on("scroll."+r.namespace+" resize."+r.namespace,function(){r.reposition()});if(r.options.autoClose){e("body").off("."+r.namespace);if(r.options.trigger=="hover"){if(u){setTimeout(function(){e("body").on("touchstart."+r.namespace,function(){r.hide()})},0)}if(r.options.interactive){if(u){r.$tooltip.on("touchstart."+r.namespace,function(e){e.stopPropagation()})}var p=null;r.$elProxy.add(r.$tooltip).on("mouseleave."+r.namespace+"-autoClose",function(){clearTimeout(p);p=setTimeout(function(){r.hide()},r.options.interactiveTolerance)}).on("mouseenter."+r.namespace+"-autoClose",function(){clearTimeout(p)})}else{r.$elProxy.on("mouseleave."+r.namespace+"-autoClose",function(){r.hide()})}if(r.options.hideOnClick){r.$elProxy.on("click."+r.namespace+"-autoClose",function(){r.hide()})}}else if(r.options.trigger=="click"){setTimeout(function(){e("body").on("click."+r.namespace+" touchstart."+r.namespace,function(){r.hide()})},0);if(r.options.interactive){r.$tooltip.on("click."+r.namespace+" touchstart."+r.namespace,function(e){e.stopPropagation()})}}}}if(r.options.timer>0){r.timerHide=setTimeout(function(){r.timerHide=null;r.hide()},r.options.timer+s)}}})},_interval_set:function(){var t=this;t.checkInterval=setInterval(function(){if(e("body").find(t.$el).length===0||e("body").find(t.$elProxy).length===0||t.Status=="hidden"||e("body").find(t.$tooltip).length===0){if(t.Status=="shown"||t.Status=="appearing")t.hide();t._interval_cancel()}else{if(t.options.positionTracker){var n=t._repositionInfo(t.$elProxy),r=false;if(o(n.dimension,t.elProxyPosition.dimension)){if(t.$elProxy.css("position")==="fixed"){if(o(n.position,t.elProxyPosition.position))r=true}else{if(o(n.offset,t.elProxyPosition.offset))r=true}}if(!r){t.reposition();t.options.positionTrackerCallback.call(t,t.$el)}}}},200)},_interval_cancel:function(){clearInterval(this.checkInterval);this.checkInterval=null},_content_set:function(e){if(typeof e==="object"&&e!==null&&this.options.contentCloning){e=e.clone(true)}this.Content=e},_content_insert:function(){var e=this,t=this.$tooltip.find(".tooltipster-content");if(typeof e.Content==="string"&&!e.options.contentAsHTML){t.text(e.Content)}else{t.empty().append(e.Content)}},_update:function(e){var t=this;t._content_set(e);if(t.Content!==null){if(t.Status!=="hidden"){t._content_insert();t.reposition();if(t.options.updateAnimation){if(l()){t.$tooltip.css({width:"","-webkit-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-moz-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-o-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-ms-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms",transition:"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms"}).addClass("tooltipster-content-changing");setTimeout(function(){if(t.Status!="hidden"){t.$tooltip.removeClass("tooltipster-content-changing");setTimeout(function(){if(t.Status!=="hidden"){t.$tooltip.css({"-webkit-transition":t.options.speed+"ms","-moz-transition":t.options.speed+"ms","-o-transition":t.options.speed+"ms","-ms-transition":t.options.speed+"ms",transition:t.options.speed+"ms"})}},t.options.speed)}},t.options.speed)}else{t.$tooltip.fadeTo(t.options.speed,.5,function(){if(t.Status!="hidden"){t.$tooltip.fadeTo(t.options.speed,1)}})}}}}else{t.hide()}},_repositionInfo:function(e){return{dimension:{height:e.outerHeight(false),width:e.outerWidth(false)},offset:e.offset(),position:{left:parseInt(e.css("left")),top:parseInt(e.css("top"))}}},hide:function(n){var r=this;if(n)r.callbacks.hide.push(n);r.callbacks.show=[];clearTimeout(r.timerShow);r.timerShow=null;clearTimeout(r.timerHide);r.timerHide=null;var i=function(){e.each(r.callbacks.hide,function(e,t){t.call(r.$el)});r.callbacks.hide=[]};if(r.Status=="shown"||r.Status=="appearing"){r.Status="disappearing";var s=function(){r.Status="hidden";if(typeof r.Content=="object"&&r.Content!==null){r.Content.detach()}r.$tooltip.remove();r.$tooltip=null;e(t).off("."+r.namespace);e("body").off("."+r.namespace).css("overflow-x",r.bodyOverflowX);e("body").off("."+r.namespace);r.$elProxy.off("."+r.namespace+"-autoClose");r.options.functionAfter.call(r.$el,r.$el);i()};if(l()){r.$tooltip.clearQueue().removeClass("tooltipster-"+r.options.animation+"-show").addClass("tooltipster-dying");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(s)}else{r.$tooltip.stop().fadeOut(r.options.speed,s)}}else if(r.Status=="hidden"){i()}return r},show:function(e){this._showNow(e);return this},update:function(e){return this.content(e)},content:function(e){if(typeof e==="undefined"){return this.Content}else{this._update(e);return this}},reposition:function(){var n=this;if(e("body").find(n.$tooltip).length!==0){n.$tooltip.css("width","");n.elProxyPosition=n._repositionInfo(n.$elProxy);var r=null,i=e(t).width(),s=n.elProxyPosition,o=n.$tooltip.outerWidth(false),u=n.$tooltip.innerWidth()+1,a=n.$tooltip.outerHeight(false);if(n.$elProxy.is("area")){var f=n.$elProxy.attr("shape"),l=n.$elProxy.parent().attr("name"),c=e('img[usemap="#'+l+'"]'),h=c.offset().left,p=c.offset().top,d=n.$elProxy.attr("coords")!==undefined?n.$elProxy.attr("coords").split(","):undefined;if(f=="circle"){var v=parseInt(d[0]),m=parseInt(d[1]),g=parseInt(d[2]);s.dimension.height=g*2;s.dimension.width=g*2;s.offset.top=p+m-g;s.offset.left=h+v-g}else if(f=="rect"){var v=parseInt(d[0]),m=parseInt(d[1]),y=parseInt(d[2]),b=parseInt(d[3]);s.dimension.height=b-m;s.dimension.width=y-v;s.offset.top=p+m;s.offset.left=h+v}else if(f=="poly"){var w=[],E=[],S=0,x=0,T=0,N=0,C="even";for(var k=0;k<d.length;k++){var L=parseInt(d[k]);if(C=="even"){if(L>T){T=L;if(k===0){S=T}}if(L<S){S=L}C="odd"}else{if(L>N){N=L;if(k==1){x=N}}if(L<x){x=L}C="even"}}s.dimension.height=N-x;s.dimension.width=T-S;s.offset.top=p+x;s.offset.left=h+S}else{s.dimension.height=c.outerHeight(false);s.dimension.width=c.outerWidth(false);s.offset.top=p;s.offset.left=h}}var A=0,O=0,M=0,_=parseInt(n.options.offsetY),D=parseInt(n.options.offsetX),P=n.options.position;function H(){var n=e(t).scrollLeft();if(A-n<0){r=A-n;A=n}if(A+o-n>i){r=A-(i+n-o);A=i+n-o}}function B(n,r){if(s.offset.top-e(t).scrollTop()-a-_-12<0&&r.indexOf("top")>-1){P=n}if(s.offset.top+s.dimension.height+a+12+_>e(t).scrollTop()+e(t).height()&&r.indexOf("bottom")>-1){P=n;M=s.offset.top-a-_-12}}if(P=="top"){var j=s.offset.left+o-(s.offset.left+s.dimension.width);A=s.offset.left+D-j/2;M=s.offset.top-a-_-12;H();B("bottom","top")}if(P=="top-left"){A=s.offset.left+D;M=s.offset.top-a-_-12;H();B("bottom-left","top-left")}if(P=="top-right"){A=s.offset.left+s.dimension.width+D-o;M=s.offset.top-a-_-12;H();B("bottom-right","top-right")}if(P=="bottom"){var j=s.offset.left+o-(s.offset.left+s.dimension.width);A=s.offset.left-j/2+D;M=s.offset.top+s.dimension.height+_+12;H();B("top","bottom")}if(P=="bottom-left"){A=s.offset.left+D;M=s.offset.top+s.dimension.height+_+12;H();B("top-left","bottom-left")}if(P=="bottom-right"){A=s.offset.left+s.dimension.width+D-o;M=s.offset.top+s.dimension.height+_+12;H();B("top-right","bottom-right")}if(P=="left"){A=s.offset.left-D-o-12;O=s.offset.left+D+s.dimension.width+12;var F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_;if(A<0&&O+o>i){var I=parseFloat(n.$tooltip.css("border-width"))*2,q=o+A-I;n.$tooltip.css("width",q+"px");a=n.$tooltip.outerHeight(false);A=s.offset.left-D-q-12-I;F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_}else if(A<0){A=s.offset.left+D+s.dimension.width+12;r="left"}}if(P=="right"){A=s.offset.left+D+s.dimension.width+12;O=s.offset.left-D-o-12;var F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_;if(A+o>i&&O<0){var I=parseFloat(n.$tooltip.css("border-width"))*2,q=i-A-I;n.$tooltip.css("width",q+"px");a=n.$tooltip.outerHeight(false);F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_}else if(A+o>i){A=s.offset.left-D-o-12;r="right"}}if(n.options.arrow){var R="tooltipster-arrow-"+P;if(n.options.arrowColor.length<1){var U=n.$tooltip.css("background-color")}else{var U=n.options.arrowColor}if(!r){r=""}else if(r=="left"){R="tooltipster-arrow-right";r=""}else if(r=="right"){R="tooltipster-arrow-left";r=""}else{r="left:"+Math.round(r)+"px;"}if(P=="top"||P=="top-left"||P=="top-right"){var z=parseFloat(n.$tooltip.css("border-bottom-width")),W=n.$tooltip.css("border-bottom-color")}else if(P=="bottom"||P=="bottom-left"||P=="bottom-right"){var z=parseFloat(n.$tooltip.css("border-top-width")),W=n.$tooltip.css("border-top-color")}else if(P=="left"){var z=parseFloat(n.$tooltip.css("border-right-width")),W=n.$tooltip.css("border-right-color")}else if(P=="right"){var z=parseFloat(n.$tooltip.css("border-left-width")),W=n.$tooltip.css("border-left-color")}else{var z=parseFloat(n.$tooltip.css("border-bottom-width")),W=n.$tooltip.css("border-bottom-color")}if(z>1){z++}var X="";if(z!==0){var V="",J="border-color: "+W+";";if(R.indexOf("bottom")!==-1){V="margin-top: -"+Math.round(z)+"px;"}else if(R.indexOf("top")!==-1){V="margin-bottom: -"+Math.round(z)+"px;"}else if(R.indexOf("left")!==-1){V="margin-right: -"+Math.round(z)+"px;"}else if(R.indexOf("right")!==-1){V="margin-left: -"+Math.round(z)+"px;"}X='<span class="tooltipster-arrow-border" style="'+V+" "+J+';"></span>'}n.$tooltip.find(".tooltipster-arrow").remove();var K='<div class="'+R+' tooltipster-arrow" style="'+r+'">'+X+'<span style="border-color:'+U+';"></span></div>';n.$tooltip.append(K)}n.$tooltip.css({top:Math.round(M)+"px",left:Math.round(A)+"px"})}return n},enable:function(){this.enabled=true;return this},disable:function(){this.hide();this.enabled=false;return this},destroy:function(){var t=this;t.hide();if(t.$el[0]!==t.$elProxy[0]){t.$elProxy.remove()}t.$el.removeData(t.namespace).off("."+t.namespace);var n=t.$el.data("tooltipster-ns");if(n.length===1){var r=null;if(t.options.restoration==="previous"){r=t.$el.data("tooltipster-initialTitle")}else if(t.options.restoration==="current"){r=typeof t.Content==="string"?t.Content:e("<div></div>").append(t.Content).html()}if(r){t.$el.attr("title",r)}t.$el.removeClass("tooltipstered").removeData("tooltipster-ns").removeData("tooltipster-initialTitle")}else{n=e.grep(n,function(e,n){return e!==t.namespace});t.$el.data("tooltipster-ns",n)}return t},elementIcon:function(){return this.$el[0]!==this.$elProxy[0]?this.$elProxy[0]:undefined},elementTooltip:function(){return this.$tooltip?this.$tooltip[0]:undefined},option:function(e,t){if(typeof t=="undefined")return this.options[e];else{this.options[e]=t;return this}},status:function(){return this.Status}};e.fn[r]=function(){var t=arguments;if(this.length===0){if(typeof t[0]==="string"){var n=true;switch(t[0]){case"setDefaults":e.extend(i,t[1]);break;default:n=false;break}if(n)return true;else return this}else{return this}}else{if(typeof t[0]==="string"){var r="#*$~&";this.each(function(){var n=e(this).data("tooltipster-ns"),i=n?e(this).data(n[0]):null;if(i){if(typeof i[t[0]]==="function"){var s=i[t[0]](t[1],t[2])}else{throw new Error('Unknown method .tooltipster("'+t[0]+'")')}if(s!==i){r=s;return false}}else{throw new Error("You called Tooltipster's \""+t[0]+'" method on an uninitialized element')}});return r!=="#*$~&"?r:this}else{var o=[],u=t[0]&&typeof t[0].multiple!=="undefined",a=u&&t[0].multiple||!u&&i.multiple,f=t[0]&&typeof t[0].debug!=="undefined",l=f&&t[0].debug||!f&&i.debug;this.each(function(){var n=false,r=e(this).data("tooltipster-ns"),i=null;if(!r){n=true}else if(a){n=true}else if(l){console.log('Tooltipster: one or more tooltips are already attached to this element: ignoring. Use the "multiple" option to attach more tooltips.')}if(n){i=new s(this,t[0]);if(!r)r=[];r.push(i.namespace);e(this).data("tooltipster-ns",r);e(this).data(i.namespace,i)}o.push(i)});if(a)return o;else return this}}};var u=!!("ontouchstart"in t);var a=false;e("body").one("mousemove",function(){a=true})})(jQuery,window,document);
/**
 * Featherlight - ultra slim jQuery lightbox
 * Version 1.3.5 - http://noelboss.github.io/featherlight/
 *
 * Copyright 2015, Noël Raoul Bossart (http://www.noelboss.com)
 * MIT Licensed.
**/
(function($) {
	"use strict";

	if('undefined' === typeof $) {
		if('console' in window){ window.console.info('Too much lightness, Featherlight needs jQuery.'); }
		return;
	}

	/* Featherlight is exported as $.featherlight.
	   It is a function used to open a featherlight lightbox.

	   [tech]
	   Featherlight uses prototype inheritance.
	   Each opened lightbox will have a corresponding object.
	   That object may have some attributes that override the
	   prototype's.
	   Extensions created with Featherlight.extend will have their
	   own prototype that inherits from Featherlight's prototype,
	   thus attributes can be overriden either at the object level,
	   or at the extension level.
	   To create callbacks that chain themselves instead of overriding,
	   use chainCallbacks.
	   For those familiar with CoffeeScript, this correspond to
	   Featherlight being a class and the Gallery being a class
	   extending Featherlight.
	   The chainCallbacks is used since we don't have access to
	   CoffeeScript's `super`.
	*/

	function Featherlight($content, config) {
		if(this instanceof Featherlight) {  /* called with new */
			this.id = Featherlight.id++;
			this.setup($content, config);
			this.chainCallbacks(Featherlight._callbackChain);
		} else {
			var fl = new Featherlight($content, config);
			fl.open();
			return fl;
		}
	}

	var opened = [],
		pruneOpened = function(remove) {
			opened = $.grep(opened, function(fl) {
				return fl !== remove && fl.$instance.closest('body').length > 0;
			} );
			return opened;
		};

	// structure({iframeMinHeight: 44, foo: 0}, 'iframe')
	//   #=> {min-height: 44}
	var structure = function(obj, prefix) {
		var result = {},
			regex = new RegExp('^' + prefix + '([A-Z])(.*)');
		for (var key in obj) {
			var match = key.match(regex);
			if (match) {
				var dasherized = (match[1] + match[2].replace(/([A-Z])/g, '-$1')).toLowerCase();
				result[dasherized] = obj[key];
			}
		}
		return result;
	};

	/* document wide key handler */
	var eventMap = { keyup: 'onKeyUp', resize: 'onResize' };

	var globalEventHandler = function(event) {
		$.each(Featherlight.opened().reverse(), function() {
			if (!event.isDefaultPrevented()) {
				if (false === this[eventMap[event.type]](event)) {
					event.preventDefault(); event.stopPropagation(); return false;
			  }
			}
		});
	};

	var toggleGlobalEvents = function(set) {
			if(set !== Featherlight._globalHandlerInstalled) {
				Featherlight._globalHandlerInstalled = set;
				var events = $.map(eventMap, function(_, name) { return name+'.'+Featherlight.prototype.namespace; } ).join(' ');
				$(window)[set ? 'on' : 'off'](events, globalEventHandler);
			}
		};

	Featherlight.prototype = {
		constructor: Featherlight,
		/*** defaults ***/
		/* extend featherlight with defaults and methods */
		namespace:      'featherlight',        /* Name of the events and css class prefix */
		targetAttr:     'data-featherlight',   /* Attribute of the triggered element that contains the selector to the lightbox content */
		variant:        null,                  /* Class that will be added to change look of the lightbox */
		resetCss:       false,                 /* Reset all css */
		background:     null,                  /* Custom DOM for the background, wrapper and the closebutton */
		openTrigger:    'click',               /* Event that triggers the lightbox */
		closeTrigger:   'click',               /* Event that triggers the closing of the lightbox */
		filter:         null,                  /* Selector to filter events. Think $(...).on('click', filter, eventHandler) */
		root:           'body',                /* Where to append featherlights */
		openSpeed:      250,                   /* Duration of opening animation */
		closeSpeed:     250,                   /* Duration of closing animation */
		closeOnClick:   'background',          /* Close lightbox on click ('background', 'anywhere' or false) */
		closeOnEsc:     true,                  /* Close lightbox when pressing esc */
		closeIcon:      '&#10005;',            /* Close icon */
		loading:        '',                    /* Content to show while initial content is loading */
		persist:        false,                 /* If set, the content will persist and will be shown again when opened again. 'shared' is a special value when binding multiple elements for them to share the same content */
		otherClose:     null,                  /* Selector for alternate close buttons (e.g. "a.close") */
		beforeOpen:     $.noop,                /* Called before open. can return false to prevent opening of lightbox. Gets event as parameter, this contains all data */
		beforeContent:  $.noop,                /* Called when content is loaded. Gets event as parameter, this contains all data */
		beforeClose:    $.noop,                /* Called before close. can return false to prevent opening of lightbox. Gets event as parameter, this contains all data */
		afterOpen:      $.noop,                /* Called after open. Gets event as parameter, this contains all data */
		afterContent:   $.noop,                /* Called after content is ready and has been set. Gets event as parameter, this contains all data */
		afterClose:     $.noop,                /* Called after close. Gets event as parameter, this contains all data */
		onKeyUp:        $.noop,                /* Called on key up for the frontmost featherlight */
		onResize:       $.noop,                /* Called after new content and when a window is resized */
		type:           null,                  /* Specify type of lightbox. If unset, it will check for the targetAttrs value. */
		contentFilters: ['jquery', 'image', 'html', 'ajax', 'iframe', 'text'], /* List of content filters to use to determine the content */

		/*** methods ***/
		/* setup iterates over a single instance of featherlight and prepares the background and binds the events */
		setup: function(target, config){
			/* all arguments are optional */
			if (typeof target === 'object' && target instanceof $ === false && !config) {
				config = target;
				target = undefined;
			}

			var self = $.extend(this, config, {target: target}),
				css = !self.resetCss ? self.namespace : self.namespace+'-reset', /* by adding -reset to the classname, we reset all the default css */
				$background = $(self.background || [
					'<div class="'+css+'-loading '+css+'">',
						'<div class="'+css+'-content">',
							'<span class="'+css+'-close-icon '+ self.namespace + '-close">',
								self.closeIcon,
							'</span>',
							'<div class="'+self.namespace+'-inner">' + self.loading + '</div>',
						'</div>',
					'</div>'].join('')),
				closeButtonSelector = '.'+self.namespace+'-close' + (self.otherClose ? ',' + self.otherClose : '');

			self.$instance = $background.clone().addClass(self.variant); /* clone DOM for the background, wrapper and the close button */

			/* close when click on background/anywhere/null or closebox */
			self.$instance.on(self.closeTrigger+'.'+self.namespace, function(event) {
				var $target = $(event.target);
				if( ('background' === self.closeOnClick  && $target.is('.'+self.namespace))
					|| 'anywhere' === self.closeOnClick
					|| $target.closest(closeButtonSelector).length ){
					self.close(event);
					event.preventDefault();
				}
			});

			return this;
		},

		/* this method prepares the content and converts it into a jQuery object or a promise */
		getContent: function(){
			if(this.persist !== false && this.$content) {
				return this.$content;
			}
			var self = this,
				filters = this.constructor.contentFilters,
				readTargetAttr = function(name){ return self.$currentTarget && self.$currentTarget.attr(name); },
				targetValue = readTargetAttr(self.targetAttr),
				data = self.target || targetValue || '';

			/* Find which filter applies */
			var filter = filters[self.type]; /* check explicit type like {type: 'image'} */

			/* check explicit type like data-featherlight="image" */
			if(!filter && data in filters) {
				filter = filters[data];
				data = self.target && targetValue;
			}
			data = data || readTargetAttr('href') || '';

			/* check explicity type & content like {image: 'photo.jpg'} */
			if(!filter) {
				for(var filterName in filters) {
					if(self[filterName]) {
						filter = filters[filterName];
						data = self[filterName];
					}
				}
			}

			/* otherwise it's implicit, run checks */
			if(!filter) {
				var target = data;
				data = null;
				$.each(self.contentFilters, function() {
					filter = filters[this];
					if(filter.test)  {
						data = filter.test(target);
					}
					if(!data && filter.regex && target.match && target.match(filter.regex)) {
						data = target;
					}
					return !data;
				});
				if(!data) {
					if('console' in window){ window.console.error('Featherlight: no content filter found ' + (target ? ' for "' + target + '"' : ' (no target specified)')); }
					return false;
				}
			}
			/* Process it */
			return filter.process.call(self, data);
		},

		/* sets the content of $instance to $content */
		setContent: function($content){
			var self = this;
			/* we need a special class for the iframe */
			if($content.is('iframe') || $('iframe', $content).length > 0){
				self.$instance.addClass(self.namespace+'-iframe');
			}

			self.$instance.removeClass(self.namespace+'-loading');

			/* replace content by appending to existing one before it is removed
			   this insures that featherlight-inner remain at the same relative
				 position to any other items added to featherlight-content */
			self.$instance.find('.'+self.namespace+'-inner')
				.not($content)                /* excluded new content, important if persisted */
				.slice(1).remove().end()      /* In the unexpected event where there are many inner elements, remove all but the first one */
				.replaceWith($.contains(self.$instance[0], $content[0]) ? '' : $content);

			self.$content = $content.addClass(self.namespace+'-inner');

			return self;
		},

		/* opens the lightbox. "this" contains $instance with the lightbox, and with the config.
			Returns a promise that is resolved after is successfully opened. */
		open: function(event){
			var self = this;
			self.$instance.hide().appendTo(self.root);
			if((!event || !event.isDefaultPrevented())
				&& self.beforeOpen(event) !== false) {

				if(event){
					event.preventDefault();
				}
				var $content = self.getContent();

				if($content) {
					opened.push(self);

					toggleGlobalEvents(true);

					self.$instance.fadeIn(self.openSpeed);
					self.beforeContent(event);

					/* Set content and show */
					return $.when($content)
						.always(function($content){
							self.setContent($content);
							self.afterContent(event);
						})
						.then(self.$instance.promise())
						/* Call afterOpen after fadeIn is done */
						.done(function(){ self.afterOpen(event); });
				}
			}
			self.$instance.detach();
			return $.Deferred().reject().promise();
		},

		/* closes the lightbox. "this" contains $instance with the lightbox, and with the config
			returns a promise, resolved after the lightbox is successfully closed. */
		close: function(event){
			var self = this,
				deferred = $.Deferred();

			if(self.beforeClose(event) === false) {
				deferred.reject();
			} else {

				if (0 === pruneOpened(self).length) {
					toggleGlobalEvents(false);
				}

				self.$instance.fadeOut(self.closeSpeed,function(){
					self.$instance.detach();
					self.afterClose(event);
					deferred.resolve();
				});
			}
			return deferred.promise();
		},

		/* Utility function to chain callbacks
		   [Warning: guru-level]
		   Used be extensions that want to let users specify callbacks but
		   also need themselves to use the callbacks.
		   The argument 'chain' has callback names as keys and function(super, event)
		   as values. That function is meant to call `super` at some point.
		*/
		chainCallbacks: function(chain) {
			for (var name in chain) {
				this[name] = $.proxy(chain[name], this, $.proxy(this[name], this));
			}
		}
	};

	$.extend(Featherlight, {
		id: 0,                                    /* Used to id single featherlight instances */
		autoBind:       '[data-featherlight]',    /* Will automatically bind elements matching this selector. Clear or set before onReady */
		defaults:       Featherlight.prototype,   /* You can access and override all defaults using $.featherlight.defaults, which is just a synonym for $.featherlight.prototype */
		/* Contains the logic to determine content */
		contentFilters: {
			jquery: {
				regex: /^[#.]\w/,         /* Anything that starts with a class name or identifiers */
				test: function(elem)    { return elem instanceof $ && elem; },
				process: function(elem) { return this.persist !== false ? $(elem) : $(elem).clone(true); }
			},
			image: {
				regex: /\.(png|jpg|jpeg|gif|tiff|bmp|svg)(\?\S*)?$/i,
				process: function(url)  {
					var self = this,
						deferred = $.Deferred(),
						img = new Image(),
						$img = $('<img src="'+url+'" alt="" class="'+self.namespace+'-image" />');
					img.onload  = function() {
						/* Store naturalWidth & height for IE8 */
						$img.naturalWidth = img.width; $img.naturalHeight = img.height;
						deferred.resolve( $img );
					};
					img.onerror = function() { deferred.reject($img); };
					img.src = url;
					return deferred.promise();
				}
			},
			html: {
				regex: /^\s*<[\w!][^<]*>/, /* Anything that starts with some kind of valid tag */
				process: function(html) { return $(html); }
			},
			ajax: {
				regex: /./,            /* At this point, any content is assumed to be an URL */
				process: function(url)  {
					var self = this,
						deferred = $.Deferred();
					/* we are using load so one can specify a target with: url.html #targetelement */
					var $container = $('<div></div>').load(url, function(response, status){
						if ( status !== "error" ) {
							deferred.resolve($container.contents());
						}
						deferred.fail();
					});
					return deferred.promise();
				}
			},
			iframe: {
				process: function(url) {
					var deferred = new $.Deferred();
					var $content = $('<iframe/>')
						.hide()
						.attr('src', url)
						.css(structure(this, 'iframe'))
						.on('load', function() { deferred.resolve($content.show()); })
						// We can't move an <iframe> and avoid reloading it,
						// so let's put it in place ourselves right now:
						.appendTo(this.$instance.find('.' + this.namespace + '-content'));
					return deferred.promise();
				}
			},
			text: {
				process: function(text) { return $('<div>', {text: text}); }
			}
		},

		functionAttributes: ['beforeOpen', 'afterOpen', 'beforeContent', 'afterContent', 'beforeClose', 'afterClose'],

		/*** class methods ***/
		/* read element's attributes starting with data-featherlight- */
		readElementConfig: function(element, namespace) {
			var Klass = this,
				regexp = new RegExp('^data-' + namespace + '-(.*)'),
				config = {};
			if (element && element.attributes) {
				$.each(element.attributes, function(){
					var match = this.name.match(regexp);
					if (match) {
						var val = this.value,
							name = $.camelCase(match[1]);
						if ($.inArray(name, Klass.functionAttributes) >= 0) {  /* jshint -W054 */
							val = new Function(val);                           /* jshint +W054 */
						} else {
							try { val = $.parseJSON(val); }
							catch(e) {}
						}
						config[name] = val;
					}
				});
			}
			return config;
		},

		/* Used to create a Featherlight extension
		   [Warning: guru-level]
		   Creates the extension's prototype that in turn
		   inherits Featherlight's prototype.
		   Could be used to extend an extension too...
		   This is pretty high level wizardy, it comes pretty much straight
		   from CoffeeScript and won't teach you anything about Featherlight
		   as it's not really specific to this library.
		   My suggestion: move along and keep your sanity.
		*/
		extend: function(child, defaults) {
			/* Setup class hierarchy, adapted from CoffeeScript */
			var Ctor = function(){ this.constructor = child; };
			Ctor.prototype = this.prototype;
			child.prototype = new Ctor();
			child.__super__ = this.prototype;
			/* Copy class methods & attributes */
			$.extend(child, this, defaults);
			child.defaults = child.prototype;
			return child;
		},

		attach: function($source, $content, config) {
			var Klass = this;
			if (typeof $content === 'object' && $content instanceof $ === false && !config) {
				config = $content;
				$content = undefined;
			}
			/* make a copy */
			config = $.extend({}, config);

			/* Only for openTrigger and namespace... */
			var namespace = config.namespace || Klass.defaults.namespace,
				tempConfig = $.extend({}, Klass.defaults, Klass.readElementConfig($source[0], namespace), config),
				sharedPersist;

			$source.on(tempConfig.openTrigger+'.'+tempConfig.namespace, tempConfig.filter, function(event) {
				/* ... since we might as well compute the config on the actual target */
				var elemConfig = $.extend(
					{$source: $source, $currentTarget: $(this)},
					Klass.readElementConfig($source[0], tempConfig.namespace),
					Klass.readElementConfig(this, tempConfig.namespace),
					config);
				var fl = sharedPersist || $(this).data('featherlight-persisted') || new Klass($content, elemConfig);
				if(fl.persist === 'shared') {
					sharedPersist = fl;
				} else if(fl.persist !== false) {
					$(this).data('featherlight-persisted', fl);
				}
				elemConfig.$currentTarget.blur(); // Otherwise 'enter' key might trigger the dialog again
				fl.open(event);
			});
			return $source;
		},

		current: function() {
			var all = this.opened();
			return all[all.length - 1] || null;
		},

		opened: function() {
			var klass = this;
			pruneOpened();
			return $.grep(opened, function(fl) { return fl instanceof klass; } );
		},

		close: function(event) {
			var cur = this.current();
			if(cur) { return cur.close(event); }
		},

		/* Does the auto binding on startup.
		   Meant only to be used by Featherlight and its extensions
		*/
		_onReady: function() {
			var Klass = this;
			if(Klass.autoBind){
				/* Bind existing elements */
				$(Klass.autoBind).each(function(){
					Klass.attach($(this));
				});
				/* If a click propagates to the document level, then we have an item that was added later on */
				$(document).on('click', Klass.autoBind, function(evt) {
					if (evt.isDefaultPrevented() || evt.namespace === 'featherlight') {
						return;
					}
					evt.preventDefault();
					/* Bind featherlight */
					Klass.attach($(evt.currentTarget));
					/* Click again; this time our binding will catch it */
					$(evt.target).trigger('click.featherlight');
				});
			}
		},

		/* Featherlight uses the onKeyUp callback to intercept the escape key.
		   Private to Featherlight.
		*/
		_callbackChain: {
			onKeyUp: function(_super, event){
				if(27 === event.keyCode) {
					if (this.closeOnEsc) {
						$.featherlight.close(event);
					}
					return false;
				} else {
					return _super(event);
				}
			},

			onResize: function(_super, event){
				if (this.$content.naturalWidth) {
					var w = this.$content.naturalWidth, h = this.$content.naturalHeight;
					/* Reset apparent image size first so container grows */
					this.$content.css('width', '').css('height', '');
					/* Calculate the worst ratio so that dimensions fit */
					var ratio = Math.max(
						w  / parseInt(this.$content.parent().css('width'),10),
						h / parseInt(this.$content.parent().css('height'),10));
					/* Resize content */
					if (ratio > 1) {
						this.$content.css('width', '' + w / ratio + 'px').css('height', '' + h / ratio + 'px');
					}
				}
				return _super(event);
			},

			afterContent: function(_super, event){
				var r = _super(event);
				this.onResize(event);
				return r;
			}
		}
	});

	$.featherlight = Featherlight;

	/* bind jQuery elements to trigger featherlight */
	$.fn.featherlight = function($content, config) {
		return Featherlight.attach(this, $content, config);
	};

	/* bind featherlight on ready if config autoBind is set */
	$(document).ready(function(){ Featherlight._onReady(); });
}(jQuery));

/**
 * Featherlight Gallery – an extension for the ultra slim jQuery lightbox
 * Version 1.3.5 - http://noelboss.github.io/featherlight/
 *
 * Copyright 2015, Noël Raoul Bossart (http://www.noelboss.com)
 * MIT Licensed.
**/
(function($) {
	"use strict";

	var warn = function(m) {
		if(window.console && window.console.warn) {
			window.console.warn('FeatherlightGallery: ' + m);
		}
	};

	if('undefined' === typeof $) {
		return warn('Too much lightness, Featherlight needs jQuery.');
	} else if(!$.featherlight) {
		return warn('Load the featherlight plugin before the gallery plugin');
	}

	var isTouchAware = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
		jQueryConstructor = $.event && $.event.special.swipeleft && $,
		hammerConstructor = window.Hammer && function($el){
			var mc = new window.Hammer.Manager($el[0]);
			mc.add(new window.Hammer.Swipe());
			return mc;
		},
		swipeAwareConstructor = isTouchAware && (jQueryConstructor || hammerConstructor);
	if(isTouchAware && !swipeAwareConstructor) {
		warn('No compatible swipe library detected; one must be included before featherlightGallery for swipe motions to navigate the galleries.');
	}

	var callbackChain = {
			afterClose: function(_super, event) {
					var self = this;
					self.$instance.off('next.'+self.namespace+' previous.'+self.namespace);
					if (self._swiper) {
						self._swiper
							.off('swipeleft', self._swipeleft) /* See http://stackoverflow.com/questions/17367198/hammer-js-cant-remove-event-listener */
							.off('swiperight', self._swiperight);
						self._swiper = null;
					}
					return _super(event);
			},
			beforeOpen: function(_super, event){
					var self = this;

					self.$instance.on('next.'+self.namespace+' previous.'+self.namespace, function(event){
						var offset = event.type === 'next' ? +1 : -1;
						self.navigateTo(self.currentNavigation() + offset);
					});

					if (swipeAwareConstructor) {
						self._swiper = swipeAwareConstructor(self.$instance)
							.on('swipeleft', self._swipeleft = function()  { self.$instance.trigger('next'); })
							.on('swiperight', self._swiperight = function() { self.$instance.trigger('previous'); });
					} else {
						self.$instance.find('.'+self.namespace+'-content')
							.append(self.createNavigation('previous'))
							.append(self.createNavigation('next'));
					}
					return _super(event);
			},
			onKeyUp: function(_super, event){
				var dir = {
					37: 'previous', /* Left arrow */
					39: 'next'			/* Rigth arrow */
				}[event.keyCode];
				if(dir) {
					this.$instance.trigger(dir);
					return false;
				} else {
					return _super(event);
				}
			}
		};

	function FeatherlightGallery($source, config) {
		if(this instanceof FeatherlightGallery) {  /* called with new */
			$.featherlight.apply(this, arguments);
			this.chainCallbacks(callbackChain);
		} else {
			var flg = new FeatherlightGallery($.extend({$source: $source, $currentTarget: $source.first()}, config));
			flg.open();
			return flg;
		}
	}

	$.featherlight.extend(FeatherlightGallery, {
		autoBind: '[data-featherlight-gallery]'
	});

	$.extend(FeatherlightGallery.prototype, {
		/** Additional settings for Gallery **/
		previousIcon: '&#9664;',     /* Code that is used as previous icon */
		nextIcon: '&#9654;',         /* Code that is used as next icon */
		galleryFadeIn: 100,          /* fadeIn speed when image is loaded */
		galleryFadeOut: 300,         /* fadeOut speed before image is loaded */

		slides: function() {
			if (this.filter) {
				return this.$source.find(this.filter);
			}
			return this.$source;
		},

		images: function() {
			warn('images is deprecated, please use slides instead');
			return this.slides();
		},

		currentNavigation: function() {
			return this.slides().index(this.$currentTarget);
		},

		navigateTo: function(index) {
			var self = this,
				source = self.slides(),
				len = source.length,
				$inner = self.$instance.find('.' + self.namespace + '-inner');
			index = ((index % len) + len) % len; /* pin index to [0, len[ */

			self.$currentTarget = source.eq(index);
			self.beforeContent();
			return $.when(
				self.getContent(),
				$inner.fadeTo(self.galleryFadeOut,0.2)
			).always(function($newContent) {
					self.setContent($newContent);
					self.afterContent();
					$newContent.fadeTo(self.galleryFadeIn,1);
			});
		},

		createNavigation: function(target) {
			var self = this;
			return $('<span title="'+target+'" class="'+this.namespace+'-'+target+'"><span>'+this[target+'Icon']+'</span></span>').click(function(){
				$(this).trigger(target+'.'+self.namespace);
			});
		}
	});

	$.featherlightGallery = FeatherlightGallery;

	/* extend jQuery with selector featherlight method $(elm).featherlight(config, elm); */
	$.fn.featherlightGallery = function(config) {
		return FeatherlightGallery.attach(this, config);
	};

	/* bind featherlight on ready if config autoBind is set */
	$(document).ready(function(){ FeatherlightGallery._onReady(); });

}(jQuery));

/*! jQuery UI - v1.11.1+CommonJS - 2014-09-17
* http://jqueryui.com
* Includes: widget.js
* Copyright 2014 jQuery Foundation and other contributors; Licensed MIT */

(function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define([ "jquery" ], factory );

	} else if (typeof exports === "object") {
		// Node/CommonJS:
		factory(require("jquery"));

	} else {

		// Browser globals
		factory( jQuery );
	}
}(function( $ ) {
/*!
 * jQuery UI Widget 1.11.1
 * http://jqueryui.com
 *
 * Copyright 2014 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/jQuery.widget/
 */


var widget_uuid = 0,
	widget_slice = Array.prototype.slice;

$.cleanData = (function( orig ) {
	return function( elems ) {
		var events, elem, i;
		for ( i = 0; (elem = elems[i]) != null; i++ ) {
			try {

				// Only trigger remove when necessary to save time
				events = $._data( elem, "events" );
				if ( events && events.remove ) {
					$( elem ).triggerHandler( "remove" );
				}

			// http://bugs.jquery.com/ticket/8235
			} catch( e ) {}
		}
		orig( elems );
	};
})( $.cleanData );

$.widget = function( name, base, prototype ) {
	var fullName, existingConstructor, constructor, basePrototype,
		// proxiedPrototype allows the provided prototype to remain unmodified
		// so that it can be used as a mixin for multiple widgets (#8876)
		proxiedPrototype = {},
		namespace = name.split( "." )[ 0 ];

	name = name.split( "." )[ 1 ];
	fullName = namespace + "-" + name;

	if ( !prototype ) {
		prototype = base;
		base = $.Widget;
	}

	// create selector for plugin
	$.expr[ ":" ][ fullName.toLowerCase() ] = function( elem ) {
		return !!$.data( elem, fullName );
	};

	$[ namespace ] = $[ namespace ] || {};
	existingConstructor = $[ namespace ][ name ];
	constructor = $[ namespace ][ name ] = function( options, element ) {
		// allow instantiation without "new" keyword
		if ( !this._createWidget ) {
			return new constructor( options, element );
		}

		// allow instantiation without initializing for simple inheritance
		// must use "new" keyword (the code above always passes args)
		if ( arguments.length ) {
			this._createWidget( options, element );
		}
	};
	// extend with the existing constructor to carry over any static properties
	$.extend( constructor, existingConstructor, {
		version: prototype.version,
		// copy the object used to create the prototype in case we need to
		// redefine the widget later
		_proto: $.extend( {}, prototype ),
		// track widgets that inherit from this widget in case this widget is
		// redefined after a widget inherits from it
		_childConstructors: []
	});

	basePrototype = new base();
	// we need to make the options hash a property directly on the new instance
	// otherwise we'll modify the options hash on the prototype that we're
	// inheriting from
	basePrototype.options = $.widget.extend( {}, basePrototype.options );
	$.each( prototype, function( prop, value ) {
		if ( !$.isFunction( value ) ) {
			proxiedPrototype[ prop ] = value;
			return;
		}
		proxiedPrototype[ prop ] = (function() {
			var _super = function() {
					return base.prototype[ prop ].apply( this, arguments );
				},
				_superApply = function( args ) {
					return base.prototype[ prop ].apply( this, args );
				};
			return function() {
				var __super = this._super,
					__superApply = this._superApply,
					returnValue;

				this._super = _super;
				this._superApply = _superApply;

				returnValue = value.apply( this, arguments );

				this._super = __super;
				this._superApply = __superApply;

				return returnValue;
			};
		})();
	});
	constructor.prototype = $.widget.extend( basePrototype, {
		// TODO: remove support for widgetEventPrefix
		// always use the name + a colon as the prefix, e.g., draggable:start
		// don't prefix for widgets that aren't DOM-based
		widgetEventPrefix: existingConstructor ? (basePrototype.widgetEventPrefix || name) : name
	}, proxiedPrototype, {
		constructor: constructor,
		namespace: namespace,
		widgetName: name,
		widgetFullName: fullName
	});

	// If this widget is being redefined then we need to find all widgets that
	// are inheriting from it and redefine all of them so that they inherit from
	// the new version of this widget. We're essentially trying to replace one
	// level in the prototype chain.
	if ( existingConstructor ) {
		$.each( existingConstructor._childConstructors, function( i, child ) {
			var childPrototype = child.prototype;

			// redefine the child widget using the same prototype that was
			// originally used, but inherit from the new version of the base
			$.widget( childPrototype.namespace + "." + childPrototype.widgetName, constructor, child._proto );
		});
		// remove the list of existing child constructors from the old constructor
		// so the old child constructors can be garbage collected
		delete existingConstructor._childConstructors;
	} else {
		base._childConstructors.push( constructor );
	}

	$.widget.bridge( name, constructor );

	return constructor;
};

$.widget.extend = function( target ) {
	var input = widget_slice.call( arguments, 1 ),
		inputIndex = 0,
		inputLength = input.length,
		key,
		value;
	for ( ; inputIndex < inputLength; inputIndex++ ) {
		for ( key in input[ inputIndex ] ) {
			value = input[ inputIndex ][ key ];
			if ( input[ inputIndex ].hasOwnProperty( key ) && value !== undefined ) {
				// Clone objects
				if ( $.isPlainObject( value ) ) {
					target[ key ] = $.isPlainObject( target[ key ] ) ?
						$.widget.extend( {}, target[ key ], value ) :
						// Don't extend strings, arrays, etc. with objects
						$.widget.extend( {}, value );
				// Copy everything else by reference
				} else {
					target[ key ] = value;
				}
			}
		}
	}
	return target;
};

$.widget.bridge = function( name, object ) {
	var fullName = object.prototype.widgetFullName || name;
	$.fn[ name ] = function( options ) {
		var isMethodCall = typeof options === "string",
			args = widget_slice.call( arguments, 1 ),
			returnValue = this;

		// allow multiple hashes to be passed on init
		options = !isMethodCall && args.length ?
			$.widget.extend.apply( null, [ options ].concat(args) ) :
			options;

		if ( isMethodCall ) {
			this.each(function() {
				var methodValue,
					instance = $.data( this, fullName );
				if ( options === "instance" ) {
					returnValue = instance;
					return false;
				}
				if ( !instance ) {
					return $.error( "cannot call methods on " + name + " prior to initialization; " +
						"attempted to call method '" + options + "'" );
				}
				if ( !$.isFunction( instance[options] ) || options.charAt( 0 ) === "_" ) {
					return $.error( "no such method '" + options + "' for " + name + " widget instance" );
				}
				methodValue = instance[ options ].apply( instance, args );
				if ( methodValue !== instance && methodValue !== undefined ) {
					returnValue = methodValue && methodValue.jquery ?
						returnValue.pushStack( methodValue.get() ) :
						methodValue;
					return false;
				}
			});
		} else {
			this.each(function() {
				var instance = $.data( this, fullName );
				if ( instance ) {
					instance.option( options || {} );
					if ( instance._init ) {
						instance._init();
					}
				} else {
					$.data( this, fullName, new object( options, this ) );
				}
			});
		}

		return returnValue;
	};
};

$.Widget = function( /* options, element */ ) {};
$.Widget._childConstructors = [];

$.Widget.prototype = {
	widgetName: "widget",
	widgetEventPrefix: "",
	defaultElement: "<div>",
	options: {
		disabled: false,

		// callbacks
		create: null
	},
	_createWidget: function( options, element ) {
		element = $( element || this.defaultElement || this )[ 0 ];
		this.element = $( element );
		this.uuid = widget_uuid++;
		this.eventNamespace = "." + this.widgetName + this.uuid;
		this.options = $.widget.extend( {},
			this.options,
			this._getCreateOptions(),
			options );

		this.bindings = $();
		this.hoverable = $();
		this.focusable = $();

		if ( element !== this ) {
			$.data( element, this.widgetFullName, this );
			this._on( true, this.element, {
				remove: function( event ) {
					if ( event.target === element ) {
						this.destroy();
					}
				}
			});
			this.document = $( element.style ?
				// element within the document
				element.ownerDocument :
				// element is window or document
				element.document || element );
			this.window = $( this.document[0].defaultView || this.document[0].parentWindow );
		}

		this._create();
		this._trigger( "create", null, this._getCreateEventData() );
		this._init();
	},
	_getCreateOptions: $.noop,
	_getCreateEventData: $.noop,
	_create: $.noop,
	_init: $.noop,

	destroy: function() {
		this._destroy();
		// we can probably remove the unbind calls in 2.0
		// all event bindings should go through this._on()
		this.element
			.unbind( this.eventNamespace )
			.removeData( this.widgetFullName )
			// support: jquery <1.6.3
			// http://bugs.jquery.com/ticket/9413
			.removeData( $.camelCase( this.widgetFullName ) );
		this.widget()
			.unbind( this.eventNamespace )
			.removeAttr( "aria-disabled" )
			.removeClass(
				this.widgetFullName + "-disabled " +
				"ui-state-disabled" );

		// clean up events and states
		this.bindings.unbind( this.eventNamespace );
		this.hoverable.removeClass( "ui-state-hover" );
		this.focusable.removeClass( "ui-state-focus" );
	},
	_destroy: $.noop,

	widget: function() {
		return this.element;
	},

	option: function( key, value ) {
		var options = key,
			parts,
			curOption,
			i;

		if ( arguments.length === 0 ) {
			// don't return a reference to the internal hash
			return $.widget.extend( {}, this.options );
		}

		if ( typeof key === "string" ) {
			// handle nested keys, e.g., "foo.bar" => { foo: { bar: ___ } }
			options = {};
			parts = key.split( "." );
			key = parts.shift();
			if ( parts.length ) {
				curOption = options[ key ] = $.widget.extend( {}, this.options[ key ] );
				for ( i = 0; i < parts.length - 1; i++ ) {
					curOption[ parts[ i ] ] = curOption[ parts[ i ] ] || {};
					curOption = curOption[ parts[ i ] ];
				}
				key = parts.pop();
				if ( arguments.length === 1 ) {
					return curOption[ key ] === undefined ? null : curOption[ key ];
				}
				curOption[ key ] = value;
			} else {
				if ( arguments.length === 1 ) {
					return this.options[ key ] === undefined ? null : this.options[ key ];
				}
				options[ key ] = value;
			}
		}

		this._setOptions( options );

		return this;
	},
	_setOptions: function( options ) {
		var key;

		for ( key in options ) {
			this._setOption( key, options[ key ] );
		}

		return this;
	},
	_setOption: function( key, value ) {
		this.options[ key ] = value;

		if ( key === "disabled" ) {
			this.widget()
				.toggleClass( this.widgetFullName + "-disabled", !!value );

			// If the widget is becoming disabled, then nothing is interactive
			if ( value ) {
				this.hoverable.removeClass( "ui-state-hover" );
				this.focusable.removeClass( "ui-state-focus" );
			}
		}

		return this;
	},

	enable: function() {
		return this._setOptions({ disabled: false });
	},
	disable: function() {
		return this._setOptions({ disabled: true });
	},

	_on: function( suppressDisabledCheck, element, handlers ) {
		var delegateElement,
			instance = this;

		// no suppressDisabledCheck flag, shuffle arguments
		if ( typeof suppressDisabledCheck !== "boolean" ) {
			handlers = element;
			element = suppressDisabledCheck;
			suppressDisabledCheck = false;
		}

		// no element argument, shuffle and use this.element
		if ( !handlers ) {
			handlers = element;
			element = this.element;
			delegateElement = this.widget();
		} else {
			element = delegateElement = $( element );
			this.bindings = this.bindings.add( element );
		}

		$.each( handlers, function( event, handler ) {
			function handlerProxy() {
				// allow widgets to customize the disabled handling
				// - disabled as an array instead of boolean
				// - disabled class as method for disabling individual parts
				if ( !suppressDisabledCheck &&
						( instance.options.disabled === true ||
							$( this ).hasClass( "ui-state-disabled" ) ) ) {
					return;
				}
				return ( typeof handler === "string" ? instance[ handler ] : handler )
					.apply( instance, arguments );
			}

			// copy the guid so direct unbinding works
			if ( typeof handler !== "string" ) {
				handlerProxy.guid = handler.guid =
					handler.guid || handlerProxy.guid || $.guid++;
			}

			var match = event.match( /^([\w:-]*)\s*(.*)$/ ),
				eventName = match[1] + instance.eventNamespace,
				selector = match[2];
			if ( selector ) {
				delegateElement.delegate( selector, eventName, handlerProxy );
			} else {
				element.bind( eventName, handlerProxy );
			}
		});
	},

	_off: function( element, eventName ) {
		eventName = (eventName || "").split( " " ).join( this.eventNamespace + " " ) + this.eventNamespace;
		element.unbind( eventName ).undelegate( eventName );
	},

	_delay: function( handler, delay ) {
		function handlerProxy() {
			return ( typeof handler === "string" ? instance[ handler ] : handler )
				.apply( instance, arguments );
		}
		var instance = this;
		return setTimeout( handlerProxy, delay || 0 );
	},

	_hoverable: function( element ) {
		this.hoverable = this.hoverable.add( element );
		this._on( element, {
			mouseenter: function( event ) {
				$( event.currentTarget ).addClass( "ui-state-hover" );
			},
			mouseleave: function( event ) {
				$( event.currentTarget ).removeClass( "ui-state-hover" );
			}
		});
	},

	_focusable: function( element ) {
		this.focusable = this.focusable.add( element );
		this._on( element, {
			focusin: function( event ) {
				$( event.currentTarget ).addClass( "ui-state-focus" );
			},
			focusout: function( event ) {
				$( event.currentTarget ).removeClass( "ui-state-focus" );
			}
		});
	},

	_trigger: function( type, event, data ) {
		var prop, orig,
			callback = this.options[ type ];

		data = data || {};
		event = $.Event( event );
		event.type = ( type === this.widgetEventPrefix ?
			type :
			this.widgetEventPrefix + type ).toLowerCase();
		// the original event may come from any element
		// so we need to reset the target on the new event
		event.target = this.element[ 0 ];

		// copy original event properties over to the new event
		orig = event.originalEvent;
		if ( orig ) {
			for ( prop in orig ) {
				if ( !( prop in event ) ) {
					event[ prop ] = orig[ prop ];
				}
			}
		}

		this.element.trigger( event, data );
		return !( $.isFunction( callback ) &&
			callback.apply( this.element[0], [ event ].concat( data ) ) === false ||
			event.isDefaultPrevented() );
	}
};

$.each( { show: "fadeIn", hide: "fadeOut" }, function( method, defaultEffect ) {
	$.Widget.prototype[ "_" + method ] = function( element, options, callback ) {
		if ( typeof options === "string" ) {
			options = { effect: options };
		}
		var hasOptions,
			effectName = !options ?
				method :
				options === true || typeof options === "number" ?
					defaultEffect :
					options.effect || defaultEffect;
		options = options || {};
		if ( typeof options === "number" ) {
			options = { duration: options };
		}
		hasOptions = !$.isEmptyObject( options );
		options.complete = callback;
		if ( options.delay ) {
			element.delay( options.delay );
		}
		if ( hasOptions && $.effects && $.effects.effect[ effectName ] ) {
			element[ method ]( options );
		} else if ( effectName !== method && element[ effectName ] ) {
			element[ effectName ]( options.duration, options.easing, callback );
		} else {
			element.queue(function( next ) {
				$( this )[ method ]();
				if ( callback ) {
					callback.call( element[ 0 ] );
				}
				next();
			});
		}
	};
});

var widget = $.widget;



}));

/*
 * jQuery Iframe Transport Plugin 1.8.3
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2011, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global define, require, window, document */

(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // Register as an anonymous AMD module:
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS:
        factory(require('jquery'));
    } else {
        // Browser globals:
        factory(window.jQuery);
    }
}(function ($) {
    'use strict';

    // Helper variable to create unique names for the transport iframes:
    var counter = 0;

    // The iframe transport accepts four additional options:
    // options.fileInput: a jQuery collection of file input fields
    // options.paramName: the parameter name for the file form data,
    //  overrides the name property of the file input field(s),
    //  can be a string or an array of strings.
    // options.formData: an array of objects with name and value properties,
    //  equivalent to the return data of .serializeArray(), e.g.:
    //  [{name: 'a', value: 1}, {name: 'b', value: 2}]
    // options.initialIframeSrc: the URL of the initial iframe src,
    //  by default set to "javascript:false;"
    $.ajaxTransport('iframe', function (options) {
        if (options.async) {
            // javascript:false as initial iframe src
            // prevents warning popups on HTTPS in IE6:
            /*jshint scripturl: true */
            var initialIframeSrc = options.initialIframeSrc || 'javascript:false;',
            /*jshint scripturl: false */
                form,
                iframe,
                addParamChar;
            return {
                send: function (_, completeCallback) {
                    form = $('<form style="display:none;"></form>');
                    form.attr('accept-charset', options.formAcceptCharset);
                    addParamChar = /\?/.test(options.url) ? '&' : '?';
                    // XDomainRequest only supports GET and POST:
                    if (options.type === 'DELETE') {
                        options.url = options.url + addParamChar + '_method=DELETE';
                        options.type = 'POST';
                    } else if (options.type === 'PUT') {
                        options.url = options.url + addParamChar + '_method=PUT';
                        options.type = 'POST';
                    } else if (options.type === 'PATCH') {
                        options.url = options.url + addParamChar + '_method=PATCH';
                        options.type = 'POST';
                    }
                    // IE versions below IE8 cannot set the name property of
                    // elements that have already been added to the DOM,
                    // so we set the name along with the iframe HTML markup:
                    counter += 1;
                    iframe = $(
                        '<iframe src="' + initialIframeSrc +
                            '" name="iframe-transport-' + counter + '"></iframe>'
                    ).bind('load', function () {
                        var fileInputClones,
                            paramNames = $.isArray(options.paramName) ?
                                    options.paramName : [options.paramName];
                        iframe
                            .unbind('load')
                            .bind('load', function () {
                                var response;
                                // Wrap in a try/catch block to catch exceptions thrown
                                // when trying to access cross-domain iframe contents:
                                try {
                                    response = iframe.contents();
                                    // Google Chrome and Firefox do not throw an
                                    // exception when calling iframe.contents() on
                                    // cross-domain requests, so we unify the response:
                                    if (!response.length || !response[0].firstChild) {
                                        throw new Error();
                                    }
                                } catch (e) {
                                    response = undefined;
                                }
                                // The complete callback returns the
                                // iframe content document as response object:
                                completeCallback(
                                    200,
                                    'success',
                                    {'iframe': response}
                                );
                                // Fix for IE endless progress bar activity bug
                                // (happens on form submits to iframe targets):
                                $('<iframe src="' + initialIframeSrc + '"></iframe>')
                                    .appendTo(form);
                                window.setTimeout(function () {
                                    // Removing the form in a setTimeout call
                                    // allows Chrome's developer tools to display
                                    // the response result
                                    form.remove();
                                }, 0);
                            });
                        form
                            .prop('target', iframe.prop('name'))
                            .prop('action', options.url)
                            .prop('method', options.type);
                        if (options.formData) {
                            $.each(options.formData, function (index, field) {
                                $('<input type="hidden"/>')
                                    .prop('name', field.name)
                                    .val(field.value)
                                    .appendTo(form);
                            });
                        }
                        if (options.fileInput && options.fileInput.length &&
                                options.type === 'POST') {
                            fileInputClones = options.fileInput.clone();
                            // Insert a clone for each file input field:
                            options.fileInput.after(function (index) {
                                return fileInputClones[index];
                            });
                            if (options.paramName) {
                                options.fileInput.each(function (index) {
                                    $(this).prop(
                                        'name',
                                        paramNames[index] || options.paramName
                                    );
                                });
                            }
                            // Appending the file input fields to the hidden form
                            // removes them from their original location:
                            form
                                .append(options.fileInput)
                                .prop('enctype', 'multipart/form-data')
                                // enctype must be set as encoding for IE:
                                .prop('encoding', 'multipart/form-data');
                            // Remove the HTML5 form attribute from the input(s):
                            options.fileInput.removeAttr('form');
                        }
                        form.submit();
                        // Insert the file input fields at their original location
                        // by replacing the clones with the originals:
                        if (fileInputClones && fileInputClones.length) {
                            options.fileInput.each(function (index, input) {
                                var clone = $(fileInputClones[index]);
                                // Restore the original name and form properties:
                                $(input)
                                    .prop('name', clone.prop('name'))
                                    .attr('form', clone.attr('form'));
                                clone.replaceWith(input);
                            });
                        }
                    });
                    form.append(iframe).appendTo(document.body);
                },
                abort: function () {
                    if (iframe) {
                        // javascript:false as iframe src aborts the request
                        // and prevents warning popups on HTTPS in IE6.
                        // concat is used to avoid the "Script URL" JSLint error:
                        iframe
                            .unbind('load')
                            .prop('src', initialIframeSrc);
                    }
                    if (form) {
                        form.remove();
                    }
                }
            };
        }
    });

    // The iframe transport returns the iframe content document as response.
    // The following adds converters from iframe to text, json, html, xml
    // and script.
    // Please note that the Content-Type for JSON responses has to be text/plain
    // or text/html, if the browser doesn't include application/json in the
    // Accept header, else IE will show a download dialog.
    // The Content-Type for XML responses on the other hand has to be always
    // application/xml or text/xml, so IE properly parses the XML response.
    // See also
    // https://github.com/blueimp/jQuery-File-Upload/wiki/Setup#content-type-negotiation
    $.ajaxSetup({
        converters: {
            'iframe text': function (iframe) {
                return iframe && $(iframe[0].body).text();
            },
            'iframe json': function (iframe) {
                return iframe && $.parseJSON($(iframe[0].body).text());
            },
            'iframe html': function (iframe) {
                return iframe && $(iframe[0].body).html();
            },
            'iframe xml': function (iframe) {
                var xmlDoc = iframe && iframe[0];
                return xmlDoc && $.isXMLDoc(xmlDoc) ? xmlDoc :
                        $.parseXML((xmlDoc.XMLDocument && xmlDoc.XMLDocument.xml) ||
                            $(xmlDoc.body).html());
            },
            'iframe script': function (iframe) {
                return iframe && $.globalEval($(iframe[0].body).text());
            }
        }
    });

}));

/*
 * jQuery File Upload Plugin 5.42.3
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* jshint nomen:false */
/* global define, require, window, document, location, Blob, FormData */

(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // Register as an anonymous AMD module:
        define([
            'jquery',
            'jquery.ui.widget'
        ], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS:
        factory(
            require('jquery'),
            require('./vendor/jquery.ui.widget')
        );
    } else {
        // Browser globals:
        factory(window.jQuery);
    }
}(function ($) {
    'use strict';

    // Detect file input support, based on
    // http://viljamis.com/blog/2012/file-upload-support-on-mobile/
    $.support.fileInput = !(new RegExp(
        // Handle devices which give false positives for the feature detection:
        '(Android (1\\.[0156]|2\\.[01]))' +
            '|(Windows Phone (OS 7|8\\.0))|(XBLWP)|(ZuneWP)|(WPDesktop)' +
            '|(w(eb)?OSBrowser)|(webOS)' +
            '|(Kindle/(1\\.0|2\\.[05]|3\\.0))'
    ).test(window.navigator.userAgent) ||
        // Feature detection for all other devices:
        $('<input type="file">').prop('disabled'));

    // The FileReader API is not actually used, but works as feature detection,
    // as some Safari versions (5?) support XHR file uploads via the FormData API,
    // but not non-multipart XHR file uploads.
    // window.XMLHttpRequestUpload is not available on IE10, so we check for
    // window.ProgressEvent instead to detect XHR2 file upload capability:
    $.support.xhrFileUpload = !!(window.ProgressEvent && window.FileReader);
    $.support.xhrFormDataFileUpload = !!window.FormData;

    // Detect support for Blob slicing (required for chunked uploads):
    $.support.blobSlice = window.Blob && (Blob.prototype.slice ||
        Blob.prototype.webkitSlice || Blob.prototype.mozSlice);

    // Helper function to create drag handlers for dragover/dragenter/dragleave:
    function getDragHandler(type) {
        var isDragOver = type === 'dragover';
        return function (e) {
            e.dataTransfer = e.originalEvent && e.originalEvent.dataTransfer;
            var dataTransfer = e.dataTransfer;
            if (dataTransfer && $.inArray('Files', dataTransfer.types) !== -1 &&
                    this._trigger(
                        type,
                        $.Event(type, {delegatedEvent: e})
                    ) !== false) {
                e.preventDefault();
                if (isDragOver) {
                    dataTransfer.dropEffect = 'copy';
                }
            }
        };
    }

    // The fileupload widget listens for change events on file input fields defined
    // via fileInput setting and paste or drop events of the given dropZone.
    // In addition to the default jQuery Widget methods, the fileupload widget
    // exposes the "add" and "send" methods, to add or directly send files using
    // the fileupload API.
    // By default, files added via file input selection, paste, drag & drop or
    // "add" method are uploaded immediately, but it is possible to override
    // the "add" callback option to queue file uploads.
    $.widget('blueimp.fileupload', {

        options: {
            // The drop target element(s), by the default the complete document.
            // Set to null to disable drag & drop support:
            dropZone: $(document),
            // The paste target element(s), by the default undefined.
            // Set to a DOM node or jQuery object to enable file pasting:
            pasteZone: undefined,
            // The file input field(s), that are listened to for change events.
            // If undefined, it is set to the file input fields inside
            // of the widget element on plugin initialization.
            // Set to null to disable the change listener.
            fileInput: undefined,
            // By default, the file input field is replaced with a clone after
            // each input field change event. This is required for iframe transport
            // queues and allows change events to be fired for the same file
            // selection, but can be disabled by setting the following option to false:
            replaceFileInput: true,
            // The parameter name for the file form data (the request argument name).
            // If undefined or empty, the name property of the file input field is
            // used, or "files[]" if the file input name property is also empty,
            // can be a string or an array of strings:
            paramName: undefined,
            // By default, each file of a selection is uploaded using an individual
            // request for XHR type uploads. Set to false to upload file
            // selections in one request each:
            singleFileUploads: true,
            // To limit the number of files uploaded with one XHR request,
            // set the following option to an integer greater than 0:
            limitMultiFileUploads: undefined,
            // The following option limits the number of files uploaded with one
            // XHR request to keep the request size under or equal to the defined
            // limit in bytes:
            limitMultiFileUploadSize: undefined,
            // Multipart file uploads add a number of bytes to each uploaded file,
            // therefore the following option adds an overhead for each file used
            // in the limitMultiFileUploadSize configuration:
            limitMultiFileUploadSizeOverhead: 512,
            // Set the following option to true to issue all file upload requests
            // in a sequential order:
            sequentialUploads: false,
            // To limit the number of concurrent uploads,
            // set the following option to an integer greater than 0:
            limitConcurrentUploads: undefined,
            // Set the following option to true to force iframe transport uploads:
            forceIframeTransport: false,
            // Set the following option to the location of a redirect url on the
            // origin server, for cross-domain iframe transport uploads:
            redirect: undefined,
            // The parameter name for the redirect url, sent as part of the form
            // data and set to 'redirect' if this option is empty:
            redirectParamName: undefined,
            // Set the following option to the location of a postMessage window,
            // to enable postMessage transport uploads:
            postMessage: undefined,
            // By default, XHR file uploads are sent as multipart/form-data.
            // The iframe transport is always using multipart/form-data.
            // Set to false to enable non-multipart XHR uploads:
            multipart: true,
            // To upload large files in smaller chunks, set the following option
            // to a preferred maximum chunk size. If set to 0, null or undefined,
            // or the browser does not support the required Blob API, files will
            // be uploaded as a whole.
            maxChunkSize: undefined,
            // When a non-multipart upload or a chunked multipart upload has been
            // aborted, this option can be used to resume the upload by setting
            // it to the size of the already uploaded bytes. This option is most
            // useful when modifying the options object inside of the "add" or
            // "send" callbacks, as the options are cloned for each file upload.
            uploadedBytes: undefined,
            // By default, failed (abort or error) file uploads are removed from the
            // global progress calculation. Set the following option to false to
            // prevent recalculating the global progress data:
            recalculateProgress: true,
            // Interval in milliseconds to calculate and trigger progress events:
            progressInterval: 100,
            // Interval in milliseconds to calculate progress bitrate:
            bitrateInterval: 500,
            // By default, uploads are started automatically when adding files:
            autoUpload: true,

            // Error and info messages:
            messages: {
                uploadedBytes: 'Uploaded bytes exceed file size'
            },

            // Translation function, gets the message key to be translated
            // and an object with context specific data as arguments:
            i18n: function (message, context) {
                message = this.messages[message] || message.toString();
                if (context) {
                    $.each(context, function (key, value) {
                        message = message.replace('{' + key + '}', value);
                    });
                }
                return message;
            },

            // Additional form data to be sent along with the file uploads can be set
            // using this option, which accepts an array of objects with name and
            // value properties, a function returning such an array, a FormData
            // object (for XHR file uploads), or a simple object.
            // The form of the first fileInput is given as parameter to the function:
            formData: function (form) {
                return form.serializeArray();
            },

            // The add callback is invoked as soon as files are added to the fileupload
            // widget (via file input selection, drag & drop, paste or add API call).
            // If the singleFileUploads option is enabled, this callback will be
            // called once for each file in the selection for XHR file uploads, else
            // once for each file selection.
            //
            // The upload starts when the submit method is invoked on the data parameter.
            // The data object contains a files property holding the added files
            // and allows you to override plugin options as well as define ajax settings.
            //
            // Listeners for this callback can also be bound the following way:
            // .bind('fileuploadadd', func);
            //
            // data.submit() returns a Promise object and allows to attach additional
            // handlers using jQuery's Deferred callbacks:
            // data.submit().done(func).fail(func).always(func);
            add: function (e, data) {
                if (e.isDefaultPrevented()) {
                    return false;
                }
                if (data.autoUpload || (data.autoUpload !== false &&
                        $(this).fileupload('option', 'autoUpload'))) {
                    data.process().done(function () {
                        data.submit();
                    });
                }
            },

            // Other callbacks:

            // Callback for the submit event of each file upload:
            // submit: function (e, data) {}, // .bind('fileuploadsubmit', func);

            // Callback for the start of each file upload request:
            // send: function (e, data) {}, // .bind('fileuploadsend', func);

            // Callback for successful uploads:
            // done: function (e, data) {}, // .bind('fileuploaddone', func);

            // Callback for failed (abort or error) uploads:
            // fail: function (e, data) {}, // .bind('fileuploadfail', func);

            // Callback for completed (success, abort or error) requests:
            // always: function (e, data) {}, // .bind('fileuploadalways', func);

            // Callback for upload progress events:
            // progress: function (e, data) {}, // .bind('fileuploadprogress', func);

            // Callback for global upload progress events:
            // progressall: function (e, data) {}, // .bind('fileuploadprogressall', func);

            // Callback for uploads start, equivalent to the global ajaxStart event:
            // start: function (e) {}, // .bind('fileuploadstart', func);

            // Callback for uploads stop, equivalent to the global ajaxStop event:
            // stop: function (e) {}, // .bind('fileuploadstop', func);

            // Callback for change events of the fileInput(s):
            // change: function (e, data) {}, // .bind('fileuploadchange', func);

            // Callback for paste events to the pasteZone(s):
            // paste: function (e, data) {}, // .bind('fileuploadpaste', func);

            // Callback for drop events of the dropZone(s):
            // drop: function (e, data) {}, // .bind('fileuploaddrop', func);

            // Callback for dragover events of the dropZone(s):
            // dragover: function (e) {}, // .bind('fileuploaddragover', func);

            // Callback for the start of each chunk upload request:
            // chunksend: function (e, data) {}, // .bind('fileuploadchunksend', func);

            // Callback for successful chunk uploads:
            // chunkdone: function (e, data) {}, // .bind('fileuploadchunkdone', func);

            // Callback for failed (abort or error) chunk uploads:
            // chunkfail: function (e, data) {}, // .bind('fileuploadchunkfail', func);

            // Callback for completed (success, abort or error) chunk upload requests:
            // chunkalways: function (e, data) {}, // .bind('fileuploadchunkalways', func);

            // The plugin options are used as settings object for the ajax calls.
            // The following are jQuery ajax settings required for the file uploads:
            processData: false,
            contentType: false,
            cache: false,
            timeout: 0
        },

        // A list of options that require reinitializing event listeners and/or
        // special initialization code:
        _specialOptions: [
            'fileInput',
            'dropZone',
            'pasteZone',
            'multipart',
            'forceIframeTransport'
        ],

        _blobSlice: $.support.blobSlice && function () {
            var slice = this.slice || this.webkitSlice || this.mozSlice;
            return slice.apply(this, arguments);
        },

        _BitrateTimer: function () {
            this.timestamp = ((Date.now) ? Date.now() : (new Date()).getTime());
            this.loaded = 0;
            this.bitrate = 0;
            this.getBitrate = function (now, loaded, interval) {
                var timeDiff = now - this.timestamp;
                if (!this.bitrate || !interval || timeDiff > interval) {
                    this.bitrate = (loaded - this.loaded) * (1000 / timeDiff) * 8;
                    this.loaded = loaded;
                    this.timestamp = now;
                }
                return this.bitrate;
            };
        },

        _isXHRUpload: function (options) {
            return !options.forceIframeTransport &&
                ((!options.multipart && $.support.xhrFileUpload) ||
                $.support.xhrFormDataFileUpload);
        },

        _getFormData: function (options) {
            var formData;
            if ($.type(options.formData) === 'function') {
                return options.formData(options.form);
            }
            if ($.isArray(options.formData)) {
                return options.formData;
            }
            if ($.type(options.formData) === 'object') {
                formData = [];
                $.each(options.formData, function (name, value) {
                    formData.push({name: name, value: value});
                });
                return formData;
            }
            return [];
        },

        _getTotal: function (files) {
            var total = 0;
            $.each(files, function (index, file) {
                total += file.size || 1;
            });
            return total;
        },

        _initProgressObject: function (obj) {
            var progress = {
                loaded: 0,
                total: 0,
                bitrate: 0
            };
            if (obj._progress) {
                $.extend(obj._progress, progress);
            } else {
                obj._progress = progress;
            }
        },

        _initResponseObject: function (obj) {
            var prop;
            if (obj._response) {
                for (prop in obj._response) {
                    if (obj._response.hasOwnProperty(prop)) {
                        delete obj._response[prop];
                    }
                }
            } else {
                obj._response = {};
            }
        },

        _onProgress: function (e, data) {
            if (e.lengthComputable) {
                var now = ((Date.now) ? Date.now() : (new Date()).getTime()),
                    loaded;
                if (data._time && data.progressInterval &&
                        (now - data._time < data.progressInterval) &&
                        e.loaded !== e.total) {
                    return;
                }
                data._time = now;
                loaded = Math.floor(
                    e.loaded / e.total * (data.chunkSize || data._progress.total)
                ) + (data.uploadedBytes || 0);
                // Add the difference from the previously loaded state
                // to the global loaded counter:
                this._progress.loaded += (loaded - data._progress.loaded);
                this._progress.bitrate = this._bitrateTimer.getBitrate(
                    now,
                    this._progress.loaded,
                    data.bitrateInterval
                );
                data._progress.loaded = data.loaded = loaded;
                data._progress.bitrate = data.bitrate = data._bitrateTimer.getBitrate(
                    now,
                    loaded,
                    data.bitrateInterval
                );
                // Trigger a custom progress event with a total data property set
                // to the file size(s) of the current upload and a loaded data
                // property calculated accordingly:
                this._trigger(
                    'progress',
                    $.Event('progress', {delegatedEvent: e}),
                    data
                );
                // Trigger a global progress event for all current file uploads,
                // including ajax calls queued for sequential file uploads:
                this._trigger(
                    'progressall',
                    $.Event('progressall', {delegatedEvent: e}),
                    this._progress
                );
            }
        },

        _initProgressListener: function (options) {
            var that = this,
                xhr = options.xhr ? options.xhr() : $.ajaxSettings.xhr();
            // Accesss to the native XHR object is required to add event listeners
            // for the upload progress event:
            if (xhr.upload) {
                $(xhr.upload).bind('progress', function (e) {
                    var oe = e.originalEvent;
                    // Make sure the progress event properties get copied over:
                    e.lengthComputable = oe.lengthComputable;
                    e.loaded = oe.loaded;
                    e.total = oe.total;
                    that._onProgress(e, options);
                });
                options.xhr = function () {
                    return xhr;
                };
            }
        },

        _isInstanceOf: function (type, obj) {
            // Cross-frame instanceof check
            return Object.prototype.toString.call(obj) === '[object ' + type + ']';
        },

        _initXHRData: function (options) {
            var that = this,
                formData,
                file = options.files[0],
                // Ignore non-multipart setting if not supported:
                multipart = options.multipart || !$.support.xhrFileUpload,
                paramName = $.type(options.paramName) === 'array' ?
                    options.paramName[0] : options.paramName;
            options.headers = $.extend({}, options.headers);
            if (options.contentRange) {
                options.headers['Content-Range'] = options.contentRange;
            }
            if (!multipart || options.blob || !this._isInstanceOf('File', file)) {
                options.headers['Content-Disposition'] = 'attachment; filename="' +
                    encodeURI(file.name) + '"';
            }
            if (!multipart) {
                options.contentType = file.type || 'application/octet-stream';
                options.data = options.blob || file;
            } else if ($.support.xhrFormDataFileUpload) {
                if (options.postMessage) {
                    // window.postMessage does not allow sending FormData
                    // objects, so we just add the File/Blob objects to
                    // the formData array and let the postMessage window
                    // create the FormData object out of this array:
                    formData = this._getFormData(options);
                    if (options.blob) {
                        formData.push({
                            name: paramName,
                            value: options.blob
                        });
                    } else {
                        $.each(options.files, function (index, file) {
                            formData.push({
                                name: ($.type(options.paramName) === 'array' &&
                                    options.paramName[index]) || paramName,
                                value: file
                            });
                        });
                    }
                } else {
                    if (that._isInstanceOf('FormData', options.formData)) {
                        formData = options.formData;
                    } else {
                        formData = new FormData();
                        $.each(this._getFormData(options), function (index, field) {
                            formData.append(field.name, field.value);
                        });
                    }
                    if (options.blob) {
                        formData.append(paramName, options.blob, file.name);
                    } else {
                        $.each(options.files, function (index, file) {
                            // This check allows the tests to run with
                            // dummy objects:
                            if (that._isInstanceOf('File', file) ||
                                    that._isInstanceOf('Blob', file)) {
                                formData.append(
                                    ($.type(options.paramName) === 'array' &&
                                        options.paramName[index]) || paramName,
                                    file,
                                    file.uploadName || file.name
                                );
                            }
                        });
                    }
                }
                options.data = formData;
            }
            // Blob reference is not needed anymore, free memory:
            options.blob = null;
        },

        _initIframeSettings: function (options) {
            var targetHost = $('<a></a>').prop('href', options.url).prop('host');
            // Setting the dataType to iframe enables the iframe transport:
            options.dataType = 'iframe ' + (options.dataType || '');
            // The iframe transport accepts a serialized array as form data:
            options.formData = this._getFormData(options);
            // Add redirect url to form data on cross-domain uploads:
            if (options.redirect && targetHost && targetHost !== location.host) {
                options.formData.push({
                    name: options.redirectParamName || 'redirect',
                    value: options.redirect
                });
            }
        },

        _initDataSettings: function (options) {
            if (this._isXHRUpload(options)) {
                if (!this._chunkedUpload(options, true)) {
                    if (!options.data) {
                        this._initXHRData(options);
                    }
                    this._initProgressListener(options);
                }
                if (options.postMessage) {
                    // Setting the dataType to postmessage enables the
                    // postMessage transport:
                    options.dataType = 'postmessage ' + (options.dataType || '');
                }
            } else {
                this._initIframeSettings(options);
            }
        },

        _getParamName: function (options) {
            var fileInput = $(options.fileInput),
                paramName = options.paramName;
            if (!paramName) {
                paramName = [];
                fileInput.each(function () {
                    var input = $(this),
                        name = input.prop('name') || 'files[]',
                        i = (input.prop('files') || [1]).length;
                    while (i) {
                        paramName.push(name);
                        i -= 1;
                    }
                });
                if (!paramName.length) {
                    paramName = [fileInput.prop('name') || 'files[]'];
                }
            } else if (!$.isArray(paramName)) {
                paramName = [paramName];
            }
            return paramName;
        },

        _initFormSettings: function (options) {
            // Retrieve missing options from the input field and the
            // associated form, if available:
            if (!options.form || !options.form.length) {
                options.form = $(options.fileInput.prop('form'));
                // If the given file input doesn't have an associated form,
                // use the default widget file input's form:
                if (!options.form.length) {
                    options.form = $(this.options.fileInput.prop('form'));
                }
            }
            options.paramName = this._getParamName(options);
            if (!options.url) {
                options.url = options.form.prop('action') || location.href;
            }
            // The HTTP request method must be "POST" or "PUT":
            options.type = (options.type ||
                ($.type(options.form.prop('method')) === 'string' &&
                    options.form.prop('method')) || ''
                ).toUpperCase();
            if (options.type !== 'POST' && options.type !== 'PUT' &&
                    options.type !== 'PATCH') {
                options.type = 'POST';
            }
            if (!options.formAcceptCharset) {
                options.formAcceptCharset = options.form.attr('accept-charset');
            }
        },

        _getAJAXSettings: function (data) {
            var options = $.extend({}, this.options, data);
            this._initFormSettings(options);
            this._initDataSettings(options);
            return options;
        },

        // jQuery 1.6 doesn't provide .state(),
        // while jQuery 1.8+ removed .isRejected() and .isResolved():
        _getDeferredState: function (deferred) {
            if (deferred.state) {
                return deferred.state();
            }
            if (deferred.isResolved()) {
                return 'resolved';
            }
            if (deferred.isRejected()) {
                return 'rejected';
            }
            return 'pending';
        },

        // Maps jqXHR callbacks to the equivalent
        // methods of the given Promise object:
        _enhancePromise: function (promise) {
            promise.success = promise.done;
            promise.error = promise.fail;
            promise.complete = promise.always;
            return promise;
        },

        // Creates and returns a Promise object enhanced with
        // the jqXHR methods abort, success, error and complete:
        _getXHRPromise: function (resolveOrReject, context, args) {
            var dfd = $.Deferred(),
                promise = dfd.promise();
            context = context || this.options.context || promise;
            if (resolveOrReject === true) {
                dfd.resolveWith(context, args);
            } else if (resolveOrReject === false) {
                dfd.rejectWith(context, args);
            }
            promise.abort = dfd.promise;
            return this._enhancePromise(promise);
        },

        // Adds convenience methods to the data callback argument:
        _addConvenienceMethods: function (e, data) {
            var that = this,
                getPromise = function (args) {
                    return $.Deferred().resolveWith(that, args).promise();
                };
            data.process = function (resolveFunc, rejectFunc) {
                if (resolveFunc || rejectFunc) {
                    data._processQueue = this._processQueue =
                        (this._processQueue || getPromise([this])).pipe(
                            function () {
                                if (data.errorThrown) {
                                    return $.Deferred()
                                        .rejectWith(that, [data]).promise();
                                }
                                return getPromise(arguments);
                            }
                        ).pipe(resolveFunc, rejectFunc);
                }
                return this._processQueue || getPromise([this]);
            };
            data.submit = function () {
                if (this.state() !== 'pending') {
                    data.jqXHR = this.jqXHR =
                        (that._trigger(
                            'submit',
                            $.Event('submit', {delegatedEvent: e}),
                            this
                        ) !== false) && that._onSend(e, this);
                }
                return this.jqXHR || that._getXHRPromise();
            };
            data.abort = function () {
                if (this.jqXHR) {
                    return this.jqXHR.abort();
                }
                this.errorThrown = 'abort';
                that._trigger('fail', null, this);
                return that._getXHRPromise(false);
            };
            data.state = function () {
                if (this.jqXHR) {
                    return that._getDeferredState(this.jqXHR);
                }
                if (this._processQueue) {
                    return that._getDeferredState(this._processQueue);
                }
            };
            data.processing = function () {
                return !this.jqXHR && this._processQueue && that
                    ._getDeferredState(this._processQueue) === 'pending';
            };
            data.progress = function () {
                return this._progress;
            };
            data.response = function () {
                return this._response;
            };
        },

        // Parses the Range header from the server response
        // and returns the uploaded bytes:
        _getUploadedBytes: function (jqXHR) {
            var range = jqXHR.getResponseHeader('Range'),
                parts = range && range.split('-'),
                upperBytesPos = parts && parts.length > 1 &&
                    parseInt(parts[1], 10);
            return upperBytesPos && upperBytesPos + 1;
        },

        // Uploads a file in multiple, sequential requests
        // by splitting the file up in multiple blob chunks.
        // If the second parameter is true, only tests if the file
        // should be uploaded in chunks, but does not invoke any
        // upload requests:
        _chunkedUpload: function (options, testOnly) {
            options.uploadedBytes = options.uploadedBytes || 0;
            var that = this,
                file = options.files[0],
                fs = file.size,
                ub = options.uploadedBytes,
                mcs = options.maxChunkSize || fs,
                slice = this._blobSlice,
                dfd = $.Deferred(),
                promise = dfd.promise(),
                jqXHR,
                upload;
            if (!(this._isXHRUpload(options) && slice && (ub || mcs < fs)) ||
                    options.data) {
                return false;
            }
            if (testOnly) {
                return true;
            }
            if (ub >= fs) {
                file.error = options.i18n('uploadedBytes');
                return this._getXHRPromise(
                    false,
                    options.context,
                    [null, 'error', file.error]
                );
            }
            // The chunk upload method:
            upload = function () {
                // Clone the options object for each chunk upload:
                var o = $.extend({}, options),
                    currentLoaded = o._progress.loaded;
                o.blob = slice.call(
                    file,
                    ub,
                    ub + mcs,
                    file.type
                );
                // Store the current chunk size, as the blob itself
                // will be dereferenced after data processing:
                o.chunkSize = o.blob.size;
                // Expose the chunk bytes position range:
                o.contentRange = 'bytes ' + ub + '-' +
                    (ub + o.chunkSize - 1) + '/' + fs;
                // Process the upload data (the blob and potential form data):
                that._initXHRData(o);
                // Add progress listeners for this chunk upload:
                that._initProgressListener(o);
                jqXHR = ((that._trigger('chunksend', null, o) !== false && $.ajax(o)) ||
                        that._getXHRPromise(false, o.context))
                    .done(function (result, textStatus, jqXHR) {
                        ub = that._getUploadedBytes(jqXHR) ||
                            (ub + o.chunkSize);
                        // Create a progress event if no final progress event
                        // with loaded equaling total has been triggered
                        // for this chunk:
                        if (currentLoaded + o.chunkSize - o._progress.loaded) {
                            that._onProgress($.Event('progress', {
                                lengthComputable: true,
                                loaded: ub - o.uploadedBytes,
                                total: ub - o.uploadedBytes
                            }), o);
                        }
                        options.uploadedBytes = o.uploadedBytes = ub;
                        o.result = result;
                        o.textStatus = textStatus;
                        o.jqXHR = jqXHR;
                        that._trigger('chunkdone', null, o);
                        that._trigger('chunkalways', null, o);
                        if (ub < fs) {
                            // File upload not yet complete,
                            // continue with the next chunk:
                            upload();
                        } else {
                            dfd.resolveWith(
                                o.context,
                                [result, textStatus, jqXHR]
                            );
                        }
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        o.jqXHR = jqXHR;
                        o.textStatus = textStatus;
                        o.errorThrown = errorThrown;
                        that._trigger('chunkfail', null, o);
                        that._trigger('chunkalways', null, o);
                        dfd.rejectWith(
                            o.context,
                            [jqXHR, textStatus, errorThrown]
                        );
                    });
            };
            this._enhancePromise(promise);
            promise.abort = function () {
                return jqXHR.abort();
            };
            upload();
            return promise;
        },

        _beforeSend: function (e, data) {
            if (this._active === 0) {
                // the start callback is triggered when an upload starts
                // and no other uploads are currently running,
                // equivalent to the global ajaxStart event:
                this._trigger('start');
                // Set timer for global bitrate progress calculation:
                this._bitrateTimer = new this._BitrateTimer();
                // Reset the global progress values:
                this._progress.loaded = this._progress.total = 0;
                this._progress.bitrate = 0;
            }
            // Make sure the container objects for the .response() and
            // .progress() methods on the data object are available
            // and reset to their initial state:
            this._initResponseObject(data);
            this._initProgressObject(data);
            data._progress.loaded = data.loaded = data.uploadedBytes || 0;
            data._progress.total = data.total = this._getTotal(data.files) || 1;
            data._progress.bitrate = data.bitrate = 0;
            this._active += 1;
            // Initialize the global progress values:
            this._progress.loaded += data.loaded;
            this._progress.total += data.total;
        },

        _onDone: function (result, textStatus, jqXHR, options) {
            var total = options._progress.total,
                response = options._response;
            if (options._progress.loaded < total) {
                // Create a progress event if no final progress event
                // with loaded equaling total has been triggered:
                this._onProgress($.Event('progress', {
                    lengthComputable: true,
                    loaded: total,
                    total: total
                }), options);
            }
            response.result = options.result = result;
            response.textStatus = options.textStatus = textStatus;
            response.jqXHR = options.jqXHR = jqXHR;
            this._trigger('done', null, options);
        },

        _onFail: function (jqXHR, textStatus, errorThrown, options) {
            var response = options._response;
            if (options.recalculateProgress) {
                // Remove the failed (error or abort) file upload from
                // the global progress calculation:
                this._progress.loaded -= options._progress.loaded;
                this._progress.total -= options._progress.total;
            }
            response.jqXHR = options.jqXHR = jqXHR;
            response.textStatus = options.textStatus = textStatus;
            response.errorThrown = options.errorThrown = errorThrown;
            this._trigger('fail', null, options);
        },

        _onAlways: function (jqXHRorResult, textStatus, jqXHRorError, options) {
            // jqXHRorResult, textStatus and jqXHRorError are added to the
            // options object via done and fail callbacks
            this._trigger('always', null, options);
        },

        _onSend: function (e, data) {
            if (!data.submit) {
                this._addConvenienceMethods(e, data);
            }
            var that = this,
                jqXHR,
                aborted,
                slot,
                pipe,
                options = that._getAJAXSettings(data),
                send = function () {
                    that._sending += 1;
                    // Set timer for bitrate progress calculation:
                    options._bitrateTimer = new that._BitrateTimer();
                    jqXHR = jqXHR || (
                        ((aborted || that._trigger(
                            'send',
                            $.Event('send', {delegatedEvent: e}),
                            options
                        ) === false) &&
                        that._getXHRPromise(false, options.context, aborted)) ||
                        that._chunkedUpload(options) || $.ajax(options)
                    ).done(function (result, textStatus, jqXHR) {
                        that._onDone(result, textStatus, jqXHR, options);
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        that._onFail(jqXHR, textStatus, errorThrown, options);
                    }).always(function (jqXHRorResult, textStatus, jqXHRorError) {
                        that._onAlways(
                            jqXHRorResult,
                            textStatus,
                            jqXHRorError,
                            options
                        );
                        that._sending -= 1;
                        that._active -= 1;
                        if (options.limitConcurrentUploads &&
                                options.limitConcurrentUploads > that._sending) {
                            // Start the next queued upload,
                            // that has not been aborted:
                            var nextSlot = that._slots.shift();
                            while (nextSlot) {
                                if (that._getDeferredState(nextSlot) === 'pending') {
                                    nextSlot.resolve();
                                    break;
                                }
                                nextSlot = that._slots.shift();
                            }
                        }
                        if (that._active === 0) {
                            // The stop callback is triggered when all uploads have
                            // been completed, equivalent to the global ajaxStop event:
                            that._trigger('stop');
                        }
                    });
                    return jqXHR;
                };
            this._beforeSend(e, options);
            if (this.options.sequentialUploads ||
                    (this.options.limitConcurrentUploads &&
                    this.options.limitConcurrentUploads <= this._sending)) {
                if (this.options.limitConcurrentUploads > 1) {
                    slot = $.Deferred();
                    this._slots.push(slot);
                    pipe = slot.pipe(send);
                } else {
                    this._sequence = this._sequence.pipe(send, send);
                    pipe = this._sequence;
                }
                // Return the piped Promise object, enhanced with an abort method,
                // which is delegated to the jqXHR object of the current upload,
                // and jqXHR callbacks mapped to the equivalent Promise methods:
                pipe.abort = function () {
                    aborted = [undefined, 'abort', 'abort'];
                    if (!jqXHR) {
                        if (slot) {
                            slot.rejectWith(options.context, aborted);
                        }
                        return send();
                    }
                    return jqXHR.abort();
                };
                return this._enhancePromise(pipe);
            }
            return send();
        },

        _onAdd: function (e, data) {
            var that = this,
                result = true,
                options = $.extend({}, this.options, data),
                files = data.files,
                filesLength = files.length,
                limit = options.limitMultiFileUploads,
                limitSize = options.limitMultiFileUploadSize,
                overhead = options.limitMultiFileUploadSizeOverhead,
                batchSize = 0,
                paramName = this._getParamName(options),
                paramNameSet,
                paramNameSlice,
                fileSet,
                i,
                j = 0;
            if (!filesLength) {
                return false;
            }
            if (limitSize && files[0].size === undefined) {
                limitSize = undefined;
            }
            if (!(options.singleFileUploads || limit || limitSize) ||
                    !this._isXHRUpload(options)) {
                fileSet = [files];
                paramNameSet = [paramName];
            } else if (!(options.singleFileUploads || limitSize) && limit) {
                fileSet = [];
                paramNameSet = [];
                for (i = 0; i < filesLength; i += limit) {
                    fileSet.push(files.slice(i, i + limit));
                    paramNameSlice = paramName.slice(i, i + limit);
                    if (!paramNameSlice.length) {
                        paramNameSlice = paramName;
                    }
                    paramNameSet.push(paramNameSlice);
                }
            } else if (!options.singleFileUploads && limitSize) {
                fileSet = [];
                paramNameSet = [];
                for (i = 0; i < filesLength; i = i + 1) {
                    batchSize += files[i].size + overhead;
                    if (i + 1 === filesLength ||
                            ((batchSize + files[i + 1].size + overhead) > limitSize) ||
                            (limit && i + 1 - j >= limit)) {
                        fileSet.push(files.slice(j, i + 1));
                        paramNameSlice = paramName.slice(j, i + 1);
                        if (!paramNameSlice.length) {
                            paramNameSlice = paramName;
                        }
                        paramNameSet.push(paramNameSlice);
                        j = i + 1;
                        batchSize = 0;
                    }
                }
            } else {
                paramNameSet = paramName;
            }
            data.originalFiles = files;
            $.each(fileSet || files, function (index, element) {
                var newData = $.extend({}, data);
                newData.files = fileSet ? element : [element];
                newData.paramName = paramNameSet[index];
                that._initResponseObject(newData);
                that._initProgressObject(newData);
                that._addConvenienceMethods(e, newData);
                result = that._trigger(
                    'add',
                    $.Event('add', {delegatedEvent: e}),
                    newData
                );
                return result;
            });
            return result;
        },

        _replaceFileInput: function (data) {
            var input = data.fileInput,
                inputClone = input.clone(true);
            // Add a reference for the new cloned file input to the data argument:
            data.fileInputClone = inputClone;
            $('<form></form>').append(inputClone)[0].reset();
            // Detaching allows to insert the fileInput on another form
            // without loosing the file input value:
            input.after(inputClone).detach();
            // Avoid memory leaks with the detached file input:
            $.cleanData(input.unbind('remove'));
            // Replace the original file input element in the fileInput
            // elements set with the clone, which has been copied including
            // event handlers:
            this.options.fileInput = this.options.fileInput.map(function (i, el) {
                if (el === input[0]) {
                    return inputClone[0];
                }
                return el;
            });
            // If the widget has been initialized on the file input itself,
            // override this.element with the file input clone:
            if (input[0] === this.element[0]) {
                this.element = inputClone;
            }
        },

        _handleFileTreeEntry: function (entry, path) {
            var that = this,
                dfd = $.Deferred(),
                errorHandler = function (e) {
                    if (e && !e.entry) {
                        e.entry = entry;
                    }
                    // Since $.when returns immediately if one
                    // Deferred is rejected, we use resolve instead.
                    // This allows valid files and invalid items
                    // to be returned together in one set:
                    dfd.resolve([e]);
                },
                successHandler = function (entries) {
                    that._handleFileTreeEntries(
                        entries,
                        path + entry.name + '/'
                    ).done(function (files) {
                        dfd.resolve(files);
                    }).fail(errorHandler);
                },
                readEntries = function () {
                    dirReader.readEntries(function (results) {
                        if (!results.length) {
                            successHandler(entries);
                        } else {
                            entries = entries.concat(results);
                            readEntries();
                        }
                    }, errorHandler);
                },
                dirReader, entries = [];
            path = path || '';
            if (entry.isFile) {
                if (entry._file) {
                    // Workaround for Chrome bug #149735
                    entry._file.relativePath = path;
                    dfd.resolve(entry._file);
                } else {
                    entry.file(function (file) {
                        file.relativePath = path;
                        dfd.resolve(file);
                    }, errorHandler);
                }
            } else if (entry.isDirectory) {
                dirReader = entry.createReader();
                readEntries();
            } else {
                // Return an empy list for file system items
                // other than files or directories:
                dfd.resolve([]);
            }
            return dfd.promise();
        },

        _handleFileTreeEntries: function (entries, path) {
            var that = this;
            return $.when.apply(
                $,
                $.map(entries, function (entry) {
                    return that._handleFileTreeEntry(entry, path);
                })
            ).pipe(function () {
                return Array.prototype.concat.apply(
                    [],
                    arguments
                );
            });
        },

        _getDroppedFiles: function (dataTransfer) {
            dataTransfer = dataTransfer || {};
            var items = dataTransfer.items;
            if (items && items.length && (items[0].webkitGetAsEntry ||
                    items[0].getAsEntry)) {
                return this._handleFileTreeEntries(
                    $.map(items, function (item) {
                        var entry;
                        if (item.webkitGetAsEntry) {
                            entry = item.webkitGetAsEntry();
                            if (entry) {
                                // Workaround for Chrome bug #149735:
                                entry._file = item.getAsFile();
                            }
                            return entry;
                        }
                        return item.getAsEntry();
                    })
                );
            }
            return $.Deferred().resolve(
                $.makeArray(dataTransfer.files)
            ).promise();
        },

        _getSingleFileInputFiles: function (fileInput) {
            fileInput = $(fileInput);
            var entries = fileInput.prop('webkitEntries') ||
                    fileInput.prop('entries'),
                files,
                value;
            if (entries && entries.length) {
                return this._handleFileTreeEntries(entries);
            }
            files = $.makeArray(fileInput.prop('files'));
            if (!files.length) {
                value = fileInput.prop('value');
                if (!value) {
                    return $.Deferred().resolve([]).promise();
                }
                // If the files property is not available, the browser does not
                // support the File API and we add a pseudo File object with
                // the input value as name with path information removed:
                files = [{name: value.replace(/^.*\\/, '')}];
            } else if (files[0].name === undefined && files[0].fileName) {
                // File normalization for Safari 4 and Firefox 3:
                $.each(files, function (index, file) {
                    file.name = file.fileName;
                    file.size = file.fileSize;
                });
            }
            return $.Deferred().resolve(files).promise();
        },

        _getFileInputFiles: function (fileInput) {
            if (!(fileInput instanceof $) || fileInput.length === 1) {
                return this._getSingleFileInputFiles(fileInput);
            }
            return $.when.apply(
                $,
                $.map(fileInput, this._getSingleFileInputFiles)
            ).pipe(function () {
                return Array.prototype.concat.apply(
                    [],
                    arguments
                );
            });
        },

        _onChange: function (e) {
            var that = this,
                data = {
                    fileInput: $(e.target),
                    form: $(e.target.form)
                };
            this._getFileInputFiles(data.fileInput).always(function (files) {
                data.files = files;
                if (that.options.replaceFileInput) {
                    that._replaceFileInput(data);
                }
                if (that._trigger(
                        'change',
                        $.Event('change', {delegatedEvent: e}),
                        data
                    ) !== false) {
                    that._onAdd(e, data);
                }
            });
        },

        _onPaste: function (e) {
            var items = e.originalEvent && e.originalEvent.clipboardData &&
                    e.originalEvent.clipboardData.items,
                data = {files: []};
            if (items && items.length) {
                $.each(items, function (index, item) {
                    var file = item.getAsFile && item.getAsFile();
                    if (file) {
                        data.files.push(file);
                    }
                });
                if (this._trigger(
                        'paste',
                        $.Event('paste', {delegatedEvent: e}),
                        data
                    ) !== false) {
                    this._onAdd(e, data);
                }
            }
        },

        _onDrop: function (e) {
            e.dataTransfer = e.originalEvent && e.originalEvent.dataTransfer;
            var that = this,
                dataTransfer = e.dataTransfer,
                data = {};
            if (dataTransfer && dataTransfer.files && dataTransfer.files.length) {
                e.preventDefault();
                this._getDroppedFiles(dataTransfer).always(function (files) {
                    data.files = files;
                    if (that._trigger(
                            'drop',
                            $.Event('drop', {delegatedEvent: e}),
                            data
                        ) !== false) {
                        that._onAdd(e, data);
                    }
                });
            }
        },

        _onDragOver: getDragHandler('dragover'),

        _onDragEnter: getDragHandler('dragenter'),

        _onDragLeave: getDragHandler('dragleave'),

        _initEventHandlers: function () {
            if (this._isXHRUpload(this.options)) {
                this._on(this.options.dropZone, {
                    dragover: this._onDragOver,
                    drop: this._onDrop,
                    // event.preventDefault() on dragenter is required for IE10+:
                    dragenter: this._onDragEnter,
                    // dragleave is not required, but added for completeness:
                    dragleave: this._onDragLeave
                });
                this._on(this.options.pasteZone, {
                    paste: this._onPaste
                });
            }
            if ($.support.fileInput) {
                this._on(this.options.fileInput, {
                    change: this._onChange
                });
            }
        },

        _destroyEventHandlers: function () {
            this._off(this.options.dropZone, 'dragenter dragleave dragover drop');
            this._off(this.options.pasteZone, 'paste');
            this._off(this.options.fileInput, 'change');
        },

        _setOption: function (key, value) {
            var reinit = $.inArray(key, this._specialOptions) !== -1;
            if (reinit) {
                this._destroyEventHandlers();
            }
            this._super(key, value);
            if (reinit) {
                this._initSpecialOptions();
                this._initEventHandlers();
            }
        },

        _initSpecialOptions: function () {
            var options = this.options;
            if (options.fileInput === undefined) {
                options.fileInput = this.element.is('input[type="file"]') ?
                        this.element : this.element.find('input[type="file"]');
            } else if (!(options.fileInput instanceof $)) {
                options.fileInput = $(options.fileInput);
            }
            if (!(options.dropZone instanceof $)) {
                options.dropZone = $(options.dropZone);
            }
            if (!(options.pasteZone instanceof $)) {
                options.pasteZone = $(options.pasteZone);
            }
        },

        _getRegExp: function (str) {
            var parts = str.split('/'),
                modifiers = parts.pop();
            parts.shift();
            return new RegExp(parts.join('/'), modifiers);
        },

        _isRegExpOption: function (key, value) {
            return key !== 'url' && $.type(value) === 'string' &&
                /^\/.*\/[igm]{0,3}$/.test(value);
        },

        _initDataAttributes: function () {
            var that = this,
                options = this.options,
                data = this.element.data();
            // Initialize options set via HTML5 data-attributes:
            $.each(
                this.element[0].attributes,
                function (index, attr) {
                    var key = attr.name.toLowerCase(),
                        value;
                    if (/^data-/.test(key)) {
                        // Convert hyphen-ated key to camelCase:
                        key = key.slice(5).replace(/-[a-z]/g, function (str) {
                            return str.charAt(1).toUpperCase();
                        });
                        value = data[key];
                        if (that._isRegExpOption(key, value)) {
                            value = that._getRegExp(value);
                        }
                        options[key] = value;
                    }
                }
            );
        },

        _create: function () {
            this._initDataAttributes();
            this._initSpecialOptions();
            this._slots = [];
            this._sequence = this._getXHRPromise(true);
            this._sending = this._active = 0;
            this._initProgressObject(this);
            this._initEventHandlers();
        },

        // This method is exposed to the widget API and allows to query
        // the number of active uploads:
        active: function () {
            return this._active;
        },

        // This method is exposed to the widget API and allows to query
        // the widget upload progress.
        // It returns an object with loaded, total and bitrate properties
        // for the running uploads:
        progress: function () {
            return this._progress;
        },

        // This method is exposed to the widget API and allows adding files
        // using the fileupload API. The data parameter accepts an object which
        // must have a files property and can contain additional options:
        // .fileupload('add', {files: filesList});
        add: function (data) {
            var that = this;
            if (!data || this.options.disabled) {
                return;
            }
            if (data.fileInput && !data.files) {
                this._getFileInputFiles(data.fileInput).always(function (files) {
                    data.files = files;
                    that._onAdd(null, data);
                });
            } else {
                data.files = $.makeArray(data.files);
                this._onAdd(null, data);
            }
        },

        // This method is exposed to the widget API and allows sending files
        // using the fileupload API. The data parameter accepts an object which
        // must have a files or fileInput property and can contain additional options:
        // .fileupload('send', {files: filesList});
        // The method returns a Promise object for the file upload call.
        send: function (data) {
            if (data && !this.options.disabled) {
                if (data.fileInput && !data.files) {
                    var that = this,
                        dfd = $.Deferred(),
                        promise = dfd.promise(),
                        jqXHR,
                        aborted;
                    promise.abort = function () {
                        aborted = true;
                        if (jqXHR) {
                            return jqXHR.abort();
                        }
                        dfd.reject(null, 'abort', 'abort');
                        return promise;
                    };
                    this._getFileInputFiles(data.fileInput).always(
                        function (files) {
                            if (aborted) {
                                return;
                            }
                            if (!files.length) {
                                dfd.reject();
                                return;
                            }
                            data.files = files;
                            jqXHR = that._onSend(null, data);
                            jqXHR.then(
                                function (result, textStatus, jqXHR) {
                                    dfd.resolve(result, textStatus, jqXHR);
                                },
                                function (jqXHR, textStatus, errorThrown) {
                                    dfd.reject(jqXHR, textStatus, errorThrown);
                                }
                            );
                        }
                    );
                    return this._enhancePromise(promise);
                }
                data.files = $.makeArray(data.files);
                if (data.files.length) {
                    return this._onSend(null, data);
                }
            }
            return this._getXHRPromise(false, data && data.context);
        }

    });

}));

//! moment.js
//! version : 2.10.6
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com
!function(a,b){"object"==typeof exports&&"undefined"!=typeof module?module.exports=b():"function"==typeof define&&define.amd?define(b):a.moment=b()}(this,function(){"use strict";function a(){return Hc.apply(null,arguments)}function b(a){Hc=a}function c(a){return"[object Array]"===Object.prototype.toString.call(a)}function d(a){return a instanceof Date||"[object Date]"===Object.prototype.toString.call(a)}function e(a,b){var c,d=[];for(c=0;c<a.length;++c)d.push(b(a[c],c));return d}function f(a,b){return Object.prototype.hasOwnProperty.call(a,b)}function g(a,b){for(var c in b)f(b,c)&&(a[c]=b[c]);return f(b,"toString")&&(a.toString=b.toString),f(b,"valueOf")&&(a.valueOf=b.valueOf),a}function h(a,b,c,d){return Ca(a,b,c,d,!0).utc()}function i(){return{empty:!1,unusedTokens:[],unusedInput:[],overflow:-2,charsLeftOver:0,nullInput:!1,invalidMonth:null,invalidFormat:!1,userInvalidated:!1,iso:!1}}function j(a){return null==a._pf&&(a._pf=i()),a._pf}function k(a){if(null==a._isValid){var b=j(a);a._isValid=!(isNaN(a._d.getTime())||!(b.overflow<0)||b.empty||b.invalidMonth||b.invalidWeekday||b.nullInput||b.invalidFormat||b.userInvalidated),a._strict&&(a._isValid=a._isValid&&0===b.charsLeftOver&&0===b.unusedTokens.length&&void 0===b.bigHour)}return a._isValid}function l(a){var b=h(NaN);return null!=a?g(j(b),a):j(b).userInvalidated=!0,b}function m(a,b){var c,d,e;if("undefined"!=typeof b._isAMomentObject&&(a._isAMomentObject=b._isAMomentObject),"undefined"!=typeof b._i&&(a._i=b._i),"undefined"!=typeof b._f&&(a._f=b._f),"undefined"!=typeof b._l&&(a._l=b._l),"undefined"!=typeof b._strict&&(a._strict=b._strict),"undefined"!=typeof b._tzm&&(a._tzm=b._tzm),"undefined"!=typeof b._isUTC&&(a._isUTC=b._isUTC),"undefined"!=typeof b._offset&&(a._offset=b._offset),"undefined"!=typeof b._pf&&(a._pf=j(b)),"undefined"!=typeof b._locale&&(a._locale=b._locale),Jc.length>0)for(c in Jc)d=Jc[c],e=b[d],"undefined"!=typeof e&&(a[d]=e);return a}function n(b){m(this,b),this._d=new Date(null!=b._d?b._d.getTime():NaN),Kc===!1&&(Kc=!0,a.updateOffset(this),Kc=!1)}function o(a){return a instanceof n||null!=a&&null!=a._isAMomentObject}function p(a){return 0>a?Math.ceil(a):Math.floor(a)}function q(a){var b=+a,c=0;return 0!==b&&isFinite(b)&&(c=p(b)),c}function r(a,b,c){var d,e=Math.min(a.length,b.length),f=Math.abs(a.length-b.length),g=0;for(d=0;e>d;d++)(c&&a[d]!==b[d]||!c&&q(a[d])!==q(b[d]))&&g++;return g+f}function s(){}function t(a){return a?a.toLowerCase().replace("_","-"):a}function u(a){for(var b,c,d,e,f=0;f<a.length;){for(e=t(a[f]).split("-"),b=e.length,c=t(a[f+1]),c=c?c.split("-"):null;b>0;){if(d=v(e.slice(0,b).join("-")))return d;if(c&&c.length>=b&&r(e,c,!0)>=b-1)break;b--}f++}return null}function v(a){var b=null;if(!Lc[a]&&"undefined"!=typeof module&&module&&module.exports)try{b=Ic._abbr,require("./locale/"+a),w(b)}catch(c){}return Lc[a]}function w(a,b){var c;return a&&(c="undefined"==typeof b?y(a):x(a,b),c&&(Ic=c)),Ic._abbr}function x(a,b){return null!==b?(b.abbr=a,Lc[a]=Lc[a]||new s,Lc[a].set(b),w(a),Lc[a]):(delete Lc[a],null)}function y(a){var b;if(a&&a._locale&&a._locale._abbr&&(a=a._locale._abbr),!a)return Ic;if(!c(a)){if(b=v(a))return b;a=[a]}return u(a)}function z(a,b){var c=a.toLowerCase();Mc[c]=Mc[c+"s"]=Mc[b]=a}function A(a){return"string"==typeof a?Mc[a]||Mc[a.toLowerCase()]:void 0}function B(a){var b,c,d={};for(c in a)f(a,c)&&(b=A(c),b&&(d[b]=a[c]));return d}function C(b,c){return function(d){return null!=d?(E(this,b,d),a.updateOffset(this,c),this):D(this,b)}}function D(a,b){return a._d["get"+(a._isUTC?"UTC":"")+b]()}function E(a,b,c){return a._d["set"+(a._isUTC?"UTC":"")+b](c)}function F(a,b){var c;if("object"==typeof a)for(c in a)this.set(c,a[c]);else if(a=A(a),"function"==typeof this[a])return this[a](b);return this}function G(a,b,c){var d=""+Math.abs(a),e=b-d.length,f=a>=0;return(f?c?"+":"":"-")+Math.pow(10,Math.max(0,e)).toString().substr(1)+d}function H(a,b,c,d){var e=d;"string"==typeof d&&(e=function(){return this[d]()}),a&&(Qc[a]=e),b&&(Qc[b[0]]=function(){return G(e.apply(this,arguments),b[1],b[2])}),c&&(Qc[c]=function(){return this.localeData().ordinal(e.apply(this,arguments),a)})}function I(a){return a.match(/\[[\s\S]/)?a.replace(/^\[|\]$/g,""):a.replace(/\\/g,"")}function J(a){var b,c,d=a.match(Nc);for(b=0,c=d.length;c>b;b++)Qc[d[b]]?d[b]=Qc[d[b]]:d[b]=I(d[b]);return function(e){var f="";for(b=0;c>b;b++)f+=d[b]instanceof Function?d[b].call(e,a):d[b];return f}}function K(a,b){return a.isValid()?(b=L(b,a.localeData()),Pc[b]=Pc[b]||J(b),Pc[b](a)):a.localeData().invalidDate()}function L(a,b){function c(a){return b.longDateFormat(a)||a}var d=5;for(Oc.lastIndex=0;d>=0&&Oc.test(a);)a=a.replace(Oc,c),Oc.lastIndex=0,d-=1;return a}function M(a){return"function"==typeof a&&"[object Function]"===Object.prototype.toString.call(a)}function N(a,b,c){dd[a]=M(b)?b:function(a){return a&&c?c:b}}function O(a,b){return f(dd,a)?dd[a](b._strict,b._locale):new RegExp(P(a))}function P(a){return a.replace("\\","").replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,function(a,b,c,d,e){return b||c||d||e}).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&")}function Q(a,b){var c,d=b;for("string"==typeof a&&(a=[a]),"number"==typeof b&&(d=function(a,c){c[b]=q(a)}),c=0;c<a.length;c++)ed[a[c]]=d}function R(a,b){Q(a,function(a,c,d,e){d._w=d._w||{},b(a,d._w,d,e)})}function S(a,b,c){null!=b&&f(ed,a)&&ed[a](b,c._a,c,a)}function T(a,b){return new Date(Date.UTC(a,b+1,0)).getUTCDate()}function U(a){return this._months[a.month()]}function V(a){return this._monthsShort[a.month()]}function W(a,b,c){var d,e,f;for(this._monthsParse||(this._monthsParse=[],this._longMonthsParse=[],this._shortMonthsParse=[]),d=0;12>d;d++){if(e=h([2e3,d]),c&&!this._longMonthsParse[d]&&(this._longMonthsParse[d]=new RegExp("^"+this.months(e,"").replace(".","")+"$","i"),this._shortMonthsParse[d]=new RegExp("^"+this.monthsShort(e,"").replace(".","")+"$","i")),c||this._monthsParse[d]||(f="^"+this.months(e,"")+"|^"+this.monthsShort(e,""),this._monthsParse[d]=new RegExp(f.replace(".",""),"i")),c&&"MMMM"===b&&this._longMonthsParse[d].test(a))return d;if(c&&"MMM"===b&&this._shortMonthsParse[d].test(a))return d;if(!c&&this._monthsParse[d].test(a))return d}}function X(a,b){var c;return"string"==typeof b&&(b=a.localeData().monthsParse(b),"number"!=typeof b)?a:(c=Math.min(a.date(),T(a.year(),b)),a._d["set"+(a._isUTC?"UTC":"")+"Month"](b,c),a)}function Y(b){return null!=b?(X(this,b),a.updateOffset(this,!0),this):D(this,"Month")}function Z(){return T(this.year(),this.month())}function $(a){var b,c=a._a;return c&&-2===j(a).overflow&&(b=c[gd]<0||c[gd]>11?gd:c[hd]<1||c[hd]>T(c[fd],c[gd])?hd:c[id]<0||c[id]>24||24===c[id]&&(0!==c[jd]||0!==c[kd]||0!==c[ld])?id:c[jd]<0||c[jd]>59?jd:c[kd]<0||c[kd]>59?kd:c[ld]<0||c[ld]>999?ld:-1,j(a)._overflowDayOfYear&&(fd>b||b>hd)&&(b=hd),j(a).overflow=b),a}function _(b){a.suppressDeprecationWarnings===!1&&"undefined"!=typeof console&&console.warn&&console.warn("Deprecation warning: "+b)}function aa(a,b){var c=!0;return g(function(){return c&&(_(a+"\n"+(new Error).stack),c=!1),b.apply(this,arguments)},b)}function ba(a,b){od[a]||(_(b),od[a]=!0)}function ca(a){var b,c,d=a._i,e=pd.exec(d);if(e){for(j(a).iso=!0,b=0,c=qd.length;c>b;b++)if(qd[b][1].exec(d)){a._f=qd[b][0];break}for(b=0,c=rd.length;c>b;b++)if(rd[b][1].exec(d)){a._f+=(e[6]||" ")+rd[b][0];break}d.match(ad)&&(a._f+="Z"),va(a)}else a._isValid=!1}function da(b){var c=sd.exec(b._i);return null!==c?void(b._d=new Date(+c[1])):(ca(b),void(b._isValid===!1&&(delete b._isValid,a.createFromInputFallback(b))))}function ea(a,b,c,d,e,f,g){var h=new Date(a,b,c,d,e,f,g);return 1970>a&&h.setFullYear(a),h}function fa(a){var b=new Date(Date.UTC.apply(null,arguments));return 1970>a&&b.setUTCFullYear(a),b}function ga(a){return ha(a)?366:365}function ha(a){return a%4===0&&a%100!==0||a%400===0}function ia(){return ha(this.year())}function ja(a,b,c){var d,e=c-b,f=c-a.day();return f>e&&(f-=7),e-7>f&&(f+=7),d=Da(a).add(f,"d"),{week:Math.ceil(d.dayOfYear()/7),year:d.year()}}function ka(a){return ja(a,this._week.dow,this._week.doy).week}function la(){return this._week.dow}function ma(){return this._week.doy}function na(a){var b=this.localeData().week(this);return null==a?b:this.add(7*(a-b),"d")}function oa(a){var b=ja(this,1,4).week;return null==a?b:this.add(7*(a-b),"d")}function pa(a,b,c,d,e){var f,g=6+e-d,h=fa(a,0,1+g),i=h.getUTCDay();return e>i&&(i+=7),c=null!=c?1*c:e,f=1+g+7*(b-1)-i+c,{year:f>0?a:a-1,dayOfYear:f>0?f:ga(a-1)+f}}function qa(a){var b=Math.round((this.clone().startOf("day")-this.clone().startOf("year"))/864e5)+1;return null==a?b:this.add(a-b,"d")}function ra(a,b,c){return null!=a?a:null!=b?b:c}function sa(a){var b=new Date;return a._useUTC?[b.getUTCFullYear(),b.getUTCMonth(),b.getUTCDate()]:[b.getFullYear(),b.getMonth(),b.getDate()]}function ta(a){var b,c,d,e,f=[];if(!a._d){for(d=sa(a),a._w&&null==a._a[hd]&&null==a._a[gd]&&ua(a),a._dayOfYear&&(e=ra(a._a[fd],d[fd]),a._dayOfYear>ga(e)&&(j(a)._overflowDayOfYear=!0),c=fa(e,0,a._dayOfYear),a._a[gd]=c.getUTCMonth(),a._a[hd]=c.getUTCDate()),b=0;3>b&&null==a._a[b];++b)a._a[b]=f[b]=d[b];for(;7>b;b++)a._a[b]=f[b]=null==a._a[b]?2===b?1:0:a._a[b];24===a._a[id]&&0===a._a[jd]&&0===a._a[kd]&&0===a._a[ld]&&(a._nextDay=!0,a._a[id]=0),a._d=(a._useUTC?fa:ea).apply(null,f),null!=a._tzm&&a._d.setUTCMinutes(a._d.getUTCMinutes()-a._tzm),a._nextDay&&(a._a[id]=24)}}function ua(a){var b,c,d,e,f,g,h;b=a._w,null!=b.GG||null!=b.W||null!=b.E?(f=1,g=4,c=ra(b.GG,a._a[fd],ja(Da(),1,4).year),d=ra(b.W,1),e=ra(b.E,1)):(f=a._locale._week.dow,g=a._locale._week.doy,c=ra(b.gg,a._a[fd],ja(Da(),f,g).year),d=ra(b.w,1),null!=b.d?(e=b.d,f>e&&++d):e=null!=b.e?b.e+f:f),h=pa(c,d,e,g,f),a._a[fd]=h.year,a._dayOfYear=h.dayOfYear}function va(b){if(b._f===a.ISO_8601)return void ca(b);b._a=[],j(b).empty=!0;var c,d,e,f,g,h=""+b._i,i=h.length,k=0;for(e=L(b._f,b._locale).match(Nc)||[],c=0;c<e.length;c++)f=e[c],d=(h.match(O(f,b))||[])[0],d&&(g=h.substr(0,h.indexOf(d)),g.length>0&&j(b).unusedInput.push(g),h=h.slice(h.indexOf(d)+d.length),k+=d.length),Qc[f]?(d?j(b).empty=!1:j(b).unusedTokens.push(f),S(f,d,b)):b._strict&&!d&&j(b).unusedTokens.push(f);j(b).charsLeftOver=i-k,h.length>0&&j(b).unusedInput.push(h),j(b).bigHour===!0&&b._a[id]<=12&&b._a[id]>0&&(j(b).bigHour=void 0),b._a[id]=wa(b._locale,b._a[id],b._meridiem),ta(b),$(b)}function wa(a,b,c){var d;return null==c?b:null!=a.meridiemHour?a.meridiemHour(b,c):null!=a.isPM?(d=a.isPM(c),d&&12>b&&(b+=12),d||12!==b||(b=0),b):b}function xa(a){var b,c,d,e,f;if(0===a._f.length)return j(a).invalidFormat=!0,void(a._d=new Date(NaN));for(e=0;e<a._f.length;e++)f=0,b=m({},a),null!=a._useUTC&&(b._useUTC=a._useUTC),b._f=a._f[e],va(b),k(b)&&(f+=j(b).charsLeftOver,f+=10*j(b).unusedTokens.length,j(b).score=f,(null==d||d>f)&&(d=f,c=b));g(a,c||b)}function ya(a){if(!a._d){var b=B(a._i);a._a=[b.year,b.month,b.day||b.date,b.hour,b.minute,b.second,b.millisecond],ta(a)}}function za(a){var b=new n($(Aa(a)));return b._nextDay&&(b.add(1,"d"),b._nextDay=void 0),b}function Aa(a){var b=a._i,e=a._f;return a._locale=a._locale||y(a._l),null===b||void 0===e&&""===b?l({nullInput:!0}):("string"==typeof b&&(a._i=b=a._locale.preparse(b)),o(b)?new n($(b)):(c(e)?xa(a):e?va(a):d(b)?a._d=b:Ba(a),a))}function Ba(b){var f=b._i;void 0===f?b._d=new Date:d(f)?b._d=new Date(+f):"string"==typeof f?da(b):c(f)?(b._a=e(f.slice(0),function(a){return parseInt(a,10)}),ta(b)):"object"==typeof f?ya(b):"number"==typeof f?b._d=new Date(f):a.createFromInputFallback(b)}function Ca(a,b,c,d,e){var f={};return"boolean"==typeof c&&(d=c,c=void 0),f._isAMomentObject=!0,f._useUTC=f._isUTC=e,f._l=c,f._i=a,f._f=b,f._strict=d,za(f)}function Da(a,b,c,d){return Ca(a,b,c,d,!1)}function Ea(a,b){var d,e;if(1===b.length&&c(b[0])&&(b=b[0]),!b.length)return Da();for(d=b[0],e=1;e<b.length;++e)(!b[e].isValid()||b[e][a](d))&&(d=b[e]);return d}function Fa(){var a=[].slice.call(arguments,0);return Ea("isBefore",a)}function Ga(){var a=[].slice.call(arguments,0);return Ea("isAfter",a)}function Ha(a){var b=B(a),c=b.year||0,d=b.quarter||0,e=b.month||0,f=b.week||0,g=b.day||0,h=b.hour||0,i=b.minute||0,j=b.second||0,k=b.millisecond||0;this._milliseconds=+k+1e3*j+6e4*i+36e5*h,this._days=+g+7*f,this._months=+e+3*d+12*c,this._data={},this._locale=y(),this._bubble()}function Ia(a){return a instanceof Ha}function Ja(a,b){H(a,0,0,function(){var a=this.utcOffset(),c="+";return 0>a&&(a=-a,c="-"),c+G(~~(a/60),2)+b+G(~~a%60,2)})}function Ka(a){var b=(a||"").match(ad)||[],c=b[b.length-1]||[],d=(c+"").match(xd)||["-",0,0],e=+(60*d[1])+q(d[2]);return"+"===d[0]?e:-e}function La(b,c){var e,f;return c._isUTC?(e=c.clone(),f=(o(b)||d(b)?+b:+Da(b))-+e,e._d.setTime(+e._d+f),a.updateOffset(e,!1),e):Da(b).local()}function Ma(a){return 15*-Math.round(a._d.getTimezoneOffset()/15)}function Na(b,c){var d,e=this._offset||0;return null!=b?("string"==typeof b&&(b=Ka(b)),Math.abs(b)<16&&(b=60*b),!this._isUTC&&c&&(d=Ma(this)),this._offset=b,this._isUTC=!0,null!=d&&this.add(d,"m"),e!==b&&(!c||this._changeInProgress?bb(this,Ya(b-e,"m"),1,!1):this._changeInProgress||(this._changeInProgress=!0,a.updateOffset(this,!0),this._changeInProgress=null)),this):this._isUTC?e:Ma(this)}function Oa(a,b){return null!=a?("string"!=typeof a&&(a=-a),this.utcOffset(a,b),this):-this.utcOffset()}function Pa(a){return this.utcOffset(0,a)}function Qa(a){return this._isUTC&&(this.utcOffset(0,a),this._isUTC=!1,a&&this.subtract(Ma(this),"m")),this}function Ra(){return this._tzm?this.utcOffset(this._tzm):"string"==typeof this._i&&this.utcOffset(Ka(this._i)),this}function Sa(a){return a=a?Da(a).utcOffset():0,(this.utcOffset()-a)%60===0}function Ta(){return this.utcOffset()>this.clone().month(0).utcOffset()||this.utcOffset()>this.clone().month(5).utcOffset()}function Ua(){if("undefined"!=typeof this._isDSTShifted)return this._isDSTShifted;var a={};if(m(a,this),a=Aa(a),a._a){var b=a._isUTC?h(a._a):Da(a._a);this._isDSTShifted=this.isValid()&&r(a._a,b.toArray())>0}else this._isDSTShifted=!1;return this._isDSTShifted}function Va(){return!this._isUTC}function Wa(){return this._isUTC}function Xa(){return this._isUTC&&0===this._offset}function Ya(a,b){var c,d,e,g=a,h=null;return Ia(a)?g={ms:a._milliseconds,d:a._days,M:a._months}:"number"==typeof a?(g={},b?g[b]=a:g.milliseconds=a):(h=yd.exec(a))?(c="-"===h[1]?-1:1,g={y:0,d:q(h[hd])*c,h:q(h[id])*c,m:q(h[jd])*c,s:q(h[kd])*c,ms:q(h[ld])*c}):(h=zd.exec(a))?(c="-"===h[1]?-1:1,g={y:Za(h[2],c),M:Za(h[3],c),d:Za(h[4],c),h:Za(h[5],c),m:Za(h[6],c),s:Za(h[7],c),w:Za(h[8],c)}):null==g?g={}:"object"==typeof g&&("from"in g||"to"in g)&&(e=_a(Da(g.from),Da(g.to)),g={},g.ms=e.milliseconds,g.M=e.months),d=new Ha(g),Ia(a)&&f(a,"_locale")&&(d._locale=a._locale),d}function Za(a,b){var c=a&&parseFloat(a.replace(",","."));return(isNaN(c)?0:c)*b}function $a(a,b){var c={milliseconds:0,months:0};return c.months=b.month()-a.month()+12*(b.year()-a.year()),a.clone().add(c.months,"M").isAfter(b)&&--c.months,c.milliseconds=+b-+a.clone().add(c.months,"M"),c}function _a(a,b){var c;return b=La(b,a),a.isBefore(b)?c=$a(a,b):(c=$a(b,a),c.milliseconds=-c.milliseconds,c.months=-c.months),c}function ab(a,b){return function(c,d){var e,f;return null===d||isNaN(+d)||(ba(b,"moment()."+b+"(period, number) is deprecated. Please use moment()."+b+"(number, period)."),f=c,c=d,d=f),c="string"==typeof c?+c:c,e=Ya(c,d),bb(this,e,a),this}}function bb(b,c,d,e){var f=c._milliseconds,g=c._days,h=c._months;e=null==e?!0:e,f&&b._d.setTime(+b._d+f*d),g&&E(b,"Date",D(b,"Date")+g*d),h&&X(b,D(b,"Month")+h*d),e&&a.updateOffset(b,g||h)}function cb(a,b){var c=a||Da(),d=La(c,this).startOf("day"),e=this.diff(d,"days",!0),f=-6>e?"sameElse":-1>e?"lastWeek":0>e?"lastDay":1>e?"sameDay":2>e?"nextDay":7>e?"nextWeek":"sameElse";return this.format(b&&b[f]||this.localeData().calendar(f,this,Da(c)))}function db(){return new n(this)}function eb(a,b){var c;return b=A("undefined"!=typeof b?b:"millisecond"),"millisecond"===b?(a=o(a)?a:Da(a),+this>+a):(c=o(a)?+a:+Da(a),c<+this.clone().startOf(b))}function fb(a,b){var c;return b=A("undefined"!=typeof b?b:"millisecond"),"millisecond"===b?(a=o(a)?a:Da(a),+a>+this):(c=o(a)?+a:+Da(a),+this.clone().endOf(b)<c)}function gb(a,b,c){return this.isAfter(a,c)&&this.isBefore(b,c)}function hb(a,b){var c;return b=A(b||"millisecond"),"millisecond"===b?(a=o(a)?a:Da(a),+this===+a):(c=+Da(a),+this.clone().startOf(b)<=c&&c<=+this.clone().endOf(b))}function ib(a,b,c){var d,e,f=La(a,this),g=6e4*(f.utcOffset()-this.utcOffset());return b=A(b),"year"===b||"month"===b||"quarter"===b?(e=jb(this,f),"quarter"===b?e/=3:"year"===b&&(e/=12)):(d=this-f,e="second"===b?d/1e3:"minute"===b?d/6e4:"hour"===b?d/36e5:"day"===b?(d-g)/864e5:"week"===b?(d-g)/6048e5:d),c?e:p(e)}function jb(a,b){var c,d,e=12*(b.year()-a.year())+(b.month()-a.month()),f=a.clone().add(e,"months");return 0>b-f?(c=a.clone().add(e-1,"months"),d=(b-f)/(f-c)):(c=a.clone().add(e+1,"months"),d=(b-f)/(c-f)),-(e+d)}function kb(){return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")}function lb(){var a=this.clone().utc();return 0<a.year()&&a.year()<=9999?"function"==typeof Date.prototype.toISOString?this.toDate().toISOString():K(a,"YYYY-MM-DD[T]HH:mm:ss.SSS[Z]"):K(a,"YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")}function mb(b){var c=K(this,b||a.defaultFormat);return this.localeData().postformat(c)}function nb(a,b){return this.isValid()?Ya({to:this,from:a}).locale(this.locale()).humanize(!b):this.localeData().invalidDate()}function ob(a){return this.from(Da(),a)}function pb(a,b){return this.isValid()?Ya({from:this,to:a}).locale(this.locale()).humanize(!b):this.localeData().invalidDate()}function qb(a){return this.to(Da(),a)}function rb(a){var b;return void 0===a?this._locale._abbr:(b=y(a),null!=b&&(this._locale=b),this)}function sb(){return this._locale}function tb(a){switch(a=A(a)){case"year":this.month(0);case"quarter":case"month":this.date(1);case"week":case"isoWeek":case"day":this.hours(0);case"hour":this.minutes(0);case"minute":this.seconds(0);case"second":this.milliseconds(0)}return"week"===a&&this.weekday(0),"isoWeek"===a&&this.isoWeekday(1),"quarter"===a&&this.month(3*Math.floor(this.month()/3)),this}function ub(a){return a=A(a),void 0===a||"millisecond"===a?this:this.startOf(a).add(1,"isoWeek"===a?"week":a).subtract(1,"ms")}function vb(){return+this._d-6e4*(this._offset||0)}function wb(){return Math.floor(+this/1e3)}function xb(){return this._offset?new Date(+this):this._d}function yb(){var a=this;return[a.year(),a.month(),a.date(),a.hour(),a.minute(),a.second(),a.millisecond()]}function zb(){var a=this;return{years:a.year(),months:a.month(),date:a.date(),hours:a.hours(),minutes:a.minutes(),seconds:a.seconds(),milliseconds:a.milliseconds()}}function Ab(){return k(this)}function Bb(){return g({},j(this))}function Cb(){return j(this).overflow}function Db(a,b){H(0,[a,a.length],0,b)}function Eb(a,b,c){return ja(Da([a,11,31+b-c]),b,c).week}function Fb(a){var b=ja(this,this.localeData()._week.dow,this.localeData()._week.doy).year;return null==a?b:this.add(a-b,"y")}function Gb(a){var b=ja(this,1,4).year;return null==a?b:this.add(a-b,"y")}function Hb(){return Eb(this.year(),1,4)}function Ib(){var a=this.localeData()._week;return Eb(this.year(),a.dow,a.doy)}function Jb(a){return null==a?Math.ceil((this.month()+1)/3):this.month(3*(a-1)+this.month()%3)}function Kb(a,b){return"string"!=typeof a?a:isNaN(a)?(a=b.weekdaysParse(a),"number"==typeof a?a:null):parseInt(a,10)}function Lb(a){return this._weekdays[a.day()]}function Mb(a){return this._weekdaysShort[a.day()]}function Nb(a){return this._weekdaysMin[a.day()]}function Ob(a){var b,c,d;for(this._weekdaysParse=this._weekdaysParse||[],b=0;7>b;b++)if(this._weekdaysParse[b]||(c=Da([2e3,1]).day(b),d="^"+this.weekdays(c,"")+"|^"+this.weekdaysShort(c,"")+"|^"+this.weekdaysMin(c,""),this._weekdaysParse[b]=new RegExp(d.replace(".",""),"i")),this._weekdaysParse[b].test(a))return b}function Pb(a){var b=this._isUTC?this._d.getUTCDay():this._d.getDay();return null!=a?(a=Kb(a,this.localeData()),this.add(a-b,"d")):b}function Qb(a){var b=(this.day()+7-this.localeData()._week.dow)%7;return null==a?b:this.add(a-b,"d")}function Rb(a){return null==a?this.day()||7:this.day(this.day()%7?a:a-7)}function Sb(a,b){H(a,0,0,function(){return this.localeData().meridiem(this.hours(),this.minutes(),b)})}function Tb(a,b){return b._meridiemParse}function Ub(a){return"p"===(a+"").toLowerCase().charAt(0)}function Vb(a,b,c){return a>11?c?"pm":"PM":c?"am":"AM"}function Wb(a,b){b[ld]=q(1e3*("0."+a))}function Xb(){return this._isUTC?"UTC":""}function Yb(){return this._isUTC?"Coordinated Universal Time":""}function Zb(a){return Da(1e3*a)}function $b(){return Da.apply(null,arguments).parseZone()}function _b(a,b,c){var d=this._calendar[a];return"function"==typeof d?d.call(b,c):d}function ac(a){var b=this._longDateFormat[a],c=this._longDateFormat[a.toUpperCase()];return b||!c?b:(this._longDateFormat[a]=c.replace(/MMMM|MM|DD|dddd/g,function(a){return a.slice(1)}),this._longDateFormat[a])}function bc(){return this._invalidDate}function cc(a){return this._ordinal.replace("%d",a)}function dc(a){return a}function ec(a,b,c,d){var e=this._relativeTime[c];return"function"==typeof e?e(a,b,c,d):e.replace(/%d/i,a)}function fc(a,b){var c=this._relativeTime[a>0?"future":"past"];return"function"==typeof c?c(b):c.replace(/%s/i,b)}function gc(a){var b,c;for(c in a)b=a[c],"function"==typeof b?this[c]=b:this["_"+c]=b;this._ordinalParseLenient=new RegExp(this._ordinalParse.source+"|"+/\d{1,2}/.source)}function hc(a,b,c,d){var e=y(),f=h().set(d,b);return e[c](f,a)}function ic(a,b,c,d,e){if("number"==typeof a&&(b=a,a=void 0),a=a||"",null!=b)return hc(a,b,c,e);var f,g=[];for(f=0;d>f;f++)g[f]=hc(a,f,c,e);return g}function jc(a,b){return ic(a,b,"months",12,"month")}function kc(a,b){return ic(a,b,"monthsShort",12,"month")}function lc(a,b){return ic(a,b,"weekdays",7,"day")}function mc(a,b){return ic(a,b,"weekdaysShort",7,"day")}function nc(a,b){return ic(a,b,"weekdaysMin",7,"day")}function oc(){var a=this._data;return this._milliseconds=Wd(this._milliseconds),this._days=Wd(this._days),this._months=Wd(this._months),a.milliseconds=Wd(a.milliseconds),a.seconds=Wd(a.seconds),a.minutes=Wd(a.minutes),a.hours=Wd(a.hours),a.months=Wd(a.months),a.years=Wd(a.years),this}function pc(a,b,c,d){var e=Ya(b,c);return a._milliseconds+=d*e._milliseconds,a._days+=d*e._days,a._months+=d*e._months,a._bubble()}function qc(a,b){return pc(this,a,b,1)}function rc(a,b){return pc(this,a,b,-1)}function sc(a){return 0>a?Math.floor(a):Math.ceil(a)}function tc(){var a,b,c,d,e,f=this._milliseconds,g=this._days,h=this._months,i=this._data;return f>=0&&g>=0&&h>=0||0>=f&&0>=g&&0>=h||(f+=864e5*sc(vc(h)+g),g=0,h=0),i.milliseconds=f%1e3,a=p(f/1e3),i.seconds=a%60,b=p(a/60),i.minutes=b%60,c=p(b/60),i.hours=c%24,g+=p(c/24),e=p(uc(g)),h+=e,g-=sc(vc(e)),d=p(h/12),h%=12,i.days=g,i.months=h,i.years=d,this}function uc(a){return 4800*a/146097}function vc(a){return 146097*a/4800}function wc(a){var b,c,d=this._milliseconds;if(a=A(a),"month"===a||"year"===a)return b=this._days+d/864e5,c=this._months+uc(b),"month"===a?c:c/12;switch(b=this._days+Math.round(vc(this._months)),a){case"week":return b/7+d/6048e5;case"day":return b+d/864e5;case"hour":return 24*b+d/36e5;case"minute":return 1440*b+d/6e4;case"second":return 86400*b+d/1e3;case"millisecond":return Math.floor(864e5*b)+d;default:throw new Error("Unknown unit "+a)}}function xc(){return this._milliseconds+864e5*this._days+this._months%12*2592e6+31536e6*q(this._months/12)}function yc(a){return function(){return this.as(a)}}function zc(a){return a=A(a),this[a+"s"]()}function Ac(a){return function(){return this._data[a]}}function Bc(){return p(this.days()/7)}function Cc(a,b,c,d,e){return e.relativeTime(b||1,!!c,a,d)}function Dc(a,b,c){var d=Ya(a).abs(),e=ke(d.as("s")),f=ke(d.as("m")),g=ke(d.as("h")),h=ke(d.as("d")),i=ke(d.as("M")),j=ke(d.as("y")),k=e<le.s&&["s",e]||1===f&&["m"]||f<le.m&&["mm",f]||1===g&&["h"]||g<le.h&&["hh",g]||1===h&&["d"]||h<le.d&&["dd",h]||1===i&&["M"]||i<le.M&&["MM",i]||1===j&&["y"]||["yy",j];return k[2]=b,k[3]=+a>0,k[4]=c,Cc.apply(null,k)}function Ec(a,b){return void 0===le[a]?!1:void 0===b?le[a]:(le[a]=b,!0)}function Fc(a){var b=this.localeData(),c=Dc(this,!a,b);return a&&(c=b.pastFuture(+this,c)),b.postformat(c)}function Gc(){var a,b,c,d=me(this._milliseconds)/1e3,e=me(this._days),f=me(this._months);a=p(d/60),b=p(a/60),d%=60,a%=60,c=p(f/12),f%=12;var g=c,h=f,i=e,j=b,k=a,l=d,m=this.asSeconds();return m?(0>m?"-":"")+"P"+(g?g+"Y":"")+(h?h+"M":"")+(i?i+"D":"")+(j||k||l?"T":"")+(j?j+"H":"")+(k?k+"M":"")+(l?l+"S":""):"P0D"}var Hc,Ic,Jc=a.momentProperties=[],Kc=!1,Lc={},Mc={},Nc=/(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g,Oc=/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,Pc={},Qc={},Rc=/\d/,Sc=/\d\d/,Tc=/\d{3}/,Uc=/\d{4}/,Vc=/[+-]?\d{6}/,Wc=/\d\d?/,Xc=/\d{1,3}/,Yc=/\d{1,4}/,Zc=/[+-]?\d{1,6}/,$c=/\d+/,_c=/[+-]?\d+/,ad=/Z|[+-]\d\d:?\d\d/gi,bd=/[+-]?\d+(\.\d{1,3})?/,cd=/[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,dd={},ed={},fd=0,gd=1,hd=2,id=3,jd=4,kd=5,ld=6;H("M",["MM",2],"Mo",function(){return this.month()+1}),H("MMM",0,0,function(a){return this.localeData().monthsShort(this,a)}),H("MMMM",0,0,function(a){return this.localeData().months(this,a)}),z("month","M"),N("M",Wc),N("MM",Wc,Sc),N("MMM",cd),N("MMMM",cd),Q(["M","MM"],function(a,b){b[gd]=q(a)-1}),Q(["MMM","MMMM"],function(a,b,c,d){var e=c._locale.monthsParse(a,d,c._strict);null!=e?b[gd]=e:j(c).invalidMonth=a});var md="January_February_March_April_May_June_July_August_September_October_November_December".split("_"),nd="Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),od={};a.suppressDeprecationWarnings=!1;var pd=/^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,qd=[["YYYYYY-MM-DD",/[+-]\d{6}-\d{2}-\d{2}/],["YYYY-MM-DD",/\d{4}-\d{2}-\d{2}/],["GGGG-[W]WW-E",/\d{4}-W\d{2}-\d/],["GGGG-[W]WW",/\d{4}-W\d{2}/],["YYYY-DDD",/\d{4}-\d{3}/]],rd=[["HH:mm:ss.SSSS",/(T| )\d\d:\d\d:\d\d\.\d+/],["HH:mm:ss",/(T| )\d\d:\d\d:\d\d/],["HH:mm",/(T| )\d\d:\d\d/],["HH",/(T| )\d\d/]],sd=/^\/?Date\((\-?\d+)/i;a.createFromInputFallback=aa("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.",function(a){a._d=new Date(a._i+(a._useUTC?" UTC":""))}),H(0,["YY",2],0,function(){return this.year()%100}),H(0,["YYYY",4],0,"year"),H(0,["YYYYY",5],0,"year"),H(0,["YYYYYY",6,!0],0,"year"),z("year","y"),N("Y",_c),N("YY",Wc,Sc),N("YYYY",Yc,Uc),N("YYYYY",Zc,Vc),N("YYYYYY",Zc,Vc),Q(["YYYYY","YYYYYY"],fd),Q("YYYY",function(b,c){c[fd]=2===b.length?a.parseTwoDigitYear(b):q(b)}),Q("YY",function(b,c){c[fd]=a.parseTwoDigitYear(b)}),a.parseTwoDigitYear=function(a){return q(a)+(q(a)>68?1900:2e3)};var td=C("FullYear",!1);H("w",["ww",2],"wo","week"),H("W",["WW",2],"Wo","isoWeek"),z("week","w"),z("isoWeek","W"),N("w",Wc),N("ww",Wc,Sc),N("W",Wc),N("WW",Wc,Sc),R(["w","ww","W","WW"],function(a,b,c,d){b[d.substr(0,1)]=q(a)});var ud={dow:0,doy:6};H("DDD",["DDDD",3],"DDDo","dayOfYear"),z("dayOfYear","DDD"),N("DDD",Xc),N("DDDD",Tc),Q(["DDD","DDDD"],function(a,b,c){c._dayOfYear=q(a)}),a.ISO_8601=function(){};var vd=aa("moment().min is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548",function(){var a=Da.apply(null,arguments);return this>a?this:a}),wd=aa("moment().max is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548",function(){var a=Da.apply(null,arguments);return a>this?this:a});Ja("Z",":"),Ja("ZZ",""),N("Z",ad),N("ZZ",ad),Q(["Z","ZZ"],function(a,b,c){c._useUTC=!0,c._tzm=Ka(a)});var xd=/([\+\-]|\d\d)/gi;a.updateOffset=function(){};var yd=/(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/,zd=/^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/;Ya.fn=Ha.prototype;var Ad=ab(1,"add"),Bd=ab(-1,"subtract");a.defaultFormat="YYYY-MM-DDTHH:mm:ssZ";var Cd=aa("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.",function(a){return void 0===a?this.localeData():this.locale(a)});H(0,["gg",2],0,function(){return this.weekYear()%100}),H(0,["GG",2],0,function(){return this.isoWeekYear()%100}),Db("gggg","weekYear"),Db("ggggg","weekYear"),Db("GGGG","isoWeekYear"),Db("GGGGG","isoWeekYear"),z("weekYear","gg"),z("isoWeekYear","GG"),N("G",_c),N("g",_c),N("GG",Wc,Sc),N("gg",Wc,Sc),N("GGGG",Yc,Uc),N("gggg",Yc,Uc),N("GGGGG",Zc,Vc),N("ggggg",Zc,Vc),R(["gggg","ggggg","GGGG","GGGGG"],function(a,b,c,d){b[d.substr(0,2)]=q(a)}),R(["gg","GG"],function(b,c,d,e){c[e]=a.parseTwoDigitYear(b)}),H("Q",0,0,"quarter"),z("quarter","Q"),N("Q",Rc),Q("Q",function(a,b){b[gd]=3*(q(a)-1)}),H("D",["DD",2],"Do","date"),z("date","D"),N("D",Wc),N("DD",Wc,Sc),N("Do",function(a,b){return a?b._ordinalParse:b._ordinalParseLenient}),Q(["D","DD"],hd),Q("Do",function(a,b){b[hd]=q(a.match(Wc)[0],10)});var Dd=C("Date",!0);H("d",0,"do","day"),H("dd",0,0,function(a){return this.localeData().weekdaysMin(this,a)}),H("ddd",0,0,function(a){return this.localeData().weekdaysShort(this,a)}),H("dddd",0,0,function(a){return this.localeData().weekdays(this,a)}),H("e",0,0,"weekday"),H("E",0,0,"isoWeekday"),z("day","d"),z("weekday","e"),z("isoWeekday","E"),N("d",Wc),N("e",Wc),N("E",Wc),N("dd",cd),N("ddd",cd),N("dddd",cd),R(["dd","ddd","dddd"],function(a,b,c){var d=c._locale.weekdaysParse(a);null!=d?b.d=d:j(c).invalidWeekday=a}),R(["d","e","E"],function(a,b,c,d){b[d]=q(a)});var Ed="Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),Fd="Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),Gd="Su_Mo_Tu_We_Th_Fr_Sa".split("_");H("H",["HH",2],0,"hour"),H("h",["hh",2],0,function(){return this.hours()%12||12}),Sb("a",!0),Sb("A",!1),z("hour","h"),N("a",Tb),N("A",Tb),N("H",Wc),N("h",Wc),N("HH",Wc,Sc),N("hh",Wc,Sc),Q(["H","HH"],id),Q(["a","A"],function(a,b,c){c._isPm=c._locale.isPM(a),c._meridiem=a}),Q(["h","hh"],function(a,b,c){b[id]=q(a),j(c).bigHour=!0});var Hd=/[ap]\.?m?\.?/i,Id=C("Hours",!0);H("m",["mm",2],0,"minute"),z("minute","m"),N("m",Wc),N("mm",Wc,Sc),Q(["m","mm"],jd);var Jd=C("Minutes",!1);H("s",["ss",2],0,"second"),z("second","s"),N("s",Wc),N("ss",Wc,Sc),Q(["s","ss"],kd);var Kd=C("Seconds",!1);H("S",0,0,function(){return~~(this.millisecond()/100)}),H(0,["SS",2],0,function(){return~~(this.millisecond()/10)}),H(0,["SSS",3],0,"millisecond"),H(0,["SSSS",4],0,function(){return 10*this.millisecond()}),H(0,["SSSSS",5],0,function(){return 100*this.millisecond()}),H(0,["SSSSSS",6],0,function(){return 1e3*this.millisecond()}),H(0,["SSSSSSS",7],0,function(){return 1e4*this.millisecond()}),H(0,["SSSSSSSS",8],0,function(){return 1e5*this.millisecond()}),H(0,["SSSSSSSSS",9],0,function(){return 1e6*this.millisecond()}),z("millisecond","ms"),N("S",Xc,Rc),N("SS",Xc,Sc),N("SSS",Xc,Tc);var Ld;for(Ld="SSSS";Ld.length<=9;Ld+="S")N(Ld,$c);for(Ld="S";Ld.length<=9;Ld+="S")Q(Ld,Wb);var Md=C("Milliseconds",!1);H("z",0,0,"zoneAbbr"),H("zz",0,0,"zoneName");var Nd=n.prototype;Nd.add=Ad,Nd.calendar=cb,Nd.clone=db,Nd.diff=ib,Nd.endOf=ub,Nd.format=mb,Nd.from=nb,Nd.fromNow=ob,Nd.to=pb,Nd.toNow=qb,Nd.get=F,Nd.invalidAt=Cb,Nd.isAfter=eb,Nd.isBefore=fb,Nd.isBetween=gb,Nd.isSame=hb,Nd.isValid=Ab,Nd.lang=Cd,Nd.locale=rb,Nd.localeData=sb,Nd.max=wd,Nd.min=vd,Nd.parsingFlags=Bb,Nd.set=F,Nd.startOf=tb,Nd.subtract=Bd,Nd.toArray=yb,Nd.toObject=zb,Nd.toDate=xb,Nd.toISOString=lb,Nd.toJSON=lb,Nd.toString=kb,Nd.unix=wb,Nd.valueOf=vb,Nd.year=td,Nd.isLeapYear=ia,Nd.weekYear=Fb,Nd.isoWeekYear=Gb,Nd.quarter=Nd.quarters=Jb,Nd.month=Y,Nd.daysInMonth=Z,Nd.week=Nd.weeks=na,Nd.isoWeek=Nd.isoWeeks=oa,Nd.weeksInYear=Ib,Nd.isoWeeksInYear=Hb,Nd.date=Dd,Nd.day=Nd.days=Pb,Nd.weekday=Qb,Nd.isoWeekday=Rb,Nd.dayOfYear=qa,Nd.hour=Nd.hours=Id,Nd.minute=Nd.minutes=Jd,Nd.second=Nd.seconds=Kd,
Nd.millisecond=Nd.milliseconds=Md,Nd.utcOffset=Na,Nd.utc=Pa,Nd.local=Qa,Nd.parseZone=Ra,Nd.hasAlignedHourOffset=Sa,Nd.isDST=Ta,Nd.isDSTShifted=Ua,Nd.isLocal=Va,Nd.isUtcOffset=Wa,Nd.isUtc=Xa,Nd.isUTC=Xa,Nd.zoneAbbr=Xb,Nd.zoneName=Yb,Nd.dates=aa("dates accessor is deprecated. Use date instead.",Dd),Nd.months=aa("months accessor is deprecated. Use month instead",Y),Nd.years=aa("years accessor is deprecated. Use year instead",td),Nd.zone=aa("moment().zone is deprecated, use moment().utcOffset instead. https://github.com/moment/moment/issues/1779",Oa);var Od=Nd,Pd={sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},Qd={LTS:"h:mm:ss A",LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D, YYYY",LLL:"MMMM D, YYYY h:mm A",LLLL:"dddd, MMMM D, YYYY h:mm A"},Rd="Invalid date",Sd="%d",Td=/\d{1,2}/,Ud={future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},Vd=s.prototype;Vd._calendar=Pd,Vd.calendar=_b,Vd._longDateFormat=Qd,Vd.longDateFormat=ac,Vd._invalidDate=Rd,Vd.invalidDate=bc,Vd._ordinal=Sd,Vd.ordinal=cc,Vd._ordinalParse=Td,Vd.preparse=dc,Vd.postformat=dc,Vd._relativeTime=Ud,Vd.relativeTime=ec,Vd.pastFuture=fc,Vd.set=gc,Vd.months=U,Vd._months=md,Vd.monthsShort=V,Vd._monthsShort=nd,Vd.monthsParse=W,Vd.week=ka,Vd._week=ud,Vd.firstDayOfYear=ma,Vd.firstDayOfWeek=la,Vd.weekdays=Lb,Vd._weekdays=Ed,Vd.weekdaysMin=Nb,Vd._weekdaysMin=Gd,Vd.weekdaysShort=Mb,Vd._weekdaysShort=Fd,Vd.weekdaysParse=Ob,Vd.isPM=Ub,Vd._meridiemParse=Hd,Vd.meridiem=Vb,w("en",{ordinalParse:/\d{1,2}(th|st|nd|rd)/,ordinal:function(a){var b=a%10,c=1===q(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c}}),a.lang=aa("moment.lang is deprecated. Use moment.locale instead.",w),a.langData=aa("moment.langData is deprecated. Use moment.localeData instead.",y);var Wd=Math.abs,Xd=yc("ms"),Yd=yc("s"),Zd=yc("m"),$d=yc("h"),_d=yc("d"),ae=yc("w"),be=yc("M"),ce=yc("y"),de=Ac("milliseconds"),ee=Ac("seconds"),fe=Ac("minutes"),ge=Ac("hours"),he=Ac("days"),ie=Ac("months"),je=Ac("years"),ke=Math.round,le={s:45,m:45,h:22,d:26,M:11},me=Math.abs,ne=Ha.prototype;ne.abs=oc,ne.add=qc,ne.subtract=rc,ne.as=wc,ne.asMilliseconds=Xd,ne.asSeconds=Yd,ne.asMinutes=Zd,ne.asHours=$d,ne.asDays=_d,ne.asWeeks=ae,ne.asMonths=be,ne.asYears=ce,ne.valueOf=xc,ne._bubble=tc,ne.get=zc,ne.milliseconds=de,ne.seconds=ee,ne.minutes=fe,ne.hours=ge,ne.days=he,ne.weeks=Bc,ne.months=ie,ne.years=je,ne.humanize=Fc,ne.toISOString=Gc,ne.toString=Gc,ne.toJSON=Gc,ne.locale=rb,ne.localeData=sb,ne.toIsoString=aa("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)",Gc),ne.lang=Cd,H("X",0,0,"unix"),H("x",0,0,"valueOf"),N("x",_c),N("X",bd),Q("X",function(a,b,c){c._d=new Date(1e3*parseFloat(a,10))}),Q("x",function(a,b,c){c._d=new Date(q(a))}),a.version="2.10.6",b(Da),a.fn=Od,a.min=Fa,a.max=Ga,a.utc=h,a.unix=Zb,a.months=jc,a.isDate=d,a.locale=w,a.invalid=l,a.duration=Ya,a.isMoment=o,a.weekdays=lc,a.parseZone=$b,a.localeData=y,a.isDuration=Ia,a.monthsShort=kc,a.weekdaysMin=nc,a.defineLocale=x,a.weekdaysShort=mc,a.normalizeUnits=A,a.relativeTimeThreshold=Ec;var oe=a;return oe});
/*!
 * Pikaday
 *
 * Copyright © 2014 David Bushell | BSD & MIT license | https://github.com/dbushell/Pikaday
 */

(function (root, factory)
{
    'use strict';

    var moment;
    if (typeof exports === 'object') {
        // CommonJS module
        // Load moment.js as an optional dependency
        try { moment = require('moment'); } catch (e) {}
        module.exports = factory(moment);
    } else if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(function (req)
        {
            // Load moment.js as an optional dependency
            var id = 'moment';
            try { moment = req(id); } catch (e) {}
            return factory(moment);
        });
    } else {
        root.Pikaday = factory(root.moment);
    }
}(this, function (moment)
{
    'use strict';

    /**
     * feature detection and helper functions
     */
    var hasMoment = typeof moment === 'function',

    hasEventListeners = !!window.addEventListener,

    document = window.document,

    sto = window.setTimeout,

    addEvent = function(el, e, callback, capture)
    {
        if (hasEventListeners) {
            el.addEventListener(e, callback, !!capture);
        } else {
            el.attachEvent('on' + e, callback);
        }
    },

    removeEvent = function(el, e, callback, capture)
    {
        if (hasEventListeners) {
            el.removeEventListener(e, callback, !!capture);
        } else {
            el.detachEvent('on' + e, callback);
        }
    },

    fireEvent = function(el, eventName, data)
    {
        var ev;

        if (document.createEvent) {
            ev = document.createEvent('HTMLEvents');
            ev.initEvent(eventName, true, false);
            ev = extend(ev, data);
            el.dispatchEvent(ev);
        } else if (document.createEventObject) {
            ev = document.createEventObject();
            ev = extend(ev, data);
            el.fireEvent('on' + eventName, ev);
        }
    },

    trim = function(str)
    {
        return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g,'');
    },

    hasClass = function(el, cn)
    {
        return (' ' + el.className + ' ').indexOf(' ' + cn + ' ') !== -1;
    },

    addClass = function(el, cn)
    {
        if (!hasClass(el, cn)) {
            el.className = (el.className === '') ? cn : el.className + ' ' + cn;
        }
    },

    removeClass = function(el, cn)
    {
        el.className = trim((' ' + el.className + ' ').replace(' ' + cn + ' ', ' '));
    },

    isArray = function(obj)
    {
        return (/Array/).test(Object.prototype.toString.call(obj));
    },

    isDate = function(obj)
    {
        return (/Date/).test(Object.prototype.toString.call(obj)) && !isNaN(obj.getTime());
    },

    isWeekend = function(date)
    {
        var day = date.getDay();
        return day === 0 || day === 6;
    },

    isLeapYear = function(year)
    {
        // solution by Matti Virkkunen: http://stackoverflow.com/a/4881951
        return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
    },

    getDaysInMonth = function(year, month)
    {
        return [31, isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
    },

    setToStartOfDay = function(date)
    {
        if (isDate(date)) date.setHours(0,0,0,0);
    },

    compareDates = function(a,b)
    {
        // weak date comparison (use setToStartOfDay(date) to ensure correct result)
        return a.getTime() === b.getTime();
    },

    extend = function(to, from, overwrite)
    {
        var prop, hasProp;
        for (prop in from) {
            hasProp = to[prop] !== undefined;
            if (hasProp && typeof from[prop] === 'object' && from[prop] !== null && from[prop].nodeName === undefined) {
                if (isDate(from[prop])) {
                    if (overwrite) {
                        to[prop] = new Date(from[prop].getTime());
                    }
                }
                else if (isArray(from[prop])) {
                    if (overwrite) {
                        to[prop] = from[prop].slice(0);
                    }
                } else {
                    to[prop] = extend({}, from[prop], overwrite);
                }
            } else if (overwrite || !hasProp) {
                to[prop] = from[prop];
            }
        }
        return to;
    },

    adjustCalendar = function(calendar) {
        if (calendar.month < 0) {
            calendar.year -= Math.ceil(Math.abs(calendar.month)/12);
            calendar.month += 12;
        }
        if (calendar.month > 11) {
            calendar.year += Math.floor(Math.abs(calendar.month)/12);
            calendar.month -= 12;
        }
        return calendar;
    },

    /**
     * defaults and localisation
     */
    defaults = {

        // bind the picker to a form field
        field: null,

        // automatically show/hide the picker on `field` focus (default `true` if `field` is set)
        bound: undefined,

        // position of the datepicker, relative to the field (default to bottom & left)
        // ('bottom' & 'left' keywords are not used, 'top' & 'right' are modifier on the bottom/left position)
        position: 'bottom left',

        // automatically fit in the viewport even if it means repositioning from the position option
        reposition: true,

        // the default output format for `.toString()` and `field` value
        format: 'YYYY-MM-DD',

        // the initial date to view when first opened
        defaultDate: null,

        // make the `defaultDate` the initial selected value
        setDefaultDate: false,

        // first day of week (0: Sunday, 1: Monday etc)
        firstDay: 0,

        // the minimum/earliest date that can be selected
        minDate: null,
        // the maximum/latest date that can be selected
        maxDate: null,

        // number of years either side, or array of upper/lower range
        yearRange: 10,

        // show week numbers at head of row
        showWeekNumber: false,

        // used internally (don't config outside)
        minYear: 0,
        maxYear: 9999,
        minMonth: undefined,
        maxMonth: undefined,

        startRange: null,
        endRange: null,

        isRTL: false,

        // Additional text to append to the year in the calendar title
        yearSuffix: '',

        // Render the month after year in the calendar title
        showMonthAfterYear: false,

        // how many months are visible
        numberOfMonths: 1,

        // when numberOfMonths is used, this will help you to choose where the main calendar will be (default `left`, can be set to `right`)
        // only used for the first display or when a selected date is not visible
        mainCalendar: 'left',

        // Specify a DOM element to render the calendar in
        container: undefined,

        // internationalization
        i18n: {
            previousMonth : 'Previous Month',
            nextMonth     : 'Next Month',
            months        : ['January','February','March','April','May','June','July','August','September','October','November','December'],
            weekdays      : ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
            weekdaysShort : ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
        },

        // Theme Classname
        theme: null,

        // callback function
        onSelect: null,
        onOpen: null,
        onClose: null,
        onDraw: null
    },


    /**
     * templating functions to abstract HTML rendering
     */
    renderDayName = function(opts, day, abbr)
    {
        day += opts.firstDay;
        while (day >= 7) {
            day -= 7;
        }
        return abbr ? opts.i18n.weekdaysShort[day] : opts.i18n.weekdays[day];
    },

    renderDay = function(opts)
    {
        if (opts.isEmpty) {
            return '<td class="is-empty"></td>';
        }
        var arr = [];
        if (opts.isDisabled) {
            arr.push('is-disabled');
        }
        if (opts.isToday) {
            arr.push('is-today');
        }
        if (opts.isSelected) {
            arr.push('is-selected');
        }
        if (opts.isInRange) {
            arr.push('is-inrange');
        }
        if (opts.isStartRange) {
            arr.push('is-startrange');
        }
        if (opts.isEndRange) {
            arr.push('is-endrange');
        }
        return '<td data-day="' + opts.day + '" class="' + arr.join(' ') + '">' +
                 '<button class="pika-button pika-day" type="button" ' +
                    'data-pika-year="' + opts.year + '" data-pika-month="' + opts.month + '" data-pika-day="' + opts.day + '">' +
                        opts.day +
                 '</button>' +
               '</td>';
    },

    renderWeek = function (d, m, y) {
        // Lifted from http://javascript.about.com/library/blweekyear.htm, lightly modified.
        var onejan = new Date(y, 0, 1),
            weekNum = Math.ceil((((new Date(y, m, d) - onejan) / 86400000) + onejan.getDay()+1)/7);
        return '<td class="pika-week">' + weekNum + '</td>';
    },

    renderRow = function(days, isRTL)
    {
        return '<tr>' + (isRTL ? days.reverse() : days).join('') + '</tr>';
    },

    renderBody = function(rows)
    {
        return '<tbody>' + rows.join('') + '</tbody>';
    },

    renderHead = function(opts)
    {
        var i, arr = [];
        if (opts.showWeekNumber) {
            arr.push('<th></th>');
        }
        for (i = 0; i < 7; i++) {
            arr.push('<th scope="col"><abbr title="' + renderDayName(opts, i) + '">' + renderDayName(opts, i, true) + '</abbr></th>');
        }
        return '<thead>' + (opts.isRTL ? arr.reverse() : arr).join('') + '</thead>';
    },

    renderTitle = function(instance, c, year, month, refYear)
    {
        var i, j, arr,
            opts = instance._o,
            isMinYear = year === opts.minYear,
            isMaxYear = year === opts.maxYear,
            html = '<div class="pika-title">',
            monthHtml,
            yearHtml,
            prev = true,
            next = true;

        for (arr = [], i = 0; i < 12; i++) {
            arr.push('<option value="' + (year === refYear ? i - c : 12 + i - c) + '"' +
                (i === month ? ' selected': '') +
                ((isMinYear && i < opts.minMonth) || (isMaxYear && i > opts.maxMonth) ? 'disabled' : '') + '>' +
                opts.i18n.months[i] + '</option>');
        }
        monthHtml = '<div class="pika-label">' + opts.i18n.months[month] + '<select class="pika-select pika-select-month" tabindex="-1">' + arr.join('') + '</select></div>';

        if (isArray(opts.yearRange)) {
            i = opts.yearRange[0];
            j = opts.yearRange[1] + 1;
        } else {
            i = year - opts.yearRange;
            j = 1 + year + opts.yearRange;
        }

        for (arr = []; i < j && i <= opts.maxYear; i++) {
            if (i >= opts.minYear) {
                arr.push('<option value="' + i + '"' + (i === year ? ' selected': '') + '>' + (i) + '</option>');
            }
        }
        yearHtml = '<div class="pika-label">' + year + opts.yearSuffix + '<select class="pika-select pika-select-year" tabindex="-1">' + arr.join('') + '</select></div>';

        if (opts.showMonthAfterYear) {
            html += yearHtml + monthHtml;
        } else {
            html += monthHtml + yearHtml;
        }

        if (isMinYear && (month === 0 || opts.minMonth >= month)) {
            prev = false;
        }

        if (isMaxYear && (month === 11 || opts.maxMonth <= month)) {
            next = false;
        }

        if (c === 0) {
            html += '<button class="pika-prev' + (prev ? '' : ' is-disabled') + '" type="button">' + opts.i18n.previousMonth + '</button>';
        }
        if (c === (instance._o.numberOfMonths - 1) ) {
            html += '<button class="pika-next' + (next ? '' : ' is-disabled') + '" type="button">' + opts.i18n.nextMonth + '</button>';
        }

        return html += '</div>';
    },

    renderTable = function(opts, data)
    {
        return '<table cellpadding="0" cellspacing="0" class="pika-table">' + renderHead(opts) + renderBody(data) + '</table>';
    },


    /**
     * Pikaday constructor
     */
    Pikaday = function(options)
    {
        var self = this,
            opts = self.config(options);

        self._onMouseDown = function(e)
        {
            if (!self._v) {
                return;
            }
            e = e || window.event;
            var target = e.target || e.srcElement;
            if (!target) {
                return;
            }

            if (!hasClass(target.parentNode, 'is-disabled')) {
                if (hasClass(target, 'pika-button') && !hasClass(target, 'is-empty')) {
                    self.setDate(new Date(target.getAttribute('data-pika-year'), target.getAttribute('data-pika-month'), target.getAttribute('data-pika-day')));
                    if (opts.bound) {
                        sto(function() {
                            self.hide();
                            if (opts.field) {
                                opts.field.blur();
                            }
                        }, 100);
                    }
                    return;
                }
                else if (hasClass(target, 'pika-prev')) {
                    self.prevMonth();
                }
                else if (hasClass(target, 'pika-next')) {
                    self.nextMonth();
                }
            }
            if (!hasClass(target, 'pika-select')) {
                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    e.returnValue = false;
                    return false;
                }
            } else {
                self._c = true;
            }
        };

        self._onChange = function(e)
        {
            e = e || window.event;
            var target = e.target || e.srcElement;
            if (!target) {
                return;
            }
            if (hasClass(target, 'pika-select-month')) {
                self.gotoMonth(target.value);
            }
            else if (hasClass(target, 'pika-select-year')) {
                self.gotoYear(target.value);
            }
        };

        self._onInputChange = function(e)
        {
            var date;

            if (e.firedBy === self) {
                return;
            }
            if (hasMoment) {
                date = moment(opts.field.value, opts.format);
                date = (date && date.isValid()) ? date.toDate() : null;
            }
            else {
                date = new Date(Date.parse(opts.field.value));
            }
            if (isDate(date)) {
              self.setDate(date);
            }
            if (!self._v) {
                self.show();
            }
        };

        self._onInputFocus = function()
        {
            self.show();
        };

        self._onInputClick = function()
        {
            self.show();
        };

        self._onInputBlur = function()
        {
            // IE allows pika div to gain focus; catch blur the input field
            var pEl = document.activeElement;
            do {
                if (hasClass(pEl, 'pika-single')) {
                    return;
                }
            }
            while ((pEl = pEl.parentNode));

            if (!self._c) {
                self._b = sto(function() {
                    self.hide();
                }, 50);
            }
            self._c = false;
        };

        self._onClick = function(e)
        {
            e = e || window.event;
            var target = e.target || e.srcElement,
                pEl = target;
            if (!target) {
                return;
            }
            if (!hasEventListeners && hasClass(target, 'pika-select')) {
                if (!target.onchange) {
                    target.setAttribute('onchange', 'return;');
                    addEvent(target, 'change', self._onChange);
                }
            }
            do {
                if (hasClass(pEl, 'pika-single') || pEl === opts.trigger) {
                    return;
                }
            }
            while ((pEl = pEl.parentNode));
            if (self._v && target !== opts.trigger && pEl !== opts.trigger) {
                self.hide();
            }
        };

        self.el = document.createElement('div');
        self.el.className = 'pika-single' + (opts.isRTL ? ' is-rtl' : '') + (opts.theme ? ' ' + opts.theme : '');

        addEvent(self.el, 'ontouchend' in document ? 'touchend' : 'mousedown', self._onMouseDown, true);
        addEvent(self.el, 'change', self._onChange);

        if (opts.field) {
            if (opts.container) {
                opts.container.appendChild(self.el);
            } else if (opts.bound) {
                document.body.appendChild(self.el);
            } else {
                opts.field.parentNode.insertBefore(self.el, opts.field.nextSibling);
            }
            addEvent(opts.field, 'change', self._onInputChange);

            if (!opts.defaultDate) {
                if (hasMoment && opts.field.value) {
                    opts.defaultDate = moment(opts.field.value, opts.format).toDate();
                } else {
                    opts.defaultDate = new Date(Date.parse(opts.field.value));
                }
                opts.setDefaultDate = true;
            }
        }

        var defDate = opts.defaultDate;

        if (isDate(defDate)) {
            if (opts.setDefaultDate) {
                self.setDate(defDate, true);
            } else {
                self.gotoDate(defDate);
            }
        } else {
            self.gotoDate(new Date());
        }

        if (opts.bound) {
            this.hide();
            self.el.className += ' is-bound';
            addEvent(opts.trigger, 'click', self._onInputClick);
            addEvent(opts.trigger, 'focus', self._onInputFocus);
            addEvent(opts.trigger, 'blur', self._onInputBlur);
        } else {
            this.show();
        }
    };


    /**
     * public Pikaday API
     */
    Pikaday.prototype = {


        /**
         * configure functionality
         */
        config: function(options)
        {
            if (!this._o) {
                this._o = extend({}, defaults, true);
            }

            var opts = extend(this._o, options, true);

            opts.isRTL = !!opts.isRTL;

            opts.field = (opts.field && opts.field.nodeName) ? opts.field : null;

            opts.theme = (typeof opts.theme) === 'string' && opts.theme ? opts.theme : null;

            opts.bound = !!(opts.bound !== undefined ? opts.field && opts.bound : opts.field);

            opts.trigger = (opts.trigger && opts.trigger.nodeName) ? opts.trigger : opts.field;

            opts.disableWeekends = !!opts.disableWeekends;

            opts.disableDayFn = (typeof opts.disableDayFn) === 'function' ? opts.disableDayFn : null;

            var nom = parseInt(opts.numberOfMonths, 10) || 1;
            opts.numberOfMonths = nom > 4 ? 4 : nom;

            if (!isDate(opts.minDate)) {
                opts.minDate = false;
            }
            if (!isDate(opts.maxDate)) {
                opts.maxDate = false;
            }
            if ((opts.minDate && opts.maxDate) && opts.maxDate < opts.minDate) {
                opts.maxDate = opts.minDate = false;
            }
            if (opts.minDate) {
                this.setMinDate(opts.minDate);
            }
            if (opts.maxDate) {
                setToStartOfDay(opts.maxDate);
                opts.maxYear  = opts.maxDate.getFullYear();
                opts.maxMonth = opts.maxDate.getMonth();
            }

            if (isArray(opts.yearRange)) {
                var fallback = new Date().getFullYear() - 10;
                opts.yearRange[0] = parseInt(opts.yearRange[0], 10) || fallback;
                opts.yearRange[1] = parseInt(opts.yearRange[1], 10) || fallback;
            } else {
                opts.yearRange = Math.abs(parseInt(opts.yearRange, 10)) || defaults.yearRange;
                if (opts.yearRange > 100) {
                    opts.yearRange = 100;
                }
            }

            return opts;
        },

        /**
         * return a formatted string of the current selection (using Moment.js if available)
         */
        toString: function(format)
        {
            return !isDate(this._d) ? '' : hasMoment ? moment(this._d).format(format || this._o.format) : this._d.toDateString();
        },

        /**
         * return a Moment.js object of the current selection (if available)
         */
        getMoment: function()
        {
            return hasMoment ? moment(this._d) : null;
        },

        /**
         * set the current selection from a Moment.js object (if available)
         */
        setMoment: function(date, preventOnSelect)
        {
            if (hasMoment && moment.isMoment(date)) {
                this.setDate(date.toDate(), preventOnSelect);
            }
        },

        /**
         * return a Date object of the current selection
         */
        getDate: function()
        {
            return isDate(this._d) ? new Date(this._d.getTime()) : null;
        },

        /**
         * set the current selection
         */
        setDate: function(date, preventOnSelect)
        {
            if (!date) {
                this._d = null;

                if (this._o.field) {
                    this._o.field.value = '';
                    fireEvent(this._o.field, 'change', { firedBy: this });
                }

                return this.draw();
            }
            if (typeof date === 'string') {
                date = new Date(Date.parse(date));
            }
            if (!isDate(date)) {
                return;
            }

            var min = this._o.minDate,
                max = this._o.maxDate;

            if (isDate(min) && date < min) {
                date = min;
            } else if (isDate(max) && date > max) {
                date = max;
            }

            this._d = new Date(date.getTime());
            setToStartOfDay(this._d);
            this.gotoDate(this._d);

            if (this._o.field) {
                this._o.field.value = this.toString();
                fireEvent(this._o.field, 'change', { firedBy: this });
            }
            if (!preventOnSelect && typeof this._o.onSelect === 'function') {
                this._o.onSelect.call(this, this.getDate());
            }
        },

        /**
         * change view to a specific date
         */
        gotoDate: function(date)
        {
            var newCalendar = true;

            if (!isDate(date)) {
                return;
            }

            if (this.calendars) {
                var firstVisibleDate = new Date(this.calendars[0].year, this.calendars[0].month, 1),
                    lastVisibleDate = new Date(this.calendars[this.calendars.length-1].year, this.calendars[this.calendars.length-1].month, 1),
                    visibleDate = date.getTime();
                // get the end of the month
                lastVisibleDate.setMonth(lastVisibleDate.getMonth()+1);
                lastVisibleDate.setDate(lastVisibleDate.getDate()-1);
                newCalendar = (visibleDate < firstVisibleDate.getTime() || lastVisibleDate.getTime() < visibleDate);
            }

            if (newCalendar) {
                this.calendars = [{
                    month: date.getMonth(),
                    year: date.getFullYear()
                }];
                if (this._o.mainCalendar === 'right') {
                    this.calendars[0].month += 1 - this._o.numberOfMonths;
                }
            }

            this.adjustCalendars();
        },

        adjustCalendars: function() {
            this.calendars[0] = adjustCalendar(this.calendars[0]);
            for (var c = 1; c < this._o.numberOfMonths; c++) {
                this.calendars[c] = adjustCalendar({
                    month: this.calendars[0].month + c,
                    year: this.calendars[0].year
                });
            }
            this.draw();
        },

        gotoToday: function()
        {
            this.gotoDate(new Date());
        },

        /**
         * change view to a specific month (zero-index, e.g. 0: January)
         */
        gotoMonth: function(month)
        {
            if (!isNaN(month)) {
                this.calendars[0].month = parseInt(month, 10);
                this.adjustCalendars();
            }
        },

        nextMonth: function()
        {
            this.calendars[0].month++;
            this.adjustCalendars();
        },

        prevMonth: function()
        {
            this.calendars[0].month--;
            this.adjustCalendars();
        },

        /**
         * change view to a specific full year (e.g. "2012")
         */
        gotoYear: function(year)
        {
            if (!isNaN(year)) {
                this.calendars[0].year = parseInt(year, 10);
                this.adjustCalendars();
            }
        },

        /**
         * change the minDate
         */
        setMinDate: function(value)
        {
            setToStartOfDay(value);
            this._o.minDate = value;
            this._o.minYear  = value.getFullYear();
            this._o.minMonth = value.getMonth();
        },

        /**
         * change the maxDate
         */
        setMaxDate: function(value)
        {
            this._o.maxDate = value;
        },

        setStartRange: function(value)
        {
            this._o.startRange = value;
        },

        setEndRange: function(value)
        {
            this._o.endRange = value;
        },

        /**
         * refresh the HTML
         */
        draw: function(force)
        {
            if (!this._v && !force) {
                return;
            }
            var opts = this._o,
                minYear = opts.minYear,
                maxYear = opts.maxYear,
                minMonth = opts.minMonth,
                maxMonth = opts.maxMonth,
                html = '';

            if (this._y <= minYear) {
                this._y = minYear;
                if (!isNaN(minMonth) && this._m < minMonth) {
                    this._m = minMonth;
                }
            }
            if (this._y >= maxYear) {
                this._y = maxYear;
                if (!isNaN(maxMonth) && this._m > maxMonth) {
                    this._m = maxMonth;
                }
            }

            for (var c = 0; c < opts.numberOfMonths; c++) {
                html += '<div class="pika-lendar">' + renderTitle(this, c, this.calendars[c].year, this.calendars[c].month, this.calendars[0].year) + this.render(this.calendars[c].year, this.calendars[c].month) + '</div>';
            }

            this.el.innerHTML = html;

            if (opts.bound) {
                if(opts.field.type !== 'hidden') {
                    sto(function() {
                        opts.trigger.focus();
                    }, 1);
                }
            }

            if (typeof this._o.onDraw === 'function') {
                var self = this;
                sto(function() {
                    self._o.onDraw.call(self);
                }, 0);
            }
        },

        adjustPosition: function()
        {
            var field, pEl, width, height, viewportWidth, viewportHeight, scrollTop, left, top, clientRect;
            
            if (this._o.container) return;
            
            this.el.style.position = 'absolute';
            
            field = this._o.trigger;
            pEl = field;
            width = this.el.offsetWidth;
            height = this.el.offsetHeight;
            viewportWidth = window.innerWidth || document.documentElement.clientWidth;
            viewportHeight = window.innerHeight || document.documentElement.clientHeight;
            scrollTop = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop;

            if (typeof field.getBoundingClientRect === 'function') {
                clientRect = field.getBoundingClientRect();
                left = clientRect.left + window.pageXOffset;
                top = clientRect.bottom + window.pageYOffset;
            } else {
                left = pEl.offsetLeft;
                top  = pEl.offsetTop + pEl.offsetHeight;
                while((pEl = pEl.offsetParent)) {
                    left += pEl.offsetLeft;
                    top  += pEl.offsetTop;
                }
            }

            // default position is bottom & left
            if ((this._o.reposition && left + width > viewportWidth) ||
                (
                    this._o.position.indexOf('right') > -1 &&
                    left - width + field.offsetWidth > 0
                )
            ) {
                left = left - width + field.offsetWidth;
            }
            if ((this._o.reposition && top + height > viewportHeight + scrollTop) ||
                (
                    this._o.position.indexOf('top') > -1 &&
                    top - height - field.offsetHeight > 0
                )
            ) {
                top = top - height - field.offsetHeight;
            }

            this.el.style.left = left + 'px';
            this.el.style.top = top + 'px';
        },

        /**
         * render HTML for a particular month
         */
        render: function(year, month)
        {
            var opts   = this._o,
                now    = new Date(),
                days   = getDaysInMonth(year, month),
                before = new Date(year, month, 1).getDay(),
                data   = [],
                row    = [];
            setToStartOfDay(now);
            if (opts.firstDay > 0) {
                before -= opts.firstDay;
                if (before < 0) {
                    before += 7;
                }
            }
            var cells = days + before,
                after = cells;
            while(after > 7) {
                after -= 7;
            }
            cells += 7 - after;
            for (var i = 0, r = 0; i < cells; i++)
            {
                var dayConfig,
                    day = new Date(year, month, 1 + (i - before)),
                    isSelected = isDate(this._d) ? compareDates(day, this._d) : false,
                    isToday = compareDates(day, now),
                    isEmpty = i < before || i >= (days + before),
                    isStartRange = opts.startRange && compareDates(opts.startRange, day),
                    isEndRange = opts.endRange && compareDates(opts.endRange, day),
                    isInRange = opts.startRange && opts.endRange && opts.startRange < day && day < opts.endRange,
                    isDisabled = (opts.minDate && day < opts.minDate) ||
                                 (opts.maxDate && day > opts.maxDate) ||
                                 (opts.disableWeekends && isWeekend(day)) ||
                                 (opts.disableDayFn && opts.disableDayFn(day)),
                    dayConfig = {
                        day: 1 + (i - before),
                        month: month,
                        year: year,
                        isSelected: isSelected,
                        isToday: isToday,
                        isDisabled: isDisabled,
                        isEmpty: isEmpty,
                        isStartRange: isStartRange,
                        isEndRange: isEndRange,
                        isInRange: isInRange
                    };

                row.push(renderDay(dayConfig));

                if (++r === 7) {
                    if (opts.showWeekNumber) {
                        row.unshift(renderWeek(i - before, month, year));
                    }
                    data.push(renderRow(row, opts.isRTL));
                    row = [];
                    r = 0;
                }
            }
            return renderTable(opts, data);
        },

        isVisible: function()
        {
            return this._v;
        },

        show: function()
        {
            if (!this._v) {
                removeClass(this.el, 'is-hidden');
                this._v = true;
                this.draw();
                if (this._o.bound) {
                    addEvent(document, 'click', this._onClick);
                    this.adjustPosition();
                }
                if (typeof this._o.onOpen === 'function') {
                    this._o.onOpen.call(this);
                }
            }
        },

        hide: function()
        {
            var v = this._v;
            if (v !== false) {
                if (this._o.bound) {
                    removeEvent(document, 'click', this._onClick);
                }
                this.el.style.position = 'static'; // reset
                this.el.style.left = 'auto';
                this.el.style.top = 'auto';
                addClass(this.el, 'is-hidden');
                this._v = false;
                if (v !== undefined && typeof this._o.onClose === 'function') {
                    this._o.onClose.call(this);
                }
            }
        },

        /**
         * GAME OVER
         */
        destroy: function()
        {
            this.hide();
            removeEvent(this.el, 'mousedown', this._onMouseDown, true);
            removeEvent(this.el, 'change', this._onChange);
            if (this._o.field) {
                removeEvent(this._o.field, 'change', this._onInputChange);
                if (this._o.bound) {
                    removeEvent(this._o.trigger, 'click', this._onInputClick);
                    removeEvent(this._o.trigger, 'focus', this._onInputFocus);
                    removeEvent(this._o.trigger, 'blur', this._onInputBlur);
                }
            }
            if (this.el.parentNode) {
                this.el.parentNode.removeChild(this.el);
            }
        }

    };

    return Pikaday;

}));

/*!
 * @preserve
 *
 * Readmore.js jQuery plugin
 * Author: @jed_foster
 * Project home: http://jedfoster.github.io/Readmore.js
 * Licensed under the MIT license
 *
 * Debounce function from http://davidwalsh.name/javascript-debounce-function
 */

/* global jQuery */

(function($) {
  'use strict';

  var readmore = 'readmore',
      defaults = {
        speed: 100,
        collapsedHeight: 200,
        heightMargin: 16,
        moreLink: '<a href="#">Read More</a>',
        lessLink: '<a href="#">Close</a>',
        embedCSS: true,
        blockCSS: 'display: block; width: 100%;',
        startOpen: false,

        // callbacks
        beforeToggle: function(){},
        afterToggle: function(){}
      },
      cssEmbedded = {},
      uniqueIdCounter = 0;

  function debounce(func, wait, immediate) {
    var timeout;

    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (! immediate) {
          func.apply(context, args);
        }
      };
      var callNow = immediate && !timeout;

      clearTimeout(timeout);
      timeout = setTimeout(later, wait);

      if (callNow) {
        func.apply(context, args);
      }
    };
  }

  function uniqueId(prefix) {
    var id = ++uniqueIdCounter;

    return String(prefix == null ? 'rmjs-' : prefix) + id;
  }

  function setBoxHeights(element) {
    var el = element.clone().css({
          height: 'auto',
          width: element.width(),
          maxHeight: 'none',
          overflow: 'hidden'
        }).insertAfter(element),
        expandedHeight = el.outerHeight(),
        cssMaxHeight = parseInt(el.css({maxHeight: ''}).css('max-height').replace(/[^-\d\.]/g, ''), 10),
        defaultHeight = element.data('defaultHeight');

    el.remove();

    var collapsedHeight = cssMaxHeight || element.data('collapsedHeight') || defaultHeight;

    // Store our measurements.
    element.data({
      expandedHeight: expandedHeight,
      maxHeight: cssMaxHeight,
      collapsedHeight: collapsedHeight
    })
    // and disable any `max-height` property set in CSS
    .css({
      maxHeight: 'none'
    });
  }

  var resizeBoxes = debounce(function() {
    $('[data-readmore]').each(function() {
      var current = $(this),
          isExpanded = (current.attr('aria-expanded') === 'true');

      setBoxHeights(current);

      current.css({
        height: current.data( (isExpanded ? 'expandedHeight' : 'collapsedHeight') )
      });
    });
  }, 100);

  function embedCSS(options) {
    if (! cssEmbedded[options.selector]) {
      var styles = ' ';

      if (options.embedCSS && options.blockCSS !== '') {
        styles += options.selector + ' + [data-readmore-toggle], ' +
          options.selector + '[data-readmore]{' +
            options.blockCSS +
          '}';
      }

      // Include the transition CSS even if embedCSS is false
      styles += options.selector + '[data-readmore]{' +
        'transition: height ' + options.speed + 'ms;' +
        'overflow: hidden;' +
      '}';

      (function(d, u) {
        var css = d.createElement('style');
        css.type = 'text/css';

        if (css.styleSheet) {
          css.styleSheet.cssText = u;
        }
        else {
          css.appendChild(d.createTextNode(u));
        }

        d.getElementsByTagName('head')[0].appendChild(css);
      }(document, styles));

      cssEmbedded[options.selector] = true;
    }
  }

  function Readmore(element, options) {
    var $this = this;

    this.element = element;

    this.options = $.extend({}, defaults, options);

    embedCSS(this.options);

    this._defaults = defaults;
    this._name = readmore;

    this.init();

    // IE8 chokes on `window.addEventListener`, so need to test for support.
    if (window.addEventListener) {
      // Need to resize boxes when the page has fully loaded.
      window.addEventListener('load', resizeBoxes);
      window.addEventListener('resize', resizeBoxes);
    }
    else {
      window.attachEvent('load', resizeBoxes);
      window.attachEvent('resize', resizeBoxes);
    }
  }


  Readmore.prototype = {
    init: function() {
      var $this = this,
          current = $(this.element);

      current.data({
        defaultHeight: this.options.collapsedHeight,
        heightMargin: this.options.heightMargin
      });

      setBoxHeights(current);

      var collapsedHeight = current.data('collapsedHeight'),
          heightMargin = current.data('heightMargin');

      if (current.outerHeight(true) <= collapsedHeight + heightMargin) {
        // The block is shorter than the limit, so there's no need to truncate it.
        return true;
      }
      else {
        var id = current.attr('id') || uniqueId(),
            useLink = $this.options.startOpen ? $this.options.lessLink : $this.options.moreLink;

        current.attr({
          'data-readmore': '',
          'aria-expanded': false,
          'id': id
        });

        current.after($(useLink)
          .on('click', function(event) { $this.toggle(this, current[0], event); })
          .attr({
            'data-readmore-toggle': '',
            'aria-controls': id
          }));

        if (! $this.options.startOpen) {
          current.css({
            height: collapsedHeight
          });
        }
      }
    },

    toggle: function(trigger, element, event) {
      if (event) {
        event.preventDefault();
      }

      if (! trigger) {
        trigger = $('[aria-controls="' + this.element.id + '"]')[0];
      }

      if (! element) {
        element = this.element;
      }

      var $this = this,
          $element = $(element),
          newHeight = '',
          newLink = '',
          expanded = false,
          collapsedHeight = $element.data('collapsedHeight');

      if ($element.height() <= collapsedHeight) {
        newHeight = $element.data('expandedHeight') + 'px';
        newLink = 'lessLink';
        expanded = true;
      }
      else {
        newHeight = collapsedHeight;
        newLink = 'moreLink';
      }

      // Fire beforeToggle callback
      // Since we determined the new "expanded" state above we're now out of sync
      // with our true current state, so we need to flip the value of `expanded`
      $this.options.beforeToggle(trigger, element, ! expanded);

      $element.css({'height': newHeight});

      // Fire afterToggle callback
      $element.on('transitionend', function() {
        $this.options.afterToggle(trigger, element, expanded);

        $(this).attr({
          'aria-expanded': expanded
        }).off('transitionend');
      });

      $(trigger).replaceWith($($this.options[newLink])
          .on('click', function(event) { $this.toggle(this, element, event); })
          .attr({
            'data-readmore-toggle': '',
            'aria-controls': $element.attr('id')
          }));
    },

    destroy: function() {
      $(this.element).each(function() {
        var current = $(this);

        current.attr({
          'data-readmore': null,
          'aria-expanded': null
        })
          .css({
            maxHeight: '',
            height: ''
          })
          .next('[data-readmore-toggle]')
          .remove();

        current.removeData();
      });
    }
  };


  $.fn.readmore = function(options) {
    var args = arguments,
        selector = this.selector;

    options = options || {};

    if (typeof options === 'object') {
      return this.each(function() {
        if ($.data(this, 'plugin_' + readmore)) {
          var instance = $.data(this, 'plugin_' + readmore);
          instance.destroy.apply(instance);
        }

        options.selector = selector;

        $.data(this, 'plugin_' + readmore, new Readmore(this, options));
      });
    }
    else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {
      return this.each(function () {
        var instance = $.data(this, 'plugin_' + readmore);
        if (instance instanceof Readmore && typeof instance[options] === 'function') {
          instance[options].apply(instance, Array.prototype.slice.call(args, 1));
        }
      });
    }
  };

})(jQuery);


// jQuery Mask Plugin v1.13.9
// github.com/igorescobar/jQuery-Mask-Plugin
(function(a){"function"===typeof define&&define.amd?define(["jquery"],a):"object"===typeof exports?module.exports=a(require("jquery")):a(jQuery||Zepto)})(function(a){var x=function(c,e,d){var b={invalid:[],getCaret:function(){try{var r,a=0,e=c.get(0),f=document.selection,d=e.selectionStart;if(f&&-1===navigator.appVersion.indexOf("MSIE 10"))r=f.createRange(),r.moveStart("character",-b.val().length),a=r.text.length;else if(d||"0"===d)a=d;return a}catch(h){}},setCaret:function(r){try{if(c.is(":focus")){var b;
b=c.get(0).createTextRange();b.collapse(!0);b.moveEnd("character",r);b.moveStart("character",r);b.select()}}catch(a){}},events:function(){c.on("keydown.mask",function(b){c.data("mask-keycode",b.keyCode||b.which)}).on(a.jMaskGlobals.useInput?"input.mask":"keyup.mask",b.behaviour).on("paste.mask drop.mask",function(){setTimeout(function(){c.keydown().keyup()},100)}).on("change.mask",function(){c.data("changed",!0)}).on("blur.mask",function(){n===b.val()||c.data("changed")||c.trigger("change");c.data("changed",
!1)}).on("blur.mask",function(){n=b.val()}).on("focus.mask",function(b){!0===d.selectOnFocus&&a(b.target).select()}).on("focusout.mask",function(){d.clearIfNotMatch&&!k.test(b.val())&&b.val("")})},getRegexMask:function(){for(var b=[],c,a,f,d,h=0;h<e.length;h++)(c=g.translation[e.charAt(h)])?(a=c.pattern.toString().replace(/.{1}$|^.{1}/g,""),f=c.optional,(c=c.recursive)?(b.push(e.charAt(h)),d={digit:e.charAt(h),pattern:a}):b.push(f||c?a+"?":a)):b.push(e.charAt(h).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&"));
b=b.join("");d&&(b=b.replace(new RegExp("("+d.digit+"(.*"+d.digit+")?)"),"($1)?").replace(new RegExp(d.digit,"g"),d.pattern));return new RegExp(b)},destroyEvents:function(){c.off("input keydown keyup paste drop blur focusout ".split(" ").join(".mask "))},val:function(b){var a=c.is("input")?"val":"text";if(0<arguments.length){if(c[a]()!==b)c[a](b);a=c}else a=c[a]();return a},getMCharsBeforeCount:function(b,c){for(var a=0,d=0,l=e.length;d<l&&d<b;d++)g.translation[e.charAt(d)]||(b=c?b+1:b,a++);return a},
caretPos:function(c,a,d,f){return g.translation[e.charAt(Math.min(c-1,e.length-1))]?Math.min(c+d-a-f,d):b.caretPos(c+1,a,d,f)},behaviour:function(d){d=d||window.event;b.invalid=[];var e=c.data("mask-keycode");if(-1===a.inArray(e,g.byPassKeys)){var p=b.getCaret(),f=b.val().length,l=b.getMasked(),h=l.length,n=b.getMCharsBeforeCount(h-1)-b.getMCharsBeforeCount(f-1),m=p<f;b.val(l);m&&(8!==e&&46!==e&&(p=b.caretPos(p,f,h,n)),b.setCaret(p));return b.callbacks(d)}},getMasked:function(c){var a=[],p=b.val(),
f=0,l=e.length,h=0,n=p.length,m=1,k="push",t=-1,s,v;d.reverse?(k="unshift",m=-1,s=0,f=l-1,h=n-1,v=function(){return-1<f&&-1<h}):(s=l-1,v=function(){return f<l&&h<n});for(;v();){var w=e.charAt(f),u=p.charAt(h),q=g.translation[w];if(q)u.match(q.pattern)?(a[k](u),q.recursive&&(-1===t?t=f:f===s&&(f=t-m),s===t&&(f-=m)),f+=m):q.optional?(f+=m,h-=m):q.fallback?(a[k](q.fallback),f+=m,h-=m):b.invalid.push({p:h,v:u,e:q.pattern}),h+=m;else{if(!c)a[k](w);u===w&&(h+=m);f+=m}}c=e.charAt(s);l!==n+1||g.translation[c]||
a.push(c);return a.join("")},callbacks:function(a){var g=b.val(),k=g!==n,f=[g,a,c,d],l=function(b,c,a){"function"===typeof d[b]&&c&&d[b].apply(this,a)};l("onChange",!0===k,f);l("onKeyPress",!0===k,f);l("onComplete",g.length===e.length,f);l("onInvalid",0<b.invalid.length,[g,a,c,b.invalid,d])}};c=a(c);var g=this,n=b.val(),k;e="function"===typeof e?e(b.val(),void 0,c,d):e;g.mask=e;g.options=d;g.remove=function(){var a=b.getCaret();b.destroyEvents();b.val(g.getCleanVal());b.setCaret(a-b.getMCharsBeforeCount(a));
return c};g.getCleanVal=function(){return b.getMasked(!0)};g.init=function(e){e=e||!1;d=d||{};g.clearIfNotMatch=a.jMaskGlobals.clearIfNotMatch;g.byPassKeys=a.jMaskGlobals.byPassKeys;g.translation=a.extend({},a.jMaskGlobals.translation,d.translation);g=a.extend(!0,{},g,d);k=b.getRegexMask();!1===e?(d.placeholder&&c.attr("placeholder",d.placeholder),c.data("mask")&&c.attr("autocomplete","off"),b.destroyEvents(),b.events(),e=b.getCaret(),b.val(b.getMasked()),b.setCaret(e+b.getMCharsBeforeCount(e,!0))):
(b.events(),b.val(b.getMasked()))};g.init(!c.is("input"))};a.maskWatchers={};var z=function(){var c=a(this),e={},d=c.attr("data-mask");c.attr("data-mask-reverse")&&(e.reverse=!0);c.attr("data-mask-clearifnotmatch")&&(e.clearIfNotMatch=!0);"true"===c.attr("data-mask-selectonfocus")&&(e.selectOnFocus=!0);if(y(c,d,e))return c.data("mask",new x(this,d,e))},y=function(c,e,d){d=d||{};var b=a(c).data("mask"),g=JSON.stringify;c=a(c).val()||a(c).text();try{return"function"===typeof e&&(e=e(c)),"object"!==
typeof b||g(b.options)!==g(d)||b.mask!==e}catch(k){}};a.fn.mask=function(c,e){e=e||{};var d=this.selector,b=a.jMaskGlobals,g=b.watchInterval,b=e.watchInputs||b.watchInputs,k=function(){if(y(this,c,e))return a(this).data("mask",new x(this,c,e))};a(this).each(k);d&&""!==d&&b&&(clearInterval(a.maskWatchers[d]),a.maskWatchers[d]=setInterval(function(){a(document).find(d).each(k)},g));return this};a.fn.unmask=function(){clearInterval(a.maskWatchers[this.selector]);delete a.maskWatchers[this.selector];
return this.each(function(){var c=a(this).data("mask");c&&c.remove().removeData("mask")})};a.fn.cleanVal=function(){return this.data("mask").getCleanVal()};a.applyDataMask=function(c){c=c||a.jMaskGlobals.maskElements;(c instanceof a?c:a(c)).filter(a.jMaskGlobals.dataMaskAttr).each(z)};var k={maskElements:"input,td,span,div",dataMaskAttr:"*[data-mask]",dataMask:!0,watchInterval:300,watchInputs:!0,useInput:function(a){var e=document.createElement("div"),d;a="on"+a;d=a in e;d||(e.setAttribute(a,"return;"),
d="function"===typeof e[a]);return d}("input"),watchDataMask:!1,byPassKeys:[9,16,17,18,36,37,38,39,40,91],translation:{0:{pattern:/\d/},9:{pattern:/\d/,optional:!0},"#":{pattern:/\d/,recursive:!0},A:{pattern:/[a-zA-Z0-9]/},S:{pattern:/[a-zA-Z]/}}};a.jMaskGlobals=a.jMaskGlobals||{};k=a.jMaskGlobals=a.extend(!0,{},k,a.jMaskGlobals);k.dataMask&&a.applyDataMask();setInterval(function(){a.jMaskGlobals.watchDataMask&&a.applyDataMask()},k.watchInterval)});

var Moradores = {
	init : function(){

		var modulo = location.pathname.split('/')[2];

		if(modulo == 'auth')
			this.Auth.init();

		if(modulo != 'auth')
			this.Dashboard.init();

		if(modulo == 'livro-de-ocorrencias')
			this.Ocorrencias.init();

		if(modulo == 'moradores-da-unidade')
			this.MoradoresDaUnidade.init();

		if(modulo == 'veiculos-da-unidade')
			this.VeiculosDaUnidade.init();

		if(modulo == 'animais-de-estimacao')
			this.AnimaisDeEstimacao.init();

		if(modulo == 'fale-com')
			this.FaleCom.init();

		if(modulo == 'faq')
			this.FAQ.init();

		if(modulo == 'agendamento-de-mudanca')
			this.AgendamentoDeMudanca.init();

		if(modulo == 'chamados-de-manutencao')
			this.ChamadosDeManutencao.init();

		if(modulo == 'reserva-de-espacos')
			this.ReservaDeEspacos.init();

		if(modulo == 'pessoas-autorizadas')
			this.PessoasAutorizadas.init();

		if(modulo == 'pessoas-nao-autorizadas')
			this.PessoasNaoAutorizadas.init();

		if(modulo == 'prestadores-de-servico')
			this.PrestadoresDeServico.init();

		if(modulo == 'meu-perfil')
			this.MeuPerfil.init();

		if(modulo == 'alterar-senha')
			this.AlterarSenha.init();

		if(modulo == 'instrucoes-de-uso')
			this.InstrucoesDeUso.init();

		if(modulo == 'festas-particulares')
			this.FestasParticulares.init();

		if(modulo == 'amizades')
			this.Amizades.init();

		if(modulo == 'classificados')
			this.Classificados.init();

		if(modulo == 'fornecedores')
			this.Fornecedores.init();

		if($('.retorno_formulario').length){
			setTimeout( function(){
				$('.retorno_formulario').addClass('removendo').fadeOut('normal');
			}, 3000);
		}

		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		})
	}
};

Moradores.Formularios = {

	campos_obrigatorios : [],

	toggle_on_click : function () {
		$('#form-toggle').click( function(e){
			e.preventDefault();
			var form = $(this).attr('data-toggle-target');
			$(this).toggleClass('aberto');
			$('#'+form).toggleClass('aberto');
		});
	},

	toggle_on_load : function() {

		var pagina_tem_erro     = $('.input-erro').length > 0 ? true : false;
		var pagina_de_edicao    = $('.form-unidade-edicao').length > 0 ? true : false;
		var pagina_sem_registro = $('#lista-registros li').length == 0 ? true : false;
		var iniciar_fechado     = $('#form-toggle').hasClass('iniciar-fechado') ? true : false;

		if( (pagina_tem_erro || pagina_de_edicao || pagina_sem_registro) && !iniciar_fechado )
			$('#form-toggle').click();
	},

	tooltips_on_load : function() {
		var tooltips = $('.addon-tooltip');
		$.each(tooltips, function(){

			var title_original = $(this).attr('title');
			var tooltip_title = title_original;
			var tooltip_text  = $(this).attr('data-tooltip-text');

			$(this).find('img').attr('title', '');

			$(this).tooltipster({
				content: $("<strong>"+tooltip_title+"</strong><p>"+tooltip_text+"</p>"),
				maxWidth: 240,
				theme: 'tema-tooltipster-custom'
        	});
		});
	},

	modal_visibilidade_on_click : function() {
		$('.addon-tooltip-visib').click( function(e){
			e.preventDefault();

			var titulo_shadow = $(this).attr('data-shadow-title');
			var input_armazenamento = $('#'+$(this).attr('data-shadow-storage'));

			Moradores.Formularios.carregar_modal_visibilidade(titulo_shadow, input_armazenamento);
		});
	},

	carregar_modal_visibilidade : function(titulo, input) {

		var template = "";
		template += "<div id='form-visibilidade-shadow'>";
			template += "<h2>"+titulo+"</h2>";
			template += "<p>SELECIONAR PARA QUEM ESSA INFORMAÇÃO SERÁ EXIBIDA DENTRO DO SISTEMA:</p>";
			template += "<ul>";
				template += "<li><label><input type='checkbox' value='1' class='check-marcar-todos'> Todos (moradores, corpo diretivo, zelador)</label></li>";
				template += "<li><label class='marcado'><input type='checkbox' value='2' checked id='check-marcar-admin'> Somente síndico e conselho</label></li>";
				// template += "<li><label><input type='checkbox' value='3'> Portaria</label></li>";
				template += "<li><label><input type='checkbox' value='4'> Zelador</label></li>";
				template += "<li><label><input type='checkbox' value='5' id='check-marcar-moradores'> Todos os moradores</label></li>";
				template += "<li><label><input type='checkbox' value='6' id='check-marcar-amigos'> Somente moradores selecionados como amigos</label></li>";
			template += "</ul>";
			template += "<button id='fechar-visibilidade-shadow'>CONFIRMAR</button>";
		template += "</div>";

		$.featherlight(template, {
			afterContent : function(e){

				var inputs = $(".featherlight-inner input[type='checkbox']");
				var valor_atual = input.val();

				if(valor_atual.indexOf(',') !== -1){

					var valores_atuais = valor_atual.split(',')

					$.each(inputs, function(){
						if($.inArray($(this).val(), valores_atuais) !== -1){
							$(this).attr('checked', 'checked');
							$(this).parent().addClass('marcado');
						}
					});

				}else if(valor_atual != ''){
					$.each(inputs, function(){
						if($(this).val() == valor_atual){
							$(this).attr('checked', 'checked');
							$(this).parent().addClass('marcado');
						}
					});
				}

				$("#form-visibilidade-shadow .check-marcar-todos").click( function(){
					// Ao clicar em 'marcar todos', marcar ou desmarcar todos os checkboxes
					// EXCETO o de só amigos e o da administração (esse sempre ativo)
					$("#form-visibilidade-shadow input[type=checkbox]").not('#check-marcar-amigos')
																	   .not('#check-marcar-admin')
																	   .prop('checked', this.checked);

					// Sempre DESmarcar o de 'só amigos'
					$('#check-marcar-amigos').prop('checked', false);
					$('#check-marcar-amigos').parent().removeClass('marcado');

					if(this.checked){
						$("#form-visibilidade-shadow label input").not('#check-marcar-amigos')
																  .not('#check-marcar-admin')
																  .parent()
																  .addClass('marcado');
					}else{
						$("#form-visibilidade-shadow label input").not('#check-marcar-amigos')
																  .not('#check-marcar-admin')
																  .parent()
																  .removeClass('marcado');
					}
				});

				$("#form-visibilidade-shadow input[type=checkbox]").not(".check-marcar-todos").click( function(e){

					if($(this).val() == '2'){ // Sempre visível pra administração
						e.preventDefault();
						return false;
					}

					if(!this.checked){
						$("#form-visibilidade-shadow .check-marcar-todos").prop('checked', false);
						$("#form-visibilidade-shadow .check-marcar-todos").parent().removeClass('marcado');
						$(this).parent().removeClass('marcado');
					}else{
						$(this).parent().addClass('marcado');

						if($(this).val() == '5'){
							// Se value = 5 (todos os moradores)
							// Desmarca o 6 (somente amigos)
							$('#check-marcar-amigos').prop('checked', false).parent().removeClass('marcado');
						}else if($(this).val() == '6'){
							// Se value = 6 (somente amigos)
							// Desmarca o 5 (todos os moradores)
							$('#check-marcar-moradores').prop('checked', false).parent().removeClass('marcado');
						}
					}
				});

				$('#fechar-visibilidade-shadow').click( function(){
					Moradores.Formularios.salvar_modal_visibilidade(input);
					$.featherlight.current().close();
				});
			}
		});
	},

	salvar_modal_visibilidade : function(input) {
		var marcados = $(".featherlight-inner input[type='checkbox']:checked");
		var icone = input.parent().find('.addon-tooltip-visib');
		var valores = Array();

		if(marcados.length > 0){
			$.each(marcados, function(){
				valores.push($(this).val());
			});
			valores = valores.join(',');
		}else{
			valores = '';
		}

		input.val(valores);

		input.parent().removeClass('input-erro')
					  .removeClass('obrigatorio');

		if(input.val() == '')
			icone.removeClass('on').addClass('off');
		else
			icone.removeClass('off').addClass('on');
	},

	marcar_icone_visibilidade_on_load : function() {
		var icones = $('.addon-tooltip-visib');
		$.each(icones, function(){
			var input = $(this).parent().find('input[type=hidden]');
			if(input.val() != ''){
				$(this).removeClass('off').addClass('on');
			}
		});
	},

	remover_erros : function() {
		$.each($('.input-erro input, .input-erro select, .input-erro textarea'), function(){

			var evento = $(this).prop('nodeName').toLowerCase() == 'select'  ? 'click' : 'keyup';

			$(this).on(evento, function(){
				$(this).parent().removeClass('input-erro')
								.removeClass('obrigatorio')
								.removeClass('data-invalida')
								.removeClass('data-passou')
								.removeClass('email-invalido')
								.removeClass('login-fail')
								.removeClass('confirmacao-senha-invalida')
								.removeClass('email-em-uso')
								.removeClass('minimo');
			});

		});
	},

	validacao_on_submit : function() {
		$('form').not('.form-ajax').submit( function(e) {

			for (var i = 0; i < Moradores.Formularios.campos_obrigatorios.length; i++) {

				if(Moradores.Formularios.campos_obrigatorios[i] == 'fornecedores_avaliacao_nota'){

					if( $("input[name='avaliacao[nota]']:checked").length == 0 ){

						var rating_input = $('#fornecedores-form-avaliacao .rating');

						rating_input.addClass('obrigatorio').on('click', function(){
							rating_input.removeClass('obrigatorio');
						});

						e.preventDefault();
						return false;
					}

				}else{

					var check = $(Moradores.Formularios.campos_obrigatorios[i]);

					if(!check.val()){

						check.parent()
								 .addClass('input-erro')
								 .addClass('obrigatorio');

						evento = check.prop('nodeName').toLowerCase() == 'select' ? 'click' : 'keyup';

						check.focus().on(evento, function(){
							check.parent()
									 .removeClass('input-erro')
									 .removeClass('obrigatorio');
						});

						e.preventDefault();
						return false;
					}

				}
			};

		});
	},

	foto_upload : function(diretorio_imagens) {
		$(".input-foto input[type=file]").fileupload({
			url: 'moradores/'+diretorio_imagens+'/upload-foto',
			type: 'post',
	        dataType: 'json',
	        send: function(){
	        	Moradores.Formularios.upload_imagem_esconder_erro();
	        	$('.input-foto label').addClass('enviando');
	        },
	        progress: function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('#upload-progresso .barra').css(
		            'width',
		            progress + '%'
		        );
		    },
	        done: function (e, data) {
	        	$('.input-foto label').removeClass('enviando');
	        	if(data.result.success == 1){
	        		var foto = new Image;
	        		var target = $('.input-foto label');
	        		$('.input-foto .form-foto-hidden').val(data.result.filename);
	        		foto.onload = function(){
	        			target.append(foto);
	        		};
	        		foto.src = data.result.thumb;
	        	}else{
	        		Moradores.Formularios.upload_imagem_mostrar_erro(data.result.msg);
	        	}
	        }
	    });
	},

	foto_multi_upload : function(diretorio_imagens, identificador_sessao) {
		$(".input-foto input[type=file]").fileupload({
			url: 'moradores/'+diretorio_imagens+'/upload-foto',
			type: 'post',
	        dataType: 'json',
	        send: function(){
	        	Moradores.Formularios.upload_imagem_esconder_erro();
	        	$('.input-foto label').addClass('enviando');
	        },
	        progress: function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('#upload-progresso .barra').css(
		            'width',
		            progress + '%'
		        );
		    },
	        done: function (e, data) {

	        	$('.input-foto label').removeClass('enviando');

	        	if(data.result.success == 1){

	        		var target_imgs   = $('.input-foto .lista-imagens');

	        		var foto 		  = new Image;
	        		var hidden_input  = $("<input type='hidden' name='"+identificador_sessao+"[fotos][]' class='form-foto-hidden' value='"+data.result.filename+"'>");
	        		var btn_remover   = $("<a href='#' title='Remover Imagem' class='btn btn-danger btn-mini btn-remover-imagem'>x</a>")
	        		var btn_shadow    = $("<a href='#' data-featherlight='"+data.result.thumb.replace('thumbs', 'redimensionadas')+"' title='Ampliar' class='btn-ampliar-imagem'></a>");
	        		var novo_li 	  = $('<li></li>');
	        		foto.onload = function(){

	        			btn_shadow.append(foto);

	        			novo_li.append(btn_shadow)
	        				   .append(btn_remover)
	        				   .append(hidden_input);

	        			target_imgs.append(novo_li);

	        			var nova_altura = $('.input-foto').css('height');
	        			$('form').not('.form-ajax').css('min-height', parseInt(nova_altura) + 60);

	        			Moradores.Formularios.botoes_remover_imagem();

	        			$('.btn-ampliar-imagem').off('click');
	        			$('.btn-ampliar-imagem').featherlightGallery();
	        		};
	        		foto.src = data.result.thumb;
	        	}else{
	        		Moradores.Formularios.upload_imagem_mostrar_erro(data.result.msg);
	        	}
	        }
	    });
	},

	botoes_remover_imagem : function(){
		$('.btn-remover-imagem').on('click', function(e){
			e.preventDefault();
			$(this).parent().fadeOut('normal', function(){
				$(this).remove();
				$('.btn-ampliar-imagem').off('click');
	        	$('.btn-ampliar-imagem').featherlightGallery();
			})
		});
	},

	upload_imagem_esconder_erro : function() {
		$('.input-foto label').removeClass('input-erro');
	    $('.input-foto label p').html($('.input-foto label p').attr('data-html-original'));
	},

	upload_imagem_mostrar_erro : function(msg) {
		$('.input-foto label').addClass('input-erro');
	    $('.input-foto label p').html(msg);

			if($('.input-foto .form-foto-hidden').length == 1)
	    	$('.input-foto .form-foto-hidden').val('');

	    $('.input-foto label img').fadeOut('normal', function(){
	    	$(this).remove();
	    });
	},

	carregar_foto_apos_upload : function(diretorio_imagens, multiplas_imagens) {

		var file = $('.input-foto .form-foto-hidden').val();

		if(multiplas_imagens == true){

		}else{
			if($('.input-erro').length && file != ''){
				var foto = new Image;
	    		var target = $('.input-foto label');
	    		foto.onload = function(){
	    			target.append(foto);
	    		};
	    		foto.src = 'assets/images/moradores/'+diretorio_imagens+'/thumbs/'+file;
			}
		}

	},

	adicionarCampoObrigatorio : function(campo) {
		Moradores.Formularios.campos_obrigatorios.push(campo);
	},

	removerCampoObrigatorio : function (campo) {
		var indice = Moradores.Formularios.campos_obrigatorios.indexOf(campo);
		if(indice > -1)
			Moradores.Formularios.campos_obrigatorios.splice(indice, 1);
	},

	init : function(options) {
		this.toggle_on_click();
		this.toggle_on_load();
		this.tooltips_on_load();
		this.modal_visibilidade_on_click();
		this.marcar_icone_visibilidade_on_load();
		this.remover_erros();
		this.validacao_on_submit();

		this.campos_obrigatorios = options.campos_obrigatorios;

		if(options.form_com_imagem == true){

			if(options.multiplas_imagens == true)
				this.foto_multi_upload(options.diretorio_imagens, options.identificador_sessao);
			else
				this.foto_upload(options.diretorio_imagens);

			this.botoes_remover_imagem();
			this.carregar_foto_apos_upload(options.diretorio_imagens, options.multiplas_imagens);
		}
	}
};

Moradores.Auth = {

	login_form_submit : function() {
		$('.login-form form').submit( function(e){

			var campo_email = $('#login-form-email');
			var campo_passw = $('#login-form-senha');

			if(!campo_email.val()){

				campo_email.parent()
						   .addClass('erro')
						   .addClass('obrigatorio')
						   .find('input')
						   .focus();

				e.preventDefault();
				return false;
			}

			if(!campo_passw.val()){

				campo_passw.parent()
						   .addClass('erro')
						   .addClass('obrigatorio')
						   .find('input')
						   .focus();

				e.preventDefault();
				return false;
			}
		});
	},

	login_form_keyup : function() {
		$('#login-form-email, #login-form-senha').on('keyup', function(){
			$(this).parent().removeClass('erro')
							.removeClass('obrigatorio');
		});
	},

	register_tooltip_nome : function() {
		var tooltip_nome = $('.addon-tooltip-nome');
		$.each(tooltip_nome, function(){
			var title_original = $(this).attr('title');
			var conteudo = "";
			conteudo += "<strong>POR QUE ESSA INFORMAÇÃO É SOLICITADA?</strong>";
			conteudo += "<p>";
				conteudo += "Para nos certificarmos de que quem está fazendo o cadastro é um morador solicitamos um dado da unidade que possa ser confirmado.<br>";
				conteudo += "É necessário informar o nome completo do primeiro titular proprietário da unidade";
			conteudo += "</p>";
			$(this).find('img').attr('title', '');
			$(this).find('img').attr('data-old-title', title_original);
			$(this).tooltipster({
				content: $(conteudo),
				maxWidth: 240,
				theme: 'tema-tooltipster-custom'
      });
		});
	},

	register_tooltip_foto : function() {
		var tooltip_foto = $('.addon-tooltip-foto');
		$.each(tooltip_foto, function(){
			var title_original = $(this).attr('title');
			var conteudo = "";
			conteudo += "<strong>POR QUE ESSA INFORMAÇÃO É SOLICITADA?</strong>";
			conteudo += "<p>";
				conteudo += "A foto é importante para que novos funcionários possam reconhecer você no acesso ao prédio. ";
				conteudo += "Atente-se para usar uma imagem que se pareça com ";
				conteudo += "você no dia a dia e facilitar o reconhecimento. Usar ";
				conteudo += "uma foto com muita maquiagem quando ";
				conteudo += "geralmente você não se maqueia, ou com chapéu ";
				conteudo += "ou boné podem dificultar a identificação.";
			conteudo += "</p>";
			$(this).find('img').attr('title', '');
			$(this).find('img').attr('data-old-title', title_original);
			$(this).tooltipster({
				content: $(conteudo),
				maxWidth: 240,
				theme: 'tema-tooltipster-custom'
        	});
		});
	},

	register_tooltip_visibilidade : function() {
		var tooltip_visib = $('.addon-tooltip-visib');
		$.each(tooltip_visib, function(){
			var title_original = $(this).attr('title');
			$(this).find('img').attr('title', '');
			$(this).find('img').attr('data-old-title', title_original);
			$(this).tooltipster({
				content: $("<strong>SELECIONAR PARA QUEM ESSA INFORMAÇÃO SERÁ EXIBIDA.</strong>"),
				maxWidth: 180,
				theme: 'tema-tooltipster-custom'
        	});
		});
	},

	register_shadow_email : function() {
		$('.register-form-set_visibilidade.visib-email').click( function(e){
			e.preventDefault();
			Moradores.Auth.register_abrir_shadow_visibilidade('Endereço de E-mail', $('#register-form-visibilidade_email'));
		});
	},

	register_shadow_telfixo : function() {
		$('.register-form-set_visibilidade.visib-tel_fixo').click( function(e){
			e.preventDefault();
			Moradores.Auth.register_abrir_shadow_visibilidade('Telefone Fixo', $('#register-form-visibilidade_tel-fixo'));
		});
	},

	register_shadow_telcel : function() {
		$('.register-form-set_visibilidade.visib-tel_cel').click( function(e){
			e.preventDefault();
			Moradores.Auth.register_abrir_shadow_visibilidade('Telefone Celular', $('#register-form-visibilidade_tel-cel'));
		});
	},

	register_shadow_telcom : function() {
		$('.register-form-set_visibilidade.visib-tel_com').click( function(e){
			e.preventDefault();
			Moradores.Auth.register_abrir_shadow_visibilidade('Telefone Comercial', $('#register-form-visibilidade_tel-com'));
		});
	},

	register_abrir_shadow_visibilidade : function(titulo, input) {

		var template = "";
		template += "<div id='register-visibilidade-shadow'>";
			template += "<h2>"+titulo+"</h2>";
			template += "<p>SELECIONAR PARA QUEM ESSA INFORMAÇÃO SERÁ EXIBIDA DENTRO DO SISTEMA:</p>";
			template += "<ul>";
				template += "<li><label><input type='checkbox' value='1' class='check-marcar-todos'> Todos (moradores, corpo diretivo, zelador)</label></li>";
				template += "<li><label class='marcado'><input type='checkbox' value='2' checked id='check-marcar-admin'> Somente síndico e conselho</label></li>";
				// template += "<li><label><input type='checkbox' value='3'> Portaria</label></li>";
				template += "<li><label><input type='checkbox' value='4'> Zelador</label></li>";
				template += "<li><label><input type='checkbox' value='5' id='check-marcar-moradores'> Todos os moradores</label></li>";
				template += "<li><label><input type='checkbox' value='6' id='check-marcar-amigos'> Somente moradores selecionados como amigos</label></li>";
			template += "</ul>";
			template += "<button id='fechar-visibilidade-shadow'>CONFIRMAR</button>";
		template += "</div>";

		$.featherlight(template, {
			afterContent : function(e){

				var inputs = $(".featherlight-inner input[type='checkbox']");
				var valor_atual = input.val();

				if(valor_atual.indexOf(',') !== -1){

					var valores_atuais = valor_atual.split(',')

					$.each(inputs, function(){
						if($.inArray($(this).val(), valores_atuais) !== -1){
							$(this).attr('checked', 'checked');
							$(this).parent().addClass('marcado');
						}
					});

				}else if(valor_atual != ''){
					$.each(inputs, function(){
						if($(this).val() == valor_atual){
							$(this).attr('checked', 'checked');
							$(this).parent().addClass('marcado');
						}
					});
				}

				$("#register-visibilidade-shadow .check-marcar-todos").click( function(){
					// Ao clicar em 'marcar todos', marcar ou desmarcar todos os checkboxes
					// EXCETO o de só amigos e o da administração (esse sempre ativo)
					$("#register-visibilidade-shadow input[type=checkbox]").not('#check-marcar-amigos')
																		   .not('#check-marcar-admin')
																		   .prop('checked', this.checked);

					// Sempre DESmarcar o de 'só amigos'
					$('#check-marcar-amigos').prop('checked', false);
					$('#check-marcar-amigos').parent().removeClass('marcado');

					if(this.checked){
						$("#register-visibilidade-shadow label input").not('#check-marcar-amigos')
																	  .not('#check-marcar-admin')
																	  .parent()
																	  .addClass('marcado');
					}else{
						$("#register-visibilidade-shadow label input").not('#check-marcar-amigos')
																	  .not('#check-marcar-admin')
																	  .parent()
																	  .removeClass('marcado');
					}
				});

				$("#register-visibilidade-shadow input[type=checkbox]").not(".check-marcar-todos").click( function(e){

					if($(this).val() == '2'){ // Sempre visível pra administração
						e.preventDefault();
						return false;
					}

					if(!this.checked){
						$("#register-visibilidade-shadow .check-marcar-todos").prop('checked', false);
						$("#register-visibilidade-shadow .check-marcar-todos").parent().removeClass('marcado');
						$(this).parent().removeClass('marcado');
					}else{
						$(this).parent().addClass('marcado');

						if($(this).val() == '5'){
							// Se value = 5 (todos os moradores)
							// Desmarca o 6 (somente amigos)
							$('#check-marcar-amigos').prop('checked', false).parent().removeClass('marcado');
						}else if($(this).val() == '6'){
							// Se value = 6 (somente amigos)
							// Desmarca o 5 (todos os moradores)
							$('#check-marcar-moradores').prop('checked', false).parent().removeClass('marcado');
						}
					}
				});

				$('#fechar-visibilidade-shadow').click( function(){
					Moradores.Auth.register_salvar_visibilidade(input);
					$.featherlight.current().close();
				});
			}
		});
	},

	register_salvar_visibilidade : function(input) {
		var marcados = $(".featherlight-inner input[type='checkbox']:checked");
		var icone = input.parent().find('.register-form-set_visibilidade');
		var valores = Array();

		if(marcados.length > 0){
			$.each(marcados, function(){
				valores.push($(this).val());
			});
			valores = valores.join(',');
		}else{
			valores = '';
		}

		input.val(valores);

		input.parent().removeClass('input-erro')
					  .removeClass('obrigatorio');

		if(input.val() == '')
			icone.removeClass('on').addClass('off');
		else
			icone.removeClass('off').addClass('on');
	},

	register_marcar_icone_visibilidade : function() {
		var inputs_visibilidade = [
			'#register-form-visibilidade_email',
			'#register-form-visibilidade_tel-fixo',
			'#register-form-visibilidade_tel-cel',
			'#register-form-visibilidade_tel-com'
		];
		for (var i = 0; i < inputs_visibilidade.length; i++) {
			input = $(inputs_visibilidade[i]);
			if(input.val() != ''){
				input.parent().find('.addon-tooltip-visib')
							  .removeClass('off')
							  .addClass('on');
			}
		};
	},

	register_remover_erro_keyup : function() {
		$('.input-erro input').keyup( function(){
			$(this).parent().removeClass('input-erro')
							.removeClass('obrigatorio')
							.removeClass('email-invalido')
							.removeClass('confirmacao-senha-invalida')
							.removeClass('email-em-uso');
		});
	},

	register_carregar_foto_onload : function() {
		var file = $('#register-form-foto').val();
		if($('#register-form-foto').length && file != ''){
			var foto = new Image;
    		var target = $('.register-foto-placeholder label');
    		foto.onload = function(){
    			target.append(foto);
    		};
    		foto.src = 'assets/images/moradores/fotos/thumbs/'+file;
		}
	},

	register_foto_upload : function() {
		$('#register-form-fileupload').fileupload({
			url: 'moradores/auth/register/upload-foto',
			type: 'post',
	        dataType: 'json',
	        send: function(){
	        	Moradores.Auth.register_foto_upload_esconder_erro();
	        	$('.register-foto-placeholder label').addClass('enviando');
	        },
	        progress: function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('#upload-progresso .barra').css(
		            'width',
		            progress + '%'
		        );
		    },
	        done: function (e, data) {
	        	$('.register-foto-placeholder label').removeClass('enviando');
	        	if(data.result.success == 1){
	        		var foto = new Image;
	        		var target = $('.register-foto-placeholder label');
	        		$('#register-form-foto').val(data.result.filename);
	        		foto.onload = function(){
	        			target.append(foto);
	        		};
	        		foto.src = data.result.thumb;
	        	}else{
	        		Moradores.Auth.register_foto_upload_mostar_erro(data.result.msg);
	        		setTimeout( function(){
	        			Moradores.Auth.register_foto_upload_esconder_erro();
	        		}, 2000);
	        	}
	        }
	    });
	},

	register_foto_upload_mostar_erro: function(msg) {
		$('.register-foto-placeholder label').addClass('input-erro');
	    $('.register-foto-placeholder label p').html(msg);
	    $('#register-form-foto').val('');
	    $('.register-foto-placeholder label img').fadeOut('normal', function(){
	    	$(this).remove();
	    })
	},

	register_foto_upload_esconder_erro: function() {
		$('.register-foto-placeholder label').removeClass('input-erro');
	    $('.register-foto-placeholder label p').html($('.register-foto-placeholder label p').attr('data-html-original'));
	},

	register_form_submit : function() {
		$('#register-form form').submit( function(e) {

			var inputs_obrigatorios = [
				'#register-form-numero_unidade',
				'#register-form-bloco',
				'#register-form-nome_proprietario',
				'#register-form-nome',
				'#register-form-apelido',
				'#register-form-relacao_unidade',
				'#register-form-data_mudanca',
				'#register-form-email',
				'#register-form-visibilidade_email',
				'#register-form-senha',
				'#register-form-confirmacao_senha',
				'#register-form-telefone_fixo',
				'#register-form-visibilidade_tel-fixo',
				'#register-form-telefone_celular',
				'#register-form-visibilidade_tel-cel',
				'#register-form-telefone_comercial',
				'#register-form-visibilidade_tel-com',
				//'#register-form-foto'
			];

			for (var i = 0; i < inputs_obrigatorios.length; i++) {

				var check = $(inputs_obrigatorios[i]);

				if(!check.val()){

					check.parent().addClass('input-erro')
								  .addClass('obrigatorio');

					check.focus().on('keyup', function(){
								  	check.parent().removeClass('input-erro')
								  				  .removeClass('obrigatorio');
								  });

					e.preventDefault();
					return false;
				}
			};

			var campo = $('#register-form-email');
			var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			var email = campo.val();
    		if(!regex.test(email)){

    			campo.parent().addClass('input-erro')
							  .addClass('email-invalido');

				campo.focus().on('keyup', function(){
							  	campo.parent().removeClass('input-erro')
							  				  .removeClass('email-invalido');
							  });

				e.preventDefault();
				return false;
    		}

    		var input_senha = $('#register-form-senha');
    		var input_conf_senha = $('#register-form-confirmacao_senha');
    		if(input_senha.val() != input_conf_senha.val()){

    			input_senha.parent().addClass('input-erro')
							  		.addClass('confirmacao-senha-invalida');

				input_senha.focus().on('keyup', function(){
							  		input_senha.parent().removeClass('input-erro')
							  				   .removeClass('confirmacao-senha-invalida');
							  	});

				e.preventDefault();
				return false;
    		}

		});
	},

	register_form_pikaday : function() {

		var picker1 = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('register-form-data_mudanca'),
			format: 'DD/MM/YYYY',
			yearRange: [1900, 2050],
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			onSelect : function(){
				$('#register-form-data_mudanca').parent()
												.removeClass('input-erro')
												.removeClass('obrigatorio');
			}
		});

		var picker2 = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('register-form-data_nascimento'),
			format: 'DD/MM/YYYY',
			yearRange: [1900, 2050],
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			onSelect : function(){
				$('#register-form-data_nascimento').parent()
												.removeClass('input-erro')
												.removeClass('obrigatorio');
			}
		});
	},

	mascara_telefones : function(){
		var telefones = '#register-form-telefone_fixo, #register-form-telefone_celular, #register-form-telefone_comercial';
		$(telefones).mask('(00) 00000-0000');
	},

	init : function() {
		this.login_form_submit();
		this.login_form_keyup();
		this.register_tooltip_nome();
		this.register_tooltip_foto();
		this.register_tooltip_visibilidade();
		this.register_shadow_email();
		this.register_shadow_telfixo();
		this.register_shadow_telcel();
		this.register_shadow_telcom();
		this.register_foto_upload();
		this.register_form_submit();
		this.register_marcar_icone_visibilidade();
		this.register_remover_erro_keyup();
		this.register_carregar_foto_onload();

		this.register_form_pikaday();
		this.mascara_telefones();
	}
};

Moradores.Dashboard = {

	controlar_submenus : function() {
		$('.dashboard-mostra-areas .area').click( function(e){
			e.preventDefault();
			$(this).toggleClass('ativo');
			$('.dashboard-mostra-areas .area.ativo').not($(this)).removeClass('ativo');
			return false;
		});
	},

	fechar_submenus_click : function() {
		$(window).on('click', function(){
			$('.dashboard-mostra-areas .area.ativo').removeClass('ativo');
		});
	},

	avisos_marcar_como_lido : function() {
		$(".aviso-marcar-lido-inferior input[type=checkbox], .aviso-marcar-lido input[type=checkbox]").change( function(){

			var avisos_id = $(this).val();
			var resultado = $(this).parent().parent();
			var check_inferior = $('.aviso-marcar-lido-inferior');

			$(this).attr('disabled', true);

			$.post('moradores/avisos/lido', {
				avisos_id : avisos_id
			}, function(resposta){

				var resposta = "<span class='aviso-lido-data apagado'>Lido em: <br>"+resposta.lido_em+"</span>";

				resultado.find('label').fadeOut('normal', function(){

					resultado.append(resposta)
							 .find('.apagado')
							 .removeClass('apagado');

				});

				/**
				* Se existir um elemento com a classe -inferior
				* estamos na página de detalhes, além do elemento
				* .parent().parent() precisamos colocar o timestamp
				* no inferior também, além de remover o checkbox que
				* não foi utilizado para marcação de lido
				*/
				if(check_inferior.length && resultado.hasClass('aviso-marcar-lido-inferior')){

					$('#avisos-detalhes .aviso-marcar-lido').find('label').fadeOut('normal', function(){
						$('#avisos-detalhes .aviso-marcar-lido').append(resposta)
																.find('.apagado')
							 									.removeClass('apagado');
					});

				}else if(check_inferior.length){

					$('#avisos-detalhes .aviso-marcar-lido-inferior').find('label').fadeOut('normal', function(){
						$('#avisos-detalhes .aviso-marcar-lido-inferior').append(resposta)
																.find('.apagado')
							 											 .removeClass('apagado');
					});

				}
			});
		});
	},

	tooltip_notificacoes : function() {
		$.each($('.notificacao-tooltip'), function(){
			$(this).tooltipster({
				maxWidth: 240,
				theme: 'tema-tooltipster-custom',
				contentAsHTML: true
			});
		});
	},

	init : function() {

		this.controlar_submenus();
		this.fechar_submenus_click();

		if($('.aviso-marcar-lido').length)
			this.avisos_marcar_como_lido();

		this.tooltip_notificacoes();

	}
};

Moradores.Ocorrencias = {

	registrar_form_submit : function(){
		$('#ocorrencias-registrar-form').submit( function(e){

			var campo_texto = $(this).find('textarea'); //$('#ocorrencias-registrar-form label textarea');

			if(campo_texto.val() == ''){

				campo_texto.parent()
						   .addClass('erro')
						   .addClass('obrigatorio')
						   .find('textarea')
						   .focus();

				e.preventDefault();
				return false;
			}

			if(campo_texto.val().length < 10){

				campo_texto.parent()
						   .addClass('erro')
						   .addClass('minimo')
						   .find('textarea')
						   .focus();

				e.preventDefault();
				return false;
			}
		});
	},

	registrar_form_keyup : function() {
		$('#ocorrencias-registrar-form textarea').on('keyup', function(){
			$(this).parent().removeClass('erro')
							.removeClass('obrigatorio');
		});
	},

	expandir_texto : function() {
		$.each($('.lista-ocorrencias .ocorrencia-texto'), function(){

			var aberto = $(this).find('.erro-resposta-ocorrencia').length > 0 ? true : false;

			if(aberto)
				$(this).addClass('expandido');

			$(this).readmore({
				collapsedHeight: 70,
				speed: 300,
				startOpen : aberto,
				moreLink: "<a href='#' class='ocorrencia-toggle-texto abrir'>ABRIR PARA LER MAIS</a>",
				lessLink: "<a href='#' class='ocorrencia-toggle-texto fechar'>RECOLHER</a>",
				beforeToggle: function(trigger, element, expanded) {
				    if(!expanded) { // Texto expandido
				    	$(element).addClass('expandido');
				    }else{
				    	$(element).removeClass('expandido');
				    }
				}
			});
		});
	},

	finalizar_ocorrencia : function() {
		$('.finalizar-ocorrencia input[type=checkbox]').change( function(){

			var ocorrencia_id = $(this).val();
			var container_form = $(this).parent().parent();

			$(this).attr('disabled', true);

			$.post('moradores/livro-de-ocorrencias/finalizar', {
				ocorrencia_id : ocorrencia_id
			}, function(resposta){

				var resposta = "<span class='ocorrencia-finalizada apagado'>Assunto finalizado em: "+resposta.finalizada_em+" pelo "+resposta.finalizada_por+"</span>";

				// desativar form
				// esconder textarea
				// esconder submit
				// trocar conteúdo de .finalizar-ocorrencia
				// Reaplicar .readmore()

				container_form.attr('action', '');
				container_form.find('textarea').fadeOut('normal');
				container_form.find('input[type=submit]').fadeOut('normal', function(){
					container_form.find('.finalizar-ocorrencia').html(resposta);
					container_form.find('.apagado').removeClass('apagado');
					container_form.parent().parent().readmore({
						collapsedHeight: 70,
						speed: 300,
						startOpen : true,
						moreLink: "<a href='#' class='ocorrencia-toggle-texto abrir'>ABRIR PARA LER MAIS</a>",
						lessLink: "<a href='#' class='ocorrencia-toggle-texto fechar'>RECOLHER</a>",
						beforeToggle: function(trigger, element, expanded) {
						    if(!expanded) { // Texto expandido
						    	$(element).addClass('expandido');
						    }else{
						    	$(element).removeClass('expandido');
						    }
						}
					});
				});

			});
		});
	},

	init : function(){
		this.registrar_form_submit();
		this.registrar_form_keyup();
		this.expandir_texto();
		this.finalizar_ocorrencia();
	}
};
Moradores.MoradoresDaUnidade = {

	form_datepicker : function(){
		var picker = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('moradores-da-unidade-form-data'),
			format: 'DD/MM/YYYY',
			yearRange: [1900, 2050],
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			onSelect : function(){
				$('#moradores-da-unidade-form-data').parent()
													.removeClass('input-erro')
													.removeClass('obrigatorio');
			}
		});
	},

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#moradores-da-unidade-form-nome',
				'#moradores-da-unidade-form-visibilidade',
				'#moradores-da-unidade-form-data',
				'#moradores-da-unidade-form-parentesco',
				//'#moradores-da-unidade-form-foto'
			],
			form_com_imagem   : true,
			multiplas_imagens : false,
			diretorio_imagens : 'moradores-da-unidade'
		});

		this.form_datepicker();

	}
};

Moradores.VeiculosDaUnidade = {

	mercoSulMaskBehavior : function (val) {
        var myMask = 'AAA0A00';
        var mercosul = /([A-Za-z]{3}[0-9]{1}[A-Za-z]{1})/;
        var normal = /([A-Za-z]{3}[0-9]{2})/;
        var replaced = val.replace(/[^\w]/g, '');
        if (normal.exec(replaced)) {
            myMask = 'AAA-0000';
        } else if (mercosul.exec(replaced)) {
            myMask = 'AAA0A00';
        }
        return myMask;
	},
	
	mercoSulOptions : {
		placeholder : 'placa',
		clearIfNotMatch: true,
		onKeyPress: function(val, e, field, options) {
			field.val(val.toUpperCase())
			field.mask(Moradores.VeiculosDaUnidade.mercoSulMaskBehavior.apply({}, arguments), Moradores.VeiculosDaUnidade.mercoSulOptions);
		}
	},

	form_mascara_placa : function() {
		field = '#veiculos-da-unidade-form-placa'
		$(field).bind('paste', function(e) {
			$(this).unmask();
        });
        $(field).bind('input', function(e) {
			$(field).mask(Moradores.VeiculosDaUnidade.mercoSulMaskBehavior, Moradores.VeiculosDaUnidade.mercoSulOptions);
        });
	},

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#veiculos-da-unidade-form-veiculo',
				'#veiculos-da-unidade-form-visibilidade',
				'#veiculos-da-unidade-form-cor',
				'#veiculos-da-unidade-form-placa'
			],
			form_com_imagem : false
		});

		this.form_mascara_placa();

	}
};

Moradores.AnimaisDeEstimacao = {

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#animais-de-estimacao-form-nome',
				'#animais-de-estimacao-form-visibilidade',
				'#animais-de-estimacao-form-especie',
				'#animais-de-estimacao-form-sexo',
				'#animais-de-estimacao-form-foto'
			],
			form_com_imagem   : true,
			multiplas_imagens : false,
			diretorio_imagens : 'animais-de-estimacao'
		});

	}
};
Moradores.FaleCom = {

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#fale-com-assunto',
				'#fale-com-mensagem'
			],
			form_com_imagem : false
		});

	}
};
Moradores.FAQ = {

	toggle_resposta : function(){
		$('#lista-faq .questao').click( function(e){
			e.preventDefault();

			$(this).parent().toggleClass('ativo');
		});
	},

	init : function() {
		this.toggle_resposta();
	}
};
Moradores.AgendamentoDeMudanca = {

	form_datepicker : function(){
		if($('#agendamento-data').length){
			var picker = new Pikaday({
				theme: 'gallery-theme',
				field: document.getElementById('agendamento-data'),
				format: 'DD/MM/YYYY',
				i18n: {
				    previousMonth : 'Mês Anterior',
				    nextMonth     : 'Próximo Mês',
				    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
				    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
				    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
				},
				onSelect : function(){
					$('#agendamento-data').parent()
										  .removeClass('input-erro')
										  .removeClass('obrigatorio');
				}
			});
		}
	},

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#agendamento-data',
				'#agendamento-periodo'
			],
			form_com_imagem : false
		});

		this.form_datepicker();

	}
};
Moradores.ChamadosDeManutencao = {

	form_datepicker : function(){
		if($('#chamados-de-manutencao-form-data').length){
			var picker = new Pikaday({
				theme: 'gallery-theme',
				field: document.getElementById('chamados-de-manutencao-form-data'),
				format: 'DD/MM/YYYY',
				i18n: {
				    previousMonth : 'Mês Anterior',
				    nextMonth     : 'Próximo Mês',
				    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
				    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
				    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
				},
				onSelect : function(){
					$('#chamados-de-manutencao-form-data').parent()
														.removeClass('input-erro')
														.removeClass('obrigatorio');
				}
			});
		}
	},

	init : function(){

		this.form_datepicker()

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#chamados-de-manutencao-form-data',
				'#chamados-de-manutencao-form-horario',
				'#chamados-de-manutencao-form-localizacao',
				'#chamados-de-manutencao-form-descricao'
			],
			form_com_imagem   : true,
			multiplas_imagens : true,
			identificador_sessao : 'chamados_de_manutencao',
			diretorio_imagens : 'chamados-de-manutencao'
		});

	}
};
Moradores.ReservaDeEspacos = {

	is_periodo : false,
	diarias_bloqueadas : [],
	is_convidados : false,

	form_datepicker : function(){
		var picker = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('datepicker'),
			format: 'DD/MM/YYYY',
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			minDate: new Date(),
			bound: false,
			container: document.getElementById('calendario'),
			onSelect: function() {

				// Lógica para buscar as reservas e retornar o grid
				// Só é válido para reservas por períodos

				if(Moradores.ReservaDeEspacos.is_periodo){

					var tabela_grid = $('table.tabela-horarios');

					var url = 'moradores/reserva-de-espacos/grid-por-periodo/';
					url += this.getMoment().format('YYYY-MM-DD');
					url += '/';
					url += tabela_grid.attr('data-slug-espaco');

					$.ajax(url, {
          	beforeSend : function(){

          		tabela_grid.animate({
          			'opacity': 0
          		}, 400);

          	},
          	success : function(resultado){

          		setTimeout( function(){

            		tabela_grid.html(resultado);

            		tabela_grid.animate({
            			'opacity': 1
            		}, 400);

          		}, 400);
          	}
          });
				}
	    },
      onOpen: function(){
      	if(Moradores.ReservaDeEspacos.is_periodo){

        	$('#navegacao-por-dia').click( function(e){

						var moment = picker.getMoment();

						e.preventDefault();
						moment.add(2, 'day');
						picker.setDate(moment.format('YYYY-MM-DD'));
						
					});

      	}
      },
      disableDayFn: function(data){
      	var data_moment = moment(data);
      	return Moradores.ReservaDeEspacos.diarias_bloqueadas.indexOf(data_moment.format('YYYY-MM-DD')) !== -1;
      }
		});
	},

	marcar_dia : function(){
		$(document).on('click', 'tr td label', function(){
			var input = $(this).find("input[type=checkbox]");
			if(input.is(':checked')){
				$(this).addClass('selecionado');

				var data = $(this).attr('data-formatada');
				var ini = $(this).attr('data-horario-inicio');
				var fim = $(this).attr('data-horario-fim');

				Moradores.ReservaDeEspacos.abrir_modal_anotacoes_form(data,ini,fim);
			}else{
				$(this).removeClass('selecionado');
			}
		});
	},

	abrir_modal_anotacoes_form: function(data,ini,fim){

		var template = "";
		template += "<div id='modal-reservar-espacos'>";
			template += "<div class='informacoes-reserva'>";
				template += "Confirmar agendamento para:<br>";
				template += "<strong>"+data+"</strong><br>";
				template += "<em>início: <strong>"+ini+"</strong> | fim: <strong>"+fim+"</strong></em>";
			template += "</div>";
			template += "<p>ANOTAÇÕES SOBRE O AGENDAMENTO:</p>";
			template += "<textarea id='textarea-anotacoes'></textarea>";
			template += "<div class='botoes'>";
				template += "<button id='enviar-form-reserva-espacos' class='btn btn-info'>SIM</button>";
				template += "<button id='fechar-shadow-anotacoes' class='btn btn-info'>NÃO</button>";
			template += "</div>";
		template += "</div>";

		$.featherlight(template, {
			otherClose : '#fechar-shadow-anotacoes',
			afterContent : function(e){

				$('textarea')[0].focus();

				$('#enviar-form-reserva-espacos').click( function(){

					// Salvar conteudo do textarea na página do form antes de enviar
					var anotacoes = $('#textarea-anotacoes').val();
					$('#input-anotacoes').val(anotacoes);

					Moradores.ReservaDeEspacos.enviar_formulario();
				});
			},
			afterClose : function(e){
				var marcado = $('label.selecionado');
				var checkbox = marcado.find('input');

				marcado.removeClass('selecionado');
				checkbox.attr('checked', false);
			}
		});

	},

	enviar_formulario: function(){
		$('#grid-por-periodo > form').submit();
	},

	abrir_modal_anotacoes_visualizacao: function(){
		$(document).on('click', '.btn-visualizar-reserva', function(e){
			e.preventDefault();

			var data = $(this).attr('data-formatada');
			var h_inicio = $(this).attr('data-horario-inicio');
			var h_termino = $(this).attr('data-horario-fim');
			var anotacoes = $(this).attr('data-anotacoes');

			var template = "";
			template += "<div id='modal-reservar-espacos'>";
				template += "<div class='informacoes-reserva'>";
					template += "Agendamento confirmado para:<br>";
					template += "<strong>"+data+"</strong><br>";
					template += "<em>início: <strong>"+h_inicio+"</strong> | fim: <strong>"+h_termino+"</strong></em>";
				template += "</div>";
				template += "<p>ANOTAÇÕES SOBRE O AGENDAMENTO:</p>";
				template += "<div class='anotacoes-gravadas'>"+anotacoes+"</div>";
				template += "<div class='botoes'>";
					template += "<button id='fechar-shadow-anotacoes' class='btn btn-info'>FECHAR</button>";
				template += "</div>";
			template += "</div>";

			$.featherlight(template, {
				afterContent : function(e){
					$('#fechar-shadow-anotacoes').click( function(){
						$.featherlight.current().close();
					});
				}
			});
		});
	},

	toggle_textos_confirmacao: function(){
		var botao = $(".swap-texto .inicial input[type='button']");
		var txt_inicial = $(".swap-texto .inicial");
		var txt_final = $(".swap-texto .final");

		if(botao.length > 0){

			botao.click( function(e){
				e.preventDefault();
				txt_inicial.fadeOut('normal', function(){
					txt_final.fadeIn('normal');
				});
			});

		}
	},

	enviar_form_confirmacao_reserva: function(){
		$('#form-confirmar-reserva').submit( function(e){

			var aceite = $('#aceite_input');

			if(!aceite.is(':checked')){
				var mensagem = "<p class='retorno_formulario erro'>";
				mensagem += "É necessário aceitar os termos para confirmar a reserva.";
				mensagem += "</p>";

				$('#erros-formulario').html(mensagem).show('normal');
				e.preventDefault();
				return false;
			}

		});
	},

	cancelar_diaria: function(){
		$('.btn-cancelar-diaria').click( function(e){
			e.preventDefault();

			var regras_cancelamento;
			var rid = $(this).attr('data-rid');

			$.ajax('moradores/reserva-de-espacos/verificar-multa/'+rid,
			{
				success : function(regras_cancelamento){
					var template = "";
					template += "<div id='modal-cancelar-diaria'>";

					if(regras_cancelamento.tem_multa){

						template += "<p class='p-espacado'>";
							template += "O cancelamento desta reserva nesta data acarreta cobrança de multa por estar dentro do ";
							template += "período inferior a " + regras_cancelamento.dias_carencia + " dias de antecedência.";
						template += "</p>";

						template += "<p>";
							template += "Se confirmado o cancelamento a multa constará em seu boleto do condomínio do próximo mês."
						template += "</p>";

					}else{

						template += "<p>";
							template += "O cancelamento desta reserva nesta data <strong>não</strong> acarreta cobrança de multa.";
						template += "</p>";

					}

					template += "<hr>";
					template += "<h2>CONFIRMAR O CANCELAMENTO DA RESERVA?</h2>";

					template += "<div class='botoes'>";
						template += "<a href='moradores/reserva-de-espacos/reservar/" + regras_cancelamento.slug_espaco + "/remover/" + regras_cancelamento.timestamp_reserva + "' title='Confirmar Cancelamento' class='btn btn-info'>SIM</a>";
						template += "<a href='#' title='Desistir do Cancelamento' class='btn btn-info' id='fechar-cancelar-reserva'>NÃO</a>";
					template += "</div>";

					template += "</div>";

					$.featherlight(template, {
						variant : 'cancelamento_diaria',
						otherClose : '#fechar-cancelar-reserva'
					});

				}
			});


		});
	},

	verificar_envio_form_diaria: function(){
		$('#form-selecao-data-diaria').submit( function(e){

			$('#mensagens-erro').html('');

			if($('#datepicker').val() == ''){

				var aviso = $("<p class='retorno_formulario erro'>Informe a Data da Festa</p>");

				$('#mensagens-erro').html($(aviso));

				setTimeout( function(){
					aviso.fadeOut('fast', function(){
						aviso.remove();
					});
				}, 3000);

				e.preventDefault();
				return false;
			}

			if($('#input-titulo-festa').val() == ''){

				var aviso = $("<p class='retorno_formulario erro'>Informe um Título para a Festa</p>");

				$('#mensagens-erro').html($(aviso));

				setTimeout( function(){
					aviso.fadeOut('fast', function(){
						aviso.remove();
					});
				}, 3000);

				e.preventDefault();
				return false;
			}

			if($('#input-horario-festa').val() == ''){

				var aviso = $("<p class='retorno_formulario erro'>Informe o Horário aproximado da Festa</p>");

				$('#mensagens-erro').html($(aviso));

				setTimeout( function(){
					aviso.fadeOut('fast', function(){
						aviso.remove();
					});
				}, 3000);

				e.preventDefault();
				return false;
			}

		});
	},

	gerenciar_convidados : function(){
		this.focar_input_convidados();
		this.inserir_lista_colada();
		this.inserir_no_enter();
		this.remover_convidado();
	},

	focar_input_convidados : function(){
		document.getElementById('convidados-textarea').focus();
	},

	inserir_lista_colada: function(){
		$("#convidados-textarea").on('paste', function (e) {
			setTimeout( function(){
				Moradores.ReservaDeEspacos.inserir_convidado(false);
			}, 100);
	    });
	},

	inserir_no_enter: function(){
		$("#convidados-textarea").on('keyup', function (e) {
			if(e.which == 13){
				Moradores.ReservaDeEspacos.inserir_convidado(true);
			}
	    });
	},

	inserir_convidado: function(prepend){
		var lista = $("#convidados-textarea").val().split('\n');
		var nro_nomes = lista.length;

		for (var i = 0; i < nro_nomes; i++) {
			if(lista[i] != ''){

				var item = $("<li style='display: none;'>"+lista[i]+" <a href='#' class='btn btn-danger btn-mini'>x</a><input type='hidden' name='lista_convidados[]' value='"+lista[i]+"'></li>");

				if(prepend)
					$('#convidados-lista').prepend(item);
				else
					$('#convidados-lista').append(item);

				item.fadeIn('normal');
			}
		};

		$("#convidados-textarea").val('');
	},

	remover_convidado: function(){
		$(document).on('click', '#convidados-lista > li > a', function(e){

			e.preventDefault();

			var convidado = $(this).parent();
			convidado.fadeOut('normal', function(){
				convidado.remove();
			});
		});
	},

	init: function(){

		this.is_periodo = $('#grid-por-periodo').length > 0;
		this.diarias_bloqueadas = $('#diarias_bloqueadas_json').length > 0 ? $('#diarias_bloqueadas_json').val() : [];
		this.is_convidados = $("#convidados-textarea").length;

		this.form_datepicker();

		if(this.is_convidados)
			this.gerenciar_convidados();

		if(this.is_periodo){

			this.marcar_dia();
			this.abrir_modal_anotacoes_visualizacao();

		}else{

			this.toggle_textos_confirmacao();
			this.enviar_form_confirmacao_reserva();
			this.cancelar_diaria();

			this.verificar_envio_form_diaria();

		}


	}
};
Moradores.PessoasAutorizadas = {

	form_datepicker : function(){
		var picker = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('pessoas-autorizadas-form-data'),
			format: 'DD/MM/YYYY',
			yearRange: [1900, 2050],
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			onSelect : function(){
				$('#pessoas-autorizadas-form-data').parent()
													.removeClass('input-erro')
													.removeClass('obrigatorio');
			}
		});
	},

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#pessoas-autorizadas-form-nome',
				'#pessoas-autorizadas-form-visibilidade',
				'#pessoas-autorizadas-form-data',
				'#pessoas-autorizadas-form-parentesco',
				//'#pessoas-autorizadas-form-foto'
			],
			form_com_imagem   : true,
			multiplas_imagens : false,
			diretorio_imagens : 'pessoas-autorizadas'
		});

		this.form_datepicker();

	}
};

Moradores.PessoasNaoAutorizadas = {

	form_datepicker : function(){
		var picker = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('pessoas-nao-autorizadas-form-data'),
			format: 'DD/MM/YYYY',
			yearRange: [1900, 2050],
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			onSelect : function(){
				$('#pessoas-nao-autorizadas-form-data').parent()
													.removeClass('input-erro')
													.removeClass('obrigatorio');
			}
		});
	},

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#pessoas-nao-autorizadas-form-nome',
				'#pessoas-nao-autorizadas-form-visibilidade',
				'#pessoas-nao-autorizadas-form-data',
				'#pessoas-nao-autorizadas-form-parentesco'
			],
			form_com_imagem   : true,
			multiplas_imagens : false,
			diretorio_imagens : 'pessoas-nao-autorizadas'
		});

		this.form_datepicker();

	}
};
Moradores.PrestadoresDeServico = {

	form_datepickers : function(){

		var datas = [
			'prestadores-de-servico-form-data_nascimento',
			'prestadores-de-servico-form-permissao_inicio',
			'prestadores-de-servico-form-permissao_termino'
		];

		var pickers = [];

		for (var i = datas.length - 1; i >= 0; i--) {

			id = datas[i];

			pickers[i] = new Pikaday({
				theme: 'gallery-theme',
				field: document.getElementById(id),
				format: 'DD/MM/YYYY',
				yearRange: [1900, 2050],
				i18n: {
				    previousMonth : 'Mês Anterior',
				    nextMonth     : 'Próximo Mês',
				    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
				    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
				    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
				},
				onSelect : function(){
					$(id).parent()
						 .removeClass('input-erro')
						 .removeClass('obrigatorio');
				}
			});
		};

	},

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#prestadores-de-servico-form-nome',
				'#prestadores-de-servico-form-visibilidade',
				'#prestadores-de-servico-form-documento',
				// '#prestadores-de-servico-form-empresa',
				// '#prestadores-de-servico-form-data_nascimento',
				'#prestadores-de-servico-form-permissao_inicio',
				'#prestadores-de-servico-form-permissao_termino'
			],
			form_com_imagem   : false
		});

		this.form_datepickers();

	}
};
Moradores.MeuPerfil = {

	register_tooltip_foto : function() {
		var tooltip_foto = $('.addon-tooltip-foto');
		var conteudo = "";
		conteudo += "<strong>POR QUE ESSA INFORMAÇÃO É SOLICITADA?</strong>";
		conteudo += "<p>";
			conteudo += "A foto é importante para que novos funcionários possam reconhecer você no acesso ao prédio. ";
			conteudo += "Atente-se para usar uma imagem que se pareça com ";
			conteudo += "você no dia a dia e facilitar o reconhecimento. Usar ";
			conteudo += "uma foto com muita maquiagem quando ";
			conteudo += "geralmente você não se maqueia, ou com chapéu ";
			conteudo += "ou boné podem dificultar a identificação.";
		conteudo += "</p>";
		$.each(tooltip_foto, function(){
			var title_original = $(this).attr('title');
			$(this).find('img').attr('title', '');
			$(this).find('img').attr('data-old-title', title_original);
			$(this).tooltipster({
				content: $(conteudo),
				maxWidth: 240,
				theme: 'tema-tooltipster-custom'
        	});
		});
	},

	register_tooltip_visibilidade : function() {
		var tooltip_visib = $('.addon-tooltip-visib');
		$.each(tooltip_visib, function(){
			var title_original = $(this).attr('title');
			$(this).find('img').attr('title', '');
			$(this).find('img').attr('data-old-title', title_original);
			$(this).tooltipster({
				content: $("<strong>SELECIONAR PARA QUEM ESSA INFORMAÇÃO SERÁ EXIBIDA.</strong>"),
				maxWidth: 180,
				theme: 'tema-tooltipster-custom'
        	});
		});
	},

	register_shadow_email : function() {
		$('.register-form-set_visibilidade.visib-email').click( function(e){
			e.preventDefault();
			Moradores.Auth.register_abrir_shadow_visibilidade('Endereço de E-mail', $('#register-form-visibilidade_email'));
		});
	},

	register_shadow_telfixo : function() {
		$('.register-form-set_visibilidade.visib-tel_fixo').click( function(e){
			e.preventDefault();
			Moradores.Auth.register_abrir_shadow_visibilidade('Telefone Fixo', $('#register-form-visibilidade_tel-fixo'));
		});
	},

	register_shadow_telcel : function() {
		$('.register-form-set_visibilidade.visib-tel_cel').click( function(e){
			e.preventDefault();
			Moradores.Auth.register_abrir_shadow_visibilidade('Telefone Celular', $('#register-form-visibilidade_tel-cel'));
		});
	},

	register_shadow_telcom : function() {
		$('.register-form-set_visibilidade.visib-tel_com').click( function(e){
			e.preventDefault();
			Moradores.Auth.register_abrir_shadow_visibilidade('Telefone Comercial', $('#register-form-visibilidade_tel-com'));
		});
	},

	register_abrir_shadow_visibilidade : function(titulo, input) {

		var template = "";
		template += "<div id='register-visibilidade-shadow'>";
			template += "<h2>"+titulo+"</h2>";
			template += "<p>SELECIONAR PARA QUEM ESSA INFORMAÇÃO SERÁ EXIBIDA DENTRO DO SISTEMA:</p>";
			template += "<ul>";
				template += "<li><label><input type='checkbox' value='1' class='check-marcar-todos'> Todos (moradores, corpo diretivo, zelador)</label></li>";
				template += "<li><label class='marcado'><input type='checkbox' value='2' checked id='check-marcar-admin'> Somente síndico e conselho</label></li>";
				// template += "<li><label><input type='checkbox' value='3'> Portaria</label></li>";
				template += "<li><label><input type='checkbox' value='4'> Zelador</label></li>";
				template += "<li><label><input type='checkbox' value='5' id='check-marcar-moradores'> Todos os moradores</label></li>";
				template += "<li><label><input type='checkbox' value='6' id='check-marcar-amigos'> Somente moradores selecionados como amigos</label></li>";
			template += "</ul>";
			template += "<button id='fechar-visibilidade-shadow'>CONFIRMAR</button>";
		template += "</div>";

		$.featherlight(template, {
			afterContent : function(e){

				var inputs = $(".featherlight-inner input[type='checkbox']");
				var valor_atual = input.val();

				if(valor_atual.indexOf(',') !== -1){

					var valores_atuais = valor_atual.split(',')

					$.each(inputs, function(){
						if($.inArray($(this).val(), valores_atuais) !== -1){
							$(this).attr('checked', 'checked');
							$(this).parent().addClass('marcado');
						}
					});

				}else if(valor_atual != ''){
					$.each(inputs, function(){
						if($(this).val() == valor_atual){
							$(this).attr('checked', 'checked');
							$(this).parent().addClass('marcado');
						}
					});
				}

				$("#register-visibilidade-shadow .check-marcar-todos").click( function(){
					// Ao clicar em 'marcar todos', marcar ou desmarcar todos os checkboxes
					// EXCETO o de só amigos e o da administração (esse sempre ativo)
					$("#register-visibilidade-shadow input[type=checkbox]").not('#check-marcar-amigos')
																		   .not('#check-marcar-admin')
																		   .prop('checked', this.checked);

					// Sempre DESmarcar o de 'só amigos'
					$('#check-marcar-amigos').prop('checked', false);
					$('#check-marcar-amigos').parent().removeClass('marcado');

					if(this.checked){
						$("#register-visibilidade-shadow label input").not('#check-marcar-amigos')
																	  .not('#check-marcar-admin')
																	  .parent()
																	  .addClass('marcado');
					}else{
						$("#register-visibilidade-shadow label input").not('#check-marcar-amigos')
																	  .not('#check-marcar-admin')
																	  .parent()
																	  .removeClass('marcado');
					}
				});

				$("#register-visibilidade-shadow input[type=checkbox]").not(".check-marcar-todos").click( function(e){

					if($(this).val() == '2'){ // Sempre visível pra administração
						e.preventDefault();
						return false;
					}

					if(!this.checked){
						$("#register-visibilidade-shadow .check-marcar-todos").prop('checked', false);
						$("#register-visibilidade-shadow .check-marcar-todos").parent().removeClass('marcado');
						$(this).parent().removeClass('marcado');
					}else{
						$(this).parent().addClass('marcado');

						if($(this).val() == '5'){
							// Se value = 5 (todos os moradores)
							// Desmarca o 6 (somente amigos)
							$('#check-marcar-amigos').prop('checked', false).parent().removeClass('marcado');
						}else if($(this).val() == '6'){
							// Se value = 6 (somente amigos)
							// Desmarca o 5 (todos os moradores)
							$('#check-marcar-moradores').prop('checked', false).parent().removeClass('marcado');
						}
					}
				});

				$('#fechar-visibilidade-shadow').click( function(){
					Moradores.Auth.register_salvar_visibilidade(input);
					$.featherlight.current().close();
				});
			}
		});
	},

	register_salvar_visibilidade : function(input) {
		var marcados = $(".featherlight-inner input[type='checkbox']:checked");
		var icone = input.parent().find('.register-form-set_visibilidade');
		var valores = Array();

		if(marcados.length > 0){
			$.each(marcados, function(){
				valores.push($(this).val());
			});
			valores = valores.join(',');
		}else{
			valores = '';
		}

		input.val(valores);

		input.parent().removeClass('input-erro')
					  .removeClass('obrigatorio');

		if(input.val() == '')
			icone.removeClass('on').addClass('off');
		else
			icone.removeClass('off').addClass('on');
	},

	register_marcar_icone_visibilidade : function() {
		var inputs_visibilidade = [
			'#register-form-visibilidade_email',
			'#register-form-visibilidade_tel-fixo',
			'#register-form-visibilidade_tel-cel',
			'#register-form-visibilidade_tel-com'
		];
		for (var i = 0; i < inputs_visibilidade.length; i++) {
			input = $(inputs_visibilidade[i]);
			if(input.val() != ''){
				input.parent().find('.addon-tooltip-visib')
							  .removeClass('off')
							  .addClass('on');
			}
		};
	},

	register_remover_erro_keyup : function() {
		$('.input-erro input').keyup( function(){
			$(this).parent().removeClass('input-erro')
							.removeClass('obrigatorio')
							.removeClass('email-invalido')
							.removeClass('confirmacao-senha-invalida')
							.removeClass('email-em-uso');
		});
	},

	register_carregar_foto_onload : function() {
		var file = $('#register-form-foto').val();
		if($('#register-form-foto').length && file != ''){
			var foto = new Image;
    		var target = $('.register-foto-placeholder label');
    		foto.onload = function(){
    			target.append(foto);
    		};
    		foto.src = 'assets/images/moradores/fotos/thumbs/'+file;
		}
	},

	register_foto_upload : function() {
		$('#register-form-fileupload').fileupload({
			url: 'moradores/meu-perfil/upload-foto',
			type: 'post',
	        dataType: 'json',
	        send: function(){
	        	Moradores.Auth.register_foto_upload_esconder_erro();
	        	$('.register-foto-placeholder label').addClass('enviando');
	        },
	        progress: function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('#upload-progresso .barra').css(
		            'width',
		            progress + '%'
		        );
		    },
	        done: function (e, data) {
	        	$('.register-foto-placeholder label').removeClass('enviando');
	        	if(data.result.success == 1){
	        		var foto = new Image;
	        		var target = $('.register-foto-placeholder label');
	        		$('#register-form-foto').val(data.result.filename);
	        		foto.onload = function(){
	        			target.append(foto);
	        		};
	        		foto.src = data.result.thumb;
	        	}else{
	        		Moradores.Auth.register_foto_upload_mostar_erro(data.result.msg);
	        		setTimeout( function(){
	        			Moradores.Auth.register_foto_upload_esconder_erro();
	        		}, 2000);
	        	}
	        }
	    });
	},

	register_foto_upload_mostar_erro: function(msg) {
		$('.register-foto-placeholder label').addClass('input-erro');
	    $('.register-foto-placeholder label p').html(msg);
	    $('#register-form-foto').val('');
	    $('.register-foto-placeholder label img').fadeOut('normal', function(){
	    	$(this).remove();
	    })
	},

	register_foto_upload_esconder_erro: function() {
		$('.register-foto-placeholder label').removeClass('input-erro');
	    $('.register-foto-placeholder label p').html($('.register-foto-placeholder label p').attr('data-html-original'));
	},

	register_form_submit : function() {
		$('#register-form form').submit( function(e) {

			var inputs_obrigatorios = [
				'#register-form-numero_unidade',
				'#register-form-bloco',
				'#register-form-nome_proprietario',
				'#register-form-nome',
				'#register-form-apelido',
				'#register-form-relacao_unidade',
				'#register-form-data_mudanca',
				'#register-form-email',
				'#register-form-visibilidade_email',
				'#register-form-telefone_fixo',
				'#register-form-visibilidade_tel-fixo',
				'#register-form-telefone_celular',
				'#register-form-visibilidade_tel-cel',
				'#register-form-telefone_comercial',
				'#register-form-visibilidade_tel-com',
				'#register-form-foto'
			];

			for (var i = 0; i < inputs_obrigatorios.length; i++) {

				var check = $(inputs_obrigatorios[i]);

				if(!check.val()){

					check.parent().addClass('input-erro')
								  .addClass('obrigatorio');

					check.focus().on('keyup', function(){
								  	check.parent().removeClass('input-erro')
								  				  .removeClass('obrigatorio');
								  });

					e.preventDefault();
					return false;
				}
			};

			var campo = $('#register-form-email');
			var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			var email = campo.val();
    		if(!regex.test(email)){

    			campo.parent().addClass('input-erro')
							  .addClass('email-invalido');

				campo.focus().on('keyup', function(){
							  	campo.parent().removeClass('input-erro')
							  				  .removeClass('email-invalido');
							  });

				e.preventDefault();
				return false;
    		}

		});
	},

	register_form_pikaday : function() {

		var picker1 = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('register-form-data_mudanca'),
			format: 'DD/MM/YYYY',
			yearRange: [1900, 2050],
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			onSelect : function(){
				$('#register-form-data_mudanca').parent()
												.removeClass('input-erro')
												.removeClass('obrigatorio');
			}
		});

		var picker2 = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('register-form-data_nascimento'),
			format: 'DD/MM/YYYY',
			yearRange: [1900, 2050],
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			onSelect : function(){
				$('#register-form-data_nascimento').parent()
												.removeClass('input-erro')
												.removeClass('obrigatorio');
			}
		});

	},

	mascara_telefones : function(){
		var telefones = '#register-form-telefone_fixo, #register-form-telefone_celular, #register-form-telefone_comercial';
		$(telefones).mask('(00) 00000-0000');
	},

	init : function() {
		this.register_tooltip_foto();
		this.register_tooltip_visibilidade();
		this.register_shadow_email();
		this.register_shadow_telfixo();
		this.register_shadow_telcel();
		this.register_shadow_telcom();
		this.register_foto_upload();
		this.register_form_submit();
		this.register_marcar_icone_visibilidade();
		this.register_remover_erro_keyup();
		this.register_carregar_foto_onload();
		this.register_form_pikaday();
		this.mascara_telefones();
	}
};

Moradores.AlterarSenha = {

	alterar_senha_form_submit : function() {
		$('#alterar-senha-main form').submit( function(e) {

			var inputs_obrigatorios = [
				'#register-form-senha-atual',
				'#register-form-senha-nova',
				'#register-form-confirmacao_senha'
			];

			for (var i = 0; i < inputs_obrigatorios.length; i++) {

				var check = $(inputs_obrigatorios[i]);

				if(!check.val()){

					check.parent().addClass('input-erro')
								  .addClass('obrigatorio');

					check.focus().on('keyup', function(){
								  	check.parent().removeClass('input-erro')
								  				  .removeClass('obrigatorio');
								  });

					e.preventDefault();
					return false;
				}
			};

			var input_senha = $('#register-form-senha-nova');
    		var input_conf_senha = $('#register-form-confirmacao_senha');
    		if(input_senha.val() != input_conf_senha.val()){

    			input_senha.parent().addClass('input-erro')
							  		.addClass('confirmacao-senha-invalida');

				input_senha.focus().on('keyup', function(){
							  		input_senha.parent().removeClass('input-erro')
							  				   .removeClass('confirmacao-senha-invalida');
							  	});

				e.preventDefault();
				return false;
    		}

		});
	},

	init : function() {
		this.alterar_senha_form_submit();
	}
};
Moradores.InstrucoesDeUso = {

	toggle_resposta : function(){
		$('#lista-instrucoes-de-uso .questao').click( function(e){
			e.preventDefault();

			$(this).parent().toggleClass('ativo');
		});
	},

	init : function() {
		this.toggle_resposta();
	}
};
Moradores.FestasParticulares = {

	diarias_bloqueadas : [],

	form_datepicker : function(){
		var picker = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('datepicker'),
			format: 'DD/MM/YYYY',
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			minDate: new Date(),
			bound: false,
			container: document.getElementById('calendario'),
			disableDayFn: function(data){
	        	var data_moment = moment(data);
	        	return Moradores.FestasParticulares.diarias_bloqueadas.indexOf(data_moment.format('YYYY-MM-DD')) !== -1;
	        }
		});
	},

	verificar_envio_form_diaria: function(){
		$('#form-selecao-data-festa-particular').submit( function(e){

			$('#mensagens-erro').html('');

			if($('#datepicker').val() == ''){

				var aviso = $("<p class='retorno_formulario erro'>Informe a Data da Festa</p>");

				$('#mensagens-erro').html($(aviso));

				setTimeout( function(){
					aviso.fadeOut('fast', function(){
						aviso.remove();
					});
				}, 3000);

				e.preventDefault();
				return false;
			}

			if($('#input-titulo-festa').val() == ''){

				var aviso = $("<p class='retorno_formulario erro'>Informe um Título para a Festa</p>");

				$('#mensagens-erro').html($(aviso));

				setTimeout( function(){
					aviso.fadeOut('fast', function(){
						aviso.remove();
					});
				}, 3000);

				e.preventDefault();
				return false;
			}

		});
	},

	gerenciar_convidados : function(){
		this.focar_input_convidados();
		this.inserir_lista_colada();
		this.inserir_no_enter();
		this.remover_convidado();
	},

	focar_input_convidados : function(){
		document.getElementById('convidados-textarea').focus();
	},

	inserir_lista_colada: function(){
		$("#convidados-textarea").on('paste', function (e) {
			setTimeout( function(){
				Moradores.FestasParticulares.inserir_convidado(false);
			}, 100);
	    });
	},

	inserir_no_enter: function(){
		$("#convidados-textarea").on('keyup', function (e) {
			if(e.which == 13){
				Moradores.FestasParticulares.inserir_convidado(true);
			}
	    });
	},

	inserir_convidado: function(prepend){
		var lista = $("#convidados-textarea").val().split('\n');
		var nro_nomes = lista.length;

		for (var i = 0; i < nro_nomes; i++) {
			if(lista[i] != ''){

				var item = $("<li style='display: none;'>"+lista[i]+" <a href='#' class='btn btn-danger btn-mini'>x</a><input type='hidden' name='lista_convidados[]' value='"+lista[i]+"'></li>");

				if(prepend)
					$('#convidados-lista').prepend(item);
				else
					$('#convidados-lista').append(item);

				item.fadeIn('normal');
			}
		};

		$("#convidados-textarea").val('');
	},

	remover_convidado: function(){
		$(document).on('click', '#convidados-lista > li > a', function(e){

			e.preventDefault();

			var convidado = $(this).parent();
			convidado.fadeOut('normal', function(){
				convidado.remove();
			});
		});
	},

	init : function(){

		this.is_convidados 		= $("#convidados-textarea").length;
		this.diarias_bloqueadas = $('#diarias_bloqueadas_json').length > 0 ? $('#diarias_bloqueadas_json').val() : [];

		this.form_datepicker();
		this.verificar_envio_form_diaria();

		if(this.is_convidados)
			this.gerenciar_convidados();
	}
};
Moradores.Amizades = {

  timeout: '',
  resultados: '',

  registra_keyup: function()
  {
    $('#input-busca-amigos').on('keyup', function(e){

			var resumo = $(this).val().toLowerCase();

      clearTimeout(Moradores.Amizades.timeout);

      Moradores.Amizades.timeout = setTimeout( function(){
        Moradores.Amizades.buscarMorador(resumo);
      }, 200);

		});
  },

  buscarMorador: function(termo)
  {
    if(termo != ''){

      this.mostrarCarregamento();

      $.post('moradores/amizades/buscar', {
        termo: termo
      }, function(resposta){

        setTimeout( function(){
          Moradores.Amizades.resultados = resposta;
          Moradores.Amizades.mostrarResultados();
        }, 1000);

      });

    }else{

      $('#carregando-resultados').fadeOut('normal');
      $('#lista-amizades').fadeOut('normal', function(){
        $('#lista-amizades').html('');
      });

    }
  },

  mostrarCarregamento: function()
  {
    $('#lista-amizades').fadeOut('fast', function(){
      $('#carregando-resultados').fadeIn('normal');
    });
  },

  mostrarResultados: function()
  {
    $('#carregando-resultados').fadeOut('normal', function(){

      Moradores.Amizades.criarLista();

      $('#lista-amizades').fadeIn('normal');
    });
  },

  criarLista: function()
  {
    $('#lista-amizades').html(this.resultados);
  },

  marcarTodosAmigos: function()
  {
    $('#marcar-todos').change( function(){

      var marcar = $(this).is(':checked');
      var checkboxes = document.getElementsByName('amigo_id[]');

      for(var i=0, n=checkboxes.length; i<n; i++)
        checkboxes[i].checked = marcar;

      if(marcar)
        $('#marcar-todos-label').html('Desmarcar todos');
      else
        $('#marcar-todos-label').html('Marcar todos');

    });
  },

  verificarTodosMarcados: function()
  {
    var checkboxes = document.getElementsByName('amigo_id[]');
    var marcados = 0;
    var n = checkboxes.length;

    for(var i=0, n; i<n; i++)
      if(checkboxes[i].checked) marcados++;

    if(marcados == n){
      $('#marcar-todos').prop('checked', true);
      $('#marcar-todos-label').html('Desmarcar todos');
    }else{
      $('#marcar-todos').prop('checked', false);
      $('#marcar-todos-label').html('Marcar todos');
    }
  },

  desmarcarBotaoAoTirarTodos: function()
  {
    $("input[type='checkbox']").not('#marcar-todos').change( function(){
      Moradores.Amizades.verificarTodosMarcados();
    });
  },

  init: function()
  {
    this.registra_keyup();

    this.verificarTodosMarcados();
    this.marcarTodosAmigos();
    this.desmarcarBotaoAoTirarTodos();
  }

};
Moradores.Classificados = {

  init: function()
  {

    Moradores.Formularios.init({
			campos_obrigatorios : [
				'#classificados-form-categoria',
				'#classificados-form-titulo'
			],
			form_com_imagem   : true,
			multiplas_imagens : true,
			identificador_sessao : 'anuncio',
			diretorio_imagens : 'classificados'
		});

  }
};

Moradores.Fornecedores = {

  mostrar_campo_outros: function(select){
    if(select.val() == 'outros'){
      Moradores.Formularios.adicionarCampoObrigatorio('#fornecedores-form-outra_categoria');
      $('#fornecedores-form-label-outra_categoria').fadeIn('normal').css('display', 'block');
    }else{
      Moradores.Formularios.removerCampoObrigatorio('#fornecedores-form-outra_categoria');
      $('#fornecedores-form-label-outra_categoria').fadeOut('normal');
    }
  },

  mostrar_campo_outros_onload: function(){
    Moradores.Fornecedores.mostrar_campo_outros($('#fornecedores-form-categoria'));
  },

  mostrar_campo_outros_onselect: function(){
    $('#fornecedores-form-categoria').on('change', function(){
      Moradores.Fornecedores.mostrar_campo_outros($(this));
    });
  },

  validar_form_avaliacao: function(){
    var form = $('#avaliar-form');

    if(form.length > 0){
      form.on('submit', function(e) {

        if($('#avaliacao-form-texto').val() == ''){

          var campo = $('#avaliacao-form-texto');
          var parent = campo.parent();

          parent.addClass('obrigatorio');

          campo.on('keyup', function(){
            parent.removeClass('obrigatorio');
          });

          e.preventDefault();
          return false;
        }

        if( $("input[name='avaliacao[nota]']:checked").length == 0 ){

          var rating_input = $('#avaliacao-form .rating');

          rating_input.addClass('obrigatorio').on('click', function(){
            rating_input.removeClass('obrigatorio');
          });

          e.preventDefault();
          return false;
        }

      });

    }
  },

  toggle_avaliacao_form: function() {
    $('#toggle-avaliacao-form').on('click', function(e){
      e.preventDefault();
      $('#avaliar').addClass('aberto');
    });
  },

  init : function(){

    if($('#avaliar').length){

      this.validar_form_avaliacao();
      this.toggle_avaliacao_form();

    }else{

      Moradores.Formularios.init({
        campos_obrigatorios : [
          '#fornecedores-form-categoria',
          '#fornecedores-form-nome',
          '#fornecedores-form-avaliacao_texto',
          "fornecedores_avaliacao_nota"
        ],
        form_com_imagem   : true,
        multiplas_imagens : true,
        identificador_sessao : 'fornecedores',
        diretorio_imagens : 'fornecedores'
      });

      this.mostrar_campo_outros_onload();
      this.mostrar_campo_outros_onselect();

    }

  }
};

$('document').ready( function(){

	Moradores.init();

	$('.modulo-inativo').click( function(e){

		e.preventDefault();

		var template = "";
		template += "<div id='modulo-inativo-shadow'>";
			template += "<h2>Módulo não implementado</h2>";
			template += "<p class='p-extra-espaco'>";
				template += "Este módulo ainda não está disponível.";
			template += "</p>";
			template += "<button id='fechar-shadow'>OK</button>";
		template += "</div>";

		$.featherlight(template, {
			otherClose : '#fechar-shadow'
		});
	});

});
