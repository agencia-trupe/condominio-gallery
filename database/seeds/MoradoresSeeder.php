<?php

use Illuminate\Database\Seeder;

class MoradoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('moradores')->delete();

        DB::table('moradores')->insert([
		      'unidades_id' => 1,
          'relacao_unidade' => 'proprietario', // proprietario | locatario
          'data_mudanca' => '2014-09-01',
          'data_nascimento' => '1986-12-06',
          'nome' => 'Bruno Monteiro Gomes',
          'apelido' => 'Bruno',
          'foto' => '2016010513400501 - cmeTLGa.jpg',
          'email' => 'bruno@trupe.net',
          'senha' => bcrypt('senhatrupe'),
          'telefone_fixo' => '(11) 5555-5555',
          'telefone_celular' => '(11) 5555-5555',
          'telefone_comercial' => '(11) 5555-5555',
          'status' => 1,
          'created_at' => date('Y-m-d'),
          'is_teste' = 1
        ]);

        DB::table('moradores')->insert([
            'unidades_id' => 37,
            'relacao_unidade' => 'proprietario', // proprietario | locatario
            'data_mudanca' => '2014-09-01',
            'data_nascimento' => '1986-12-06',
            'nome' => 'Leila Arruda',
            'apelido' => 'Leila',
            'foto' => '',
            'email' => 'leila@trupe.net',
            'senha' => bcrypt('senhatrupe'),
            'telefone_fixo' => '(11) 5555-5555',
            'telefone_celular' => '(11) 5555-5555',
            'telefone_comercial' => '(11) 5555-5555',
            'status' => 1,
            'created_at' => date('Y-m-d')
        ]);

        DB::table('moradores')->insert([
            'unidades_id' => 37,
            'relacao_unidade' => 'proprietario', // proprietario | locatario
            'data_mudanca' => '2015-03-21',
            'data_nascimento' => '1986-12-06',
            'nome' => 'Fernando Vicente de Jesus Rocha',
            'apelido' => 'Fernando',
            'foto' => '2016010516095812195943_1006142116110171_4061870461937010345_n.jpg',
            'email' => 'fernando@trupe.net',
            'senha' => '$2y$10$VbFgDduGmrRPmckq0aeKL.MBOcK.Q1h5qauC3v7BU1BVFTlgOV70a',
            'telefone_fixo' => '(11) 5555-5555',
            'telefone_celular' => '(11) 5555-5555',
            'telefone_comercial' => '(11) 5555-5555',
            'status' => 1,
            'created_at' => date('Y-m-d')
        ]);

        DB::table('moradores')->insert([
            'unidades_id' => 126,
            'relacao_unidade' => 'proprietario', // proprietario | locatario
            'data_mudanca' => '2014-09-01',
            'data_nascimento' => '1986-12-06',
            'nome' => 'Jason Bardy Figueiredo Passos',
            'apelido' => 'Jason',
            'foto' => '20160106111129IMG_3383.JPG',
            'email' => 'jasonbardy@gmail.com',
            'senha' => '$2y$10$VmVxE5B.77O.k7Oj5uz80.rdg6PeENbO.Stl1M/pXKbREsMYfY9Ua',
            'telefone_fixo' => '1123693849',
            'telefone_celular' => '11992163309',
            'telefone_comercial' => '1137923710',
            'status' => 1,
            'created_at' => date('Y-m-d')
        ]);
    }
}
