<?php

use Illuminate\Database\Seeder;

class UnidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unidades')->delete();

        DB::table('unidades')->insert([
			[
				'unidade' => '11',
				'bloco' => 'A',
				'proprietario' => 'RENATO PUERTAS GARCIA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '11',
				'bloco' => 'B',
				'proprietario' => 'ENIR CIRO FERREIRA COUTINHO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '12',
				'bloco' => 'A',
				'proprietario' => 'RAFAEL SARAIVA GAIA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '12',
				'bloco' => 'B',
				'proprietario' => 'TADASHI EMURA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '13',
				'bloco' => 'A',
				'proprietario' => 'KAREM BATSCHNSKI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '13',
				'bloco' => 'B',
				'proprietario' => 'JAIRO COUTO CABRAL DA SILVA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '14',
				'bloco' => 'A',
				'proprietario' => 'TATYANA BOTELHO ANDRE',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '14',
				'bloco' => 'B',
				'proprietario' => 'EMERSON RONALDO MORRESI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '21',
				'bloco' => 'A',
				'proprietario' => 'JOSE DE FIGUEIREDO JUNIOR',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '21',
				'bloco' => 'B',
				'proprietario' => 'ANA PAULA DELICOLI FIGUEIRA DE MELL',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '22',
				'bloco' => 'A',
				'proprietario' => 'JOSE WILSON AMARO LOZON',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '22',
				'bloco' => 'B',
				'proprietario' => 'ELISANGELA SOARES DE SOUZA HIRATSUK',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '23',
				'bloco' => 'A',
				'proprietario' => 'WALMILIAN CRISTINA PAULINO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '23',
				'bloco' => 'B',
				'proprietario' => 'GUILHERME DREHER',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '24',
				'bloco' => 'A',
				'proprietario' => 'MARCUS ROBERTO CARDOSO FERRAO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '24',
				'bloco' => 'B',
				'proprietario' => 'JULIANA MOLNAR CINTRA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '31',
				'bloco' => 'A',
				'proprietario' => 'FABIO FARIA BRAZ',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '31',
				'bloco' => 'B',
				'proprietario' => 'VALDIR MEIRA SERTAO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '32',
				'bloco' => 'A',
				'proprietario' => 'MARCOS JOSE PANTANI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '32',
				'bloco' => 'B',
				'proprietario' => 'EDSON TEIXEIRA VASCONCELOS',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '33',
				'bloco' => 'A',
				'proprietario' => 'DARLENE MARIA SOLA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '33',
				'bloco' => 'B',
				'proprietario' => 'BRUNO SERRANO DOMIENSE NOVARETTI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '34',
				'bloco' => 'A',
				'proprietario' => 'ADRIANA GAETA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '34',
				'bloco' => 'B',
				'proprietario' => 'JONSON CHUNG',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '41',
				'bloco' => 'A',
				'proprietario' => 'RICARDO PUCCIONI KATSUDA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '41',
				'bloco' => 'B',
				'proprietario' => 'VALDIR DUARTE',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '42',
				'bloco' => 'A',
				'proprietario' => 'CRISTINA D ABRONZO AMORIM',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '42',
				'bloco' => 'B',
				'proprietario' => 'MARCELO DE SOUZA PENTEADO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '43',
				'bloco' => 'A',
				'proprietario' => 'HELOISA RIBEIRO DA COSTA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '43',
				'bloco' => 'B',
				'proprietario' => 'DANIEL GARCIA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '44',
				'bloco' => 'A',
				'proprietario' => 'ANA CLAUDIA BALDI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '44',
				'bloco' => 'B',
				'proprietario' => 'DEISE CRISTINA BAIONI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '51',
				'bloco' => 'A',
				'proprietario' => 'BRUNO ULISSES GARCIA DIAS',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '51',
				'bloco' => 'B',
				'proprietario' => 'IGOR BUENO CARNEIRO RODRIGUES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '52',
				'bloco' => 'A',
				'proprietario' => 'VALQUIRIA DIAS PEITL REDES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '52',
				'bloco' => 'B',
				'proprietario' => 'DANIELA MANZOLI BRAVO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '53',
				'bloco' => 'A',
				'proprietario' => 'FERNANDO VICENTE DE JESUS ROCHA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '53',
				'bloco' => 'B',
				'proprietario' => 'CAROLINE PIERRE SANDRINI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '54',
				'bloco' => 'A',
				'proprietario' => 'MAURICIO DE FREITAS NODA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '54',
				'bloco' => 'B',
				'proprietario' => 'ANTONIO MARCOS SILVA SANTIAGO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '61',
				'bloco' => 'A',
				'proprietario' => 'ALINE NAZARIO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '61',
				'bloco' => 'B',
				'proprietario' => 'MARCELO DOS SANTOS RIBEIRO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '62',
				'bloco' => 'A',
				'proprietario' => 'RAFAEL DAHMER ROCHA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '62',
				'bloco' => 'B',
				'proprietario' => 'LUIZ GUSTAVO NASCIMENTO GALVAO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '63',
				'bloco' => 'A',
				'proprietario' => 'MARCELO SALES VECCHI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '63',
				'bloco' => 'B',
				'proprietario' => 'ANDRE DEMITO SAAB',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '64',
				'bloco' => 'A',
				'proprietario' => 'GABRIELA S REIS ADREGA DE MOURA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '64',
				'bloco' => 'B',
				'proprietario' => 'ANDRE ALVES MENEZES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '71',
				'bloco' => 'A',
				'proprietario' => 'ANTONIO MARCOS SILVA SANTIAGO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '71',
				'bloco' => 'B',
				'proprietario' => 'MARCIA REGINA CARACCIOLO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '72',
				'bloco' => 'A',
				'proprietario' => 'PEDRO HENRIQUE ALVES DOS SANTOS',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '72',
				'bloco' => 'B',
				'proprietario' => 'FLU XUS CABELO ESTETICA LTDA ME',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '73',
				'bloco' => 'A',
				'proprietario' => 'RENAN OLIVEIRA FERNANDES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '73',
				'bloco' => 'B',
				'proprietario' => 'VANILSON SOARES DOS SANTOS',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '74',
				'bloco' => 'A',
				'proprietario' => 'MAIKON S BOMFIM',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '74',
				'bloco' => 'B',
				'proprietario' => 'CLEBER GAETA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '81',
				'bloco' => 'A',
				'proprietario' => 'FRANCISCO EMANUEL RICARDO JUNIOR',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '81',
				'bloco' => 'B',
				'proprietario' => 'POLLYANA BORGES DE ARAUJO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '82',
				'bloco' => 'A',
				'proprietario' => 'RICARDO NOGUEIRA MAEKAWA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '82',
				'bloco' => 'B',
				'proprietario' => 'JOSE ROMAO DA SILVA NETO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '83',
				'bloco' => 'A',
				'proprietario' => 'RICARDO NOBUITI YUKI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '83',
				'bloco' => 'B',
				'proprietario' => 'KARIN MILENA LOSADA BARONI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '84',
				'bloco' => 'A',
				'proprietario' => 'LEANDRO NACARATO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '84',
				'bloco' => 'B',
				'proprietario' => 'CRISTIANE ALVES MENEZES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '91',
				'bloco' => 'A',
				'proprietario' => 'SANDRO BATTESINI MARTINS',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '91',
				'bloco' => 'B',
				'proprietario' => 'GIOVANNA CORREIA LIMA BARROS S',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '92',
				'bloco' => 'A',
				'proprietario' => 'GHADA ABBAS CHEAIB',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '92',
				'bloco' => 'B',
				'proprietario' => 'ALFREDO COSME AMENDOLA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '93',
				'bloco' => 'A',
				'proprietario' => 'ALEXANDRE NOVISCKI DE LUCAS',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '93',
				'bloco' => 'B',
				'proprietario' => 'PAULO ROBERTO SCHMID',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '94',
				'bloco' => 'A',
				'proprietario' => 'MARIO ALFREDO LEWIN',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '94',
				'bloco' => 'B',
				'proprietario' => 'SIMONE STAEL FRANCISCONI MARTINELLI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '101',
				'bloco' => 'A',
				'proprietario' => 'PATRICIA GOES CARONE',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '101',
				'bloco' => 'B',
				'proprietario' => 'FRANCISCO EMANUEL RICARDO JUNIOR',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '102',
				'bloco' => 'A',
				'proprietario' => 'ANDRE LUIZ CAVALCANTI FORTE',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '102',
				'bloco' => 'B',
				'proprietario' => 'THIAGO GABRIEL FANTUCCI ROCHA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '103',
				'bloco' => 'A',
				'proprietario' => 'PAULO ROBERTO SCHMID',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '103',
				'bloco' => 'B',
				'proprietario' => 'PAULO ROBERTO SCHMID',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '104',
				'bloco' => 'A',
				'proprietario' => 'GILVANE FRANCA ROCHA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '104',
				'bloco' => 'B',
				'proprietario' => 'NIVALDO DOS SANTOS ALVES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '111',
				'bloco' => 'A',
				'proprietario' => 'LUIS FILIPE AMARAL DOS SANTOS',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '111',
				'bloco' => 'B',
				'proprietario' => 'BRUNO TADEU RADTKE GONCALVES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '112',
				'bloco' => 'A',
				'proprietario' => 'DELCIO PIMENTEL',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '112',
				'bloco' => 'B',
				'proprietario' => 'ABNER ELI BEZERRA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '113',
				'bloco' => 'A',
				'proprietario' => 'RICARDO NOBUITI YUKI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '113',
				'bloco' => 'B',
				'proprietario' => 'RENATO NADER CURY DE AGUIAR',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '114',
				'bloco' => 'A',
				'proprietario' => 'SERGIO OSEAS DO AMARAL PUPO FILHO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '114',
				'bloco' => 'B',
				'proprietario' => 'MARIO ALFREDO LEWIN',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '121',
				'bloco' => 'A',
				'proprietario' => 'LEONARDO GOUVEIA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '121',
				'bloco' => 'B',
				'proprietario' => 'LEANDRO MARTINS TUAO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '122',
				'bloco' => 'A',
				'proprietario' => 'QUELLI SANTOS BARRETO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '122',
				'bloco' => 'B',
				'proprietario' => 'ANDERSON FIORIN GONZALES SPEZZANO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '123',
				'bloco' => 'A',
				'proprietario' => 'TEREZINHA DE SOUZA BOTELHO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '123',
				'bloco' => 'B',
				'proprietario' => 'BENEDITO PEDRO FURLAN',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '124',
				'bloco' => 'A',
				'proprietario' => 'UNAYARA DE JESUS SILVA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '124',
				'bloco' => 'B',
				'proprietario' => 'DANILLO CUONO DE OLIVEIRA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '131',
				'bloco' => 'A',
				'proprietario' => 'FABRICIO DA COSTA PINHEIRO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '131',
				'bloco' => 'B',
				'proprietario' => 'VANESSA MORAIS RENNO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '132',
				'bloco' => 'A',
				'proprietario' => 'SALVATORE BRUNO JUNIOR',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '132',
				'bloco' => 'B',
				'proprietario' => 'MARINA GONZAGA BARRETO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '133',
				'bloco' => 'A',
				'proprietario' => 'MILENA PAOLETTI ROSENFELD',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '133',
				'bloco' => 'B',
				'proprietario' => 'LUIZ MAURO DE MOURA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '134',
				'bloco' => 'A',
				'proprietario' => 'PATRICIA MARTINS CASTANHO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '134',
				'bloco' => 'B',
				'proprietario' => 'RAMZI RAMZI ABDINE',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '141',
				'bloco' => 'A',
				'proprietario' => 'JOAO ARMANDO FRACASSO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '141',
				'bloco' => 'B',
				'proprietario' => 'JOSE ANTONIO FERNANDES NUNES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '142',
				'bloco' => 'A',
				'proprietario' => 'WLADIMIR GAMA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '142',
				'bloco' => 'B',
				'proprietario' => 'SERGIO WOLFF WECHSLER',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '143',
				'bloco' => 'A',
				'proprietario' => 'ANDRE LUIS BARBOSA DE CASTRO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '143',
				'bloco' => 'B',
				'proprietario' => 'BRUNO LAFERL LEIRIAO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '144',
				'bloco' => 'A',
				'proprietario' => 'OSVALDO NAKASHIMA BRITO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '144',
				'bloco' => 'B',
				'proprietario' => 'CLAUDIA MARIA DA SILVA OLIVEIRA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '151',
				'bloco' => 'A',
				'proprietario' => 'FERNANDA OKASZESKI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '151',
				'bloco' => 'B',
				'proprietario' => 'JOAO GABRIEL ROSSI MAGANA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '152',
				'bloco' => 'A',
				'proprietario' => 'JOAO CLAUDIO RAMOS SOARES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '152',
				'bloco' => 'B',
				'proprietario' => 'JOSE AUGUSTO GONZAGA BARRETO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '153',
				'bloco' => 'A',
				'proprietario' => 'ELAINE PORTO NUNES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '153',
				'bloco' => 'B',
				'proprietario' => 'RENATO DE OLIVEIRA BRAUN',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '154',
				'bloco' => 'A',
				'proprietario' => 'LUIS HENRIQUE DE CARVALHO HATMAN',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '154',
				'bloco' => 'B',
				'proprietario' => 'MAURICIO LUCAS PINHEIRO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '161',
				'bloco' => 'A',
				'proprietario' => 'GILENO NASCIMENTO ALMEIDA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '161',
				'bloco' => 'B',
				'proprietario' => 'FABIOLA DE CAMPOS BRAGA MATTOZINHO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '162',
				'bloco' => 'A',
				'proprietario' => 'FERNANDO FACCO PERGAMO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '162',
				'bloco' => 'B',
				'proprietario' => 'DANIELLA HARUMI MIURA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '163',
				'bloco' => 'A',
				'proprietario' => 'EMANUELA M BORGES DE ALMEIDA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '163',
				'bloco' => 'B',
				'proprietario' => 'JASON BARDY FIGUEIREDO PASSOS',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '164',
				'bloco' => 'A',
				'proprietario' => 'TERESA CRISTINA RODRIGUES TARSIA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '164',
				'bloco' => 'B',
				'proprietario' => 'ALESSANDRO SOUZA SILVA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '171',
				'bloco' => 'A',
				'proprietario' => 'GILBERTO AVANZO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '171',
				'bloco' => 'B',
				'proprietario' => 'BRUNO YUDY ARAKL WANABE',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '172',
				'bloco' => 'A',
				'proprietario' => 'GLAUCIA APARECIDA NAHUM',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '172',
				'bloco' => 'B',
				'proprietario' => 'DENISE ASSUMPCAO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '173',
				'bloco' => 'A',
				'proprietario' => 'RENATA NEVES MOREIRA AMANCIO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '173',
				'bloco' => 'B',
				'proprietario' => 'MARCELO SIMIONI PONTES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '174',
				'bloco' => 'A',
				'proprietario' => 'JOAO GABRIEL ROSSI MAGANA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '174',
				'bloco' => 'B',
				'proprietario' => 'NATHALIE CALHAU MONTEIRO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '181',
				'bloco' => 'A',
				'proprietario' => 'HAMILTON CESAR DA SILVA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '181',
				'bloco' => 'B',
				'proprietario' => 'LETICIA MARTINS ARANTES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '182',
				'bloco' => 'A',
				'proprietario' => 'SIDNEY ALVES DE MELLO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '182',
				'bloco' => 'B',
				'proprietario' => 'MARIANA LOURENCO DE LIMA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '183',
				'bloco' => 'A',
				'proprietario' => 'RAFAEL BENELLE CIPOLINI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '183',
				'bloco' => 'B',
				'proprietario' => 'GISELA NEVES MOREIRA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '184',
				'bloco' => 'A',
				'proprietario' => 'RAFAEL SILVA E MELO RAMOS',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '184',
				'bloco' => 'B',
				'proprietario' => 'MARCO ANTONIO FERREIRA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '191',
				'bloco' => 'A',
				'proprietario' => 'MAIARA LUANA SOLER RIBEIRO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '191',
				'bloco' => 'B',
				'proprietario' => 'RUTH NEUHAUSER MAGALHAES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '192',
				'bloco' => 'A',
				'proprietario' => 'ELISABETH HELEN DE REGO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '192',
				'bloco' => 'B',
				'proprietario' => 'RICARDO ANDRE CORTINOVE',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '193',
				'bloco' => 'A',
				'proprietario' => 'RICARDO A BARALDI PEREIRA DE MELO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '193',
				'bloco' => 'B',
				'proprietario' => 'DIEGO LAMBERTUS THIJSSEN',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '194',
				'bloco' => 'A',
				'proprietario' => 'MICHELLE PEREIRA DE SOUZA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '194',
				'bloco' => 'B',
				'proprietario' => 'VINICIUS SILVA RAMOS',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '201',
				'bloco' => 'A',
				'proprietario' => 'ORLANDO LOPES JUNIOR',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '201',
				'bloco' => 'B',
				'proprietario' => 'JOSE ITIRO UMENO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '202',
				'bloco' => 'A',
				'proprietario' => 'AMERICO KIYOSHI KITAHARA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '202',
				'bloco' => 'B',
				'proprietario' => 'ALBERTO FRAGA POMPONET',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '203',
				'bloco' => 'A',
				'proprietario' => 'EDUARDO LOPES SANDRE',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '203',
				'bloco' => 'B',
				'proprietario' => 'RONDINELI FERREIRA PINTO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '204',
				'bloco' => 'A',
				'proprietario' => 'RACHEL DACHTELBERG',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '204',
				'bloco' => 'B',
				'proprietario' => 'DARIO CLAPP PEREIRA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '211',
				'bloco' => 'A',
				'proprietario' => 'MARIZA RUSSO LEAL',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '211',
				'bloco' => 'B',
				'proprietario' => 'HELIO FERREIRA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '212',
				'bloco' => 'A',
				'proprietario' => 'LUIS EDUARDO BAILONI MUNOZ',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '212',
				'bloco' => 'B',
				'proprietario' => 'JOAO MARTINS FERREIRA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '213',
				'bloco' => 'A',
				'proprietario' => 'GILBERTO FERRARI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '213',
				'bloco' => 'B',
				'proprietario' => 'CRISTIANO DA COSTA SANTANA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '214',
				'bloco' => 'A',
				'proprietario' => 'FERNANDO APARECIDO DE FLORIO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '214',
				'bloco' => 'B',
				'proprietario' => 'ANDRE PAIVA VELICEV',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '221',
				'bloco' => 'A',
				'proprietario' => 'SANDRA COPPINI ALVES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '221',
				'bloco' => 'B',
				'proprietario' => 'DENIS VIEIRA LIMA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '222',
				'bloco' => 'A',
				'proprietario' => 'JULIO CESAR PEREIRA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '222',
				'bloco' => 'B',
				'proprietario' => 'ANDERSON E SAITO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '223',
				'bloco' => 'A',
				'proprietario' => 'ELEVEN EMPREENDIMENTOS E PART LTDA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '223',
				'bloco' => 'B',
				'proprietario' => 'JURANDYR CORREA NETO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '224',
				'bloco' => 'A',
				'proprietario' => 'LUIS FERNANDO ARAUJO JUNIOR',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '224',
				'bloco' => 'B',
				'proprietario' => 'IRIS ADDED',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '231',
				'bloco' => 'A',
				'proprietario' => 'ANDRE MORAES PURGAILIS',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '231',
				'bloco' => 'B',
				'proprietario' => 'JONG YEULL LEE',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '232',
				'bloco' => 'A',
				'proprietario' => 'JOSE ANTONIO GERVASIO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '233',
				'bloco' => 'A',
				'proprietario' => 'HUMBERTO DO NASCIMENTO G JUNIOR',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '233',
				'bloco' => 'B',
				'proprietario' => 'CARLA ADRIANA PEREIRA PARDO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '234',
				'bloco' => 'A',
				'proprietario' => 'ANTONIO CARLOS MADEIRA DE ARRUDA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '234',
				'bloco' => 'B',
				'proprietario' => 'JOAB ALEXANDRE BARBOSA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '241',
				'bloco' => 'A',
				'proprietario' => 'ANTONIO FERNANDO PAPARELLA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '241',
				'bloco' => 'B',
				'proprietario' => 'BETTY CELESTINO PRATES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '242',
				'bloco' => 'A',
				'proprietario' => 'ALI AHMAD HASSAN',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '242',
				'bloco' => 'B',
				'proprietario' => 'EVERTON ADELINO BONET',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '243',
				'bloco' => 'A',
				'proprietario' => 'MARCO ANTONIO PEGORELLI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '243',
				'bloco' => 'B',
				'proprietario' => 'CARLOS CLOVIS CREPALDI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '244',
				'bloco' => 'A',
				'proprietario' => 'FERNANDA LEONIE DAIREAUX',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '244',
				'bloco' => 'B',
				'proprietario' => 'RAFAEL THOT',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '251',
				'bloco' => 'A',
				'proprietario' => 'TADASHI EMURA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '251',
				'bloco' => 'B',
				'proprietario' => 'GUSTAVO DA COSTA LEME',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '252',
				'bloco' => 'A',
				'proprietario' => 'ALESSANDRO BARBOSA DOS REIS',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '253',
				'bloco' => 'A',
				'proprietario' => 'MARCO ANTONIO BEOLCHI ADAMI',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '253',
				'bloco' => 'B',
				'proprietario' => 'NEY LUIZ QUAGIO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '254',
				'bloco' => 'A',
				'proprietario' => 'ULISSES GOMES DA ROCHA NETO',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '254',
				'bloco' => 'B',
				'proprietario' => 'MARIA DA PENHA DE SA FERNANDES',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			]
        ]);
    }
}

