<?php

use Illuminate\Database\Seeder;

class MoradoresPrivacidadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('moradores_privacidade')->delete();

        DB::table('moradores_privacidade')->insert(
		[
			'moradores_id' => 1,
			'dado' => 'email',
			'visibilidade' => 1
        ],
        [
			'moradores_id' => 1,
			'dado' => 'email',
			'visibilidade' => 2
        ],
        [
			'moradores_id' => 1,
			'dado' => 'email',
			'visibilidade' => 3
        ],
        [
			'moradores_id' => 1,
			'dado' => 'email',
			'visibilidade' => 4
        ],
        [
			'moradores_id' => 1,
			'dado' => 'email',
			'visibilidade' => 5
        ]
        );

        DB::table('moradores_privacidade')->insert(
		[
			'moradores_id' => 1,
			'dado' => 'telefone_fixo',
			'visibilidade' => 1
        ],
        [
			'moradores_id' => 1,
			'dado' => 'telefone_fixo',
			'visibilidade' => 2
        ],
        [
			'moradores_id' => 1,
			'dado' => 'telefone_fixo',
			'visibilidade' => 3
        ],
        [
			'moradores_id' => 1,
			'dado' => 'telefone_fixo',
			'visibilidade' => 4
        ],
        [
			'moradores_id' => 1,
			'dado' => 'telefone_fixo',
			'visibilidade' => 5
        ]
        );

        DB::table('moradores_privacidade')->insert(
		[
			'moradores_id' => 1,
			'dado' => 'telefone_celular',
			'visibilidade' => 1
        ],
        [
			'moradores_id' => 1,
			'dado' => 'telefone_celular',
			'visibilidade' => 2
        ],
        [
			'moradores_id' => 1,
			'dado' => 'telefone_celular',
			'visibilidade' => 3
        ],
        [
			'moradores_id' => 1,
			'dado' => 'telefone_celular',
			'visibilidade' => 4
        ],
        [
			'moradores_id' => 1,
			'dado' => 'telefone_celular',
			'visibilidade' => 5
        ]
        );

        DB::table('moradores_privacidade')->insert(
		[
			'moradores_id' => 1,
			'dado' => 'telefone_comercial',
			'visibilidade' => 1
        ],
        [
			'moradores_id' => 1,
			'dado' => 'telefone_comercial',
			'visibilidade' => 2
        ],
        [
			'moradores_id' => 1,
			'dado' => 'telefone_comercial',
			'visibilidade' => 3
        ],
        [
			'moradores_id' => 1,
			'dado' => 'telefone_comercial',
			'visibilidade' => 4
        ],
        [
			'moradores_id' => 1,
			'dado' => 'telefone_comercial',
			'visibilidade' => 5
        ]
        );
    }
}
