<?php

use Illuminate\Database\Seeder;

class FaleComSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fale_com')->delete();

        DB::table('fale_com')->insert([
			'contato_sindico' => '<p>Informações de contato do síndico</p>',
			'contato_conselho' => '<p>Informações de contato do conselho</p>',
			'contato_zelador' => '<p>Informações de contato do zelador</p>',
			'contato_portaria' => '<p>Informações de contato da portaria</p>',
			'contato_administradora' => '<p>Informações de contato da administrador</p>a'
        ]);
    }
}
