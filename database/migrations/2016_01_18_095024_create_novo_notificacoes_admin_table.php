<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNovoNotificacoesAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administradores_notificacoes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('tipo_notificacao');
            /*
            *   Tipos: ocorrencia
            *          mensagem
            *          mudanca
            *          chamado_manutencao
            *          reserva_periodo
            *          reserva_diaria
            *          reserva_cancelada_com_multa
            *          festa_particular
            *          comunicacao_da_portaria (adicionado na migration
            *          2016_02_16_140531_alter_notificacoes_table_comunicacao.php )
            */
            $table->string('mensagem');

            $table->integer('administradores_id')->unsigned();
            $table->foreign('administradores_id')->references('id')->on('administradores')->onDelete('cascade');

            // tipo_notificacao = ocorrencias
            $table->integer('ocorrencias_id')->nullable()->unsigned();
            $table->foreign('ocorrencias_id')->references('id')->on('ocorrencias')->onDelete('cascade');

            // tipo_notificacao = mensagens
            $table->integer('fale_com_mensagens_id')->nullable()->unsigned();
            $table->foreign('fale_com_mensagens_id')->references('id')->on('fale_com_mensagens')->onDelete('cascade');

            // tipo_notificacao = mudancas
            $table->integer('mudancas_agendamento_id')->nullable()->unsigned();
            $table->foreign('mudancas_agendamento_id')->references('id')->on('mudancas_agendamento')->onDelete('cascade');

            // tipo_notificacao = chamados_manutencao
            $table->integer('chamados_de_manutencao_id')->nullable()->unsigned();
            $table->foreign('chamados_de_manutencao_id')->references('id')->on('chamados_de_manutencao')->onDelete('cascade');

            // tipo_notificacao = reserva_periodo
            $table->integer('espacos_reservas_por_periodo_id')->nullable()->unsigned();
            $table->foreign('espacos_reservas_por_periodo_id', 'fk_reservas_periodo')->references('id')->on('espacos_reservas_por_periodo')->onDelete('cascade');

            // tipo_notificacao = reserva_diaria
            $table->integer('espacos_reservas_por_diaria_id')->nullable()->unsigned();
            $table->foreign('espacos_reservas_por_diaria_id', 'fk_reservas_diaria')->references('id')->on('espacos_reservas_por_diaria')->onDelete('cascade');

            // tipo_notificacao = reserva_cancelada_com_multa
            $table->integer('diaria_com_multa_id')->nullable()->unsigned();
            $table->foreign('diaria_com_multa_id')->references('id')->on('espacos_reservas_por_diaria')->onDelete('cascade');

            // tipo_notificacao = festas_particulares
            $table->integer('festas_particulares_id')->nullable()->unsigned();
            $table->foreign('festas_particulares_id')->references('id')->on('festas_particulares')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('administradores_notificacoes');
    }
}
