<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEncomendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encomendas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('unidades_id')->unsigned();
            $table->foreign('unidades_id')->references('id')->on('unidades')->onDelete('cascade');

            $table->integer('is_entregue')->default(0); // 0 | 1
            $table->datetime('entregue_em')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('encomendas');
    }
}
