<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLavanderiaFichasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lavanderia_fichas', function (Blueprint $table) {
          $table->text('motivo_cancelamento')->after('is_retirada_solicitada');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lavanderia_fichas', function (Blueprint $table) {
          $table->dropColumn('motivo_cancelamento');
        });
    }
}
