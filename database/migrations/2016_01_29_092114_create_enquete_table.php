<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnqueteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('enquete_questoes', function (Blueprint $table) {
        $table->increments('id');
        $table->text('texto');
	      $table->integer('ativa')->default(0); // 0 || 1
        $table->integer('historico')->default(0); // 0 || 1
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('enquete_questoes');
    }
}
