<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedores', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('fornecedores_categorias_id')
                  ->nullable()
                  ->unsigned();

            $table->foreign('fornecedores_categorias_id')
                  ->references('id')
                  ->on('fornecedores_categorias')
                  ->onDelete('set null');

            $table->string('outra_categoria');

            $table->string('nome');
            $table->string('telefone');
            $table->string('email');
            $table->string('site');
            $table->text('descritivo');

            $table->integer('indicado_por')
                  ->nullable()
                  ->unsigned();

            $table->foreign('indicado_por')
                  ->references('id')
                  ->on('moradores')
                  ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fornecedores');
    }
}
