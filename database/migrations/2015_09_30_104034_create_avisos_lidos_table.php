<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvisosLidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avisos_lidos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('avisos_id')->unsigned();
            $table->foreign('avisos_id')->references('id')->on('avisos')->onDelete('cascade');

            $table->integer('moradores_id')->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('avisos_lidos');
    }
}
