<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComunicacaoAdministracaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('comunicacao_administracao', function (Blueprint $table) {
        $table->increments('id');

        $table->string('titulo');
        $table->string('prioridade');
        $table->string('descricao');

        $table->string('remetente');
        $table->string('destinatario');

        $table->tinyinteger('is_resposta')->default(0);

        $table->integer('em_resposta_a')->nullable()->unsigned();
        $table->foreign('em_resposta_a')
              ->references('id')
              ->on('comunicacao_administracao')
              ->onDelete('cascade');

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('comunicacao_administracao');
    }
}
