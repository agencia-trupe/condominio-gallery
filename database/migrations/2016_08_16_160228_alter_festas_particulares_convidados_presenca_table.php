<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFestasParticularesConvidadosPresencaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('festas_particulares_convidados', function (Blueprint $table) {
            $table->integer('presenca')->default('0')->after('nome');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('festas_particulares_convidados', function (Blueprint $table) {
            $table->dropColumn('presenca');
        });
    }
}
