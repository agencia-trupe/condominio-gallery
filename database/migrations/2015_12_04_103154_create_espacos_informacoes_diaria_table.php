<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspacosInformacoesDiariaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espacos_informacoes_diaria', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('espacos_id')->nullable()->unsigned();
            $table->foreign('espacos_id')->references('id')->on('espacos')->onDelete('cascade');

            $table->integer('valor');
            $table->integer('dias_carencia');
            $table->text('texto');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('espacos_informacoes_diaria');
    }
}
