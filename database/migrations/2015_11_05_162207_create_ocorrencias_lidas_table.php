<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOcorrenciasLidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocorrencias_lidas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ocorrencias_id')->unsigned();
            $table->foreign('ocorrencias_id')->references('id')->on('ocorrencias')->onDelete('cascade');

            $table->integer('administradores_id')->unsigned();
            $table->foreign('administradores_id')->references('id')->on('administradores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ocorrencias_lidas');
    }
}
