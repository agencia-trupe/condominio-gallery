<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnqueteVotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquete_votos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('enquete_questoes_id')->unsigned();
            $table->foreign('enquete_questoes_id')->references('id')->on('enquete_questoes')->onDelete('cascade');

            $table->integer('enquete_opcoes_id')->unsigned();
            $table->foreign('enquete_opcoes_id')->references('id')->on('enquete_opcoes')->onDelete('cascade');

            $table->integer('moradores_id')->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enquete_votos');
    }
}
