<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamadosDeManutencaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chamados_de_manutencao', function (Blueprint $table) {
            $table->increments('id');

            // Morador que cadastrou o chamado
            $table->integer('moradores_id')->nullable()->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('cascade');

            $table->date('data');
            $table->string('horario');
            $table->string('localizacao');
            $table->text('descricao');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chamados_de_manutencao');
    }
}
