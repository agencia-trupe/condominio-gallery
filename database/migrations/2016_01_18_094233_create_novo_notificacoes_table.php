<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNovoNotificacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        *   Substituir sistema de Notificações
        */

        Schema::drop('notificacoes');

        Schema::create('moradores_notificacoes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('tipo_notificacao');
            /*
            *   Tipos: ocorrencia
            *          correspondencia
            *          encomenda
            */
            $table->string('mensagem');

            $table->integer('moradores_id')->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('cascade');

            // tipo_notificacao = ocorrencias
            $table->integer('ocorrencias_id')->nullable()->unsigned();
            $table->foreign('ocorrencias_id')->references('id')->on('ocorrencias')->onDelete('cascade');

            // tipo_notificacao = correspondencias
            $table->integer('correspondencias_id')->nullable()->unsigned();
            $table->foreign('correspondencias_id')->references('id')->on('correspondencias')->onDelete('cascade');

            // tipo_notificacao = encomendas
            $table->integer('encomendas_id')->nullable()->unsigned();
            $table->foreign('encomendas_id')->references('id')->on('encomendas')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('moradores_notificacoes');

        /* Esquema antigo de notificações */
        Schema::create('notificacoes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('tipo'); // ocorrencia | eventos | correspondencia | encomenda

            $table->string('destino'); // admin | morador

            $table->integer('moradores_id')->nullable()->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('cascade');

            $table->string('mensagem');
            $table->string('link');

            $table->timestamps();
        });
    }
}
