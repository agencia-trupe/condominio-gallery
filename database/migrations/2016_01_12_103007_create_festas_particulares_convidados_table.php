<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFestasParticularesConvidadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('festas_particulares_convidados', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('festas_particulares_id')->unsigned();
            $table->foreign('festas_particulares_id')->references('id')->on('festas_particulares')->onDelete('cascade');

            $table->string('nome');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('festas_particulares_convidados');
    }
}
