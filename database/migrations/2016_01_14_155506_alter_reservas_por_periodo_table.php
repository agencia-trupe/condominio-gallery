<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReservasPorPeriodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('espacos_reservas_por_periodo', function (Blueprint $table) {

            $table->integer('unidades_id')->unsigned()->nullable()->after('moradores_id');
            $table->foreign('unidades_id')->references('id')->on('unidades')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('espacos_reservas_por_periodo', function (Blueprint $table) {

            $table->dropForeign('espacos_reservas_por_periodo_unidades_id_foreign');
            $table->dropColumn('unidades_id');

        });
    }
}
