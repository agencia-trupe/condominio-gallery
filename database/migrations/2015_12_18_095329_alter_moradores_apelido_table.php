<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMoradoresApelidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('moradores', function (Blueprint $table) {
            $table->renameColumn('sobrenome', 'apelido');
            $table->string('relacao_unidade')->after('unidades_id'); // locatario | proprietario
            $table->date('data_mudanca')->nullable()->after('relacao_unidade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('moradores', function (Blueprint $table) {
            $table->renameColumn('apelido', 'sobrenome');
            $table->dropColumn('relacao_unidade');
            $table->dropColumn('data_mudanca');
        });
    }
}
