<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNotificacoesTableComunicacao extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('administradores_notificacoes', function (Blueprint $table) {
      $table->integer('comunicacao_adm_id')
            ->after('festas_particulares_id')
            ->nullable()
            ->unsigned();

      $table->foreign('comunicacao_adm_id')
            ->references('id')
            ->on('comunicacao_administracao')
            ->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('administradores_notificacoes', function (Blueprint $table) {
      $table->dropForeign('administradores_notificacoes_comunicacao_adm_id_foreign');
      $table->dropColumn('comunicacao_adm_id');
    });
  }
}
