<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEspacosReservasPorDiariaConvidadosPresencaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('espacos_reservas_por_diaria_convidados', function (Blueprint $table) {
            $table->integer('presenca')->default('0')->after('nome');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('espacos_reservas_por_diaria_convidados', function (Blueprint $table) {
            $table->dropColumn('presenca');
        });
    }
}
