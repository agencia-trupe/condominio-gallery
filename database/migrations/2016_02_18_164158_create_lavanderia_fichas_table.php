<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLavanderiaFichasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('lavanderia_fichas', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('quantidade');

      $table->integer('moradores_id')
            ->nullable()
            ->unsigned();

      $table->foreign('moradores_id')
            ->references('id')
            ->on('moradores')
            ->onDelete('cascade');

      $table->integer('is_faturado')->default(0);

      $table->integer('is_retirada_solicitada')->default(0);

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('lavanderia_fichas');
  }
}
