<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspacosReservasPorDiariaConvidadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espacos_reservas_por_diaria_convidados', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('reservas')->unsigned()->nullable();

            $table->string('nome');

            $table->timestamps();
        });

        Schema::table('espacos_reservas_por_diaria_convidados', function (Blueprint $table) {
            $table->foreign('reservas')->references('id')->on('espacos_reservas_por_diaria')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('espacos_reservas_por_diaria_convidados');
    }
}
