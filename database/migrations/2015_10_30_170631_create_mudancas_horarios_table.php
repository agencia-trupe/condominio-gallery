<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMudancasHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mudancas_informacoes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('horario_manha');
            $table->string('horario_tarde');

            $table->string('regulamento');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mudancas_informacoes');
    }
}
