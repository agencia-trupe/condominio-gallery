<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimaisDeEstimacaoPrivacidadeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animais_de_estimacao_privacidade', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('animais_de_estimacao_id')->unsigned();
            $table->foreign('animais_de_estimacao_id')->references('id')->on('animais_de_estimacao')->onDelete('cascade');

            /*
             Visibilidade :
                Todos            => 1
                Admin            => 2
                Portaria         => 3
                Zelador          => 4
                Moradores        => 5
                Moradores Amigos => 6
            */
            /*
                Como é possível selecionar uma combinação
                de diferentes valores de visibilidade
                (Ex: portaria + moradores amigos),
                vão haver registros múltiplos
                para o mesmo registro.

                Visibilidade | moradores_da_unidade_id | visibilidade
                             |       3                 |       3
                             |       3                 |       6
            */
            $table->string('visibilidade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('animais_de_estimacao_privacidade');
    }
}
