<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOcorrenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ocorrencias', function (Blueprint $table) {
            // Se houver id foi finalizada por morador, se não foi finalizada pela adm
            $table->datetime('finalizada_em')->nullable()->after('finalizada');
            $table->integer('finalizada_por')->nullable()->unsigned()->after('finalizada_em');
            $table->foreign('finalizada_por')->references('id')->on('moradores')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ocorrencias', function (Blueprint $table) {
            $table->dropForeign('ocorrencias_finalizada_por_foreign');
            $table->dropColumn('finalizada_por');
            $table->dropColumn('finalizada_em');
        });
    }
}
