<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('documentos_categoria_id')->unsigned();
            $table->foreign('documentos_categoria_id')->references('id')->on('documentos_categorias')->onDelete('cascade');

            $table->string('titulo');
            $table->string('slug');

            $table->text('texto');
            $table->string('arquivo');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documentos');
    }
}
