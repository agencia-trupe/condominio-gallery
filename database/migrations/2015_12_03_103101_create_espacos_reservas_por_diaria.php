<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspacosReservasPorDiaria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espacos_reservas_por_diaria', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('espacos_id')->nullable()->unsigned();
            $table->foreign('espacos_id')->references('id')->on('espacos')->onDelete('cascade');

            $table->integer('moradores_id')->nullable()->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('cascade');

            $table->date('reserva');

            $table->tinyInteger('is_bloqueio')->default(0); // 0 | 1

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('espacos_reservas_por_diaria');
    }
}
