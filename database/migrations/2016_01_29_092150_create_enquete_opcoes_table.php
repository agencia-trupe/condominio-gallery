<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnqueteOpcoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquete_opcoes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('enquete_questoes_id')->unsigned();
            $table->foreign('enquete_questoes_id')->references('id')->on('enquete_questoes')->onDelete('cascade');

      			$table->text('texto');
      			$table->integer('ordem');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enquete_opcoes');
    }
}
