<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoradoresDaUnidadeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moradores_da_unidade', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nome');
            $table->date('data_nascimento');
            $table->string('parentesco');
            $table->string('foto');

            $table->integer('moradores_id')->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('cascade');

            $table->integer('unidades_id')->unsigned();
            $table->foreign('unidades_id')->references('id')->on('unidades')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('moradores_da_unidade');
    }
}
