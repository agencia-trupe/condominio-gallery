<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendaAdministracaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_administracao', function (Blueprint $table) {
            $table->increments('id');

            $table->date('data');
            $table->string('titulo');
            $table->text('texto');
            $table->string('horario');
            $table->string('local');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agenda_administracao');
    }
}
