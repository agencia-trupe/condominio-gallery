<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMudancasAgendamentoLidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mudancas_agendamento_lidas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('mudancas_agendamento_id')->nullable()->unsigned();
            $table->foreign('mudancas_agendamento_id')->references('id')->on('mudancas_agendamento')->onDelete('cascade');

            $table->integer('administradores_id')->nullable()->unsigned();
            $table->foreign('administradores_id')->references('id')->on('administradores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mudancas_agendamento_lidas');
    }
}
