<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFaleComTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fale_com_mensagens', function (Blueprint $table) {
            $table->string('morador_removido')->after('destino');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fale_com_mensagens', function (Blueprint $table) {
            $table->dropColumn('morador_removido');
        });
    }
}
