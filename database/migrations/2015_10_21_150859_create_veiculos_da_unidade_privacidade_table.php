<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeiculosDaUnidadePrivacidadeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veiculos_da_unidade_privacidade', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('veiculos_da_unidade_id')->unsigned();
            $table->foreign('veiculos_da_unidade_id')->references('id')->on('veiculos_da_unidade')->onDelete('cascade');

            /*
             Visibilidade :
                Todos            => 1
                Admin            => 2
                Portaria         => 3
                Zelador          => 4
                Moradores        => 5
                Moradores Amigos => 6
            */
            /*
                Como é possível selecionar uma combinação
                de diferentes valores de visibilidade
                (Ex: portaria + moradores amigos),
                vão haver registros múltiplos
                para o mesmo registro.

                Visibilidade | moradores_da_unidade_id | visibilidade
                             |       3                 |       3
                             |       3                 |       6
            */
            $table->string('visibilidade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('veiculos_da_unidade_privacidade');
    }
}
