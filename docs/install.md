
$ git clone REPO --depth=1 && rm -R ./.git

$ cp .env.local .env

$ sudo chmod 0777 -R bootstrap/

$ sudo chmod 0777 -R storage/

$ sudo chmod 0777 -R public_html/assets/images/moradores/

$ composer install

$ npm install

$ bower update

$ gulp --production
