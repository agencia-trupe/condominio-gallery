Quais tabelas tem ralação com o morador?

--- ocorrencias - set null
quando o morador criar, utiliza o campo autor_id
quando o morador recebe resposta da adm, utiliza o campo moradores_id

--- fale com - set null
utiliza o campo autor_id

privacidade - remover
amizades - remover
avisos lidos - remover
notificacoes - remover
moradores da unidade - remover
veiculos da unidade - remover (alterado para nullable posteriormente)
animais de estimacao - remover
agendamento de mudanças - remover
chamados de manutenção - remover
reservas de espaço - remover
pessoas autorizadas - remover
pessoas não autorizadas - remover
prestadores de serviço - remover
reservas diarias - remover
festas particulares - remover
votos nas enquetes - remover
classificados - remover
preferencias - remover
fichas lavanderia - remover
avaliações de fornecedores - remover


# Ao excluir, manter a informação resumida do morador num campo de texto em algum lugar

[X] Criar migrations para adicionar campo de texto nas tabelas a serem alteradas
alter table `ocorrencias` add `morador_removido` varchar(255) not null after `finalizada_por`
alter table `fale_com_mensagens` add `morador_removido` varchar(255) not null after `destino`

[X] Atualizar esses campos ao remover morador


[ ] Criar Acessor para quando for exibir o registro mostrar esse campo se moradores_id for null

testar todos os cenários acima
id:     55
email:  brunotrg@hotmail.com
senha:  senhatrupe